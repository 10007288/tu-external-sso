﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using System.Net;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using Microsoft.AspNet.Membership.OpenAuth;

public partial class Login : Page
{
    public string ReturnUrl { get; set; }

    public IEnumerable<ProviderDetails> GetProviderNames()
    {
        return OpenAuth.AuthenticationClients.GetAll();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ReturnUrl = Request.QueryString["ReturnUrl"];

        bool isAuth = (System.Web.HttpContext.Current.User != null) && System.Web.HttpContext.Current.User.Identity.IsAuthenticated;

        if (isAuth)
            Response.Redirect("~/Default.aspx");

        if (!IsPostBack)
        {
            PrepareLoginForm();
        }
    }

    protected void btnRequestLogin_Click(object sender, EventArgs e)
    {
        Session["EmpID"] = Request.QueryString["EmpID"];
        var redirectUrl = "~/ExternalLandingPage.aspx";

        if (!String.IsNullOrEmpty(ReturnUrl))
        {
            var resolvedReturnUrl = ResolveUrl(ReturnUrl);
            redirectUrl += "?ReturnUrl=" + HttpUtility.UrlEncode(resolvedReturnUrl);
        }

        OpenAuth.RequestAuthentication("google", redirectUrl);
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        MembershipProvider provider;

        if (txtUsername.Text.Contains("@"))
        {
            provider = Membership.Providers["TWWMembershipProvider"];
            var user = provider.GetUser(txtUsername.Text.Trim(), true);

            if (user != null)
            {
                var sSafePassword = Server.HtmlEncode(txtPassword.Text.Trim());

                if (provider.ValidateUser(txtUsername.Text.Trim(), sSafePassword))
                {
                    FormsAuthentication.Authenticate(txtPassword.Text.Trim(), sSafePassword);
                    FormsAuthentication.SetAuthCookie(txtUsername.Text, true);

                    Session["CIMNumber"] = txtUsername.Text;
                    Response.Redirect("~/Default.aspx");
                }
                else
                    lblNotify.Text = "Invalid Email or Password.";
            }
            else
                lblNotify.Text = "Invalid Email or Password.";
        }
        else
            lblNotify.Text = "Invalid Email or Password.";
    }

    protected void PrepareLoginForm()
    {
        var lv = (LoginView)Master.FindControl("LoginView1");
        lv.Visible = false;
        var lvWelcome = (LoginView)Master.FindControl("lvWelcome");
        lvWelcome.Visible = false;

        var hlpCourse = (HtmlAnchor)Master.FindControl("hlpCourseCatalog");
        hlpCourse.Visible = false;
        //var hlpCalendar = (HtmlAnchor)Master.FindControl("hlpCalendar");
        //hlpCalendar.Visible = false;
        //var hlpFeatCourses = (HtmlAnchor)Master.FindControl("hlpFeatCourses");
        //hlpFeatCourses.Visible = false;

        txtUsername.Text = String.Empty;
        txtUsername.Focus();
    }
}