﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    public string LoginName
    {
        get
        {
            var value = HttpContext.Current.Session["LoginNameSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["LoginNameSession"] = value;
        }
    }

    public string AccessMode
    {
        get
        {
            var value = HttpContext.Current.Session["AccessModeSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["AccessModeSession"] = value;
        }
    }

    public string LoginFirstName
    {
        get
        {
            var value = HttpContext.Current.Session["LoginFirstNameSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["LoginFirstNameSession"] = value;
        }
    }

    public int NotifyEventForApprovalCnt
    {
        get
        {
            var value = HttpContext.Current.Session["EventForApprovalSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["EventForApprovalSession"] = value;
        }
    }

    public int NotifyEnrollmentCnt
    {
        get
        {
            var value = HttpContext.Current.Session["NotifyEnrollmentSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["NotifyEnrollmentSession"] = value;
        }
    }

    public int NotifyEnrolApprovedCnt
    {
        get
        {
            var value = HttpContext.Current.Session["NotifyEnrolApprovedSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["NotifyEnrolApprovedSession"] = value;
        }
    }

    public int NotifyEnrollDeniedCnt
    {
        get
        {
            var value = HttpContext.Current.Session["NotifyEnrollDeniedSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["NotifyEnrollDeniedSession"] = value;
        }
    }

    protected void GetGlobals()
    {
        AccessMode = ConfigurationManager.AppSettings["AccessMode"];
        LoginName = Context.User.Identity.Name;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Disable Page Cache 
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!IsPostBack)
        {
            LabelDate.Text = DateTime.Now.ToString("D");

            //if (Roles.IsUserInRole("Non-APAC Admin") || Roles.IsUserInRole("Non-APAC Learner"))
            //{
            //    //hlpCalendar.Visible = false;
            //    hlpFeatCourses.Visible = false;
            //}

            //if (AccessMode == "External")
            //{
            //    //hlpCalendar.Visible = false;
            //    hlpFeatCourses.Visible = false;
            //}

            if (!string.IsNullOrEmpty(LoginName))
            {
                string user = string.Empty;
                var lblUname = (Label)lvWelcome.FindControl("lblUname");
                if (LoginName.Contains("@"))
                {
                    user = DataHelper.GetExternalUserName(LoginName);
                }
                else
                {
                    user = DataHelper.GetUserDetails(Convert.ToInt32(LoginName)).firstname.ToString();
                }

                if (user != null)
                {
                    LoginFirstName = user;
                    lblUname.Text = LoginFirstName;
                }
            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetGlobals();
            SetNotifications();

            hidCurrentPage.Value = Parent.ToString();
            if (!string.IsNullOrEmpty(LoginName))
            {
                if (LoginName.Contains("@"))
                {
                    if (DataHelper.IsChangePassReq(LoginName))
                    {
                        if (hidCurrentPage.Value != "ASP.changepassword_aspx")
                        {
                            Response.Redirect("~/ChangePassword.aspx");
                        }
                    }
                }
                else
                {
                    if (AccessMode == "Internal" || !LoginName.Contains("@"))
                    {
                        //var db = new IntranetDBDataContext();
                        //var result =
                        //    new List<pr_TranscomUniversity_Rst_CheckAccountResult>(
                        //        db.pr_TranscomUniversity_Rst_CheckAccount(Convert.ToInt32(LoginName)));

                        if ((int)TWW.Data.Intranet.SPs.PrTranscomUniversityRstCheckAccount(Convert.ToInt32(LoginName)).ExecuteScalar() == 0)
                        {
                            if (!Request.Url.ToString().Contains("ViewProfile.aspx"))
                            {
                                Response.Redirect("~/ViewProfile.aspx");
                            }
                        }
                    }
                }
            }
        }
    }

    private void SetNotifications()
    {
        var url = Request.Url.AbsolutePath;

        if (!string.IsNullOrEmpty(LoginName) && !LoginName.Contains("@"))
        {
            //NOTIFICATIONS
            var lv = (LoginView)FindControl("loginView1");

            if (lv != null)
            {
                //DISABLED FOR NOW
                //if (Roles.IsUserInRole("Admin") || Roles.IsUserInRole("Coordinator"))
                //{
                //    NotifyEventForApprovalCnt = DataHelper.CountRoomReservations_ForApproval();
                //    var notificationEvent = (HtmlGenericControl)lv.FindControl("notification_event");

                //    if (NotifyEventForApprovalCnt > 0)
                //    {
                //        if (!url.Contains("CalendarEvents.aspx"))
                //        {
                //            notificationEvent.InnerHtml = NotifyEventForApprovalCnt.ToString();
                //            notificationEvent.Visible = true;
                //        }
                //        else
                //        {
                //            notificationEvent.Visible = false;
                //        }
                //    }
                //    else
                //    {
                //        notificationEvent.Visible = false;
                //    }
                //}

                //SUPERVISOR - MY TEAM
                if (Roles.IsUserInRole("Manager"))
                {
                    // all enrollment under supervisor
                    NotifyEnrollmentCnt = DataHelper.CountEnrollments_Sup_ForApproval(Convert.ToInt32(LoginName));
                    var notificationMyTeam = (HtmlGenericControl)lv.FindControl("notification_myteam");

                    if (NotifyEnrollmentCnt > 0)
                    {
                        if (!url.Contains("MyTeam.aspx"))
                        {
                            notificationMyTeam.InnerHtml = NotifyEnrollmentCnt.ToString();
                            notificationMyTeam.Visible = true;
                        }
                        else
                        {
                            notificationMyTeam.Visible = false;
                        }
                    }
                    else
                    {
                        notificationMyTeam.Visible = false;
                    }
                }

                //USER - COURSE ENROLMENT
                // all enrollment under supervisor
                NotifyEnrolApprovedCnt = DataHelper.CountEnrollments_Approved_NotChecked(Convert.ToInt32(LoginName));
                // all enrollment under supervisor
                NotifyEnrollDeniedCnt = DataHelper.CountEnrollments_Denied_NotChecked(Convert.ToInt32(LoginName));
                var notificationMyPlan = (HtmlGenericControl)lv.FindControl("notification_myplan");

                int assignmentCnt = DataHelper.GetAssignCourseNotificationCount(Convert.ToInt32(LoginName)).AssignedCount;

                if (NotifyEnrolApprovedCnt > 0 || NotifyEnrollDeniedCnt > 0 || assignmentCnt > 0)
                {
                    notificationMyPlan.InnerHtml = (NotifyEnrolApprovedCnt + NotifyEnrollDeniedCnt + assignmentCnt).ToString();
                    notificationMyPlan.Visible = true;
                }
                else
                {
                    notificationMyPlan.Visible = false;
                }
            }
        }
    }

    public void AlertMessage(bool success, string msg)
    {
        if (success)
        {
            lblNote.Text = msg;
            //lblNote.ForeColor = System.Drawing.Color.FromArgb(255, 215, 0);
            lblNote.ForeColor = System.Drawing.Color.FromArgb(255, 255, 255);
            lblNote.Font.Size = new FontUnit(Unit.Point(11));
            notify.TitleIcon = "ok";
            notify.Title = "Success!";
            notify.Opacity = 100;
            notify.Show();
        }
        else
        {
            lblNote.Text = msg;
            lblNote.ForeColor = System.Drawing.Color.FromArgb(255, 215, 0);
            lblNote.Font.Size = new FontUnit(Unit.Point(10));
            notify.TitleIcon = "warning";
            notify.Title = "Error!";
            notify.Opacity = 90;
            notify.Show();
        }
    }

    protected void lgnStatus_LoggedOut(Object sender, EventArgs e)
    {
        Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.Cache.SetNoStore();

        var redirectUrl = FormsAuthentication.LoginUrl + "?ReturnUrl=Default.aspx";

        Session.Clear();
        Session.Abandon();
        FormsAuthentication.SignOut();
        Response.Redirect(redirectUrl);
    }
}

