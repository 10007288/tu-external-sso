﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.ComponentModel;
using System.Globalization;
using System.Web.UI;
using System.Web;

/// <summary>
/// Summary description for LogicHelper
/// </summary>
public class LogicHelper
{
    public LogicHelper()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable ConvertToDataTable<T>(IList<T> data)
    {
        var properties =
            TypeDescriptor.GetProperties(typeof(T));
        var table = new DataTable();
        foreach (PropertyDescriptor prop in properties)
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        foreach (T item in data)
        {
            var row = table.NewRow();
            foreach (PropertyDescriptor prop in properties)
                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            table.Rows.Add(row);
        }
        return table;
    }

    //For Finding deep Nested Controls

    public static T FindControl<T>(ControlCollection controls, string controlId)
    {
        T ctrl = default(T);
        foreach (Control ctl in controls)
        {
            if (ctl.GetType() == typeof(T) && ctl.ClientID.Substring(ctl.ClientID.Length - controlId.Length) == controlId)
            {
                    ctrl = (T)Convert.ChangeType(ctl, typeof(T), CultureInfo.InvariantCulture);
                    return ctrl;
            }
            if (ctl.Controls.Count > 0 && ctrl == null)
                ctrl = FindControl<T>(ctl.Controls, controlId);
        }
        return ctrl;
    }
}

public static class InClauseObjectExtensions
{
    public static bool In<T>(this T @object, params T[] values)
    {
        // this is LINQ expression. If you don't want to use LINQ,
        // you can use a simple foreach and return true 
        // if object is found in the array
        return values.Contains(@object);
    }

    public static bool In<T>(this T @object, IEnumerable<T> valueList)
    {
        // this is LINQ expression. If you don't want to use LINQ,
        // you can use a simple foreach and return true if object 
        // is found in the array
        return valueList.Contains(@object);
    }
}

