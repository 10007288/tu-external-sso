﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ScormDataHelper
/// </summary>
public class ScormDataHelper
{
    public class ScormPackageCourses
    {
        public int CurriculumId { get; set; }
        public int CurriculumCourseId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }

    public static tbl_TranscomUniversity_Cor_Course Get_ScormCourse_ByCourseId(int id)
    {
        var db = new IntranetDBDataContext();
        var course = (from a in db.tbl_TranscomUniversity_Cor_Courses
                    where a.CourseID == id
                    select a).FirstOrDefault();
        return course;
    }

    public static string GetOverallProgressByCourseID(string CIM, int CourseID)
    {
        var db = new IntranetDBDataContext { CommandTimeout = 0 };
        return db.pr_TranscomUniversity_Lkp_GetScormOverallProgress(CIM, CourseID).FirstOrDefault().Score;
    }

    //public static List<ScormPackageCourses> Get_ScormPackage_ByCurriculumId(int id)
    //{
    //    var db = new SCORMDBDataContext();
    //    var scormCourse = (from a in db.tbl_scorm_cor_Courses
    //                       where a.CurriculumID == id
    //                       select new ScormPackageCourses
    //                           {
    //                               CurriculumCourseId = a.curriculumcourseID,
    //                               CurriculumId = a.CurriculumID,
    //                               Title = a.Course,
    //                               Description = a.Course
    //                           });

    //    return scormCourse.ToList();
    //}

    public static List<pr_TranscomUniversity_lkp_PackageDetailsResult> Get_AllScormPackages(int curriculumId, int curriculumCourseId, string CIM)
    {
        var db = new IntranetDBDataContext { CommandTimeout = 0 };
        return new List<pr_TranscomUniversity_lkp_PackageDetailsResult>(db.pr_TranscomUniversity_lkp_PackageDetails(CIM, curriculumId, curriculumCourseId)).ToList();
    }

    public static List<pr_TranscomUniversity_lkp_GetScormPackageResult> Get_ScormPackage_ByCurriculumId(int id, string CIM)
    {
        var db = new IntranetDBDataContext { CommandTimeout = 0 };
        return new List<pr_TranscomUniversity_lkp_GetScormPackageResult>(db.pr_TranscomUniversity_lkp_GetScormPackage(CIM, id)).ToList();
    }

    public static List<pr_TranscomUniversity_lkp_GetTrainerResourcesResult> Get_TrainerResource(int? id)
    {
        var db = new IntranetDBDataContext { CommandTimeout = 0 };
        return new List<pr_TranscomUniversity_lkp_GetTrainerResourcesResult>(db.pr_TranscomUniversity_lkp_GetTrainerResources("0",id)).ToList();
    }
}