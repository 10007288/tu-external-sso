﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Configuration;
using System.Data.SqlClient;

public class DataHelper
{
    #region .USER

    public static List<DataHelperModel.User> GetUsers_ByTwwIds(string[] twwIds)
    {
        var db = new IntranetDBDataContext();
        var result = (from a in db.vw_Personnel_Cor_Employees
                      where twwIds.Contains(a.CIMNumber.ToString())
                      select new DataHelperModel.User
                      {
                          CimNo = a.CIMNumber,
                          FirstName = a.FirstName,
                          LastName = a.LastName,
                          CimName = a.CIMName,
                          Name = a.Name
                      }).ToList();
        return result;
    }

    public static List<DataHelperModel.User> GetUsers_ByFirstNames(string firstName)
    {
        var db = new IntranetDBDataContext();
        var result = (from a in db.vw_Personnel_Cor_Employees
                      where a.FirstName.Contains(firstName)
                      select new DataHelperModel.User
                      {
                          CimNo = a.CIMNumber,
                          FirstName = a.FirstName,
                          LastName = a.LastName,
                          CimName = a.CIMName,
                          Name = a.Name
                      }).ToList();
        return result;
    }

    public static List<DataHelperModel.User> GetUsers_ByLastNames(string lastName)
    {
        var db = new IntranetDBDataContext();
        var result = (from a in db.vw_Personnel_Cor_Employees
                      where a.LastName.Contains(lastName)
                      select new DataHelperModel.User
                      {
                          CimNo = a.CIMNumber,
                          FirstName = a.FirstName,
                          LastName = a.LastName,
                          CimName = a.CIMName,
                          Name = a.Name
                      }).ToList();
        return result;
    }

    public static string GetExternalUserName (string email)
    {
        //return (from h in _DB.History
        //        where h.ApplicantId.Equals(Id)
        //        select h.AbuseComment).FirstOrDefault();

        var db = new IntranetDBDataContext();
        var result = (from a in db.tbl_testing_lkp_externalUsers
                      where a.Email.Contains(email)
                      select a.FirstName).FirstOrDefault();
        return result;
    }

    //public static string GetCountry(string Country)
    //{
    //    var db = new IntranetDBDataContext();
    //    var result = (from a in db.tbl_TranscomUniversity_Lkp_Countries
    //                  where a.CountryName.Contains(Country)
    //                  select a.CountryName).FirstOrDefault();
    //    return result;
    //}

    //Raymark - Get employee site
    public static string GetEmployeeCompanySite(int CIMNumber)
    {
        var db = new IntranetDBDataContext();
        return (db.pr_TranscomUniversity_Lkp_EmployeeCompanySite(CIMNumber)).FirstOrDefault().Country;
    }

    public static sp_Get_UserProfilesResult GetUserDetails(int cim)
    {
        var db = new NuSkillCheckDataContext();
        return new List<sp_Get_UserProfilesResult>(db.sp_Get_UserProfiles(cim)).FirstOrDefault();
    }

    public static string GetUserEmailAddress(string twwId)
    {
        var db = new NuSkillCheckDataContext();
        return db.Users.SingleOrDefault(p => p.TWWID == twwId).email;
    }

    public static pr_SubmissionForm_GetUserLevelResult GetUserLevel(int cim)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_SubmissionForm_GetUserLevelResult>(db.pr_SubmissionForm_GetUserLevel(cim)).FirstOrDefault();
    }

    public static pr_TranscomUniversity_Lkp_WelcomeMessageResult GetWelcomeMsg(int userlvl)
    {
        var db = new IntranetDBDataContext();
        return
            new List<pr_TranscomUniversity_Lkp_WelcomeMessageResult>(db.pr_TranscomUniversity_Lkp_WelcomeMessage(userlvl))
                .FirstOrDefault();
    }

    #endregion

    #region .SUPERVISOR

    public static pr_TranscomUniversity_Lkp_SupervisorResult GetSupervisorDetails(int twwid)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_SupervisorResult>(db.pr_TranscomUniversity_Lkp_Supervisor(twwid)).Single();
    }

    public static bool ValidateSupervisor(int agentCim, int supCim)
    {
        var retVal = false;
        var db = new IntranetDBDataContext();
        var result = new List<pr_TranscomUniversity_Lkp_ValidateReportingMgrResult>(db.pr_TranscomUniversity_Lkp_ValidateReportingMgr(agentCim, supCim)).FirstOrDefault();

        if (result != null) retVal = result.Column1 == 1;

        return retVal;
    }

    public static List<pr_TranscomUniversity_Lkp_GetDirectReportsResult> GetTwwidByTeam(int cimNo)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_GetDirectReportsResult>(db.pr_TranscomUniversity_Lkp_GetDirectReports(cimNo)).ToList();
    }

    #endregion

    #region .ROLE

    //public static string GetUserRoles(string username)
    //{
    //    var db = new IntranetDBDataContext();
    //    var strRoles = new StringBuilder();

    //    var emp = new Employee();

    //    if (username.Contains("@"))
    //    {
    //        strRoles = new StringBuilder("");
    //    }
    //    else
    //    {
    //        if (emp.LoadEmployeeByCIM(int.Parse(username), DateTime.Today)
    //            && DateTime.Parse(emp.EndDate) > DateTime.Today)
    //        {
    //            var hasRole = new List<pr_TranscomUniversity_Rst_AdminAccessResult>(db.pr_TranscomUniversity_Rst_AdminAccess(int.Parse(emp.EmployeeID))).FirstOrDefault();

    //            if (hasRole.Return > 0)
    //            {
    //                var userRoles = new List<pr_TranscomUniversity_Lkp_RoleResult>(db.pr_TranscomUniversity_Lkp_Role(int.Parse(emp.EmployeeID))).ToArray();

    //                foreach (var role in userRoles)
    //                {
    //                    strRoles.Append(role.UserRole + ",");
    //                }

    //                strRoles = new StringBuilder(strRoles.ToString().TrimEnd(','));
    //            }
    //            else
    //            {
    //                strRoles = new StringBuilder("");
    //            }
    //        }
    //    }

    //    return strRoles.ToString();
    //}

    #endregion

    #region .CATEGORY

    //public static List<DataHelperModel.Categories> GetCategories()
    //{
    //    var db = new IntranetDBDataContext();
    //    var result = (from a in db.tbl_TranscomUniversity_Lkp_Categories
    //                  where a.HideFromList == 0
    //                  orderby a.Category
    //                  select new DataHelperModel.Categories
    //                      {
    //                          CategoryId = a.CategoryID,
    //                          Category = a.Category,
    //                          UpdatedBy = a.UpdatedBy,
    //                          DateTimeUpdated = a.DateTimeUpdated
    //                      }).ToList();
    //    return result;
    //}

    public static List<DataHelperModel.Categories> GetCategories(string accessmode)
    {
        var db = new IntranetDBDataContext();

        if (accessmode == "Admin")
        {
            var result = (from a in db.tbl_TranscomUniversity_Lkp_Categories
                          where a.HideFromList == 0
                          orderby a.Category
                          select new DataHelperModel.Categories
                              {
                                  CategoryId = a.CategoryID,
                                  Category = a.Category,
                                  UpdatedBy = a.UpdatedBy,
                                  DateTimeUpdated = a.DateTimeUpdated
                              }).OrderBy(a => a.Category).ToList();
            return result;
        }
        else
        {
            var result = (from a in db.tbl_TranscomUniversity_Lkp_Categories
                          join b in db.tbl_TranscomUniversity_Cor_Courses
                              on a.CategoryID equals b.CategoryID
                          where (a.HideFromList == 0) && (b.AccessMode == accessmode
                                                          || b.AccessMode == "Both")
                          select new DataHelperModel.Categories
                              {
                                  CategoryId = a.CategoryID,
                                  Category = a.Category,
                                  UpdatedBy = a.UpdatedBy,
                                  DateTimeUpdated = a.DateTimeUpdated
                              }).Distinct().OrderBy(a => a.Category).ToList();

            return result;
        }
    }

    public static bool InsertCategory(string category, int employeeId)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_Sav_Category(category, employeeId);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool UpdateCategory(int categoryId, string category, int updatedBy)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_Upd_Category(categoryId, category, updatedBy);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool DeleteCategory(int categoryId, int updatedBy)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_Hid_Category(categoryId, updatedBy);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    #endregion

    #region .SUB-CATEGORY

    public static bool ChkIfSubcategoryHasChild(int? subcategId)
    {
        var db = new IntranetDBDataContext();
        var result = (from a in db.tbl_TranscomUniversity_Lkp_Subcategories
                      where a.ParentSubcategoryID == subcategId
                      select a).Count();
        return result > 0;
    }

    public static DataHelperModel.SubCategories GetSubCategoryById(int? subcategId)
    {
        var db = new IntranetDBDataContext();
        var result = (from a in db.tbl_TranscomUniversity_Lkp_Subcategories
                      where a.SubCategoryID == subcategId
                      select new DataHelperModel.SubCategories
                      {
                          SubcategoryId = a.SubCategoryID,
                          Subcategory = a.Subcategory,
                          CategoryId = a.CategoryID,
                          HideFromList = a.HideFromList,
                          ParentSubcategoryId = a.ParentSubcategoryID
                      }).FirstOrDefault();
        return result;
    }

    //public static List<DataHelperModel.SubCategories> GetSubCategoriesByCategoryId(int? categoryId)
    //{
    //    var db = new IntranetDBDataContext();
    //    var result = (from a in db.tbl_TranscomUniversity_Lkp_Subcategories
    //                  join b in db.tbl_TranscomUniversity_Lkp_Categories on a.CategoryID equals b.CategoryID
    //                  where b.CategoryID == categoryId && a.HideFromList == 0
    //                  orderby a.Subcategory
    //                  select new DataHelperModel.SubCategories
    //                      {
    //                          SubcategoryId = a.SubCategoryID,
    //                          Subcategory = a.Subcategory,
    //                          CategoryId = a.CategoryID,
    //                          UpdatedBy = a.UpdatedBy,
    //                          DateTimeUpdated = a.DateTimeUpdated,
    //                          ParentSubcategoryId = a.ParentSubcategoryID
    //                      }).ToList();
    //    return result;
    //}

    public static List<DataHelperModel.SubCategories> GetSubCategoriesByCategoryId(int? categoryId, string accessmode)
    {
        var db = new IntranetDBDataContext();
        if (accessmode == "Admin")
        {
            var result = (from a in db.tbl_TranscomUniversity_Lkp_Subcategories
                          join b in db.tbl_TranscomUniversity_Lkp_Categories on a.CategoryID equals b.CategoryID
                          where b.CategoryID == categoryId && a.HideFromList == 0
                          orderby a.Subcategory
                          select new DataHelperModel.SubCategories
                          {
                              SubcategoryId = a.SubCategoryID,
                              Subcategory = a.Subcategory,
                              CategoryId = a.CategoryID,
                              UpdatedBy = a.UpdatedBy,
                              DateTimeUpdated = a.DateTimeUpdated,
                              ParentSubcategoryId = a.ParentSubcategoryID
                          }).OrderBy(a => a.Subcategory).ToList();
            return result;
        }
        else
        {
            var result = (from a in db.tbl_TranscomUniversity_Lkp_Subcategories
                          join b in db.tbl_TranscomUniversity_Lkp_Categories
                          on a.CategoryID equals b.CategoryID

                          join c in db.tbl_TranscomUniversity_Cor_Courses
                          on a.SubCategoryID equals c.SubcategoryID

                          where b.CategoryID == categoryId && a.HideFromList == 0 &&
                          (c.AccessMode == accessmode || c.AccessMode == "Both")

                          orderby a.Subcategory
                          select new DataHelperModel.SubCategories
                          {
                              SubcategoryId = a.SubCategoryID,
                              Subcategory = a.Subcategory,
                              CategoryId = a.CategoryID,
                              UpdatedBy = a.UpdatedBy,
                              DateTimeUpdated = a.DateTimeUpdated,
                              ParentSubcategoryId = a.ParentSubcategoryID
                          }).Distinct().OrderBy(a => a.Subcategory).ToList();
            return result;
        }
    }

    public static List<DataHelperModel.SubCategories> GetSubCategoriesByCategoryId_Child(int? categoryId, int? parentSubcategoryId)
    {
        var db = new IntranetDBDataContext();
        var result = (from a in db.tbl_TranscomUniversity_Lkp_Subcategories
                      join b in db.tbl_TranscomUniversity_Lkp_Categories on a.CategoryID equals b.CategoryID
                      where b.CategoryID == categoryId && a.HideFromList == 0 && a.ParentSubcategoryID == parentSubcategoryId
                      orderby a.Subcategory
                      select new DataHelperModel.SubCategories
                      {
                          SubcategoryId = a.SubCategoryID,
                          Subcategory = a.Subcategory,
                          CategoryId = a.CategoryID,
                          UpdatedBy = a.UpdatedBy,
                          DateTimeUpdated = a.DateTimeUpdated,
                          ParentSubcategoryId = a.ParentSubcategoryID
                      }).ToList();
        return result;
    }

    public static List<DataHelperModel.SubCategories> GetSubCategoriesByCategoryId_Parent(int? categoryId)
    {
        var db = new IntranetDBDataContext();
        var result = (from a in db.tbl_TranscomUniversity_Lkp_Subcategories
                      join b in db.tbl_TranscomUniversity_Lkp_Categories on a.CategoryID equals b.CategoryID
                      where b.CategoryID == categoryId && a.HideFromList == 0 && a.ParentSubcategoryID == null
                      orderby a.Subcategory
                      select new DataHelperModel.SubCategories
                      {
                          SubcategoryId = a.SubCategoryID,
                          Subcategory = a.Subcategory,
                          CategoryId = a.CategoryID,
                          UpdatedBy = a.UpdatedBy,
                          DateTimeUpdated = a.DateTimeUpdated,
                          ParentSubcategoryId = a.ParentSubcategoryID
                      }).ToList();
        return result;
    }

    public static bool InsertSubCategory(string subCategory, int? categoryId, int? employeeId, int? parentSubcategId)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_Sav_Subcategory(subCategory, categoryId, employeeId, parentSubcategId);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool UpdateSubCategory(int subCategoryId, string subCategory, int categoryId, int employeeId, int? parentSubcategId)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_Upd_Subcategory(subCategoryId, subCategory, categoryId, employeeId, parentSubcategId);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool DeleteSubCategory(int subCategoryId, int updatedBy)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_Hid_Subcategory(subCategoryId, updatedBy);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    #endregion

    #region .COURSE

    public static IQueryable<DataHelperModel.Course> GetCourse_ById(long courseId)
    {
        var db = new IntranetDBDataContext();
        var qry = (from a in db.tbl_TranscomUniversity_Cor_Courses
                   where a.CourseID == courseId
                   select new DataHelperModel.Course
                       {
                           CourseId = a.CourseID,
                           Title = a.Title,
                           Description = a.Description
                       });
        return qry;
    }

    public static List<pr_TranscomUniversity_Rlt_SearchResult> DoSearch(string searchVal, string accessMode)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Rlt_SearchResult>(db.pr_TranscomUniversity_Rlt_Search(searchVal, accessMode)).ToList();
    }

    public static List<pr_TranscomUniversity_Lkp_CourseResult> GetCourses(int? categoryId, int? subCategoryId, string accessMode, string courseIDs)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_CourseResult>(db.pr_TranscomUniversity_Lkp_Course(categoryId, subCategoryId, accessMode, courseIDs)).OrderBy(p => p.Title).ToList();
    }

    public static List<pr_TranscomUniversity_Lkp_CourseResult> GetNonScormCourses(int? categoryId, int? subCategoryId, string accessMode, string courseIDs)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_CourseResult>(db.pr_TranscomUniversity_Lkp_Course(categoryId, subCategoryId, accessMode, courseIDs)).OrderBy(p => p.Title).Where(p => p.CourseTypeID != 2).ToList();
    }

    public static bool InsertCourse(int categoryId, int subcategoryId,
           string title, string description, bool enrolmentReq, string encryptedCourseId,
           string accessMode, DateTime? startDate, DateTime? endDate, int employeeId,
           int? duration, int? version, string author, string deptOwnership, string trainingcost, DateTime? lastmod, string targetAudience
           , DataTable dtPreReq, decimal requiredTenure, decimal requiredTenureinRole, bool legacy, int requiredProgramId, int? nucommCourseId, int courseTypeId, string keywords)
    {
        bool retVal;
        int? courseId = null;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_Sav_Course(categoryId, subcategoryId, title, description, enrolmentReq, encryptedCourseId,
                                                accessMode, startDate, endDate, employeeId, duration, version.ToString(), author,
                                                deptOwnership, lastmod, trainingcost, targetAudience, legacy, nucommCourseId, ref courseId, courseTypeId, keywords);

            db.SubmitChanges();

            if (dtPreReq.Rows.Count > 0)
            {

                var db2 = new MyDataContext();
                var results = from row in dtPreReq.AsEnumerable()
                              select new tbl_TranscomUniversity_Rlt_Prequisite
                              {
                                  PrerequisiteTypeID = 1,
                                  CourseID = Convert.ToInt32(courseId),
                                  Active = true,
                                  Value = Convert.ToDecimal(row.Field<int>("CourseID"))
                              };
                db2.BulkInsertAll(results);
            }
            if (requiredTenure > 0)
            {
                var newPreReq = new tbl_TranscomUniversity_Rlt_Prequisite
                {
                    PrerequisiteTypeID = 2,
                    CourseID = 0,
                    Value = requiredTenure,
                    Active = true
                };
                db.tbl_TranscomUniversity_Rlt_Prequisites.InsertOnSubmit(newPreReq);
            }

            if (requiredTenureinRole > 0)
            {
                var newPreReqRole = new tbl_TranscomUniversity_Rlt_Prequisite
                {
                    PrerequisiteTypeID = 3,
                    CourseID = Convert.ToInt32(courseId),
                    Value = requiredTenureinRole,
                    Active = true
                };
                db.tbl_TranscomUniversity_Rlt_Prequisites.InsertOnSubmit(newPreReqRole);
            }

            if (requiredProgramId > 0)
            {
                var newPreReqRole = new tbl_TranscomUniversity_Rlt_Prequisite
                {
                    PrerequisiteTypeID = 4,
                    CourseID = Convert.ToInt32(courseId),
                    Value = requiredProgramId,
                    Active = true
                };
                db.tbl_TranscomUniversity_Rlt_Prequisites.InsertOnSubmit(newPreReqRole);
            }

            db.SubmitChanges();

            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    private static IEnumerable<tbl_TranscomUniversity_Rlt_Prequisite> ConvertToTankReadings(DataTable dataTable, int courseId)
    {
        foreach (DataRow row in dataTable.Rows)
        {
            yield return new tbl_TranscomUniversity_Rlt_Prequisite
            {
                PrerequisiteTypeID = 1,
                CourseID = courseId,
                Active = true,
                Value = Convert.ToInt32(row["CourseID"])
            };
        }
    }

    public static bool UpdateCourse(int courseId, int categoryId, int subcategoryId,
                                     string title, string description, bool enrolmentReq, string encryptedCourseId,
                                     string accessMode,
                                     DateTime? startDate, DateTime? endDate, int employeeId, int? duration, int? version,
                                     string author, string deptOwnership, string trainingcost, string targetAudience,
                                     DataTable dtAdd, DataTable dtDelete, decimal tenure, decimal tenureInRole,
                                     bool legacy, decimal programId, int? nucommcourseId, int? courseTypeId, string keywords)
    {
        bool retVal;
        try
        {
            using (var db = new IntranetDBDataContext())
            {
                db.pr_TranscomUniversity_Upd_Course(courseId, categoryId, subcategoryId, title, description,
                                                    enrolmentReq,
                                                    encryptedCourseId, accessMode, startDate, endDate, employeeId,
                                                    duration,
                                                    version.ToString(), author, deptOwnership, null, trainingcost,
                                                    targetAudience, legacy, nucommcourseId, courseTypeId, keywords);
                db.SubmitChanges();


                //Mass Insert
                if (dtAdd != null)
                {
                    var db2 = new MyDataContext();
                    var results = ConvertToTankReadings(dtAdd, courseId);

                    db2.BulkInsertAll(results);
                }
                db.SubmitChanges();

                //Mass Delete
                var ids = string.Empty;

                if (dtDelete != null)
                {
                    foreach (DataRow row in dtDelete.Rows)
                    {
                        ids = ids + row["PrerequisiteID"] + ",";
                    }

                    ids = ids.Substring(0, ids.Length - 1);

                    var sql =
                        "UPDATE [tbl_TranscomUniversity_Rlt_Prequisite] SET Active = 0 WHERE PrerequisiteID in (" + ids +
                        ")";
                    db.ExecuteCommand(sql);
                }

                if (
                    db.tbl_TranscomUniversity_Rlt_Prequisites.Any(
                        u => u.CourseID == courseId && u.PrerequisiteTypeID == 2))
                {
                    var prereq =
                        db.tbl_TranscomUniversity_Rlt_Prequisites.First(
                            u => u.CourseID == courseId && u.PrerequisiteTypeID == 2);
                    //If Tenure Active
                    if (
                        db.tbl_TranscomUniversity_Rlt_Prequisites.Any(
                            u => u.CourseID == courseId && u.PrerequisiteTypeID == 2 && u.Active))
                    {
                        //If zero set inactive
                        if (tenure == 0)
                        {
                            prereq.Active = false;
                            prereq.Value = 0;
                        }
                        //set value
                        else
                        {
                            prereq.Value = tenure;
                        }
                    }
                    else
                    {
                        //set value and set as active
                        if (tenure != 0)
                        {
                            prereq.Active = true;
                            prereq.Value = tenure;
                        }
                    }

                }
                else if (tenure != 0)
                {
                    var newPreReq = new tbl_TranscomUniversity_Rlt_Prequisite
                    {
                        PrerequisiteTypeID = 2,
                        CourseID = Convert.ToInt32(courseId),
                        Value = tenure,
                        Active = true
                    };

                    db.tbl_TranscomUniversity_Rlt_Prequisites.InsertOnSubmit(newPreReq);
                }

                if (
                    db.tbl_TranscomUniversity_Rlt_Prequisites.Any(
                        u => u.CourseID == courseId && u.PrerequisiteTypeID == 3))
                {
                    var prereq =
                        db.tbl_TranscomUniversity_Rlt_Prequisites.First(
                            u => u.CourseID == courseId && u.PrerequisiteTypeID == 3);
                    //If TenureInRole Active
                    if (
                        db.tbl_TranscomUniversity_Rlt_Prequisites.Any(
                            u => u.CourseID == courseId && u.PrerequisiteTypeID == 3 && u.Active))
                    {
                        //If zero set inactive
                        if (tenureInRole == 0)
                        {
                            prereq.Active = false;
                            prereq.Value = 0;
                        }
                        //set value
                        else
                        {
                            prereq.Value = tenureInRole;
                        }
                    }
                    else
                    {
                        //set value and set as active
                        if (tenureInRole != 0)
                        {
                            prereq.Active = true;
                            prereq.Value = tenureInRole;
                        }
                    }

                }
                else if (tenureInRole != 0)
                {
                    var newPreReqRole = new tbl_TranscomUniversity_Rlt_Prequisite
                    {
                        PrerequisiteTypeID = 3,
                        CourseID = Convert.ToInt32(courseId),
                        Value = tenureInRole,
                        Active = true
                    };
                    db.tbl_TranscomUniversity_Rlt_Prequisites.InsertOnSubmit(newPreReqRole);
                }

                //for program
                if (
                    db.tbl_TranscomUniversity_Rlt_Prequisites.Any(
                        u => u.CourseID == courseId && u.PrerequisiteTypeID == 4))
                {
                    var prereq =
                        db.tbl_TranscomUniversity_Rlt_Prequisites.First(
                            u => u.CourseID == courseId && u.PrerequisiteTypeID == 4);
                    //If Program Active
                    if (
                        db.tbl_TranscomUniversity_Rlt_Prequisites.Any(
                            u => u.CourseID == courseId && u.PrerequisiteTypeID == 4 && u.Active))
                    {
                        //If zero set inactive
                        if (programId == 0)
                        {
                            prereq.Active = false;
                            prereq.Value = 0;
                        }
                        //set value
                        else
                        {
                            prereq.Value = programId;
                        }
                    }
                    else
                    {
                        //set value and set as active
                        if (tenure != 0)
                        {
                            prereq.Active = true;
                            prereq.Value = programId;
                        }
                    }

                }
                else if (programId != 0)
                {
                    var newPreReq = new tbl_TranscomUniversity_Rlt_Prequisite
                    {
                        PrerequisiteTypeID = 4,
                        CourseID = Convert.ToInt32(courseId),
                        Value = programId,
                        Active = true
                    };

                    db.tbl_TranscomUniversity_Rlt_Prequisites.InsertOnSubmit(newPreReq);
                }

                db.SubmitChanges();

                retVal = true;

            }
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool DeleteCourse(int courseId, int updatedBy)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_Hid_Course(courseId, updatedBy);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static List<pr_TranscomUniversity_lkp_getExternalUserCourseAccessResult> GetCourseAccess(string email)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_lkp_getExternalUserCourseAccessResult>(db.pr_TranscomUniversity_lkp_getExternalUserCourseAccess(email)).ToList();

    }

    public static List<pr_TranscomUniversity_GetExternalUserCourseHistory_ByCategoryIDResult>
        GetCourseHistoryByCategoryId(string cim, int categoryId)
    {
        var db = new NuSkillCheckDataContext();
        return new List<pr_TranscomUniversity_GetExternalUserCourseHistory_ByCategoryIDResult>(db.pr_TranscomUniversity_GetExternalUserCourseHistory_ByCategoryID(cim, categoryId)).ToList();
    }

    #region .EXPORT TO EXCEL

    public static DataTable GetCoursesForExport(int? categoryId, int? subCategoryId, string accessMode)
    {
        var db = new IntranetDBDataContext();
        var result = db.pr_TranscomUniversity_Lkp_Course(categoryId, subCategoryId, accessMode,"").ToList();

        var dt = Load(result);
        return dt;
    }

    protected static DataTable Load<T>(List<T> param)
    {
        var table = new DataTable();

        var info = param[0].GetType().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);

        foreach (var i in info)
        {
            table.Columns.Add(i.Name);
        }

        var values = new object[info.Length];

        foreach (var item in param)
        {
            for (var i = 0; i < values.Length; i++)
            {
                table.NewRow();
                values[i] = info[i].GetValue(item, null);
            }
            table.Rows.Add(values);
        }

        return table;
    }

    #endregion

    #endregion

    #region .COURSE ENROLMENT / LEARNING PLAN

    public static List<pr_TranscomUniversity_Lkp_GetEnrolledAssignedEncryptedResult> GetEnrolledAssignedEncryptedIDs(int? twwid)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_GetEnrolledAssignedEncryptedResult>(db.pr_TranscomUniversity_Lkp_GetEnrolledAssignedEncrypted(twwid)).ToList();
    }

    //public static List<pr_TranscomUniversity_Lkp_AssignedEnrolledCoursesResult> GetAssignedEnrolledCourse_ByTwwId(int? twwid)
    //{
    //    var db = new IntranetDBDataContext();
    //    return new List<pr_TranscomUniversity_Lkp_AssignedEnrolledCoursesResult>(db.pr_TranscomUniversity_Lkp_AssignedEnrolledCourses(twwid)).ToList();
    //}

    public static List<pr_TranscomUniversity_Lkp_AssignedEnrolledCoursesResult> GetAssignedEnrolledCourse_ByTwwId(int? twwid)
    {
        var db = new IntranetDBDataContext() { CommandTimeout = 0 };
        return new List<pr_TranscomUniversity_Lkp_AssignedEnrolledCoursesResult>(db.pr_TranscomUniversity_Lkp_AssignedEnrolledCourses(twwid)).ToList();
    }

    public static List<pr_TranscomUniversity_Lkp_AssignedEnrolledCoursesTeamResult> GetAssignedEnrolledCourse_ByTeam(int twwid)
    {
        var db = new IntranetDBDataContext { CommandTimeout = 0 };
        return new List<pr_TranscomUniversity_Lkp_AssignedEnrolledCoursesTeamResult>(db.pr_TranscomUniversity_Lkp_AssignedEnrolledCoursesTeam(twwid)).ToList();
    }


    public static bool InsertCourseEnrolment(int courseId, int cimNo, int supCimNo)
    {
        var db = new IntranetDBDataContext();
        bool retVal;

        try
        {
            var newEnrolment = new tbl_TranscomUniversity_CourseEnrollment
            {
                CourseId = courseId,
                EnrolleeCimNo = cimNo,
                SupCimNo = supCimNo,
                EnrolmentStatus = 1,
                CreateDate = DateTime.Now,
                Active = true,
                IsViewed = false
            };

            db.tbl_TranscomUniversity_CourseEnrollments.InsertOnSubmit(newEnrolment);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }

        return retVal;
    }

    public static short ChkUserEnrolmentStatusToCourse(int courseId, int cimNo)
    {
        var db = new IntranetDBDataContext();
        short stat = 0;
        var qry = (from a in db.tbl_TranscomUniversity_CourseEnrollments
                   where a.CourseId == courseId && a.EnrolleeCimNo == cimNo //&& a.Active
                   select a).FirstOrDefault();
        if (qry != null)
        {
            stat = qry.EnrolmentStatus;
        }
        return stat;
    }

    public static int CountEnrollments_Emp_ForApproval(int cimNo)
    {
        var db = new IntranetDBDataContext();
        var result =
            (from a in db.tbl_TranscomUniversity_CourseEnrollments where a.EnrolmentStatus == 1 && a.EnrolleeCimNo == cimNo && a.Active select a)
                .Count();
        return result;
    }

    public static int CountEnrollments_Sup_ForApproval(int supervisorCimNo)
    {
        var db = new IntranetDBDataContext();

        var retVal = 0;

        var result = GetTwwidByTeam(supervisorCimNo).ToList();

        if (result.Count > 0)
        {
            var list = new List<int>();
            foreach (var emp in result)
            {
                list.Add(Convert.ToInt32(emp.TwwId));
            }

            var twwIds = list.ToArray();

            var qry = (from a in db.tbl_TranscomUniversity_CourseEnrollments
                       where a.EnrolmentStatus == 1 && twwIds.Contains(a.EnrolleeCimNo) && a.Active
                       select a).Count();

            retVal = qry > 0 ? qry : 0;
        }

        return retVal;
    }

    public static int CountEnrollments_Approved_NotChecked(int cimNo)
    {
        var db = new IntranetDBDataContext();
        var result =
            (from a in db.tbl_TranscomUniversity_CourseEnrollments where a.EnrolmentStatus == 2 && a.EnrolleeCimNo == cimNo && a.Active && a.IsViewed == false select a)
                .Count();
        return result;
    }

    public static int CountEnrollments_Denied_NotChecked(int cimNo)
    {
        var db = new IntranetDBDataContext();
        var result =
            (from a in db.tbl_TranscomUniversity_CourseEnrollments where a.EnrolmentStatus == 3 && a.EnrolleeCimNo == cimNo && a.IsViewed == false select a)
                .Count();
        return result;
    }

    public static IQueryable<DataHelperModel.CourseEnrolmentAssigned> GetCourseEnrolment_ByStatus(short status, int cim)
    {
        var db = new IntranetDBDataContext();

        var result = (from a in db.tbl_TranscomUniversity_CourseEnrollments
                      join b in db.tbl_TranscomUniversity_Cor_Courses on a.CourseId equals b.CourseID
                      join c in db.ref_TranscomUniversity_Status on a.EnrolmentStatus equals c.StatusId
                      where a.EnrolleeCimNo == cim && a.EnrolmentStatus == status && a.Active
                      orderby a.CreateDate descending
                      select new DataHelperModel.CourseEnrolmentAssigned
                      {
                          EnrolleeCim = a.EnrolleeCimNo,
                          EnrollmentId = a.EnrollmentId,
                          CourseId = a.CourseId,
                          EncryptedCourseId = b.EncryptedCourseID,
                          Title = b.Title,
                          Description = b.Description,
                          EnrolmentRequired = b.EnrolmentRequired,
                          SupCim = a.SupCimNo,
                          Comment = a.Comments,
                          StatusId = a.EnrolmentStatus,
                          Status = c.Status,
                          ApproveDenyCim = a.ApproveDenyBy,
                          ApproveDenyDate = a.ApproveDenyDate,
                          CreateDate = a.CreateDate,
                          Active = a.Active,
                          IsViewed = a.IsViewed,
                          CourseType = "Enrolled",
                          DueDate = null
                      }).GroupBy(x => new { x.EnrolleeCim, x.CourseId }).Select(p => p.First());
        return result;
    }

    //public static IQueryable<DataHelperModel.CourseEnrolmentAssigned> GetCourseEnrolment_ByStatus(int[] statusIds, int[] twwIds)
    //{
    //    var db = new IntranetDBDataContext();

    //    var result = (from a in db.tbl_TranscomUniversity_CourseEnrollments
    //                  join b in db.tbl_TranscomUniversity_Cor_Courses on a.CourseId equals b.CourseID
    //                  join c in db.ref_TranscomUniversity_Status on a.EnrolmentStatus equals c.StatusId
    //                  join d in db.vw_Personnel_Cor_Employees on a.EnrolleeCimNo equals d.CIMNumber
    //                  where twwIds.Contains(a.EnrolleeCimNo) && statusIds.Contains(a.EnrolmentStatus)
    //                  orderby a.ApproveDenyDate descending
    //                  select new DataHelperModel.CourseEnrolmentAssigned
    //                  {
    //                      EnrolleeCim = a.EnrolleeCimNo,
    //                      CimName = d.CIMName,
    //                      Name = d.Name,
    //                      EnrollmentId = a.EnrollmentId,
    //                      CourseId = a.CourseId,
    //                      EncryptedCourseId = b.EncryptedCourseID,
    //                      Title = b.Title,
    //                      Description = b.Description,
    //                      EnrolmentRequired = b.EnrolmentRequired,
    //                      SupCim = a.SupCimNo,
    //                      Comment = a.Comments,
    //                      StatusId = a.EnrolmentStatus,
    //                      Status = c.Status,
    //                      ApproveDenyCim = a.ApproveDenyBy,
    //                      ApproveDenyDate = a.ApproveDenyDate,
    //                      CreateDate = a.CreateDate,
    //                      Active = a.Active,
    //                      IsViewed = a.IsViewed,
    //                      CourseType = "Enrolled",
    //                      DueDate = null
    //                  }).GroupBy(x => new { x.EnrolleeCim, x.CourseId }).Select(p => p.First());
    //    return result;
    //}

    public static IQueryable<DataHelperModel.CourseEnrolmentAssigned> GetCourseEnrolment_Employees(int[] statusIds, int[] twwIds)
    {
        var db = new IntranetDBDataContext();

        var result = (from a in db.tbl_TranscomUniversity_CourseEnrollments
                      join b in db.tbl_TranscomUniversity_Cor_Courses on a.CourseId equals b.CourseID
                      join c in db.ref_TranscomUniversity_Status on a.EnrolmentStatus equals c.StatusId
                      join d in db.vw_Personnel_Cor_Employees on a.EnrolleeCimNo equals d.CIMNumber
                      where twwIds.Contains(a.EnrolleeCimNo) && statusIds.Contains(a.EnrolmentStatus)
                      orderby a.ApproveDenyDate descending
                      select new DataHelperModel.CourseEnrolmentAssigned
                      {
                          EnrolleeCim = a.EnrolleeCimNo,
                          CimName = d.CIMName,
                          Name = d.Name,
                          EnrollmentId = a.EnrollmentId,
                          CourseId = a.CourseId,
                          EncryptedCourseId = b.EncryptedCourseID,
                          Title = b.Title,
                          Description = b.Description,
                          EnrolmentRequired = b.EnrolmentRequired,
                          SupCim = a.SupCimNo,
                          Comment = a.Comments,
                          StatusId = a.EnrolmentStatus,
                          Status = c.Status,
                          ApproveDenyCim = a.ApproveDenyBy,
                          ApproveDenyDate = a.ApproveDenyDate,
                          CreateDate = a.CreateDate,
                          Active = a.Active,
                          IsViewed = a.IsViewed
                      }).GroupBy(x => new { x.EnrolleeCim, x.CourseId }).Select(p => p.First());
        return result;
    }

    public static IQueryable<DataHelperModel.CourseEnrolmentAssigned> GetCourseEnrolment_History(int twwid, int courseId)
    {
        var db = new IntranetDBDataContext();

        var result = (from a in db.tbl_TranscomUniversity_CourseEnrollments
                      join b in db.ref_TranscomUniversity_Status on a.EnrolmentStatus equals b.StatusId into s
                      from b in s.DefaultIfEmpty()
                      join c in db.vw_Personnel_Cor_Employees on a.ApproveDenyBy equals c.CIMNumber into e
                      from c in e.DefaultIfEmpty()
                      where a.EnrolleeCimNo == twwid && a.CourseId == courseId
                      select new DataHelperModel.CourseEnrolmentAssigned
                      {
                          EnrollmentId = a.EnrollmentId,
                          Status = b.Status,
                          CimName = c.Name ?? "-",
                          Comment = a.Comments ?? "-",
                          CreateDate = a.CreateDate
                      });
        return result;
    }

    public static List<pr_TranscomUniversity_Lkp_AssignedCourse_HistoryResult> GetAssignedCourse_History(int twwId, int courseId)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_AssignedCourse_HistoryResult>(db.pr_TranscomUniversity_Lkp_AssignedCourse_History(twwId, courseId)).ToList();
    }

    public static bool UpdateCourseEnrolmentStatus(long enrolmentId, string comment, short status, int approveOrCancelBy, bool active)
    {
        bool retVal;

        try
        {
            using (var db = new IntranetDBDataContext())
            {
                var course = db.tbl_TranscomUniversity_CourseEnrollments.First(p => p.EnrollmentId == enrolmentId);

                if (course != null)
                {
                    course.EnrollmentId = enrolmentId;
                    course.Comments = string.IsNullOrEmpty(comment) ? null : comment;
                    course.EnrolmentStatus = status;
                    course.ApproveDenyBy = approveOrCancelBy;
                    course.ApproveDenyDate = DateTime.Now;
                    course.Active = active;

                    db.SubmitChanges();
                    retVal = true;
                }
                else
                {
                    retVal = false;
                }
            }
        }
        catch (Exception)
        {
            retVal = false;
        }

        return retVal;
    }

    public static bool UpdateCourseEnrolmentViewStatus(int cimNo)
    {
        bool retVal;

        try
        {
            var db = new IntranetDBDataContext();
            db.ExecuteCommand("UPDATE [tbl_TranscomUniversity_CourseEnrollments] SET [IsViewed] = 1 WHERE EnrolleeCimNo = {0} AND EnrolmentStatus IN (2, 3)", cimNo);
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }

        return retVal;
    }

    //public static bool DeclineCourseEnrolment(int enrollmentId, int deniedBy)
    //{
    //    bool retVal;

    //    try
    //    {
    //        var db = new IntranetDBDataContext();
    //        db.ExecuteCommand("UPDATE [tbl_TranscomUniversity_CourseEnrollments] SET [Active] = 0, [EnrolmentStatus] = 3, [ApproveDenyBy] = {0} WHERE EnrollmentId = {1}", deniedBy, enrollmentId);
    //        retVal = true;
    //    }
    //    catch (Exception)
    //    {
    //        retVal = false;
    //    }

    //    return retVal;
    //}

    //public static bool CancelCourseEnrolment(int enrollmentId, int deniedBy)
    //{
    //    bool retVal;

    //    try
    //    {
    //        var db = new IntranetDBDataContext();
    //        db.ExecuteCommand("UPDATE [tbl_TranscomUniversity_CourseEnrollments] SET [Active] = 0, [EnrolmentStatus] = 4, [ApproveDenyBy] = {0} WHERE EnrollmentId = {1}", deniedBy, enrollmentId);
    //        retVal = true;
    //    }
    //    catch (Exception)
    //    {
    //        retVal = false;
    //    }

    //    return retVal;
    //}

    #endregion

    #region .COURSE HISTORY 
    public static List<pr_TranscomUniversity_Rpt_MyTranscriptResult> GetTranscript(string CIMs)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Rpt_MyTranscriptResult>(db.pr_TranscomUniversity_Rpt_MyTranscript(CIMs)).ToList();
    }
    #endregion

    #region .CAREER PATH

    public static List<tbl_TranscomUniversity_Lkp_CareerPath> GetCareerPath()
    {
        var db = new IntranetDBDataContext();
        return new List<tbl_TranscomUniversity_Lkp_CareerPath>(db.tbl_TranscomUniversity_Lkp_CareerPaths).ToList();
    }

    public static bool InsertCareerPath(string careerName, string careerPathDescription, byte hideFromList, string createdBy)
    {
        bool retVal;

        try
        {
            var db = new IntranetDBDataContext();

            var careerPath = new tbl_TranscomUniversity_Lkp_CareerPath
            {
                CareerName = careerName,
                Description = careerPathDescription,
                HideFromList = hideFromList,
                CreatedBy = createdBy,
                CreateDate = DateTime.Now,
                ModifiedBy = createdBy,
                ModifyDate = DateTime.Now
            };

            db.tbl_TranscomUniversity_Lkp_CareerPaths.InsertOnSubmit(careerPath);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool UpdateCareerPath(int careerPathId, string careerName, string description, byte hideFromList, string modifiedBy)
    {
        bool retVal;

        try
        {
            var db = new IntranetDBDataContext();

            var careerPath = new tbl_TranscomUniversity_Lkp_CareerPath
            {
                CareerPathID = careerPathId,
                CareerName = careerName,
                Description = description,
                HideFromList = hideFromList,
                ModifiedBy = modifiedBy,
                ModifyDate = DateTime.Now
            };

            db.tbl_TranscomUniversity_Lkp_CareerPaths.Attach(careerPath);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }

        return retVal;
    }

    public static void DeleteCareerPath(int careerPathId)
    {
        var db = new IntranetDBDataContext();
        db.ExecuteCommand("DELETE FROM [tbl_TranscomUniversity_Lkp_CareerPath] WHERE CareerPathID = {0}", careerPathId);
    }

    #endregion

    #region .CAREER COURSE

    public static List<vw_TranscomUniversity_CareerCourse> GetCareerCourses(int careerPathId)
    {
        var db = new IntranetDBDataContext();
        return new List<vw_TranscomUniversity_CareerCourse>(db.vw_TranscomUniversity_CareerCourses.Where(p => p.CareerPathID == careerPathId)).ToList();
    }

    public static bool InsertCareerCourse(int careerPathId, int courseId, string createdBy)
    {
        bool retVal;

        try
        {
            var db = new IntranetDBDataContext();

            var careerCourse = new tbl_TranscomUniversity_Cor_CareerCourse
            {
                CareerPathID = careerPathId,
                CourseID = courseId,
                CreateDate = DateTime.Now,
                CreatedBy = createdBy,
                ModifiedBy = createdBy,
                ModifyDate = DateTime.Now
            };

            db.tbl_TranscomUniversity_Cor_CareerCourses.InsertOnSubmit(careerCourse);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    #endregion

    #region .ROOM RESERVATION

    public static IQueryable<DataHelperModel.ReservationsEvents> GetReservations_ById(int reservationId)
    {
        var db = new IntranetDBDataContext();

        var result = (from a in db.vw_TranscomUniversity_ReservationsEvents
                      where a.ReservationReqId == reservationId
                      select new DataHelperModel.ReservationsEvents
                      {
                          //RESERVATION DETAILS
                          ReservationReqId = a.ReservationReqId,
                          TrainingTypeId = a.TrainingTypeId,
                          TrainingTypeOther = a.TrainingType_Other,
                          TrainingType = a.TrainingTypeId != 6 ? a.TrainingType : a.TrainingType_Other,
                          AttritionGrowthId = a.AttritionGrowthId,
                          AttritionGrowth = a.AttritionGrowth,
                          FteCount = a.FTECount,
                          RtpTandim = a.RTPTandim,
                          WaveNo = a.WaveNo,
                          SiteId = a.SiteId,
                          CompanySite = a.CompanySite,
                          ClientId = a.ClientId,
                          Client = a.Client,
                          CampaignId = a.CampaignId,
                          Campaign = a.Campaign,
                          Date = a.Date,
                          DateTo = a.DateTo,
                          TimeSlotId = a.TimeSlotId,
                          TimeSlot = a.TimeSlot,
                          TrainerName = a.TrainerName,
                          TrainingClassName = a.TrainingClassName,
                          WeekNo = a.WeekNo,
                          Advisory = a.Advisory,
                          ApproveByCoordinator = a.ApproveBy_Coordinator,
                          StatusId = a.StatusId,
                          ReservationStatus = a.ReservationStatus,
                          CreatedByReserv = a.CreatedBy_Reserv,
                          CreateDateReserv = a.CreateDate_Reserv.ToShortDateString(),
                          UpdatedByReserv = a.UpdatedBy_Reserv,
                          ActiveReserv = a.Active_Reserv,
                          //EVENT DETAILS
                          AssignedRoomId = a.AssignedRoomId,
                          RoomAssignment = a.Room,
                          CoordinatorAdvisory = a.CoordinatorAdvisory,
                          EnrollmentInstruction = a.EnrollmentInstruction,
                          CourseDescription = a.CourseDescription,
                          CreatedByEvent = a.CreatedBy_Event,
                          UpdatedByEvent = a.UpdatedBy_Event
                      });

        return result;
    }

    public static List<DataHelperModel.ReservationsEvents> GetReservations_ByStatus(int[] statusIds, string twwid)
    {
        var db = new IntranetDBDataContext();

        var result = (from a in db.vw_TranscomUniversity_ReservationsEvents
                      where statusIds.Contains(a.StatusId) && a.Active_Reserv && a.DateTo >= DateTime.Today
                      select new DataHelperModel.ReservationsEvents
                          {
                              //RESERVATION FIELDS
                              ReservationReqId = a.ReservationReqId,
                              TrainingTypeId = a.TrainingTypeId,
                              TrainingTypeOther = a.TrainingType_Other,
                              TrainingType = a.TrainingTypeId != 6 ? a.TrainingType : a.TrainingType_Other,
                              AttritionGrowthId = a.AttritionGrowthId,
                              AttritionGrowth = a.AttritionGrowth,
                              FteCount = a.FTECount,
                              RtpTandim = a.RTPTandim,
                              WaveNo = a.WaveNo,
                              SiteId = a.SiteId,
                              CompanySite = a.CompanySite,
                              ClientId = a.ClientId,
                              Client = a.Client,
                              CampaignId = a.CampaignId,
                              Campaign = a.Campaign,
                              Date = a.Date,
                              DateTo = a.DateTo,
                              TimeSlotId = a.TimeSlotId,
                              TimeSlot = a.TimeSlot,
                              TimeSortId = a.SortId,
                              TrainerName = a.TrainerName,
                              TrainingClassName = a.TrainingClassName,
                              WeekNo = a.WeekNo,
                              Advisory = a.Advisory,
                              ApproveByCoordinator = a.ApproveBy_Coordinator,
                              StatusId = a.StatusId,
                              ReservationStatus = a.ReservationStatus,
                              CreatedByReserv = a.CreatedBy_Reserv,
                              CreateDateReserv = a.CreateDate_Reserv.ToShortDateString(),
                              UpdatedByReserv = a.UpdatedBy_Reserv,
                              ActiveReserv = a.Active_Reserv,
                              //EVENT FIELDS
                              EventId = a.EventId,
                              AssignedRoomId = a.AssignedRoomId,
                              RoomAssignment = a.Room,
                              CoordinatorAdvisory = a.CoordinatorAdvisory,
                              EnrollmentInstruction = a.EnrollmentInstruction,
                              CourseDescription = a.CourseDescription,
                              CreatedByEvent = a.CreatedBy_Event,
                              UpdatedByEvent = a.UpdatedBy_Event,
                              ActiveEvent = a.Active_Event,
                              CreateDateEvent = a.CreateDate_Event.ToString()
                          }).OrderBy(p => p.Date).ThenBy(q => q.TimeSortId).ToList(); //.OrderBy(p => p.StatusId).ThenByDescending(p => p.ReservationReqId).ToList();

        if (!string.IsNullOrEmpty(twwid))
        {
            result = result.Where(p => p.CreatedByReserv == twwid).OrderBy(p => p.Date).ThenBy(q => q.TimeSortId).ToList(); //OrderBy(p => p.StatusId).ThenByDescending(p => p.ReservationReqId).ToList();
        }

        return result;
    }

    public static bool InsertRoomReservation(short? trainingTypeId, string trainingTypeOther, short attritionGrowth, int fteCount,
                                             string rtpTandim, int waveNo, int siteId, int clientId, int? campaignId,
                                             DateTime date, DateTime dateTo, short timeSlotId, string trainerName,
                                             string trainingClassName, short weekNo, string advisory,
                                             string approveByCoordinator, string approveByTrainer, short statusId,
                                             string createdBy, DateTime createDate)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_Sav_RoomReservation(trainingTypeId, trainingTypeOther, attritionGrowth, fteCount, rtpTandim, waveNo, siteId,
                                                         clientId, 0, date, dateTo, timeSlotId, trainerName,
                                                         trainingClassName, weekNo, advisory, approveByCoordinator,
                                                         approveByTrainer, statusId, createdBy, createDate);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool UpdateReservation_ById(int reservationId, short? trainingTypeId, string trainingTypeOther, short attritionGrowth, int fteCount,
                                             string rtpTandim, int? waveNo, int siteId, int clientId, int? campaignId,
                                             DateTime date, short timeSlotId, string trainerName,
                                             string trainingClassName, short weekNo, string advisory,
                                             string approveByCoordinator, short statusId, string updatedBy)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_Upd_RoomReservation(reservationId, trainingTypeId, trainingTypeOther,
                                                         attritionGrowth, fteCount,
                                                         rtpTandim, waveNo, siteId, clientId, campaignId,
                                                         date, timeSlotId, trainerName,
                                                         trainingClassName, weekNo, advisory,
                                                         approveByCoordinator, statusId, updatedBy);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    /// <summary>
    /// RESERVATION STATUS
    /// 0 = Draft;
    /// 1 = For Approval;
    /// 2 = Approved - Event created;
    /// 3 = Discarded/Cancelled;
    /// </summary>
    public static bool UpdateReservation_Status_ById(short statusId, string approvedByCoordinator, int reservationId)
    {
        bool retVal;
        try
        {
            using (var db = new IntranetDBDataContext())
            {
                var res = db.tbl_TranscomUniversity_Reservations.First(p => p.ReservationReqId == reservationId);

                if (statusId == 3) //Cancelled
                {
                    res.StatusId = statusId;
                    res.ApproveBy_Coordinator = approvedByCoordinator;
                    res.Active = false;
                }
                else
                {
                    res.StatusId = statusId;
                    res.ApproveBy_Coordinator = approvedByCoordinator;
                }

                db.SubmitChanges();
                retVal = true;
            }
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    //public static bool DeactivateReservation_ById(int reservationId)
    //{
    //    bool retVal;
    //    var db = new IntranetDBDataContext();
    //    try
    //    {
    //        db.ExecuteCommand("UPDATE dbo.tbl_TranscomUniversity_Reservations SET [Active]=0 WHERE [ReservationReqId]={0} ", reservationId);
    //        retVal = true;
    //    }
    //    catch (Exception)
    //    {
    //        retVal = false;
    //    }
    //    return retVal;
    //}

    #endregion

    #region .CALENDAR EVENTS

    public static bool InsertCalendarEvent(int reservationReqId, short assignedRoomId, string coordinatorAdvisory, string enrollmentInstruction, string courseDescription, string createdBy)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            var calendarEvent = new tbl_TranscomUniversity_Event
                {
                    ReservationReqId = reservationReqId,
                    AssignedRoomId = assignedRoomId,
                    CoordinatorAdvisory = coordinatorAdvisory,
                    EnrollmentInstruction = enrollmentInstruction,
                    CourseDescription = courseDescription,
                    CreatedBy = createdBy,
                    CreateDate = DateTime.Now,
                    Active = true
                };

            db.tbl_TranscomUniversity_Events.InsertOnSubmit(calendarEvent);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool UpdateCalendarEvent(int eventId, short assignedRoomId, string coorAdvisory, string enrollIns, string courseDesc, string updatedBy)
    {
        bool retVal;
        try
        {
            using (var db = new IntranetDBDataContext())
            {
                var ev = db.tbl_TranscomUniversity_Events.First(p => p.EventId == eventId);

                ev.AssignedRoomId = assignedRoomId;
                ev.CoordinatorAdvisory = coorAdvisory;
                ev.EnrollmentInstruction = enrollIns;
                ev.CourseDescription = courseDesc;
                ev.UpdatedBy = updatedBy;

                db.SubmitChanges();
                retVal = true;
            }
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool DeactivateEvent_ById(int eventId, string updatedBy)
    {
        bool retVal;
        var db = new IntranetDBDataContext();
        try
        {
            db.ExecuteCommand("UPDATE dbo.tbl_TranscomUniversity_Events SET [Active]=0 WHERE [EventId]={0} ", eventId);
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static List<DataHelperModel.ReservationsEvents> GetCalendarEvents_ByStatus(int[] statusIds)
    {
        var db = new IntranetDBDataContext();

        var result = (from a in db.vw_TranscomUniversity_ReservationsEvents
                      where
                          statusIds.Contains(a.StatusId) && a.Active_Reserv && a.DateTo >= DateTime.Today
                      select new DataHelperModel.ReservationsEvents
                          {
                              ReservationReqId = a.ReservationReqId,
                              TrainingTypeId = a.TrainingTypeId,
                              TrainingTypeOther = a.TrainingType_Other,
                              TrainingType = a.TrainingTypeId != 6 ? a.TrainingType : a.TrainingType_Other,
                              AttritionGrowthId = a.AttritionGrowthId,
                              AttritionGrowth = a.AttritionGrowth,
                              FteCount = a.FTECount,
                              RtpTandim = a.RTPTandim,
                              WaveNo = a.WaveNo,
                              SiteId = a.SiteId,
                              CompanySite = a.CompanySite,
                              ClientId = a.ClientId,
                              Client = a.Client,
                              CampaignId = a.CampaignId,
                              Campaign = a.Campaign,
                              Date = a.Date,
                              DateTo = a.DateTo,
                              TimeSlotId = a.TimeSlotId,
                              TimeSlot = a.TimeSlot,
                              TimeSortId = a.SortId,
                              TrainerName = a.TrainerName,
                              TrainingClassName = a.TrainingClassName,
                              WeekNo = a.WeekNo,
                              Advisory = a.Advisory,
                              ApproveByCoordinator = a.ApproveBy_Coordinator,
                              StatusId = a.StatusId,
                              CreatedByReserv = a.CreatedBy_Reserv,
                              CreateDateReserv = a.CreateDate_Reserv.ToShortDateString(),
                              UpdatedByReserv = a.UpdatedBy_Reserv,
                              ActiveReserv = a.Active_Reserv,
                              EventId = a.EventId,
                              AssignedRoomId = a.AssignedRoomId,
                              RoomAssignment = a.Room,
                              CoordinatorAdvisory = a.CoordinatorAdvisory,
                              EnrollmentInstruction = a.EnrollmentInstruction,
                              CourseDescription = a.CourseDescription,
                              CreatedByEvent = a.CreatedBy_Event,
                              UpdatedByEvent = a.UpdatedBy_Event,
                              ActiveEvent = (bool)a.Active_Event,
                              CreateDateEvent = a.CreateDate_Event.ToString(),
                              ReservationStatus = a.ReservationStatus
                          }).OrderBy(p => p.Date).ThenBy(q => q.TimeSortId).ToList();

        return result;
    }

    public static int CountRoomReservations_ForApproval()
    {
        var db = new IntranetDBDataContext();
        var result =
            (from a in db.vw_TranscomUniversity_ReservationsEvents where a.StatusId == 1 && a.Active_Reserv && a.DateTo >= DateTime.Today select a)
                .Count();
        return result;
    }

    #endregion

    #region .SITE/CLIENT/CAMPAIGN DATA

    public static List<pr_TranscomUniversity_Lkp_CampaignResult> GetCampaigns_ByClientId(int? clientId)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_CampaignResult>(db.pr_TranscomUniversity_Lkp_Campaign(clientId)).ToList();
    }

    public static List<pr_TranscomUniversity_Lkp_ClientResult> GetClients()
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_ClientResult>(db.pr_TranscomUniversity_Lkp_Client()).ToList();
    }

    #endregion

    #region .EXTERNAL USER MANAGEMENT

    public static List<pr_TranscomUniversity_Lkp_ExternalUsersResult> GetExternalUsers(int displayType)
    {
        var db = new IntranetDBDataContext();
        return
            new List<pr_TranscomUniversity_Lkp_ExternalUsersResult>(
                db.pr_TranscomUniversity_Lkp_ExternalUsers(displayType)).ToList();
    }


    public static bool UpdateExternalUser(int userId, string firstName, string lastName, string email,
                                          int clientId, DateTime? startDate, DateTime? endDate, int categoryId,
                                          int subcategory, int updatedBy)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_upd_externalUser(userId, firstName, lastName, email, clientId, startDate, endDate,
                                                      updatedBy, categoryId, subcategory);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool InsertExternalUser(string firstName, string lastName, string email,
                                          int clientId, DateTime? startDate, DateTime? endDate, int categoryId,
                                          int subcategoryId, int updatedBy)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_Sav_externalUser(firstName, lastName, email, clientId, startDate, endDate,
                                                      categoryId, subcategoryId, updatedBy);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool ResetExternalUserPassword(int? userId, int? updatedBy)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_TranscomUniversity_fnc_resetpassword(userId, updatedBy);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            //var e = ex.Message;
            retVal = false;
        }
        return retVal;
    }

    public static bool IsChangePassReq(string email)
    {
        var db = new IntranetDBDataContext();
        var list = db.pr_TranscomUniversity_rst_IsChangePassReq(email).ToList();
        return Convert.ToBoolean(list[0].ReqChangePass);
    }

    public static string ChangePassword(string email, string password, string oldpassword)
    {
        var db = new IntranetDBDataContext();
        var list = db.pr_TranscomUniversity_rst_ChangePassword(email, password, oldpassword).ToList();
        return list[0].Return;
    }

    public static bool ExternalUserExists(string email)
    {
        var db = new IntranetDBDataContext();
        var list = db.pr_TranscomUniversity_Chk_externalUser(email).ToList();
        return Convert.ToBoolean(list[0].Column1);
    }

    #endregion

    #region .MY TEAM

    public static List<pr_TranscomUniversity_Rpt_MyTeam_ByTLCimResult> GetTeam_Report(int cimNumber, bool isExport)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Rpt_MyTeam_ByTLCimResult>(db.pr_TranscomUniversity_Rpt_MyTeam_ByTLCim(cimNumber)).OrderBy(p => p.TwwId).ToList();
    }

    public static List<pr_TranscomUniversity_Lkp_GetDirectReportsResult> GetTeamProfiles(int cimNumber)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_GetDirectReportsResult>(db.pr_TranscomUniversity_Lkp_GetDirectReports(cimNumber).OrderBy(p => p.TwwId)).ToList();
    }

    public static List<sp_Get_UserProfilesResult> GetUserProfile(int cimNumber)
    {
        var db = new NuSkillCheckDataContext();
        return new List<sp_Get_UserProfilesResult>(db.sp_Get_UserProfiles(cimNumber)).ToList();
    }

    public static DataSet GetTeamExtract(DataTable dt, int manager)
    {
        var cimNumbers = string.Empty;

        var query = from a in dt.AsEnumerable()
                    select new
                    {
                        CimNumber = a.Field<int?>("TWWID"),
                        FirstName = a.Field<string>("Firstname"),
                        LastName = a.Field<string>("Lastname"),
                        Supervisor = a.Field<string>("TLName") ?? "",
                        ReportingManager = a.Field<string>("ManagerName") ?? "",
                        Department = a.Field<string>("DepartmentName"),
                        Country = a.Field<string>("Country") ?? "",
                        CompanySite = a.Field<string>("CompanySite"),
                        DateHired = a.Field<string>("startdate"),
                        EmailAddress = a.Field<string>("email") ?? "",
                        MobileNo = a.Field<string>("MobileNo") ?? "",
                        Gender = a.Field<string>("gender"),
                        Birthday = a.Field<string>("birthday"),
                        YearswithTranscom = a.Field<int?>("YearsWithTranscom") ?? 0,
                        Role = a.Field<string>("Role") ?? "",
                        YearsWithRole = a.Field<int?>("YearsWithRole") ?? 0,
                        Client = a.Field<string>("ClientName") ?? "",
                        Campaign = a.Field<string>("CampaignName") ?? "",
                        Language = a.Field<string>("LanguageName") ?? "",
                        EducationalAttainment = a.Field<string>("EducationalLevel") ?? "",
                        Major = a.Field<string>("CourseMajor") ?? "",
                        TrainingsSeminars = a.Field<string>("additionalcourses_trainings") ?? "",
                        CourseInterests = a.Field<string>("CourseInterests") ?? "",
                        TypeOfSupport = a.Field<string>("supporttype_name") ?? "",
                        CareerPath = a.Field<string>("careerpath") ?? "",
                        specialization = a.Field<string>("specialization_skills") ?? "",
                        UpdateVia = a.Field<string>("updatevia_name") ?? ""
                    };


        foreach (var rowObj in query)
        {
            cimNumbers += rowObj.CimNumber + ",";
        }

        cimNumbers = cimNumbers.Substring(0, cimNumbers.Length - 1);

        var db = new DbHelper("NuSkillCheck");
        var retds = db.GetCoursesEmployee(cimNumbers);

        var query2 = DataHelper.GetAssignedEnrolledCourse_ByTeam(manager).ToList();

        var dtProfile = Load(query.ToList());
        var dtLearningPlan = Load(query2);
        retds.Tables.Add(dtProfile);
        retds.Tables.Add(dtLearningPlan);

        retds.Tables[0].TableName = "Scores";
        retds.Tables[1].TableName = "Profile";
        retds.Tables[2].TableName = "Learning Plan";

        return retds;
    }

    #endregion

    #region .MY CLASS

    public static List<pr_Infinity_Lkp_ParticipantsPerClassResult> GetClassParticipant(int classId, int cimNo)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_Infinity_Lkp_ParticipantsPerClassResult>(db.pr_Infinity_Lkp_ParticipantsPerClass(classId, null, cimNo, 1)).ToList();
    }

    public static bool UpdateParticipant(char? command, int? participantId, char? deleteStat, int? classId,
                                        DateTime? dateJoin, string comment, int? cimNo, char? status, char? certStat,
                                        int? reason, int? desc, int? tandimNo, string otherDesc)
    {
        bool retVal;
        try
        {
            var db = new IntranetDBDataContext();
            db.pr_Infinity_Sav_Participant(command, participantId, ref deleteStat, classId, dateJoin, comment, cimNo, status,
                                           certStat, reason, desc, tandimNo, otherDesc);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }

        return retVal;
    }

    #endregion

    #region .CLASS NAME MAINTENANCE

    public static List<DataHelperModel.TrainingTypes> GetTrainingTypes()
    {
        var db = new IntranetDBDataContext();

        try
        {
            var clients = db.ExecuteQuery<DataHelperModel.TrainingTypes>
                (@"SELECT TOP 5 [TrainingTypeId], [TrainingType] FROM [ref_TranscomUniversity_TrainingType] WHERE ([Active] = 1) ORDER BY [TrainingTypeId]").ToList();

            return clients;
        }
        catch (Exception)
        {
            return null;
        }
    }

    public static List<DataHelperModel.TrainingTypes> GetTrainingTypes_OD()
    {
        var types = new List<DataHelperModel.TrainingTypes>
            {
                new DataHelperModel.TrainingTypes
                    {
                        TrainingTypeId = 1, 
                        TrainingType = "Agent Training", 
                        Active = true
                    },
                new DataHelperModel.TrainingTypes
                    {
                        TrainingTypeId = 2,
                        TrainingType = "Supervisor Training",
                        Active = true
                    },
                new DataHelperModel.TrainingTypes
                    {
                        TrainingTypeId = 3,
                        TrainingType = "Manager Training",
                        Active = true
                    },
                new DataHelperModel.TrainingTypes
                    {
                        TrainingTypeId = 4,
                        TrainingType = "Off Phone Employees (All)",
                        Active = true
                    }
            };

        return types;
    }

    public static List<pr_TranscomUniversity_Lkp_ClassResult> GetClassNames(int? clientId, int? trainTypeId)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_ClassResult>(db.pr_TranscomUniversity_Lkp_Class(clientId, trainTypeId)).ToList();
    }

    public static List<pr_TranscomUniversity_Lkp_ClassResult> GetClassNames_Active(int? clientId, int? trainTypeId)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_ClassResult>(db.pr_TranscomUniversity_Lkp_Class(clientId, trainTypeId)).Where(p => p.Active == true).ToList();

    }

    public static bool InsertClass(string className, int clientId, int trainTypeId, bool active)
    {
        bool retVal;

        try
        {
            var db = new IntranetDBDataContext();

            var newClass = new ref_TranscomUniversity_Class
            {
                ClassName = className,
                ClientID = clientId,
                TrainingTypeID = trainTypeId,
                Active = active
            };

            db.ref_TranscomUniversity_Classes.InsertOnSubmit(newClass);
            db.SubmitChanges();
            retVal = true;
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool UpdateClass(int id, string className, int clientId, int trainTypeId, bool active)
    {
        bool retVal;
        try
        {
            using (var db = new IntranetDBDataContext())
            {
                var res = db.ref_TranscomUniversity_Classes.First(p => p.ID == id);

                res.ID = id;
                res.ClassName = className;
                res.ClientID = clientId;
                res.TrainingTypeID = trainTypeId;
                res.Active = active;

                db.SubmitChanges();
                retVal = true;
            }
        }
        catch (Exception)
        {
            retVal = false;
        }
        return retVal;
    }

    public static bool ChkIfClassExist(string className, int clientId, int trainTypeId)
    {
        var db = new IntranetDBDataContext();
        var exist = (from a in db.ref_TranscomUniversity_Classes
                     where a.ClassName == className && a.ClientID == clientId && a.TrainingTypeID == trainTypeId
                     select a).Any();
        var retVal = exist;
        return retVal;
    }

    public static bool ChkIfClassExist(string className, int clientId, int trainTypeId, bool active)
    {
        var db = new IntranetDBDataContext();
        var exist = (from a in db.ref_TranscomUniversity_Classes
                     where a.ClassName == className && a.ClientID == clientId && a.TrainingTypeID == trainTypeId && a.Active == active
                     select a).Any();
        var retVal = exist;
        return retVal;
    }

    #endregion

    #region .AUDIENCE

    public static List<DataHelperModel.CIMList> GetMembersCimList(int audienceId)
    {
        var db = new IntranetDBDataContext { CommandTimeout = 0 };
        var query = db.ExecuteQuery<DataHelperModel.CIMList>("Exec pr_TranscomUniversity_Lkp_MemberCIMList " + audienceId);
        return query.ToList();
    }

    public static IQueryable<DataHelperModel.Audience> GetAudience(bool includeElapsed)
    {
        var db = new IntranetDBDataContext();

        if (includeElapsed)
        {
            var query = (from tbl in db.tbl_TranscomUniversity_Cor_Audiences
                         orderby tbl.DateUpdated descending
                         select new DataHelperModel.Audience
                         {
                             AudienceId = tbl.AudienceID,
                             AudienceName = tbl.AudienceName,
                             DateStart = tbl.DateStart,
                             DateEnd = tbl.DateEnd,
                             DateUpdated = tbl.DateUpdated,
                             UpdatedBy = tbl.UpdateBy
                         });
            return query;
        }
        else
        {
            var query = (from tbl in db.tbl_TranscomUniversity_Cor_Audiences
                         where (DateTime.Now <= tbl.DateEnd)
                         select new DataHelperModel.Audience
                         {
                             AudienceId = tbl.AudienceID,
                             AudienceName = tbl.AudienceName,
                             DateStart = tbl.DateStart,
                             DateEnd = tbl.DateEnd,
                             DateUpdated = tbl.DateUpdated,
                             UpdatedBy = tbl.UpdateBy
                         });
            return query;
        }
    }

    public static bool BulkAddAudience(string audienceName, DateTime dateStart, DateTime dateEnd, int addedBy, DataTable dt)
    {
        bool retVal;

        try
        {
            var db = new IntranetDBDataContext();

            if (db.tbl_TranscomUniversity_Cor_Audiences.Any(u => u.AudienceName == audienceName))
            {
                retVal = false;
            }
            else
            {
                var newAudience = new tbl_TranscomUniversity_Cor_Audience
                {
                    AudienceName = audienceName,
                    DateStart = dateStart,
                    DateEnd = dateEnd,
                    UpdateBy = addedBy,
                    DateUpdated = DateTime.Now
                };

                db.tbl_TranscomUniversity_Cor_Audiences.InsertOnSubmit(newAudience);
                db.SubmitChanges();

                var distinctNames = dt.AsEnumerable()
                   .Select(s => new
                   {
                       Names = s.Field<string>("AudienceMemberName"),
                   })
                   .Distinct().ToList();

                foreach (var row in distinctNames)
                {
                    var newAudienceMember = new tbl_TranscomUniversity_Rlt_AudienceMember
                    {
                        AudienceID = newAudience.AudienceID,
                        AudienceMemberName = row.Names,
                        Active = true
                    };

                    db.tbl_TranscomUniversity_Rlt_AudienceMembers.InsertOnSubmit(newAudienceMember);
                    db.SubmitChanges();

                    var results = from rowResults in dt.AsEnumerable()
                                  where rowResults.Field<string>("AudienceMemberName") == row.Names
                                  select new tbl_TranscomUniversity_Rlt_AudienceMemberFilter
                                  {
                                      AudienceMemberID = newAudienceMember.AudienceMemberID,
                                      AudienceTypeID = rowResults.Field<int>("AudienceTypeID"),
                                      Value = rowResults.Field<int>("Value"),
                                      Active = true
                                  };
                    var db2 = new MyDataContext();

                    db2.BulkInsertAll(results);
                }
                retVal = true;
            }
        }
        catch (Exception)
        {
            retVal = false;
        }

        return retVal;
    }
    public static bool UpdateAudience(int audienceId, string audienceCurrent, string audienceName, DateTime dateStart, DateTime dateEnd, int updatedBy, DataTable dtAdd, DataTable dtDelete)
    {
        bool retVal;

        try
        {
            using (var db = new IntranetDBDataContext())
            {

                if (audienceCurrent != audienceName)
                {
                    if (db.tbl_TranscomUniversity_Cor_Audiences.Any(u => u.AudienceName == audienceName))
                    {
                        return false;
                    }
                }

                var aud = db.tbl_TranscomUniversity_Cor_Audiences.First(u => u.AudienceID == audienceId);

                aud.AudienceName = audienceName;
                aud.DateStart = dateStart;
                aud.DateEnd = dateEnd;
                aud.DateUpdated = DateTime.Now;
                aud.UpdateBy = updatedBy;

                //Mass Insert
                if (dtAdd != null)
                {
                    var distinctNames = dtAdd.AsEnumerable()
                 .Select(s => new
                 {
                     Names = s.Field<string>("AudienceMemberName"),
                 })
                 .Distinct().ToList();

                    foreach (var row in distinctNames)
                    {
                        var newAudienceMember = new tbl_TranscomUniversity_Rlt_AudienceMember
                        {
                            AudienceID = audienceId,
                            AudienceMemberName = row.Names,
                            Active = true
                        };

                        db.tbl_TranscomUniversity_Rlt_AudienceMembers.InsertOnSubmit(newAudienceMember);
                        db.SubmitChanges();

                        var results = from rowResults in dtAdd.AsEnumerable()
                                      where rowResults.Field<string>("AudienceMemberName") == row.Names
                                      select new tbl_TranscomUniversity_Rlt_AudienceMemberFilter
                                      {
                                          AudienceMemberID = newAudienceMember.AudienceMemberID,
                                          AudienceTypeID = rowResults.Field<int>("AudienceTypeID"),
                                          Value = rowResults.Field<int>("Value"),
                                          Active = true
                                      };
                        var db2 = new MyDataContext();
                        db2.BulkInsertAll(results);

                    }
                }
                db.SubmitChanges();

                //Mass Delete
                var ids = string.Empty;

                if (dtDelete != null)
                {
                    foreach (DataRow row in dtDelete.Rows)
                    {
                        ids = ids + row["AudienceMemberID"] + ",";
                    }

                    ids = ids.Substring(0, ids.Length - 1);

                    var sql = "UPDATE [tbl_TranscomUniversity_Rlt_AudienceMember] SET Active = 0 WHERE AudienceMemberID in (" + ids + ")";
                    db.ExecuteCommand(sql);
                }

                retVal = true;
            }
        }
        catch (Exception)
        {
            retVal = false;
        }

        return retVal;
    }

    public static List<pr_TranscomUniversity_lkp_AudienceMembersResult> GetAudienceMembers(int audienceId)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_lkp_AudienceMembersResult>(db.pr_TranscomUniversity_lkp_AudienceMembers(audienceId)).ToList();

    }


    public static bool DeleteAudienceMember(int audienceId, int updatedBy, int audienceMemberId)
    {
        bool retVal;

        try
        {
            using (var db = new IntranetDBDataContext())
            {
                var aud = db.tbl_TranscomUniversity_Cor_Audiences.First(u => u.AudienceID == audienceId);

                aud.DateUpdated = DateTime.Now;
                aud.UpdateBy = updatedBy;

                var audMember = db.tbl_TranscomUniversity_Rlt_AudienceMembers.First(u => u.AudienceMemberID == audienceMemberId);
                audMember.Active = false;

                db.SubmitChanges();
                retVal = true;
            }
        }
        catch (Exception)
        {
            retVal = false;
        }

        return retVal;
    }

    public static pr_TranscomUniversity_Lkp_IsValidCIMResult IsValidCim(int cimNumber)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_Lkp_IsValidCIMResult>(db.pr_TranscomUniversity_Lkp_IsValidCIM(cimNumber)).FirstOrDefault();
    }

    #endregion

    #region .ASSIGN COURSE

    public static List<pr_TranscomUniversity_lkp_UnassignedCourseResult> GetUnassignedCourses(int assignmentTypeId,
                                                                                              int value, int categoryId,
                                                                                              int subcategoryId)
    {
        var db = new IntranetDBDataContext();
        return
            new List<pr_TranscomUniversity_lkp_UnassignedCourseResult>(
                db.pr_TranscomUniversity_lkp_UnassignedCourse(assignmentTypeId, value, categoryId, subcategoryId, ""))
                .ToList();
    }


    public static List<pr_TranscomUniversity_lkp_UnassignedCourseResult> GetUnassignedTests(int assignmentTypeId,
                                                                                            int value, int categoryId,
                                                                                            int subcategoryId,
                                                                                            string courseIDs)
    {
        var db = new IntranetDBDataContext();
        return
            new List<pr_TranscomUniversity_lkp_UnassignedCourseResult>(
                db.pr_TranscomUniversity_lkp_UnassignedCourse(assignmentTypeId, value, categoryId, subcategoryId,
                                                              courseIDs));
    }

    public static List<pr_TranscomUniversity_lkp_AssignedCourseResult> GetAssignedCourses(int assignmentTypeId, int value)
    {
        var db = new IntranetDBDataContext();
        return
            new List<pr_TranscomUniversity_lkp_AssignedCourseResult>(
                db.pr_TranscomUniversity_lkp_AssignedCourse(assignmentTypeId, value)).ToList();

    }

    public static bool BulkAddAssignCourse(int courseAssignmentTypeId, int value, int addedBy, DataTable dt)
    {
        bool retVal;

        try
        {
            if (dt.Rows.Count > 0)
            {

                var db2 = new MyDataContext();
                var results = from row in dt.AsEnumerable()
                              select new tbl_TranscomUniversity_rlt_CourseAssignment
                              {
                                  CourseID = row.Field<int>("CourseID"),
                                  CourseAssignmentTypeID = courseAssignmentTypeId,
                                  Value = value,
                                  DueDate = row.Field<DateTime>("DueDate"),
                                  UpdatedDate = DateTime.Now,
                                  UpdatedBy = addedBy,
                                  Active = true,
                                  CreateDate = DateTime.Now
                              };
                db2.BulkInsertAll(results);
            }
            retVal = true;
        }
        catch (Exception ex)
        {
            retVal = false;
        }

        return retVal;
    }

    public static bool DeleteCourseAssignment(int courseAssignmentId, int updatedBy)
    {
        bool retVal;

        try
        {
            using (var db = new IntranetDBDataContext())
            {
                var ass = db.tbl_TranscomUniversity_rlt_CourseAssignments.First(u => u.CourseAssignmentID == courseAssignmentId);

                ass.UpdatedDate = DateTime.Now;
                ass.UpdatedBy = updatedBy;
                ass.Active = false;

                db.SubmitChanges();
                retVal = true;
            }
        }
        catch (Exception)
        {
            retVal = false;
        }

        return retVal;
    }

    public static bool UpdateCourseAssignment(DateTime dueDate, int courseAssignmentId, int updatedBy)
    {
        bool retVal;

        try
        {
            using (var db = new IntranetDBDataContext())
            {
                var ass = db.tbl_TranscomUniversity_rlt_CourseAssignments.First(u => u.CourseAssignmentID == courseAssignmentId);

                ass.UpdatedDate = DateTime.Now;
                ass.UpdatedBy = updatedBy;
                ass.DueDate = dueDate;

                db.SubmitChanges();
                retVal = true;
            }
        }
        catch (Exception)
        {
            retVal = false;
        }

        return retVal;
    }

    public static List<pr_transcomuniversity_lkp_myteamcourseassignmentResult> GetMyTeamAssignedCourses(int cim, bool includeElapsed, int assignmentType)
    {
        var db = new IntranetDBDataContext();
        return
            new List<pr_transcomuniversity_lkp_myteamcourseassignmentResult>(
                db.pr_transcomuniversity_lkp_myteamcourseassignment(cim, includeElapsed, assignmentType)).ToList();
    }

    public static pr_TranscomUniversity_lkp_AssignCourseNotifCountResult GetAssignCourseNotificationCount(int cim)
    {
        var db = new IntranetDBDataContext();
        return
            new List<pr_TranscomUniversity_lkp_AssignCourseNotifCountResult>(db.pr_TranscomUniversity_lkp_AssignCourseNotifCount(cim)).FirstOrDefault();
    }

    public static bool ResetNotifCount(int cim)
    {
        var db = new IntranetDBDataContext();
        var retVal = true;
        try
        {
            db.ExecuteCommand("UPDATE tbl_TranscomUniversity_Cor_Notifications SET NotifCount = 0 WHERE CIMNumber = {0}", cim);
        }
        catch
        {
            retVal = false;
        }
        return retVal;
    }
    #endregion

    #region .PREREQUISITE

    public static List<pr_TranscomUniversity_lkp_AvailableCourseResult> GetAvailableCourses(int courseId, int categoryId, int subcategoryId)
    {
        var db = new IntranetDBDataContext();
        return
            new List<pr_TranscomUniversity_lkp_AvailableCourseResult>(
                db.pr_TranscomUniversity_lkp_AvailableCourse(courseId, categoryId, subcategoryId)).ToList();
    }

    public static List<pr_TranscomUniversity_lkp_PrerequisiteCoursesResult> GetPrerequisiteCourses(int courseId)
    {
        var db = new IntranetDBDataContext();
        return
            new List<pr_TranscomUniversity_lkp_PrerequisiteCoursesResult>(
                db.pr_TranscomUniversity_lkp_PrerequisiteCourses(courseId)).ToList();
    }


    public static List<pr_TranscomUniversity_lkp_GetPrerequisiteEncryptedResult> GetPrereqEncryptedIDs(int courseId)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_lkp_GetPrerequisiteEncryptedResult>(db.pr_TranscomUniversity_lkp_GetPrerequisiteEncrypted(courseId)).ToList();
    }

    public static pr_TranscomUniversity_lkp_PrerequisiteMetResult ArePrerequisiteMet(int cim, int courseId, string strXml)
    {
        var db = new IntranetDBDataContext();
        return new List<pr_TranscomUniversity_lkp_PrerequisiteMetResult>(db.pr_TranscomUniversity_lkp_PrerequisiteMet(cim, courseId, strXml)).Single();
    }

    public static pr_TranscomUniversity_lkp_FilterCoursesWithPrerequisiteResult FilterPrerequisiteCourses(string courseIDs)
    {
        var db = new IntranetDBDataContext();
        return
            new List<pr_TranscomUniversity_lkp_FilterCoursesWithPrerequisiteResult>(
                db.pr_TranscomUniversity_lkp_FilterCoursesWithPrerequisite(courseIDs)).Single();
    }

    #endregion

    #region .REPORTS

    public static List<pr_TranscomUniversity_Rpt_CourseLoginDetailResult> GetCourseLoginDetail(DateTime startDate, DateTime finishDate, string courseIDs)
    {
        var db = new IntranetDBDataContext { CommandTimeout = 0 };
        return new List<pr_TranscomUniversity_Rpt_CourseLoginDetailResult>(db.pr_TranscomUniversity_Rpt_CourseLoginDetail(startDate, finishDate, courseIDs)).ToList();
    }

    #endregion
}