using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for DBHelper
/// </summary>
public class DbHelper
{
    readonly Database _db;

    public DbHelper()
    {
    }

    public DbHelper(string database)
    {
        _db = DatabaseFactory.CreateDatabase(database);
    }

    #region .SCORM

    public DataSet GetCurriculumPackages(int CurriculumID, int CurriculumCourseID)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("pr_Scorm_Cor_GetCurriculumPackages");
        _db.AddInParameter(com, "CurriculumID", DbType.Int32, CurriculumID);
        _db.AddInParameter(com, "CurriculumCourseID", DbType.Int32, CurriculumCourseID);
        try
        {
            _db.LoadDataSet(com, ds, "GetCurriculumPackages");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetCurriculumPackages", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    #endregion

    #region .MY CLASS

    public DataSet GetClass(string userCim)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("pr_Infinity_Lkp_ClassDetails");

        _db.AddInParameter(com, "TrainerCIM", DbType.String, userCim);

        try
        {
            _db.LoadDataSet(com, ds, "getExternalUsersCourseHistory");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetExternalUserCourseHistory", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    public DataSet GetParticipants(int classId, string status, string userCim, int isParticipant)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("pr_Infinity_Lkp_ParticipantsPerClass");

        _db.AddInParameter(com, "ClassID", DbType.String, classId);
        _db.AddInParameter(com, "Status", DbType.String, status);
        _db.AddInParameter(com, "CIM", DbType.String, userCim);
        _db.AddInParameter(com, "isParticipant", DbType.String, isParticipant);

        try
        {
            _db.LoadDataSet(com, ds, "getExternalUsersCourseHistory");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetExternalUserCourseHistory", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    public DataSet GetAttendance(int? classId, DateTime classDate)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("pr_Infinity_Lkp_Attendance");

        _db.AddInParameter(com, "ClassID", DbType.String, classId);
        _db.AddInParameter(com, "AttendanceDate", DbType.String, classDate);

        try
        {
            _db.LoadDataSet(com, ds, "getExternalUsersCourseHistory");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetExternalUserCourseHistory", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    public DataSet GetTestScore(int classId, int testTypeId, int status)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("pr_Infinity_Lkp_TestScores1");

        _db.AddInParameter(com, "ClassID", DbType.String, classId);
        _db.AddInParameter(com, "TestTypeID", DbType.String, testTypeId);
        _db.AddInParameter(com, "Status", DbType.String, status);

        try
        {
            _db.LoadDataSet(com, ds, "getExternalUsersCourseHistory");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetExternalUserCourseHistory", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    public DataSet GetTests(int classId, string testType)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("pr_Infinity_Lkp_TestsTaken");

        _db.AddInParameter(com, "ClassID", DbType.String, classId);
        _db.AddInParameter(com, "TestType", DbType.String, testType);

        try
        {
            _db.LoadDataSet(com, ds, "getExternalUsersCourseHistory");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetExternalUserCourseHistory", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    public DataSet GetJournal(int? classId, string dateFrom)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("pr_Infinity_Cor_Journal");

        _db.AddInParameter(com, "ClassID", DbType.String, classId);
        _db.AddInParameter(com, "DateFrom", DbType.String, dateFrom);

        try
        {
            _db.LoadDataSet(com, ds, "getExternalUsersCourseHistory");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetExternalUserCourseHistory", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    public DataSet GetCert(int classId)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("pr_Infinity_Lkp_CertDetails");

        _db.AddInParameter(com, "ClassID", DbType.String, classId);

        try
        {
            _db.LoadDataSet(com, ds, "getCertDetails");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetCertDetails", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    public DataSet GetTuTestCategories()
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("pr_Campaign_SelectParents");

        try
        {
            _db.LoadDataSet(com, ds, "GetTUTestCategories");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetTUTestCategories", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    public DataSet GetTuTestSubCategories(int? campaignId)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("pr_Campaign_SelectFromParent");

        _db.AddInParameter(com, "CampaignID", DbType.Int32, campaignId);
        _db.AddInParameter(com, "IncludeNone", DbType.Byte, 1);

        try
        {
            _db.LoadDataSet(com, ds, "GetTUTestSubCategories");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetTUTestSubCategories", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    #endregion

    //public AccessLevel GetAccessLevel(int intEmployeeId)
    //{
    //    var alReturn = AccessLevel.User; // User Access Level 

    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Rst_AdminAccess");

    //    _db.AddInParameter(com, "EmployeeID", DbType.Int32, intEmployeeId);

    //    try
    //    {
    //        IDataReader dr = _db.ExecuteReader(com);
    //        if (!dr.IsClosed)
    //        {
    //            while (dr.Read())
    //            {
    //                if (int.Parse(dr["Return"].ToString()) > 0)
    //                    alReturn = AccessLevel.Admin;
    //            }
    //            dr.Close();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("getAccessLevel", ex);
    //        alReturn = AccessLevel.User;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return alReturn;
    //}

    //public string GetWelcomeText(string accessMode)
    //{
    //    string WelcomeText;
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Lkp_WelcomeMessage");

    //    _db.AddInParameter(com, "AccessMode", DbType.String, accessMode);

    //    try
    //    {
    //        return WelcomeText = Convert.ToString(_db.ExecuteScalar(com));
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("getWelcomeText", ex);
    //        return null;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }
    //}

    //public bool SaveCategory(string strCategory, int intEmployeeId)
    //{
    //    bool blnReturn;
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Sav_Category");

    //    _db.AddInParameter(com, "Category", DbType.String, strCategory);
    //    _db.AddInParameter(com, "CreatedBy", DbType.Int32, intEmployeeId);

    //    try
    //    {
    //        var i = _db.ExecuteNonQuery(com);
    //        blnReturn = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("saveCategory", ex);
    //        blnReturn = false;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return blnReturn;
    //}

    //public DataSet GetCategory(string accessMode)
    //{
    //    var ds = new DataSet();
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Rst_Category");

    //    _db.AddInParameter(com, "AccessMode", DbType.String, accessMode);

    //    try
    //    {
    //        _db.LoadDataSet(com, ds, "Category");
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("getCategory", ex);
    //        ds = null;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return ds;
    //}

    public DataSet GetExternalUsersCourseHistory(string email)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_GetExternalUserCourseHistory");

        _db.AddInParameter(com, "Email", DbType.String, email);

        try
        {
            _db.LoadDataSet(com, ds, "getExternalUsersCourseHistory");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetExternalUserCourseHistory", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    public DataSet GetUsersInfo(string cim)
    {
        var ds = new DataSet();
        DbCommand com = _db.GetStoredProcCommand("sp_Get_User_ByTWWID");

        _db.AddInParameter(com, "Empid", DbType.String, cim);
        _db.AddInParameter(com, "IsReport", DbType.String, "Yes");

        try
        {
            _db.LoadDataSet(com, ds, "getExternalUsersCourseHistory");
        }
        catch (Exception ex)
        {
            ProcessHandledException("GetExternalUserCourseHistory", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    //public bool EditCategory(int categoryId, string category, int updatedBy)
    //{
    //    bool blnReturn;
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Upd_Category");

    //    _db.AddInParameter(com, "CategoryID", DbType.String, categoryId);
    //    _db.AddInParameter(com, "Category", DbType.String, category);
    //    _db.AddInParameter(com, "UpdatedBy", DbType.Int32, updatedBy);

    //    try
    //    {
    //        int i = _db.ExecuteNonQuery(com);
    //        blnReturn = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("editCategory", ex);
    //        blnReturn = false;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return blnReturn;
    //}

    //public bool DeleteCategory(int categoryId, int updatedBy)
    //{
    //    bool blnReturn;
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Hid_Category");

    //    _db.AddInParameter(com, "CategoryID", DbType.String, categoryId);
    //    _db.AddInParameter(com, "UpdatedBy", DbType.Int32, updatedBy);

    //    try
    //    {
    //        int i = _db.ExecuteNonQuery(com);
    //        blnReturn = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("deleteCategory", ex);
    //        blnReturn = false;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return blnReturn;
    //}

    //public bool SaveSubcategory(string subcategory, int categoryId, int employeeId)
    //{
    //    bool blnReturn;
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Sav_Subcategory");

    //    _db.AddInParameter(com, "Subcategory", DbType.String, subcategory);
    //    _db.AddInParameter(com, "CategoryID", DbType.Int32, categoryId);
    //    _db.AddInParameter(com, "CreatedBy", DbType.Int32, employeeId);

    //    try
    //    {
    //        int i = _db.ExecuteNonQuery(com);
    //        blnReturn = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("saveSubcategory", ex);
    //        blnReturn = false;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return blnReturn;
    //}

    public DataSet GetSubcategory_CoursesActive(int? categoryId, string accessMode)
    {
        var ds = new DataSet();
        var com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Rst_Subcategory");

        _db.AddInParameter(com, "CategoryID", DbType.String, categoryId);
        _db.AddInParameter(com, "AccessMode", DbType.String, accessMode);

        try
        {
            _db.LoadDataSet(com, ds, "Subcategory");
        }
        catch (Exception ex)
        {
            ProcessHandledException("getSubcategory", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    //public bool EditSubcategory(int subcategoryId, string subcategory, int categoryId, int updatedBy)
    //{
    //    bool blnReturn;
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Upd_Subcategory");

    //    _db.AddInParameter(com, "SubcategoryID", DbType.String, subcategoryId);
    //    _db.AddInParameter(com, "Subcategory", DbType.String, subcategory);
    //    _db.AddInParameter(com, "CategoryID", DbType.String, categoryId);
    //    _db.AddInParameter(com, "UpdatedBy", DbType.Int32, updatedBy);

    //    try
    //    {
    //        int i = _db.ExecuteNonQuery(com);
    //        blnReturn = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("editSubcategory", ex);
    //        blnReturn = false;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return blnReturn;
    //}

    //public bool DeleteSubcategory(int subcategoryId, int updatedBy)
    //{
    //    bool blnReturn;
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Hid_Subcategory");

    //    _db.AddInParameter(com, "SubcategoryID", DbType.String, subcategoryId);
    //    _db.AddInParameter(com, "UpdatedBy", DbType.Int32, updatedBy);

    //    try
    //    {
    //        int i = _db.ExecuteNonQuery(com);
    //        blnReturn = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("deleteSubcategory", ex);
    //        blnReturn = false;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return blnReturn;
    //}

    //public bool SaveCourse(int categoryId, int subcategoryId, 
    //    string title, string description, string encryptedCourseId, 
    //    string accessMode, string startDate, string EndDate, int EmployeeID, 
    //    string Duration, string Version, string Author, string DeptOwnership, string DateLastModified,
    //    string TrainingCost, string TargetAudience)
    //{
    //    bool blnReturn = false;
    //    DataSet ds = new DataSet();
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Sav_Course");

    //    _db.AddInParameter(com, "CategoryID", DbType.Int32, categoryId);
    //    _db.AddInParameter(com, "SubcategoryID", DbType.Int32, subcategoryId);
    //    _db.AddInParameter(com, "Title", DbType.String, title);
    //    _db.AddInParameter(com, "Description", DbType.String, description);
    //    _db.AddInParameter(com, "EncryptedCourseID", DbType.String, encryptedCourseId);
    //    _db.AddInParameter(com, "AccessMode", DbType.String, accessMode);
    //    _db.AddInParameter(com, "StartDate", DbType.String, startDate);
    //    _db.AddInParameter(com, "EndDate", DbType.String, EndDate);
    //    _db.AddInParameter(com, "PublishedBy", DbType.Int32, EmployeeID);
    //    _db.AddInParameter(com, "Duration", DbType.String, Duration);
    //    _db.AddInParameter(com, "Version", DbType.String, Version);
    //    _db.AddInParameter(com, "Author", DbType.String, Author);
    //    _db.AddInParameter(com, "DeptOwnership", DbType.String, DeptOwnership);
    //    _db.AddInParameter(com, "DateLastModified", DbType.String, DateLastModified); //TODO: GLA 02242014

    //    _db.AddInParameter(com, "TrainingCost", DbType.String, TrainingCost);
    //    _db.AddInParameter(com, "TargetAudience", DbType.String, TargetAudience);

    //    try
    //    {
    //        int i = _db.ExecuteNonQuery(com);
    //        blnReturn = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("saveCourse", ex);
    //        blnReturn = false;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return blnReturn;
    //}

    //public bool editCourse(int CourseID, int CategoryID, int SubcategoryID, 
    //    string Title, string Description, string EncryptedCourseID, string AccessMode, 
    //    string StartDate, string EndDate, int EmployeeID, int Duration, int Version,
    //    string Author, string DeptOwnership, string DateLastModified, string TrainingCost, string TargetAudience)
    //{
    //    bool blnReturn = false;
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Upd_Course");

    //    _db.AddInParameter(com, "CourseID", DbType.String, CourseID);
    //    _db.AddInParameter(com, "CategoryID", DbType.String, CategoryID);
    //    _db.AddInParameter(com, "SubcategoryID", DbType.String, SubcategoryID);
    //    _db.AddInParameter(com, "Title", DbType.String, Title);
    //    _db.AddInParameter(com, "Description", DbType.String, Description);
    //    _db.AddInParameter(com, "EncryptedCourseID", DbType.String, EncryptedCourseID);
    //    _db.AddInParameter(com, "AccessMode", DbType.String, AccessMode);
    //    _db.AddInParameter(com, "StartDate", DbType.String, StartDate);
    //    _db.AddInParameter(com, "EndDate", DbType.String, EndDate);
    //    _db.AddInParameter(com, "UpdatedBy", DbType.String, EmployeeID);
    //    _db.AddInParameter(com, "Duration", DbType.String, Duration);
    //    _db.AddInParameter(com, "Version", DbType.String, Version);
    //    _db.AddInParameter(com, "Author", DbType.String, Author);
    //    _db.AddInParameter(com, "DeptOwnership", DbType.String, DeptOwnership);

    //    _db.AddInParameter(com, "TrainingCost", DbType.String, TrainingCost);
    //    _db.AddInParameter(com, "TargetAudience", DbType.String, TargetAudience);

    //    _db.AddInParameter(com, "DateLastModified", DbType.String, DateLastModified); //TODO: GLA 02242014
    //    try
    //    {
    //        int i = _db.ExecuteNonQuery(com);
    //        blnReturn = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("editCourse", ex);
    //        blnReturn = false;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return blnReturn;
    //}

    //public bool deleteCourse(int CourseID, int UpdatedBy)
    //{
    //    bool blnReturn = false;
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Hid_Course");

    //    _db.AddInParameter(com, "CourseID", DbType.String, CourseID);
    //    _db.AddInParameter(com, "UpdatedBy", DbType.Int32, UpdatedBy);

    //    try
    //    {
    //        int i = _db.ExecuteNonQuery(com);
    //        blnReturn = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("deleteCourse", ex);
    //        blnReturn = false;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return blnReturn;
    //}

    //public DataSet getCourses(int CategoryID, int SubcategoryID, string AccessMode)
    //{
    //    DataSet ds = new DataSet();
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Lkp_Course");

    //    _db.AddInParameter(com, "CategoryID", DbType.String, CategoryID);
    //    _db.AddInParameter(com, "SubcategoryID", DbType.String, SubcategoryID);
    //    _db.AddInParameter(com, "AccessMode", DbType.String, AccessMode);

    //    try
    //    {
    //        _db.LoadDataSet(com, ds, "Courses");
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("Courses", ex);
    //        ds = null;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return ds;
    //}

    //public DataSet DoSearch(string searchVal, string AccessMode)
    //{
    //    DataSet ds = new DataSet();
    //    DbCommand com = _db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Rlt_Search");

    //    _db.AddInParameter(com, "Value", DbType.String, searchVal);
    //    _db.AddInParameter(com, "AccessMode", DbType.String, AccessMode);

    //    try
    //    {
    //        _db.LoadDataSet(com, ds, "Courses");
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("Courses", ex);
    //        ds = null;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return ds;
    //}

    private void ProcessHandledException(string sender, Exception ex)
    {
        if (!sender.Equals("ProcessHandledException"))
        {
            Database db = DatabaseFactory.CreateDatabase("Intranet");
            DbCommand com = db.GetStoredProcCommand("dbo.pr_TranscomUniversity_Sav_Exception");

            db.AddInParameter(com, "Message", DbType.String, ex.Message);
            db.AddInParameter(com, "Source", DbType.String, ex.Source);
            db.AddInParameter(com, "Object", DbType.String, sender);

            try
            {
                int i = db.ExecuteNonQuery(com);
            }
            catch (Exception procEx)
            {
                ProcessHandledException("ProcessHandledException", procEx);
            }
            finally
            {
                com.Connection.Close();
            }
        }
    }

    //public DataSet getCoursesEmployee(int userID)
    //{
    //    DataSet ds = new DataSet();
    //    DbCommand com = db.GetStoredProcCommand(@"pr_transcomuniversity_lkp_EmployeeCourseHistory");
    //    db.AddInParameter(com, "UserID", DbType.String, userID.ToString());

    //    try
    //    {
    //        db.LoadDataSet(com, ds, "CoursesEmployee");
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("Courses", ex);
    //        ds = null;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return ds;
    //}

    //    public bool IsBMRole(int EmployeeID)
    //    {
    //        bool returnValue = false;

    //        DataSet ds = new DataSet();
    //        DbCommand com = _db.GetSqlStringCommand(@"
    //            select r.Role from [susl3psqldb02].cimenterprise.dbo.tbl_personnel_cor_employee e
    //            inner join [susl3psqldb02].cimenterprise.dbo.tbl_Personnel_Cor_Role r on e.RoleID = r.RoleID
    //            where e.EmployeeID = @EmployeeID and r.Role like '%BM%'
    //        ");

    //        _db.AddInParameter(com, "EmployeeID", DbType.Int32, EmployeeID);

    //        try
    //        {
    //            if (Convert.ToString(_db.ExecuteScalar(com)).Contains("BM"))
    //                returnValue = true;
    //        }
    //        catch (Exception ex)
    //        {
    //            ProcessHandledException("IsBMRole", ex);
    //            returnValue = false;
    //        }
    //        finally
    //        {
    //            com.Connection.Close();
    //        }

    //        return returnValue;
    //    }

    //    public bool IsHRMRole(int EmployeeID)
    //    {
    //        bool returnValue = false;

    //        DataSet ds = new DataSet();
    //        DbCommand com = _db.GetSqlStringCommand(@"
    //            select r.Role from [susl3psqldb02].cimenterprise.dbo.tbl_personnel_cor_employee e
    //            inner join [susl3psqldb02].cimenterprise.dbo.tbl_Personnel_Cor_Role r on e.RoleID = r.RoleID
    //            where e.EmployeeID = @EmployeeID and r.Role like '%HRM%'
    //        ");

    //        _db.AddInParameter(com, "EmployeeID", DbType.Int32, EmployeeID);

    //        try
    //        {
    //            if (Convert.ToString(_db.ExecuteScalar(com)).Contains("HRM"))
    //                returnValue = true;
    //        }
    //        catch (Exception ex)
    //        {
    //            ProcessHandledException("IsHRMRole", ex);
    //            returnValue = false;
    //        }
    //        finally
    //        {
    //            com.Connection.Close();
    //        }

    //        return returnValue;
    //    }

    public bool IsTrainer(int employeeId)
    {
        var returnValue = false;

        var ds = new DataSet();
        DbCommand com = _db.GetSqlStringCommand(@"
        Select count(*) AS intCount from [susl3psqldb02].cimenterprise.dbo.tbl_personnel_cor_employee e
            inner join [susl3psqldb02].cimenterprise.dbo.tbl_Personnel_Cor_Role r on e.RoleID = r.RoleID
            where e.CimNumber = @EmployeeID and (r.Role like '%train%' OR r.Role LIKE '%TQM%')
        ");

        _db.AddInParameter(com, "EmployeeID", DbType.Int32, employeeId);

        try
        {
            if (Convert.ToInt32(_db.ExecuteScalar(com)).Equals(1))
                returnValue = true;
        }
        catch (Exception ex)
        {
            ProcessHandledException("IsTrainer", ex);
            returnValue = false;
        }
        finally
        {
            com.Connection.Close();
        }

        return returnValue;

    }

    public DataSet GetSupHeirarchy(int employeeId)
    {
        var ds = new DataSet();
        var com = _db.GetSqlStringCommand(@"
            WITH Members AS
            (
	            --Anchor Member Definition
	            select
		            emp.EmployeeID,
		            empSup.EmployeeID as SupEmployeeID
	            from [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee emp
	            inner join [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employeecompany ec
		            on emp.EmployeeID = ec.EmployeeID and ec.EndDate >= getdate()
	            left outer join [susl3psqldb02].CIMEnterprise.dbo.tbl_hierarchy_rlt_employee he1
		            on emp.EmployeeID = he1.EmployeeID and he1.EndDate >= getdate()
	            left outer join [susl3psqldb02].CIMEnterprise.dbo.tbl_hierarchy_rlt_employee he2
		            on he1.ParentHierarchyID = he2.HierarchyID
	            left outer join [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee empSup
		            on empSup.EmployeeID = he2.EmployeeID 
	            where empSup.CIMNumber = @SupCIMNumber
	
	            union all
	
	            --Recursive Member Definition
	            select
		            emp.EmployeeID,
		            empSup.EmployeeID as SupEmployeeID
	            from [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee emp
	            inner join [susl3psqldb02].CIMEnterprise.dbo.tbl_hierarchy_rlt_employee he1
		            on emp.EmployeeID = he1.EmployeeID and he1.EndDate >= getdate()
	            inner join [susl3psqldb02].CIMEnterprise.dbo.tbl_hierarchy_rlt_employee he2
		            on he1.ParentHierarchyID = he2.HierarchyID
	            inner join [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee empSup
		            on empSup.EmployeeID = he2.EmployeeID
	            inner join Members d
		            on empSup.EmployeeID = d.EmployeeID
            )
            SELECT
	            emp.CIMNumber,
	            emp.LastName + ', ' + emp.FirstName as EmployeeName,
	            empSup.CIMNumber as SupCIMNumber,
	            empSup.LastName + ', ' + empSup.FirstName as ReportsTo
            FROM Members m
            inner join [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee emp
	            on m.EmployeeID = emp.EmployeeID
            inner join [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee empSup
	            on m.SupEmployeeID = empSup.EmployeeID
        ");

        _db.AddInParameter(com, "SupCIMNumber", DbType.Int32, employeeId);

        try
        {
            _db.LoadDataSet(com, ds, "SupervisorHeirarchy");
        }
        catch (Exception ex)
        {
            ProcessHandledException("SupervisorHeirarchy", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    #region toBeSP'ed
    //public bool isAgent(int CIM)
    //{
    //    bool blnReturn = false;
    //    DbCommand com = db.GetSqlStringCommand(@"DECLARE @CIM INT SET @CIM = " + CIM + " DECLARE @RoleGroupID INT SELECT @RoleGroupID = RG.RoleGroupID FROM CIMEnterprise.dbo.tbl_personnel_cor_employee E INNER JOIN CIMEnterprise.dbo.tbl_personnel_cor_role R ON E.RoleID = r.RoleID INNER JOIN CIMEnterprise.dbo.tbl_Personnel_Lkp_RoleGroup RG ON R.RoleGroupID = RG.RoleGroupID WHERE CIMNUMBER = @CIM IF(@RoleGroupID = 2) BEGIN SELECT 1 END ELSE BEGIN SELECT 0 END");

    //    //db.AddInParameter(com, "CIM", DbType.String, CIM);

    //    try
    //    {
    //        int i = Convert.ToInt32(db.ExecuteScalar(com));
    //        if (i == 1)
    //            blnReturn = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("deleteSubcategory", ex);
    //        blnReturn = false;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return blnReturn;
    //}

    public DataSet GetCoursesEmployee(string userId)
    {
        var ds = new DataSet();
        //New Code For Multiple Cim Query Tandim #2566672
        var str = userId.Split(","[0]);
        var paramValues = new string[str.Length];
        var _paramNames = new string[str.Length];

        for (Int16 i = 0; i < str.Length; i++)
        {
            _paramNames[i] = "@UserID" + (i + 1);
            paramValues[i] = str[i];
        }

        var paramNames = String.Join(",", _paramNames);
        var query = string.Format(@"
           SELECT *
            INTO #TestTakenTemp
            FROM susl3psqldb05.nuskillcheck.dbo.tbl_testing_testtaken TT
            WHERE TT.UserID IN ({0})

            SELECT E.CIMNumber,
	            CL.*
            INTO #CourseLogin
            FROM intranet.dbo.tbl_NuCommUniversity_Cor_EmployeeCourseLogin CL
            INNER JOIN [susl3psqldb02].CIMEnterprise.dbo.tbl_Personnel_Cor_Employee E
	            ON CL.EmployeeID = E.EmployeeID
            WHERE E.CIMNumber IN ({0})

            SELECT C.CourseID,
	            CL.CIMNumber,
	            C.CourseName,
	            MAX(LoginTime) AS DateCourseTaken,
	            CAST(NULL AS DATETIME) AS DateTestTaken,
	            CAST(NULL AS VARCHAR) Score
            INTO #HoldTable
            FROM #CourseLogin CL
            INNER JOIN [susl3psqldb02].intranet.dbo.tbl_NuCommUniversity_Lkp_Course C
	            ON CL.CourseID = C.CourseID
            GROUP BY C.CourseID,
	            CL.CIMNumber,
	            C.CourseName

            DELETE
            FROM #HoldTable
            WHERE CourseID IN (
		            SELECT C.CourseID
		            FROM susl3psqldb05.nuskillcheck.dbo.tbl_testing_testcategory TC
		            LEFT JOIN [susl3psqldb02].intranet.dbo.tbl_NuCommUniversity_Lkp_Course C
			            ON TC.CourseID = C.CourseID
		            )

              SELECT DISTINCT TC.CourseID,
	            TT.UserID AS CIMNumber,
	            C.CourseName,
	            MAX(CL.LoginTime) AS DateCourseTaken,
	            MAX(TT.DateStartTaken) AS DateTestTaken,
				TC.NumberofQuestions,
				COUNT(Q.QuestionnaireID) OVER (PARTITION BY TC.TestCategoryID) 'QNumberofQuestions',
				TT.Score,
	            --CASE 
		           -- WHEN NumberofQuestions = 0
			          --  THEN CONVERT(VARCHAR, CAST(ISNULL(Score, 0) AS FLOAT))
		           -- ELSE CAST(CONVERT(DECIMAL(10), (CAST(ISNULL(Score, 0) AS FLOAT) / ISNULL(CAST(NumberofQuestions AS FLOAT), 1) * 100)) AS VARCHAR)
		           -- END + ' %' AS Score,
	            TC.TestCategoryID AS TestCategoryID,
	            TT.Passed
            INTO #TestTakenTemp2
            FROM #TestTakenTemp TT
            LEFT JOIN susl3psqldb05.nuskillcheck.dbo.tbl_testing_testcategory TC
	            ON TT.TestCategoryID = TC.TestCategoryID
            LEFT JOIN [susl3psqldb02].intranet.dbo.tbl_NuCommUniversity_Lkp_Course C
	            ON TC.CourseID = C.CourseID
            LEFT JOIN #CourseLogin CL
	            ON TC.CourseID = CL.CourseID
			LEFT JOIN susl3psqldb05.nuskillcheck.[dbo].[tbl_testing_questionnaire] Q
				ON Q.TestCategoryID = TC.TestCategoryID AND Q.HideFromList= 0
            GROUP BY TC.CourseID,
	            TT.UserID,
	            C.CourseID,
	            C.CourseName,
	            Score,
	            NumberofQuestions,
	            TC.TestCategoryID,
	            TT.Passed,
				Q.QuestionnaireID


			SELECT
				CourseID,
	            CIMNumber,
	            CourseName,
	            DateCourseTaken,
	            DateTestTaken,
				CASE 
		            WHEN NumberofQuestions = 0
						THEN	CASE
									WHEN QNumberofQuestions = 0 THEN CONVERT(VARCHAR, CAST(ISNULL(Score, 0) AS FLOAT))
									ELSE CAST(CONVERT(DECIMAL(10), (CAST(ISNULL(Score, 0) AS FLOAT) / ISNULL(CAST(QNumberofQuestions AS FLOAT), 1) * 100)) AS VARCHAR)
								END
		            ELSE CAST(CONVERT(DECIMAL(10), (CAST(ISNULL(Score, 0) AS FLOAT) / ISNULL(CAST(NumberofQuestions AS FLOAT), 1) * 100)) AS VARCHAR)
		        END + ' %' AS Score,
				TestCategoryID,
	            Passed
			INTO #TestTaken
			FROM
				#TestTakenTemp2

		--SCORM with Elearning,Assesment and Survey
		SELECT DISTINCT tuc.CourseID,
	            UI1.[Key] AS CimNumber,
	            --'<strong>' + c.Course + '</strong> - ' +  tuc.Title AS CourseName,
				C.Course 'Bundle',
				tuc.Title 'Curriculum',
	            ai1.StartedTimestamp AS DateCourseTaken,
	            ai2.StartedTimestamp AS DateTestTaken,
	            CASE 
		            WHEN aai2.AttemptID IS NOT NULL
			            THEN CONVERT(VARCHAR(10), [dbo].[fnc_GetAssessmentScore](aai2.AttemptID)) + ' %'
		            ELSE NULL
		            END AS Score,
	            0 AS TestCategoryID,
	            1 AS Passed,
				CASE WHEN PI1.PackageFormat = 1
				THEN
					[dbo].[fnc_GetScormV1dot2SCompletionStatus](AI1.ID)
				ELSE
					ISNULL(AI1.CompletionStatus, 0) 
			END AS 'ElearningCompletionStatus',
				CASE WHEN PI2.PackageFormat = 1
				THEN
					[dbo].[fnc_GetScormV1dot2SCompletionStatus](AI2.ID)
				ELSE
					ISNULL(AI2.CompletionStatus, 0) 
			END AS 'AssessmentCompletionStatus',
					CASE WHEN PI3.PackageFormat = 1
				THEN
					[dbo].[fnc_GetScormV1dot2SCompletionStatus](AI3.ID)
				ELSE
					ISNULL(AI3.CompletionStatus, 0) 
			END AS 'SurveyCompletionStatus'
			INTO #SCORM
				FROM dbo.tbl_TranscomUniversity_Cor_Course tuc
				INNER JOIN tbl_scorm_cor_Course c
					ON tuc.CourseID = c.CurriculumID
				INNER JOIN tbl_Scorm_Lkp_CourseType ct
					ON ct.coursetypeid = tuc.coursetypeid
						AND tuc.coursetypeID = 2 -- Scorm
				INNER JOIN tbl_Scorm_Rlt_CurriculumCourse cc1
					ON CC1.curriculumcourseid = c.curriculumcourseid
						AND cc1.ScoTypeID = 1 -- eLearning
				INNER JOIN tbl_scorm_cor_Sco S1
					ON S1.scoid = cc1.scoid
				INNER JOIN [DotNetSCORMV2].[dbo].[PackageItem] [PI1]
					ON [PI1].[ID] = S1.ScoPackageID
				INNER JOIN [DotNetSCORMV2].[dbo].[AttemptItem] [ai1]
					ON [ai1].PackageID = [PI1].[ID]
				INNER JOIN [DotNetSCORMV2].dbo.ActivityAttemptItem aai1
					ON aai1.AttemptID = ai1.Id
				INNER JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api11
					ON api11.id = aai1.ActivityPackageID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api12
					ON api11.PackageID = api12.PackageID
						AND api12.ID = api11.ParentActivityID
				INNER JOIN [DotNetSCORMV2].[dbo].[UserItem] [UI1]
					ON [ai1].[LearnerID] = [UI1].[ID]
				LEFT JOIN tbl_Scorm_Rlt_CurriculumCourse cc2
					ON CC2.curriculumcourseid = c.curriculumcourseid
						AND cc2.ScoTypeID = 2 -- Assessment
				LEFT JOIN tbl_scorm_cor_Sco S2
					ON S2.scoid = cc2.scoid
				LEFT JOIN [DotNetSCORMV2].[dbo].[PackageItem] [PI2]
					ON [PI2].[ID] = S2.ScoPackageID
				LEFT JOIN [DotNetSCORMV2].[dbo].[AttemptItem] [ai2]
					ON [ai2].PackageID = [PI2].[ID]
						AND ai2.LearnerID = UI1.ID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityAttemptItem aai2
					ON aai2.AttemptID = ai2.Id
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api21
					ON api21.id = aai2.ActivityPackageID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api22
					ON api21.PackageID = api22.PackageID
						AND api22.ID = api21.ParentActivityID
				LEFT JOIN tbl_Scorm_Rlt_CurriculumCourse cc3
					ON CC3.curriculumcourseid = c.curriculumcourseid
						AND cc3.ScoTypeID = 4 -- Survey
				LEFT JOIN tbl_scorm_cor_Sco S3
					ON S3.scoid = cc3.scoid
				LEFT JOIN [DotNetSCORMV2].[dbo].[PackageItem] [PI3]
					ON [PI3].[ID] = S3.ScoPackageID
				LEFT JOIN [DotNetSCORMV2].[dbo].[AttemptItem] [ai3]
					ON [ai3].PackageID = [PI3].[ID]
						AND ai3.LearnerID = UI1.ID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityAttemptItem aai3
					ON aai3.AttemptID = ai3.Id
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api31
					ON api31.id = aai3.ActivityPackageID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api32
					ON api31.PackageID = api32.PackageID
						AND api32.ID = api31.ParentActivityID
				WHERE [UI1].[Key] IN ({0}) -- eLearning, Quiz, Survey
					AND (
						CAST(CAST(aai1.SequencingDataCache AS XML).query('data(/item/@completionStatus)') AS VARCHAR(20)) = 'Completed'
						OR dbo.fnc_GetLessonStatus(ai1.ID) = 'Completed'
						)
					AND (
						PI2.ID IS NOT NULL
						AND (
							CAST(CAST(aai2.SequencingDataCache AS XML).query('data(/item/@completionStatus)') AS VARCHAR(20)) = 'Completed'
							OR dbo.fnc_GetLessonStatus(ai2.ID) = 'Completed'
							OR dbo.fnc_GetLessonStatus(ai2.ID) = 'Passed'
							OR dbo.fnc_GetLessonStatus(ai2.ID) = 'Failed'
							)
						)
					AND (
						PI3.ID IS NOT NULL
						AND (
							CAST(CAST(aai3.SequencingDataCache AS XML).query('data(/item/@completionStatus)') AS VARCHAR(20)) = 'Completed'
							OR dbo.fnc_GetLessonStatus(ai3.ID) = 'Completed'
							OR dbo.fnc_GetLessonStatus(ai3.ID) = 'Passed'
							OR dbo.fnc_GetLessonStatus(ai3.ID) = 'Failed'
							)
						)

				UNION
		--SCORM with Elearning and Assesment
				SELECT DISTINCT tuc.CourseID,
					UI1.[Key] AS CimNumber,
					--'<strong>' + c.Course + '</strong> - ' +  tuc.Title AS CourseName,
					C.Course 'Bundle',
					tuc.Title 'Curriculum',
					ai1.StartedTimestamp AS DateCourseTaken,
					ai2.StartedTimestamp AS DateTestTaken,
					CASE 
						WHEN aai2.AttemptID IS NOT NULL
							THEN CONVERT(VARCHAR(10), [dbo].[fnc_GetAssessmentScore](aai2.AttemptID)) + ' %'
						ELSE NULL
						END AS Score,
					0 AS TestCategoryID,
					1 AS Passed,
					CASE WHEN PI1.PackageFormat = 1
				THEN
					[dbo].[fnc_GetScormV1dot2SCompletionStatus](AI1.ID)
				ELSE
					ISNULL(AI1.CompletionStatus, 0) 
			END AS 'ElearningCompletionStatus',
				CASE WHEN PI2.PackageFormat = 1
				THEN
					[dbo].[fnc_GetScormV1dot2SCompletionStatus](AI2.ID)
				ELSE
					ISNULL(AI2.CompletionStatus, 0) 
			END AS 'AssessmentCompletionStatus',
					1 'SurveyCompletionStatus'
				FROM dbo.tbl_TranscomUniversity_Cor_Course tuc
				INNER JOIN tbl_scorm_cor_Course c
					ON tuc.CourseID = c.CurriculumID
				INNER JOIN tbl_Scorm_Lkp_CourseType ct
					ON ct.coursetypeid = tuc.coursetypeid
						AND tuc.coursetypeID = 2 -- Scorm
				INNER JOIN tbl_Scorm_Rlt_CurriculumCourse cc1
					ON CC1.curriculumcourseid = c.curriculumcourseid
						AND cc1.ScoTypeID = 1 -- eLearning
				INNER JOIN tbl_scorm_cor_Sco S1
					ON S1.scoid = cc1.scoid
				INNER JOIN [DotNetSCORMV2].[dbo].[PackageItem] [PI1]
					ON [PI1].[ID] = S1.ScoPackageID
				INNER JOIN [DotNetSCORMV2].[dbo].[AttemptItem] [ai1]
					ON [ai1].PackageID = [PI1].[ID]
				INNER JOIN [DotNetSCORMV2].dbo.ActivityAttemptItem aai1
					ON aai1.AttemptID = ai1.Id
				INNER JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api11
					ON api11.id = aai1.ActivityPackageID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api12
					ON api11.PackageID = api12.PackageID
						AND api12.ID = api11.ParentActivityID
				INNER JOIN [DotNetSCORMV2].[dbo].[UserItem] [UI1]
					ON [ai1].[LearnerID] = [UI1].[ID]
				LEFT JOIN tbl_Scorm_Rlt_CurriculumCourse cc2
					ON CC2.curriculumcourseid = c.curriculumcourseid
						AND cc2.ScoTypeID = 2 -- Assessment
				LEFT JOIN tbl_scorm_cor_Sco S2
					ON S2.scoid = cc2.scoid
				LEFT JOIN [DotNetSCORMV2].[dbo].[PackageItem] [PI2]
					ON [PI2].[ID] = S2.ScoPackageID
				LEFT JOIN [DotNetSCORMV2].[dbo].[AttemptItem] [ai2]
					ON [ai2].PackageID = [PI2].[ID]
						AND ai2.LearnerID = UI1.ID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityAttemptItem aai2
					ON aai2.AttemptID = ai2.Id
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api21
					ON api21.id = aai2.ActivityPackageID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api22
					ON api21.PackageID = api22.PackageID
						AND api22.ID = api21.ParentActivityID
				LEFT JOIN tbl_Scorm_Rlt_CurriculumCourse cc3
					ON CC3.curriculumcourseid = c.curriculumcourseid
						AND cc3.ScoTypeID = 4 -- Survey
				LEFT JOIN tbl_scorm_cor_Sco S3
					ON S3.scoid = cc3.scoid
				LEFT JOIN [DotNetSCORMV2].[dbo].[PackageItem] [PI3]
					ON [PI3].[ID] = S3.ScoPackageID
				LEFT JOIN [DotNetSCORMV2].[dbo].[AttemptItem] [ai3]
					ON [ai3].PackageID = [PI3].[ID]
						AND ai3.LearnerID = UI1.ID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityAttemptItem aai3
					ON aai3.AttemptID = ai3.Id
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api31
					ON api31.id = aai3.ActivityPackageID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api32
					ON api31.PackageID = api32.PackageID
						AND api32.ID = api31.ParentActivityID
				WHERE [UI1].[Key] IN ({0})
					AND (
						CAST(CAST(aai1.SequencingDataCache AS XML).query('data(/item/@completionStatus)') AS VARCHAR(20)) = 'Completed'
						OR dbo.fnc_GetLessonStatus(ai1.ID) = 'Completed'
						)
					AND (
						PI2.ID IS NOT NULL
						AND (
							CAST(CAST(aai2.SequencingDataCache AS XML).query('data(/item/@completionStatus)') AS VARCHAR(20)) = 'Completed'
							OR dbo.fnc_GetLessonStatus(ai2.ID) = 'Completed'
							OR dbo.fnc_GetLessonStatus(ai2.ID) = 'Passed'
							OR dbo.fnc_GetLessonStatus(ai2.ID) = 'Failed'
							)
						)
					AND (PI3.ID IS NULL)

				UNION
		--SCORM with Elearning and Survey
				SELECT DISTINCT tuc.CourseID,
					UI1.[Key] AS CimNumber,
					--'<strong>' + c.Course + '</strong> - ' +  tuc.Title AS CourseName,
					C.Course 'Bundle',
					tuc.Title 'Curriculum',
					ai1.StartedTimestamp AS DateCourseTaken,
					ai2.StartedTimestamp AS DateTestTaken,
					CASE 
						WHEN aai2.AttemptID IS NOT NULL
							THEN CONVERT(VARCHAR(10), [dbo].[fnc_GetAssessmentScore](aai2.AttemptID)) + ' %'
						ELSE NULL
						END AS Score,
					0 AS TestCategoryID,
					1 AS Passed,
					CASE WHEN PI1.PackageFormat = 1
							THEN
								[dbo].[fnc_GetScormV1dot2SCompletionStatus](AI1.ID)
							ELSE
								ISNULL(AI1.CompletionStatus, 0) 
						END AS 'ElearningCompletionStatus',
					1  'AssessmentCompletionStatus',
					CASE WHEN PI3.PackageFormat = 1
							THEN
								[dbo].[fnc_GetScormV1dot2SCompletionStatus](AI3.ID)
					ELSE
						ISNULL(AI3.CompletionStatus, 0) 
						END AS 'SurveyCompletionStatus'
				FROM dbo.tbl_TranscomUniversity_Cor_Course tuc
				INNER JOIN tbl_scorm_cor_Course c
					ON tuc.CourseID = c.CurriculumID
				INNER JOIN tbl_Scorm_Lkp_CourseType ct
					ON ct.coursetypeid = tuc.coursetypeid
						AND tuc.coursetypeID = 2 -- Scorm
				INNER JOIN tbl_Scorm_Rlt_CurriculumCourse cc1
					ON CC1.curriculumcourseid = c.curriculumcourseid
						AND cc1.ScoTypeID = 1 -- eLearning
				INNER JOIN tbl_scorm_cor_Sco S1
					ON S1.scoid = cc1.scoid
				INNER JOIN [DotNetSCORMV2].[dbo].[PackageItem] [PI1]
					ON [PI1].[ID] = S1.ScoPackageID
				INNER JOIN [DotNetSCORMV2].[dbo].[AttemptItem] [ai1]
					ON [ai1].PackageID = [PI1].[ID]
				INNER JOIN [DotNetSCORMV2].dbo.ActivityAttemptItem aai1
					ON aai1.AttemptID = ai1.Id
				INNER JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api11
					ON api11.id = aai1.ActivityPackageID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api12
					ON api11.PackageID = api12.PackageID
						AND api12.ID = api11.ParentActivityID
				INNER JOIN [DotNetSCORMV2].[dbo].[UserItem] [UI1]
					ON [ai1].[LearnerID] = [UI1].[ID]
				LEFT JOIN tbl_Scorm_Rlt_CurriculumCourse cc2
					ON CC2.curriculumcourseid = c.curriculumcourseid
						AND cc2.ScoTypeID = 2 -- Assessment
				LEFT JOIN tbl_scorm_cor_Sco S2
					ON S2.scoid = cc2.scoid
				LEFT JOIN [DotNetSCORMV2].[dbo].[PackageItem] [PI2]
					ON [PI2].[ID] = S2.ScoPackageID
				LEFT JOIN [DotNetSCORMV2].[dbo].[AttemptItem] [ai2]
					ON [ai2].PackageID = [PI2].[ID]
						AND ai2.LearnerID = UI1.ID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityAttemptItem aai2
					ON aai2.AttemptID = ai2.Id
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api21
					ON api21.id = aai2.ActivityPackageID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api22
					ON api21.PackageID = api22.PackageID
						AND api22.ID = api21.ParentActivityID
				LEFT JOIN tbl_Scorm_Rlt_CurriculumCourse cc3
					ON CC3.curriculumcourseid = c.curriculumcourseid
						AND cc3.ScoTypeID = 4 -- Survey
				LEFT JOIN tbl_scorm_cor_Sco S3
					ON S3.scoid = cc3.scoid
				LEFT JOIN [DotNetSCORMV2].[dbo].[PackageItem] [PI3]
					ON [PI3].[ID] = S3.ScoPackageID
				LEFT JOIN [DotNetSCORMV2].[dbo].[AttemptItem] [ai3]
					ON [ai3].PackageID = [PI3].[ID]
						AND ai3.LearnerID = UI1.ID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityAttemptItem aai3
					ON aai3.AttemptID = ai3.Id
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api31
					ON api31.id = aai3.ActivityPackageID
				LEFT JOIN [DotNetSCORMV2].dbo.ActivityPackageItem api32
					ON api31.PackageID = api32.PackageID
						AND api32.ID = api31.ParentActivityID
				WHERE [UI1].[Key] IN ({0})
					AND (
						CAST(CAST(aai1.SequencingDataCache AS XML).query('data(/item/@completionStatus)') AS VARCHAR(20)) = 'Completed'
						OR dbo.fnc_GetLessonStatus(ai1.ID) = 'Completed'
						)
					AND (PI2.ID IS NULL)
					AND (
						PI3.ID IS NOT NULL
						AND (
							CAST(CAST(aai3.SequencingDataCache AS XML).query('data(/item/@completionStatus)') AS VARCHAR(20)) = 'Completed'
							OR dbo.fnc_GetLessonStatus(ai3.ID) = 'Completed'
							OR dbo.fnc_GetLessonStatus(ai3.ID) = 'Passed'
							OR dbo.fnc_GetLessonStatus(ai3.ID) = 'Failed'
							)
						)
			SELECT
				DISTINCT TUC.courseID,
				COUNT(c.CURRICULUMCOURSEID) OVER(PARTITION BY TUC.courseID) 'BundleCount'
				INTO #BUNDLECOUNT
			FROM dbo.tbl_TranscomUniversity_Cor_Course tuc
				INNER JOIN tbl_scorm_cor_Course c
				ON tuc.CourseID = c.CurriculumID
				INNER JOIN #SCORM ST
				ON ST.CourseID = TUC.CourseID

				--SELECT * FROM #SCORM
			INSERT INTO #TestTaken
			SELECT
				ST.CourseID,
				ST.CimNumber,
				CASE
					WHEN BC.BundleCount > 1 THEN '<strong>' + ST.Bundle + '</strong> - ' +  ST.Curriculum
					ELSE ST.Curriculum
				END 'CourseName',
				ST.DateCourseTaken,
				ST.DateTestTaken,
				ST. Score,
				ST.TestCategoryID,
				ST.Passed
				
			FROM
				#SCORM ST
				INNER JOIN #BUNDLECOUNT BC
					ON ST.CourseID = BC.courseID
			WHERE
			--For Completion Validation - Fix 08052015
				ElearningCompletionStatus = 1
				AND AssessmentCompletionStatus = 1
				AND SurveyCompletionStatus = 1

            SELECT CourseID,
	            CIMNumber,
	            CourseName,
	            MAX(DateCourseTaken) AS DateCourseTaken,
	            MAX(DateTestTaken) AS DateTestTaken,
	            MAX(Score) AS Score,
	            TestCategoryID AS TestCategoryID,
	            Passed AS Passed
            FROM #TestTaken
            WHERE CourseID > 0
	            AND TestCategoryID >= 0
	            AND passed = 1
            GROUP BY CourseID,
	            CIMNumber,
	            CourseName,
	            TestCategoryID,
	            Passed

            UNION ALL

            SELECT CourseID,
	            CIMNumber,
	            COurseName,
	            DateCourseTaken,
	            NULL,
	            NULL,
	            0,
	            1
            FROM #HoldTable
            ORDER BY datecoursetaken DESC

            DROP TABLE #HoldTable

            DROP TABLE #CourseLogin

            DROP TABLE #TestTakenTemp
			DROP TABLE #TestTakenTemp2
            DROP TABLE #TestTaken

			DROP TABLE #SCORM
			DROP TABLE #BUNDLECOUNT
            ", paramNames);

        DbCommand com = _db.GetSqlStringCommand(query);

        for (int i = 0; i < _paramNames.Length; i++)
        {
            _db.AddInParameter(com, _paramNames[i], DbType.String, paramValues[i]);
        }

        try
        {
            _db.LoadDataSet(com, ds, "CoursesEmployee");
        }
        catch (Exception ex)
        {
            ProcessHandledException("Courses", ex);
            ds = null;
        }
        finally
        {
            com.Connection.Close();
        }

        return ds;
    }

    #endregion

    //public bool isAgent(int CIMNumber)
    //{
    //    bool blnReturn = false;
    //    DbCommand com = _db.GetStoredProcCommand("pr_transcomuniversity_chk_IsAgent");

    //    _db.AddInParameter(com, "CIM", DbType.String, CIMNumber.ToString());

    //    try
    //    {
    //        int i = Convert.ToInt32(_db.ExecuteScalar(com));
    //        if (i == 1)
    //            blnReturn = true;
    //    }
    //    catch (Exception ex)
    //    {
    //        ProcessHandledException("isAgent", ex);
    //        blnReturn = false;
    //    }
    //    finally
    //    {
    //        com.Connection.Close();
    //    }

    //    return blnReturn;
    //}

    #region CareerPath functions

    //public TblTranscomUniversityLkpCareerPathCollection getCareerPath()
    //{
    //    TblTranscomUniversityLkpCareerPathCollection col = new TblTranscomUniversityLkpCareerPathCollection().Load();

    //    return col;
    //}

    //public VwTranscomUniversityCareerCourseCollection GetCareerPathByEmployeeID(int EmployeeID)
    //{
    //    VwTranscomUniversityCareerCourseCollection col = new VwTranscomUniversityCareerCourseCollection().Where("EmployeeID", EmployeeID).Load();

    //    return col;
    //}

    //public void InsertCareerPath(string CareerName, string CareerPathDescription, int HideFromList, string CreatedBy)
    //{
    //    TblTranscomUniversityLkpCareerPath careerPath = new TblTranscomUniversityLkpCareerPath();

    //    careerPath.CareerName = CareerName;
    //    careerPath.Description = CareerPathDescription;
    //    careerPath.HideFromList = HideFromList;
    //    careerPath.CreatedBy = CreatedBy;
    //    careerPath.CreateDate = DateTime.Now;
    //    careerPath.ModifiedBy = CreatedBy;
    //    careerPath.ModifyDate = DateTime.Now;
    //    careerPath.Save(CreatedBy);
    //}

    //public void UpdateCareerPath(int CareerPathID, string CareerName, string Description, int HideFromList, string ModifiedBy)
    //{

    //    TblTranscomUniversityLkpCareerPath item = new TblTranscomUniversityLkpCareerPath(CareerPathID);
    //    item.CareerName = CareerName;
    //    item.Description = Description;
    //    item.HideFromList = HideFromList;
    //    item.ModifiedBy = ModifiedBy;
    //    item.ModifyDate = DateTime.Now;
    //    item.Save(ModifiedBy);
    //}

    //public void DeleteCareerPath(int CareerPathID)
    //{
    //    TblTranscomUniversityLkpCareerPath.Delete("CareerPathID", CareerPathID);
    //}

    #endregion

    #region CareerCourse functions

    //public VwTranscomUniversityCareerCourseCollection GetCareerCourse(int CareerPathID)
    //{
    //    VwTranscomUniversityCareerCourseCollection col = new VwTranscomUniversityCareerCourseCollection().Where("CareerPathID", CareerPathID).Load();

    //    return col;
    //}

    //public void InsertCareerCourse(int CareerPathID, int CourseID, string CreatedBy)
    //{
    //    TblTranscomUniversityCorCareerCourse careerCourse = new TblTranscomUniversityCorCareerCourse();
    //    careerCourse.CareerPathID = CareerPathID;
    //    careerCourse.CourseID = CourseID;
    //    careerCourse.CreateDate = DateTime.Now;
    //    careerCourse.CreatedBy = CreatedBy;
    //    careerCourse.ModifiedBy = CreatedBy;
    //    careerCourse.ModifyDate = DateTime.Now;
    //    careerCourse.Save(CreatedBy);
    //}

    //public void UpdateCareerCourse(int CareerCourseID, int CareerPathID, int CourseID, string ModifiedBy)
    //{
    //    TblTranscomUniversityCorCareerCourse careerCourse = new TblTranscomUniversityCorCareerCourse(CareerCourseID);
    //    careerCourse.CareerPathID = CareerPathID;
    //    careerCourse.CourseID = CourseID;
    //    careerCourse.CreateDate = DateTime.Now;
    //    careerCourse.ModifiedBy = ModifiedBy;
    //    careerCourse.ModifyDate = DateTime.Now;
    //    careerCourse.Save(ModifiedBy);
    //}

    //public void DeleteCareerCourse(int CareerCourseID)
    //{
    //    TblTranscomUniversityCorCareerCourse.Delete(CareerCourseID);
    //}

    #endregion

    #region CareerEmployee

    //public long InsertCareerEmployee(int CareerPathID, int EmployeeID, DateTime DateTaken, string CreatedBy)
    //{
    //    TblTranscomUniversityCorCareerEmployee careerEmp = new TblTranscomUniversityCorCareerEmployee();

    //    careerEmp.EmployeeID = EmployeeID;
    //    careerEmp.CareerPathID = CareerPathID;
    //    careerEmp.CreateDate = DateTaken;
    //    careerEmp.Save(CreatedBy);

    //    return careerEmp.CareerEmployeeID;
    //}

    //public bool IsCareerEmployeeExist(int CareerPathID, int EmployeeID)
    //{
    //    SubSonic.Query qry = new SubSonic.Query(TblTranscomUniversityCorCareerEmployee.Schema);

    //    if (qry.WHERE("CareerPathID", CareerPathID).AND("EmployeeID", EmployeeID).GetRecordCount() > 0)
    //        return true;
    //    else
    //        return false;
    //}

    //    public DataSet GetCareerEmployeeList()
    //    {
    //        DataSet ds = new DataSet();
    //        DbCommand com = _db.GetSqlStringCommand(@"
    //            SELECT ce.CareerEmployeeID,
    //                e.EmployeeID,
    //	            e.CIMNumber,
    //	            e.LastName + ', ' + e.FirstName AS EmployeeName,
    //				cs.CompanySite as Site,
    //	            a.Account,
    //	            ce.CareerPathID,
    //	            cp.CareerName AS CareerPath,
    //                dbo.fnc_GetCareerProgress(CP.CareerPathID, e.EmployeeID) AS Progress
    //            FROM dbo.tbl_TranscomUniversity_Cor_CareerEmployees ce
    //            LEFT JOIN dbo.tbl_TranscomUniversity_Lkp_CareerPath cp ON ce.CareerPathID = cp.CareerPathID
    //            LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_personnel_cor_employee e ON ce.EmployeeID = e.EmployeeID
    //			LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_personnel_cor_employeecompany ec on e.EmployeeID = ec.EmployeeID
    //			LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_Personnel_Lkp_CompanySite cs on ec.CompanySiteID = cs.CompanySiteID
    //            LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_Business_cor_Account a ON e.CampaignID = a.AccountID
    //        ");


    //        try
    //        {
    //            _db.LoadDataSet(com, ds, "CareerEmployeeList");
    //        }
    //        catch (Exception ex)
    //        {
    //            ProcessHandledException("CareerEmployeeList", ex);
    //            ds = null;
    //        }
    //        finally
    //        {
    //            com.Connection.Close();
    //        }

    //        return ds;
    //    }

    //    public DataSet GetCareerEmployeeListHeirarchy(int EmployeeID)
    //    {
    //        DataSet ds = new DataSet();
    //        DbCommand com = _db.GetSqlStringCommand(@"
    //			WITH Members AS
    //            (
    //	            --Anchor Member Definition
    //	            select
    //		            emp.EmployeeID,
    //		            empSup.EmployeeID as SupEmployeeID
    //	            from [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee emp
    //	            inner join [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employeecompany ec
    //		            on emp.EmployeeID = ec.EmployeeID and ec.EndDate >= getdate()
    //	            left outer join [susl3psqldb02].CIMEnterprise.dbo.tbl_hierarchy_rlt_employee he1
    //		            on emp.EmployeeID = he1.EmployeeID and he1.EndDate >= getdate()
    //	            left outer join [susl3psqldb02].CIMEnterprise.dbo.tbl_hierarchy_rlt_employee he2
    //		            on he1.ParentHierarchyID = he2.HierarchyID
    //	            left outer join [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee empSup
    //		            on empSup.EmployeeID = he2.EmployeeID 
    //	            where empSup.EmployeeID = @EmployeeID
    //	
    //	            union all
    //	
    //	            --Recursive Member Definition
    //	            select
    //		            emp.EmployeeID,
    //		            empSup.EmployeeID as SupEmployeeID
    //	            from [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee emp
    //	            inner join [susl3psqldb02].CIMEnterprise.dbo.tbl_hierarchy_rlt_employee he1
    //		            on emp.EmployeeID = he1.EmployeeID and he1.EndDate >= getdate()
    //	            inner join [susl3psqldb02].CIMEnterprise.dbo.tbl_hierarchy_rlt_employee he2
    //		            on he1.ParentHierarchyID = he2.HierarchyID
    //	            inner join [susl3psqldb02].CIMEnterprise.dbo.tbl_personnel_cor_employee empSup
    //		            on empSup.EmployeeID = he2.EmployeeID
    //	            inner join Members d
    //		            on empSup.EmployeeID = d.EmployeeID
    //            )
    //
    //            SELECT ce.CareerEmployeeID,
    //                e.EmployeeID,
    //	            e.CIMNumber,
    //	            e.LastName + ', ' + e.FirstName AS EmployeeName,
    //				cs.CompanySite as Site,
    //	            a.Account,
    //	            ce.CareerPathID,
    //	            cp.CareerName AS CareerPath,
    //                dbo.fnc_GetCareerProgress(CP.CareerPathID, e.EmployeeID) AS Progress
    //            FROM dbo.tbl_TranscomUniversity_Cor_CareerEmployees ce
    //			INNER JOIN Members m on ce.EmployeeID = m.EmployeeID
    //            LEFT JOIN dbo.tbl_TranscomUniversity_Lkp_CareerPath cp ON ce.CareerPathID = cp.CareerPathID
    //            LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_personnel_cor_employee e ON m.EmployeeID = e.EmployeeID
    //			LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_personnel_cor_employeecompany ec on e.EmployeeID = ec.EmployeeID
    //			LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_Personnel_Lkp_CompanySite cs on ec.CompanySiteID = cs.CompanySiteID
    //            LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_Business_cor_Account a ON e.CampaignID = a.AccountID
    //        ");

    //        _db.AddInParameter(com, "EmployeeID", DbType.String, EmployeeID);

    //        try
    //        {
    //            _db.LoadDataSet(com, ds, "CareerEmployeeListHeirarchy");
    //        }
    //        catch (Exception ex)
    //        {
    //            ProcessHandledException("CareerEmployeeListHeirarchy", ex);
    //            ds = null;
    //        }
    //        finally
    //        {
    //            com.Connection.Close();
    //        }

    //        return ds;
    //    }

    //    public DataSet GetCareerEmployeeListBM(int EmployeeID)
    //    {
    //        DataSet ds = new DataSet();
    //        DbCommand com = _db.GetSqlStringCommand(@"
    //			DECLARE @Site varchar(5)
    //
    //			select @Site = cs.CompanySiteShort from [susl3psqldb02].cimenterprise.dbo.tbl_personnel_cor_employee e
    //			inner join [susl3psqldb02].cimenterprise.dbo.tbl_personnel_cor_employeecompany ec on e.EmployeeID = ec.EmployeeID
    //			inner join [susl3psqldb02].cimenterprise.dbo.tbl_Personnel_Lkp_CompanySite cs on ec.CompanySiteID = cs.CompanySiteID
    //			where e.EmployeeID = @EmployeeID          
    //
    //			SELECT ce.CareerEmployeeID,
    //                e.EmployeeID,
    //	            e.CIMNumber,
    //	            e.LastName + ', ' + e.FirstName AS EmployeeName,
    //				cs.CompanySite as Site,
    //	            a.Account,
    //	            ce.CareerPathID,
    //	            cp.CareerName AS CareerPath,
    //                dbo.fnc_GetCareerProgress(CP.CareerPathID, e.EmployeeID) AS Progress
    //            FROM dbo.tbl_TranscomUniversity_Cor_CareerEmployees ce
    //            LEFT JOIN dbo.tbl_TranscomUniversity_Lkp_CareerPath cp ON ce.CareerPathID = cp.CareerPathID
    //            LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_personnel_cor_employee e ON ce.EmployeeID = e.EmployeeID
    //			LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_personnel_cor_employeecompany ec on e.EmployeeID = ec.EmployeeID
    //			LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_Personnel_Lkp_CompanySite cs on ec.CompanySiteID = cs.CompanySiteID
    //            LEFT JOIN [susl3psqldb02].cimenterprise.dbo.tbl_Business_cor_Account a ON e.CampaignID = a.AccountID
    //			WHERE cs.CompanySiteShort = @Site
    //        ");

    //        _db.AddInParameter(com, "EmployeeID", DbType.String, EmployeeID);

    //        try
    //        {
    //            _db.LoadDataSet(com, ds, "CareerEmployeeListHeirarchy");
    //        }
    //        catch (Exception ex)
    //        {
    //            ProcessHandledException("CareerEmployeeListHeirarchy", ex);
    //            ds = null;
    //        }
    //        finally
    //        {
    //            com.Connection.Close();
    //        }

    //        return ds;
    //    }

    #endregion
}

public enum AccessMode { Internal = 1, External };
public enum AccessLevel { Admin = 1, User };




