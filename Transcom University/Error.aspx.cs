using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Error : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Disable Page Cache 
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!IsPostBack)
        {
            //PrepareLoginForm();
        }
    }

    //protected void PrepareLoginForm()
    //{
    //    var lv = (LoginView)Master.FindControl("LoginView1");
    //    lv.Visible = false;

    //    var hlpCourse = (HtmlAnchor)Master.FindControl("hlpCourseCatalog");
    //    hlpCourse.Visible = false;
    //    //var hlpCalendar = (HtmlAnchor)Master.FindControl("hlpCalendar");
    //    //hlpCalendar.Visible = false;
    //    var hlpFeatCourses = (HtmlAnchor)Master.FindControl("hlpFeatCourses");
    //    hlpFeatCourses.Visible = false;
    //}

    //protected void btnExit_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("Default.aspx");
    //}
}
