﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;
using System.Text.RegularExpressions;

public partial class MyReports : Page
{
    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;

    #region CheckAllItems

    protected void cbCheckAllItems_ItemDataBound(object o, RadComboBoxItemEventArgs e)
    {
        e.Item.Checked = true;
    }

    #endregion

    private void LoadFilterControls()
    {
        var dv = (DataView)dsParameters.Select(DataSourceSelectArguments.Empty);
        dv.RowStateFilter = DataViewRowState.CurrentRows;

        var rptId = !string.IsNullOrEmpty(cmbReportList.SelectedValue) ? Convert.ToInt32(cmbReportList.SelectedValue) : 0;

        var dsReportsQuery = (from DataRowView rowView in dv
                              where rowView.Row.Field<Int32>("ReportID") == rptId
                              select new
                              {
                                  reportid = rowView.Row.Field<int>("reportid"),
                                  telerikcontrol = rowView.Row.Field<int>("telerikcontrol"),
                                  label = rowView.Row.Field<string>("label"),
                                  datasourceid = rowView.Row.Field<string>("datasourceid"),
                                  datavaluefield = rowView.Row.Field<string>("datavaluefield"),
                                  datatextfield = rowView.Row.Field<string>("datatextfield")
                              }).ToList();

        if (dsReportsQuery.Count > 0)
        {
            for (int i = 0; i < dsReportsQuery.Count; i++)
            {
                var fieldName = dsReportsQuery.ElementAt(i);
                var cell = new System.Web.UI.HtmlControls.HtmlTableCell();

                if (fieldName.telerikcontrol.ToString() != "4")
                {
                    var lblName = new Label();
                    if (dsReportsQuery.Count >= 5)
                    {
                        lblName.Text = fieldName.label + "</br>";
                    }
                    else
                    {
                        lblName.Text = "&nbsp;&nbsp;&nbsp;" + fieldName.label + "&nbsp;";
                    }

                    cell.Controls.Add(lblName);
                }

                if (fieldName.telerikcontrol.ToString() == "1" || fieldName.telerikcontrol.ToString() == "2") //radcombobox
                {
                    var cbFilter = new RadComboBox
                        {
                            ID = "cb" + i + fieldName.reportid,
                            DataSourceID = fieldName.datasourceid,
                            DataValueField = fieldName.datavaluefield,
                            DataTextField = fieldName.datatextfield,
                            EnableViewState = false,
                            EmptyMessage = "- select " + fieldName.label.ToLower() + " -",
                            ToolTip = "Select " + fieldName.label,
                            DropDownAutoWidth = RadComboBoxDropDownAutoWidth.Enabled,
                            Width = Unit.Pixel(150),
                            MaxHeight = Unit.Pixel(300)
                        };

                    if (fieldName.telerikcontrol.ToString() == "2")
                    {
                        cbFilter.CheckBoxes = true;
                        cbFilter.EnableCheckAllItemsCheckBox = true;
                        cbFilter.ItemDataBound += cbCheckAllItems_ItemDataBound;
                    }

                    cell.Controls.Add(cbFilter);
                    tr1.Cells.Add(cell);
                    tblFilters.Rows.Add(tr1);
                    cbFilter.DataBind();
                }

                else if (fieldName.telerikcontrol.ToString() == "3") //raddatepicker
                {
                    var rdpFilter = new RadDatePicker
                        {
                            ID = fieldName.label.Replace(" ", "") + fieldName.reportid,
                            EnableTyping = false,
                            EnableViewState = false,
                            ShowPopupOnFocus = true,
                            Width = Unit.Pixel(150)
                        };

                    rdpFilter.DatePopupButton.HoverImageUrl = "Images/btnCalendar.gif";
                    rdpFilter.DatePopupButton.ImageUrl = "Images/btnCalendar.gif";
                    rdpFilter.DateInput.DateFormat = "MMMM dd, yyyy";
                    rdpFilter.DateInput.Enabled = true;

                    cell.Controls.Add(rdpFilter);
                    tr1.Cells.Add(cell);
                    tblFilters.Rows.Add(tr1);
                }

                else if (fieldName.telerikcontrol.ToString() == "4") //label
                {
                    var lblFilter = new Label
                        {
                            ID = "lbl" + i + "_" + fieldName.reportid,
                            Text = fieldName.label,
                            Visible = false
                        };

                    cell.Controls.Add(lblFilter);
                    tr1.Cells.Add(cell);
                    tblFilters.Rows.Add(tr1);
                }
            }

            tblFilters.Visible = true;
            trHoriz.Visible = true;
        }
        else
        {
            tblFilters.Visible = false;
            trHoriz.Visible = false;
        }
    }

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    public static string CourseIDs
    {
        get
        {
            var value = HttpContext.Current.Session["courseIDs"];
            return value == null ? string.Empty : value.ToString();
        }
        set { HttpContext.Current.Session["courseIDs"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!Roles.IsUserInRole("Admin") && !Roles.IsUserInRole("Coordinator") && !Roles.IsUserInRole("Manager") && !Roles.IsUserInRole("Trainer"))
        {
            Response.Redirect("NoAccess.aspx");
        }

        if (!Page.IsPostBack)
        {
            var roles = Roles.GetRolesForUser(Context.User.Identity.Name);
            var role1 = "";

            foreach (string role in roles)
            {
                if (role1 == "")
                {
                    role1 = role;
                }
                else
                {
                    role1 = role1 + "," + role;
                }
            }

            dsReports.SelectParameters["rolename"].DefaultValue = role1;

            cmbReportList.SelectedIndex = -1;
        }
        else
        {
            LoadFilterControls();
        }
    }

    public string GetComboBoxSelectedItems(RadComboBox combo)
    {
        string collection = null;
        if (combo.CheckedItems.Count > 0)
        {
            var sb = new StringBuilder();

            foreach (var item in combo.CheckedItems)
            {
                sb.Append(item.Value + ",");
            }

            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                collection = sb.ToString().TrimEnd(',');
            }
        }
        return collection;
    }

    protected void LoadReportToGridView(bool isRebind)
    {
        var dv = (DataView)dsReports.Select(DataSourceSelectArguments.Empty);
        dv.RowStateFilter = DataViewRowState.CurrentRows;

        var rptId = !string.IsNullOrEmpty(cmbReportList.SelectedValue) ? Convert.ToInt32(cmbReportList.SelectedValue) : 0;

        var dsReportsQuery = (from DataRowView rowView in dv
                              where rowView.Row.Field<Int32>("ReportID") == rptId
                              select new
                              {
                                  SPName = rowView.Row.Field<string>("SPName")
                              }).ToList();

        if (dsReportsQuery != null)
        {
            var spName = dsReportsQuery.ElementAt(0);

            var dataAdapter = new SqlDataAdapter();

            Dbconn("Intranet");
            _ocmd = new SqlCommand(spName.SPName, _oconn)
            {
                CommandTimeout = 20000,
                CommandType = CommandType.StoredProcedure
            };

            foreach (var tr in tblFilters.Controls.OfType<System.Web.UI.HtmlControls.HtmlTableRow>())
            {
                foreach (var td in tr.Controls.OfType<System.Web.UI.HtmlControls.HtmlTableCell>())
                {
                    foreach (var rdb in td.Controls.OfType<RadComboBox>())
                    {
                        _ocmd.Parameters.AddWithValue("@" + rdb.DataTextField, GetComboBoxSelectedItems(rdb));
                    }

                    foreach (var rdb in td.Controls.OfType<RadDatePicker>())
                    {
                        _ocmd.Parameters.AddWithValue("@" + Regex.Replace(rdb.ID, @"[\d-]", string.Empty), rdb.SelectedDate);
                    }


                    foreach (var rdb in td.Controls.OfType<Label>()) //get user's cim
                    {
                        if (rdb.Text.Contains("@"))
                        {
                            _ocmd.Parameters.AddWithValue(rdb.Text, Context.User.Identity.Name);
                        }
                    }
                }
            }
            _ocmd.CommandTimeout = 0;
            dataAdapter.SelectCommand = _ocmd;
            dataAdapter.SelectCommand.CommandTimeout = 0;

            var objDataSet = new DataSet();
            dataAdapter.Fill(objDataSet);

            gridReports.DataSource = objDataSet;

            if (isRebind)
            {
                gridReports.DataBind();
            }

            tblViewAndExport.Visible = true;
        }
    }

    protected void gridReports_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (cmbReportList.SelectedIndex >= 0)
        {
            {
                LoadReportToGridView(false);
            }
        }
        else
        {
            gridReports.DataSource = new int[] { };
        }
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            {
                LoadReportToGridView(true);
                gridReports.Height = Unit.Pixel(700);
                gridReports.MasterTableView.Width = Unit.Pixel(1800);
            }
        }
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        if (gridReports.MasterTableView.Items.Count > 0)
        {
            ConfigureExport();
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "AlertBox", "alert('No Data to export');", true);
        }
    }

    public void ConfigureExport()
    {
        gridReports.ExportSettings.ExportOnlyData = true;
        gridReports.ExportSettings.IgnorePaging = true;
        gridReports.ExportSettings.OpenInNewWindow = true;
        gridReports.ExportSettings.FileName = cmbReportList.SelectedItem.Text.Replace(" ", "_") + "_" + DateTime.Now;
        gridReports.MasterTableView.ExportToExcel();
    }

    protected void cmbReportList_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (cmbReportList.Text == "Quiz")
            Response.Redirect("~/MyReportQuiz.aspx");

        lblRptName.Text = string.Format("Report Name: {0}", cmbReportList.Text);
        tblViewAndExport.Visible = cmbReportList.SelectedIndex != -1;

        gridReports.Height = Unit.Pixel(20);
        gridReports.DataSource = new int[] { };
        gridReports.DataBind();
    }

    protected void dsAssignCourseReportingManager_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@CIMNumber"].Value = User.Identity.Name;
    }

}