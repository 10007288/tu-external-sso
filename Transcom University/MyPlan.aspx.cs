﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using NuComm.Security.Encryption;
using Telerik.Web.UI;

public partial class MyPlan : Page
{
    private MasterPage _master;

    public static string TwwId
    {
        get
        {
            var value = HttpContext.Current.Session["TwwIdSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["TwwIdSession"] = value; }
    }

    public static string[] SearchKeys
    {
        get
        {
            var value = HttpContext.Current.Session["SearchKeysSession"];
            return value == null ? new string[] { } : (string[])value;
        }
        set { HttpContext.Current.Session["SearchKeysSession"] = value; }
    }

    public static string CourseLinkTitleToLearner
    {
        get
        {
            var value = HttpContext.Current.Session["CourseLinkTitleToLearnerSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseLinkTitleToLearnerSession"] = value; }
    }

    public static string CourseLinkTitleToApprover
    {
        get
        {
            var value = HttpContext.Current.Session["CourseLinkTitleToApproverSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseLinkTitleToApproverSession"] = value; }
    }

    public static string CourseDescription
    {
        get
        {
            var value = HttpContext.Current.Session["CourseDescriptionSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseDescriptionSession"] = value; }
    }

    public static int SearchCriteria
    {
        get
        {
            var value = HttpContext.Current.Session["SearchCriteriaSession"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["SearchCriteriaSession"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Disable Page Cache 
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!Page.IsPostBack)
        {
            hidCIM.Value = User.Identity.Name;

            TwwId = User.Identity.Name;
            SearchKeys = new[] { TwwId };
            CourseLinkTitleToLearner = "";
            CourseLinkTitleToApprover = "";
            CourseDescription = "";
            SearchCriteria = 0;

            SetPage_RolePreviledges();

            DataHelper.UpdateCourseEnrolmentViewStatus(Convert.ToInt32(TwwId));
            DataHelper.ResetNotifCount(Convert.ToInt32(TwwId));

            cbDirectReports.DataTextField = "Name";
            cbDirectReports.DataValueField = "TwwId";
            cbDirectReports.DataBind();
        }
    }

    private void SetPage_RolePreviledges()
    {
        if (Roles.IsUserInRole("Admin") || Roles.IsUserInRole("Non-APAC Admin") || Roles.IsUserInRole("Trainer") || Roles.IsUserInRole("Manager"))
        {
            tdSearchCrit.Visible = true;
            tdMngr.Visible = true;
            tdAdminSearch.Visible = true;
            tdBtnSearch.Visible = true;
        }
    }

    private static void GetCheckedItems(RadComboBox comboBox, RadTextBox txtSearch)
    {
        var sb = new StringBuilder();
        var collection = comboBox.CheckedItems;

        if (collection.Count != 0)
        {
            foreach (var item in collection)
                sb.Append(item.Value + ",");

            txtSearch.Text = sb.ToString().TrimEnd(',');
            txtSearch.ToolTip = sb.ToString().TrimEnd(',');
        }
        else
        {
            txtSearch.Text = "";
        }
    }

    protected void cbDirectReports_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        GetCheckedItems(cbDirectReports, txtSearch);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtSearch.Text))
        {
            SearchKeys = txtSearch.Text.Split(',').ToArray();

            if (txtSearch.Text != TwwId)
            {
                gridEmployee.MasterTableView.DetailTables[0].Columns.FindByUniqueName("DeleteColumn").Display = false;
                gridEmployee.MasterTableView.DetailTables[0].GetColumn("Title").Display = true;
                gridEmployee.MasterTableView.DetailTables[0].GetColumn("TitleLink").Display = false;
            }
            else
            {
                gridEmployee.MasterTableView.DetailTables[0].Columns.FindByUniqueName("DeleteColumn").Display = true;
                gridEmployee.MasterTableView.DetailTables[0].GetColumn("Title").Display = false;
                gridEmployee.MasterTableView.DetailTables[0].GetColumn("TitleLink").Display = true;
            }

            if (SearchKeys.Count() > 1)
            {
                gridEmployee.MasterTableView.DetailTables[0].AllowPaging = true;
                gridEmployee.MasterTableView.DetailTables[0].PageSize = 10;
                gridEmployee.MasterTableView.DetailTables[0].PagerStyle.Mode = GridPagerMode.Slider;
                gridEmployee.MasterTableView.DetailTables[0].PagerStyle.AlwaysVisible = true;
            }
            else
            {
                gridEmployee.MasterTableView.DetailTables[0].AllowPaging = false;
                gridEmployee.MasterTableView.DetailTables[0].PagerStyle.AlwaysVisible = false;
            }
        }
        else
        {
            SearchKeys = new[] { TwwId };

            gridEmployee.MasterTableView.DetailTables[0].AllowPaging = false;
            gridEmployee.MasterTableView.DetailTables[0].PagerStyle.AlwaysVisible = false;

            gridEmployee.MasterTableView.DetailTables[0].Columns.FindByUniqueName("DeleteColumn").Display = true;
            gridEmployee.MasterTableView.DetailTables[0].GetColumn("Title").Display = false;
            gridEmployee.MasterTableView.DetailTables[0].GetColumn("TitleLink").Display = true;
        }

        gridEmployee.Rebind();
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        var ds = new DataSet();
        foreach (var id in SearchKeys)
        {
            var data = DataHelper.GetAssignedEnrolledCourse_ByTwwId(Convert.ToInt32(id));
            ds.Tables.Add(ConvertToDataTable(data, id));
        }

        ExcelHelper.ToExcel(ds, "TU_Report_MyLearningPlan_" + DateTime.Now + ".xls", Page.Response);
    }

    public DataTable ConvertToDataTable<T>(IList<T> data, string tableName)
    {
        var properties = TypeDescriptor.GetProperties(typeof(T));
        var table = new DataTable(tableName);

        foreach (PropertyDescriptor prop in properties)
        {
            table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
        }

        foreach (T item in data)
        {
            var row = table.NewRow();
            foreach (PropertyDescriptor prop in properties)
                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            table.Rows.Add(row);
        }

        table.Columns.Remove("EncryptedCourseId");
        table.Columns.Remove("EnrolmentRequired");
        table.Columns.Remove("RowNum");
        table.Columns.Remove("SupCim");
        table.Columns.Remove("StatusId");
        table.Columns.Remove("Active");
        table.Columns.Remove("IsViewed");

        return table;
    }

    protected void gridEmployee_PreRender(object sender, EventArgs e)
    {
        if (gridEmployee.MasterTableView.Items.Count == 1)
        {
            gridEmployee.MasterTableView.Items[0].Expanded = true;

            gridEmployee.MasterTableView.Items[0].BackColor = Color.FromArgb(0, 33, 54);
            gridEmployee.MasterTableView.Items[0].ForeColor = Color.White;

            gridEmployee.MasterTableView.ExpandCollapseColumn.Visible = false;
        }
    }

    protected void GridEmployeeNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        List<DataHelperModel.User> ds;
        switch (SearchCriteria)
        {
            case 0:
                LoadEmployeeDefault();
                break;
            case 1:
                if (!string.IsNullOrEmpty(txtSearch.Text))
                {
                    ds = DataHelper.GetUsers_ByFirstNames(txtSearch.Text).OrderBy(p => p.CimNo).ToList();
                    gridEmployee.DataSource = ds;
                }
                else
                {
                    LoadEmployeeDefault();
                }
                break;
            case 2:
                if (!string.IsNullOrEmpty(txtSearch.Text))
                {
                    ds = DataHelper.GetUsers_ByLastNames(txtSearch.Text).OrderBy(p => p.CimNo).ToList();
                    gridEmployee.DataSource = ds;
                }
                else
                {
                    LoadEmployeeDefault();
                }
                break;
        }
    }

    private void LoadEmployeeDefault()
    {
        var ds = DataHelper.GetUsers_ByTwwIds(SearchKeys).OrderBy(p => p.CimNo).ToList();
        gridEmployee.DataSource = ds;
    }

    protected void gridEmployee_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
        var dataItem = e.DetailTableView.ParentItem;

        if (e.DetailTableView.Name == "gridLearningPlan")
        {
            var twwid = dataItem.GetDataKeyValue("CimNo").ToString();

            var data = DataHelper.GetAssignedEnrolledCourse_ByTwwId(Convert.ToInt32(twwid));
            e.DetailTableView.DataSource = data;
        }
    }

    protected void GridEmployeeItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "MasterTable")
        {
            e.Item.BackColor = Color.FromArgb(217, 217, 217);
            e.Item.ForeColor = Color.Black;
        }
        if (e.Item is GridDataItem && e.Item.OwnerTableView.Name == "gridLearningPlan")
        {
            var dataItem = (GridDataItem)e.Item;

            var courseId = dataItem.GetDataKeyValue("CourseID").ToString();
            var encCourseId = Server.UrlEncode(UTF8.EncryptText(dataItem.GetDataKeyValue("CourseID").ToString()));
            var sTestCategoryId = dataItem.GetDataKeyValue("EncryptedCourseID").ToString();
            var statusId = dataItem.GetDataKeyValue("StatusId").ToString();
            var title = dataItem.GetDataKeyValue("Title").ToString();
            var courseTypeID = Convert.ToInt32(dataItem.GetDataKeyValue("CourseTypeID"));
            var courseType = dataItem.GetDataKeyValue("CourseType").ToString();
            var hasTest = Convert.ToInt32(dataItem.GetDataKeyValue("HasTest"));
            var passed = (dataItem.GetDataKeyValue("Passed") == null) ? 0 : Convert.ToInt32(dataItem.GetDataKeyValue("Passed"));

            var login = string.IsNullOrEmpty(dataItem.GetDataKeyValue("CourseLogin").ToString()) ? "" : dataItem.GetDataKeyValue("CourseLogin").ToString();

            string urlWithParams;

            if (courseTypeID == 2)
            {
                urlWithParams = string.Format("CourseLaunch.aspx?CourseID={0}", encCourseId);
            }
            else
            {
                urlWithParams = string.Format("CourseRedirect.aspx?CourseID={0}&TestCategoryID={1}", encCourseId, sTestCategoryId);
            }

            var linkTitle = dataItem.FindControl("linkCourseTitle") as LinkButton;

            var linkStat = dataItem.FindControl("btnPreviousReg") as LinkButton;

            //SET HIDDENFIELD VALUE OF PROGRESS
            var progressval = 0.00;

            if (title == "HDTV")
            {

            }

            if (courseTypeID == 1)
            {
                if (hasTest > 0) //COURSE WITH ASSESSMENT
                {
                    if (login == "1")
                    {
                        progressval = 20;

                        if (passed == 1)
                        {
                            progressval = progressval + 80;
                        }
                    }
                }
                else //COURSE WO ASSESSMENT
                {
                    if (login == "1")
                    {
                        progressval = 100;
                    }
                }

                if (progressval > 0 && progressval < 100)
                {
                    linkStat.Text = "In Progress";
                }
                else if (progressval == 100)
                {
                    linkStat.Text = "Completed";
                }
            }

            else if (courseTypeID == 2)
            {
                if (txtSearch.Text != "")
                {
                    progressval = Convert.ToDouble(ScormDataHelper.GetOverallProgressByCourseID(txtSearch.Text, Convert.ToInt32(courseId)));
                }
                else
                {
                    progressval = Convert.ToDouble(ScormDataHelper.GetOverallProgressByCourseID(HttpContext.Current.User.Identity.Name, Convert.ToInt32(courseId)));
                }

                if (courseType == "Enrolled")
                {
                    if (statusId == "2")
                    {
                        if (progressval > 0 && progressval < 100)
                        {
                            linkStat.Text = "In Progress";
                        }
                        else if (progressval >= 100)
                        {
                            linkStat.Text = "Completed";
                        }
                    }
                }
            }

            //In Progress
            //if (courseType == "Enrollment")
            //{
            //    if (statusId == "2")
            //    {
            //        if (progressval > 0 && progressval < 100)
            //        {
            //            linkStat.Text = "In Progress";
            //        }
            //        else if (progressval == 100)
            //        {
            //            linkStat.Text = "Complete";
            //        }
            //    }
            //}
            //else //Assigned
            //{

            //}

            //1	Pending Approval
            //2	Approved
            //3	Declined
            //4	Cancelled (By Learner)
            //5	Not Started
            //6	Complete
            //7	Over Due
            //8	Cancelled (By Assigner)
            //9 In Progress

            //Decorate items
            switch (statusId)
            {
                case "1":
                    linkStat.Style.Add("font-size", "10pt !important");
                    linkTitle.Style.Add("color", "gray !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return false;");
                    break;
                case "2":
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);
                    break;
                case "3":
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);

                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
                case "4":
                    linkStat.Style.Add("font-size", "8pt !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);

                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
                case "5":
                case "6":
                    linkStat.Style.Add("color", "black !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);

                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
                case "7":
                    linkStat.Style.Add("color", "red !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);

                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
                case "8":
                    linkStat.Style.Add("font-size", "8pt !important");
                    linkStat.Style.Add("color", "black !important");
                    linkTitle.Style.Add("color", "gray !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return false;");
                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
                case "9":
                    linkStat.Style.Add("color", "black !important");
                    linkTitle.Attributes["onclick"] =
                        string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}');", urlWithParams,
                                      courseId, statusId, title);

                    dataItem["DeleteColumn"].Controls[0].Visible = false;
                    break;
            }

            var hidProgressVal = (HiddenField)dataItem["Progress"].FindControl("hidProgressVal");

            if (!double.IsNaN(progressval) && progressval.ToString("#.##") != "")
            {
                hidProgressVal.Value = progressval.ToString("#.##");
            }
            else
            {
                hidProgressVal.Value = "0";
            }


            if (dataItem["DueDate"].Text == @"&nbsp;")
            {
                dataItem["DueDate"].Text = @"N/A";
            }

            dataItem.ToolTip = title;
        }
    }

    protected void GridEmployeeItemCommand(object sender, GridCommandEventArgs e)
    {
        _master = Master;
        if (e.CommandName == RadGrid.DeleteCommandName && e.Item.OwnerTableView.Name == "gridLearningPlan")
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var supQry = DataHelper.GetSupervisorDetails(Convert.ToInt32(TwwId));

                if (supQry != null)
                {
                    var enrolmentId = deletedItem.GetDataKeyValue("EnrollmentId").ToString();
                    var title = deletedItem.GetDataKeyValue("Title").ToString();
                    var desc = deletedItem.GetDataKeyValue("Description").ToString();

                    var retVal = DataHelper.UpdateCourseEnrolmentStatus(Convert.ToInt32(enrolmentId), "", 4,
                                                                        Convert.ToInt32(TwwId), false);

                    switch (retVal)
                    {
                        case true:
                            const string msg = "The course enrollment has been cancelled by learner.";

                            var owner = SendEmail.ValidateAddress(supQry.Email) ? supQry.Email : supQry.Email_fallback;
                            var recipient = SendEmail.ValidateAddress(supQry.Email1)
                                                ? supQry.Email1
                                                : supQry.Email1_fallback;

                            const string url = "http://transcomuniversity.com/MyTeam.aspx";
                            var titleLink = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url, title);

                            SendEmail.SendEmail_CourseEnrolment(owner, recipient, msg, "", titleLink, desc);

                            e.Item.OwnerTableView.Rebind();

                            const string alertMsg = "Your enrollment to the course is now cancelled.";
                            _master.AlertMessage(true, alertMsg);
                            break;
                        case false:
                            _master.AlertMessage(false, "Request failed. Please try again.");
                            break;
                    }
                }
            }
        }
        else if (e.CommandName == RadGrid.ExpandCollapseCommandName)
        {
            if (e.Item.Expanded)
            {
                e.Item.BackColor = Color.FromArgb(217, 217, 217);
                e.Item.ForeColor = Color.Black;
            }
            else
            {
                e.Item.BackColor = Color.FromArgb(0, 33, 54);
                e.Item.ForeColor = Color.White;
            }

            foreach (GridItem item in e.Item.OwnerTableView.Items)
            {
                if (item.Expanded && item != e.Item)
                {
                    item.Expanded = false;
                    item.BackColor = Color.FromArgb(217, 217, 217);
                    item.ForeColor = Color.Black;
                }
            }
        }
    }

    protected void btnPreviousReg_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        var enrollmentId = item.GetDataKeyValue("EnrollmentId").ToString();
        var enroleeCim = item.GetDataKeyValue("EnrolleeCim").ToString();
        var courseId = item.GetDataKeyValue("CourseID").ToString();
        var title = item.GetDataKeyValue("Title").ToString();

        if (enrollmentId != "0")
        {
            var script = "function f(){$find(\"" + windowEnrollmentHistory.ClientID +
                         "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(this, GetType(), "key", script, true);

            var qry =
                DataHelper.GetCourseEnrolment_History(Convert.ToInt32(enroleeCim), Convert.ToInt32(courseId))
                          .OrderByDescending(p => p.CreateDate);

            if (qry != null)
            {
                lblCourseName1.Text = title;
                gridEnrollmentHistory.DataSource = qry;
                gridEnrollmentHistory.DataBind();
            }
        }
        else
        {
            var script = "function f(){$find(\"" + windowAssignmentHistory.ClientID +
                         "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            ScriptManager.RegisterStartupScript(this, GetType(), "key", script, true);

            var qry = DataHelper.GetAssignedCourse_History(Convert.ToInt32(enroleeCim), Convert.ToInt32(courseId));

            if (qry != null)
            {
                lblCourseName2.Text = title;
                gridAssignmentHistory.DataSource = qry;
                gridAssignmentHistory.DataBind();
            }
        }
    }

    protected void btnEnrolCourse_Click(object sender, EventArgs e)
    {
        var enrolmentStatus = DataHelper.ChkUserEnrolmentStatusToCourse(Convert.ToInt32(hidCourseId.Value),
                                                                        Convert.ToInt32(TwwId));
        _master = Master;

        switch (enrolmentStatus)
        {
            case 0:
            case 3:
            case 4: //0 = New, 3 = Denied, 4 = Cancelled by user
                var supQry = DataHelper.GetSupervisorDetails(Convert.ToInt32(TwwId));

                if (supQry != null)
                {
                    var retVal = DataHelper.InsertCourseEnrolment(Convert.ToInt32(hidCourseId.Value),
                                                                  Convert.ToInt32(TwwId), supQry.Cim1);

                    if (retVal)
                    {
                        const string msgToLearner = "Your course enrollment has been received and is going through approval process.";
                        const string msgToApprover = "Course registration for your approval.";

                        var owner = SendEmail.ValidateAddress(supQry.Email) ? supQry.Email : supQry.Email_fallback;
                        var recipient = SendEmail.ValidateAddress(supQry.Email1)
                                            ? supQry.Email1
                                            : supQry.Email1_fallback;

                        SendEmail.SendEmail_CourseEnrolment(owner, owner, msgToLearner, "", CourseLinkTitleToLearner, CourseDescription);
                        SendEmail.SendEmail_CourseEnrolment(owner, recipient, msgToApprover, "", CourseLinkTitleToApprover, CourseDescription);

                        gridEmployee.MasterTableView.Rebind();

                        const string alertMsg = "Your request for enrollment has been sent to your manager for approval. Please check your email for updates on the status of your enrollment.";
                        _master.AlertMessage(true, alertMsg);
                        ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseEnrollCourseWindow('1');", true);
                    }
                    else
                    {
                        _master.AlertMessage(false, "Request failed. Please try again.");
                    }
                }
                else
                {
                    _master.AlertMessage(false, "Request failed. Please try again.");
                }
                break;
            case 1: //For Approval
                _master.AlertMessage(false,
                                     "Your enrollment request is still subject for approval. Please wait for the email notification that will be sent to your registered email.");
                ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseEnrollCourseWindow('1');", true);
                break;
            case 2: //Approved
                var url = hidUrlWithParams.Value;
                var s = "window.open('" + url +
                        "', 'popup_window', 'width=700,height=550,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=yes,resizable=yes');";
                ClientScript.RegisterStartupScript(GetType(), "script", s, true);
                ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseEnrollCourseWindow('1');", true);
                break;
        }
    }

    protected void linkCourseTitle_Click(object sender, EventArgs e)
    {
        var btn = sender as LinkButton;
        var item = btn.NamingContainer as GridDataItem;

        var title = item.GetDataKeyValue("Title").ToString();
        var desc = item.GetDataKeyValue("Description").ToString();

        const string url1 = "http://transcomuniversity.com/MyTeam.aspx";
        var titleLink1 = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url1, title);

        const string url2 = "http://transcomuniversity.com/MyPlan.aspx";
        var titleLink2 = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url2, title);

        CourseLinkTitleToApprover = titleLink1;
        CourseLinkTitleToLearner = titleLink2;
        CourseDescription = desc;
    }

    protected void cbSearchType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        SearchKeys = new[] { TwwId };

        cbDirectReports.Text = "";
        cbDirectReports.ClearCheckedItems();

        txtSearch.Text = "";

        SearchCriteria = Convert.ToInt32(cbSearchType.SelectedValue);

        switch (cbSearchType.SelectedValue)
        {
            case "0":
                cbDirectReports.Enabled = true;
                cbDirectReports.DataTextField = "Name";
                cbDirectReports.DataValueField = "TwwId";
                cbDirectReports.DataBind();
                break;
            case "1":
                cbDirectReports.Enabled = false;
                cbDirectReports.DataTextField = "Name";
                cbDirectReports.DataValueField = "TwwId";
                cbDirectReports.DataBind();
                break;
            case "2":
                cbDirectReports.Enabled = false;
                cbDirectReports.DataTextField = "Name";
                cbDirectReports.DataValueField = "TwwId";
                cbDirectReports.DataBind();
                break;
        }

        gridEmployee.Rebind();
    }

}