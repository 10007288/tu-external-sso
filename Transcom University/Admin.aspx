<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    EnableEventValidation="true" CodeFile="Admin.aspx.cs" Title="Transcom University - Admin Page"
    Inherits="Admin" %>

<%@ MasterType VirtualPath="~/MasterPage.master" %>
<%@ Register TagPrefix="AddScormCourse" TagName="ScormCourse" Src="~/SCORM/UserControls/AddScormCourse.ascx" %>
<%@ Register TagPrefix="uc1" TagName="UploadContent" Src="~/SCORM/DnsControls/UploadContent.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">

            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("btnExport") >= 0 ||
                    args.get_eventTarget().indexOf("btnExportExcel") >= 0 ||
                    args.get_eventTarget().indexOf("btnExportAudience") >= 0 ||
                    args.get_eventTarget().indexOf("btnExportAudienceMembers") >= 0
                ) {
                    args.set_enableAjax(false);
                }

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }
                var scrollLeftOffset;
                var scrollTopOffset;
                if ($telerik.isSafari) {
                    scrollTopOffset = document.body.scrollTop;
                    scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    scrollTopOffset = document.documentElement.scrollTop;
                    scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
            var popUp;
            function PopUpShowing(sender, eventArgs) {
                popUp = eventArgs.get_popUp();
                var viewportWidth = $(window).width();
                var viewportHeight = $(window).height();
                var popUpWidth = popUp.style.width.substr(0, popUp.style.width.indexOf("px"));
                var popUpHeight = popUp.style.height.substr(0, popUp.style.height.indexOf("px"));
                popUp.style.position = "fixed";
                popUp.style.left = Math.floor((viewportWidth - popUpWidth) / 2) + "px";
                popUp.style.top = Math.floor((viewportHeight - popUpHeight) / 2) + "px";
            }
            function triggerValidation() {
                Page_ClientValidate();
            }
            function validatePage1(sender, args) {
                var dropDownTree = $find("<%=ddtAddSubcategoryOnSubCat.ClientID%>");

                if (dropDownTree.get_entries().get_count() == 0) {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
            }
            function OnClientEntryAdding(sender, eventArgs) {

                setTimeout(function () { sender.closeDropDown(); }, 100);

            }

            function OpenFileExplorerDialog(button, args) {
                var wnd = $find("<%= ExplorerWindow.ClientID %>");
                wnd.show();
                return false;
            }

            function OnFileSelected(fileSelected) {

                document.getElementById("<%= hiddenFilePath.ClientID %>").value = fileSelected;
                $('#<%= lblCourseEditName.ClientID %>').text((fileSelected.replace(/^.*[\\\/]/, '')).replace('.html', ''));

                var stringData = $.ajax({
                    url: fileSelected,
                    async: false
                }).responseText;

                $find("<%=editorWelcomeMessage.ClientID%>").set_html(stringData);
            }

            function CreateNewCourseFile(fileName) {

                var path = '~/' + '<%= Page.ResolveClientUrl("~/PageFiles/Featured Courses/") %>' + fileName + '.html';
                path = path.replace('%20', ' ');
                document.getElementById("<%= hiddenFilePath.ClientID %>").value = path;

                $('#<%= lblCourseEditName.ClientID %>').text((path.replace(/^.*[\\\/]/, '')).replace('.html', ''));

                document.getElementById("<%= hiddenCourseName.ClientID %>").value = (path.replace(/^.*[\\\/]/, '')).replace('.html', '');

                $find("<%=editorWelcomeMessage.ClientID%>").set_html('');
            }

            function initialEditorSize(sender, args) {
                var editor = $find("<%= editorWelcomeMessage.ClientID %>");
                editor.get_contentAreaElement().style.height = "500px";
            }

            function ShowDialog(sender, args) {
                var dialogWindow = $find("<%=dialogWindow.ClientID %>");
                setTimeout(function () {
                    dialogWindow.show();
                }, 800);
            }

            //Prerequisites
            var prevVal = '';
            function ClosePrerequisiteForm(operation) {
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest(operation);
                var window = $find('<%=CoursePrerequisiteWindow.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 800);
            }

            function OpenPrerequisiteForm() {

                setTimeout(function () { $find("<%=CoursePrerequisiteWindow.ClientID %>").show(); }, 800);
            }

            function PassID(_courseID) {
                document.getElementById("<%= hidCourseID.ClientID %>").value = _courseID;
            }

            //AssignCourse
            function CloseAssignForm(save, msg) {

                if (save) {
                    alert(msg);
                }
                var window = $find('<%=AssignCourseWindow.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 800);
            }

            function OpenAssignForm(sender, args) {

                setTimeout(function () { $find("<%=AssignCourseWindow.ClientID %>").show(); }, 800);
            }

            function ValidateCIM(sender, args) {

                var radInput = $find("<%= txtAssignTo.ClientID %>")
                if (radInput.get_value() != prevVal) {
                    prevVal = radInput.get_value();
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("ValidateCIM");
                }
            }


            function View_AudienceMembers(sender, args) {
                setTimeout(function () { $find("<%=windowAudienceMembers.ClientID %>").show(); }, 800);
            }



            function Close_AudienceMembers(sender, args) {
                var window = $find('<%=windowAudienceMembers.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 800);
            }
            var rowNum = 1;

            function initializeControls(btnRemoveFilter, btnAddFilter, row2, row3, lblFilterValueName1, lblFilterValueName2, lblFilterValueName3,
                hidlblFilterValue1, hidlblFilterValue2, hidlblFilterValue3) {
                switch (rowNum) {
                    case 1:
                        btnRemoveFilter.attr("src", "Images/deleteFilter_inactive.png");
                        btnAddFilter.attr("src", "Images/addFilter_active.png");
                        row2.hide();
                        row3.hide();
                        break;
                    case 2:
                        btnRemoveFilter.attr("src", "Images/deleteFilter_active.png");
                        btnAddFilter.attr("src", "Images/addFilter_active.png");
                        row2.show();
                        row3.hide();
                        break;
                    case 3:
                        btnRemoveFilter.attr("src", "Images/deleteFilter_active.png");
                        btnAddFilter.attr("src", "Images/addFilter_inactive.png");
                        row2.show();
                        row3.show();
                        break;
                    default:
                        break;
                }

                lblFilterValueName1.text(hidlblFilterValue1.val());
                lblFilterValueName2.text(hidlblFilterValue2.val());
                lblFilterValueName3.text(hidlblFilterValue3.val());
            }

            function pageLoad() {
                var tabStrip = $find('<%= tsMenu.ClientID %>');

                var hidOperation = $("input:hidden[id$=hidOperation]");
                var hidRowNum = $("input:hidden[id$=hidRowNum]");

                if (tabStrip.get_selectedTab().get_text() == 'Audience') {
                    if (hidOperation.val() == 'Normal') {
                        var btnRemoveFilter = $("input[id$=btnRemoveFilter]");
                        var btnAddFilter = $("input[id$=btnAddFilter]");
                        var row2 = $("tr[id$=row2]");
                        var row3 = $("tr[id$=row3]");
                        var lblFilterValueName1 = $("[id$=lblFilterValueName1]");
                        var lblFilterValueName2 = $("[id$=lblFilterValueName2]");
                        var lblFilterValueName3 = $("[id$=lblFilterValueName3]");
                        var hidlblFilterValue1 = $("input:hidden[id$=hidlblFilterValue1]");
                        var hidlblFilterValue2 = $("input:hidden[id$=hidlblFilterValue2]");
                        var hidlblFilterValue3 = $("input:hidden[id$=hidlblFilterValue3]");

                        initializeControls(btnRemoveFilter, btnAddFilter, row2, row3, lblFilterValueName1, lblFilterValueName2, lblFilterValueName3,
                            hidlblFilterValue1, hidlblFilterValue2, hidlblFilterValue3);
                    }
                    else if (hidOperation.val() == 'Saving' || hidOperation.val() == 'Updating') {
                        hidRowNum.val(1);
                        rowNum = 1;
                        if (hidOperation.val() == 'Updating') {
                            hidOperation.val('Editing');
                        } else {
                            hidOperation.val('Normal');
                        }

                    }
                    else if (hidOperation.val() == 'Editing') {
                        var btnRemoveFilter = $("input[id$=btnRemoveFilterEdit]");
                        var btnAddFilter = $("input[id$=btnAddFilterEdit]");
                        var row2 = $("tr[id$=rowEdit2]");
                        var row3 = $("tr[id$=rowEdit3]");
                        var lblFilterValueName1 = $("[id$=lblFilterValueNameEdit1]");
                        var lblFilterValueName2 = $("[id$=lblFilterValueNameEdit2]");
                        var lblFilterValueName3 = $("[id$=lblFilterValueNameEdit3]");
                        var hidlblFilterValue1 = $("input:hidden[id$=hidlblFilterValue1]");
                        var hidlblFilterValue2 = $("input:hidden[id$=hidlblFilterValue2]");
                        var hidlblFilterValue3 = $("input:hidden[id$=hidlblFilterValue3]");

                        initializeControls(btnRemoveFilter, btnAddFilter, row2, row3, lblFilterValueName1, lblFilterValueName2, lblFilterValueName3,
                            hidlblFilterValue1, hidlblFilterValue2, hidlblFilterValue3);
                    }
                }
            }

            function ResetControl(controltype, control) {
                if (controltype == 'textbox') {
                    control.set_value('');
                }
                else {
                    control.clearSelection();
                    control.set_emptyMessage('- select an item -');
                }
            }
            function ShowHideFilter(ops) {

                var btnRemoveFilter = null;
                var btnAddFilter = null;
                var trID2;
                var trID3;
                if (ops == 'add') {
                    rowNum++;
                }
                if (ops == 'delete') {
                    rowNum--;
                }

                if ($("input:hidden[id$=hidOperation]").val() == 'Normal') {

                    btnRemoveFilter = $("input[id$=btnRemoveFilter]");
                    btnAddFilter = $("input[id$=btnAddFilter]");
                    trID2 = $("tr[id$=row2]")[0].id;
                    trID3 = $("tr[id$=row3]")[0].id;
                }
                else {
                    btnRemoveFilter = $("input[id$=btnRemoveFilterEdit]");
                    btnAddFilter = $("input[id$=btnAddFilterEdit]");

                    trID2 = $("tr[id$=rowEdit2]")[0].id;
                    trID3 = $("tr[id$=rowEdit3]")[0].id;
                }

                switch (rowNum) {
                    case 1:
                        btnRemoveFilter.attr("src", "Images/deleteFilter_inactive.png");
                        btnAddFilter.attr("src", "Images/addFilter_active.png");
                        break;
                    case 2:
                        btnRemoveFilter.attr("src", "Images/deleteFilter_active.png");
                        btnAddFilter.attr("src", "Images/addFilter_active.png");
                        break;
                    case 3:
                        btnRemoveFilter.attr("src", "Images/deleteFilter_active.png");
                        btnAddFilter.attr("src", "Images/addFilter_inactive.png");
                        break;
                    default:
                        break;
                }

                if (rowNum == 2 && ops == "add") {
                    rowID = trID2;
                }
                if (rowNum == 3 && ops == "add") {
                    rowID = trID3;
                }
                if (rowNum == 1 && ops == "delete") {
                    rowID = trID2;
                }
                if (rowNum == 2 && ops == "delete") {
                    rowID = trID3;
                }

                if (rowNum == 4) {
                    rowNum--;
                    $("input:hidden[id$=hidRowNum]").val(rowNum);
                    return;
                }
                if (rowNum == 0) {
                    rowNum++;
                    $("input:hidden[id$=hidRowNum]").val(rowNum);
                    return;
                }

                $("tr[id$=" + rowID + "]").toggle(700);

                $("input:hidden[id$=hidRowNum]").val(rowNum);
            }

            function setAudienceEditMode() {
                clearAudienceFieldsClient();
                document.getElementById("<%= hidOperation.ClientID %>").value = 'Editing';
                document.getElementById("<%= hidRowNum.ClientID %>").value = 1;
                rowNum = 1;
            }

            function EndEditMode() {
                document.getElementById("<%= hidOperation.ClientID %>").value = 'Normal';
                document.getElementById("<%= hidRowNum.ClientID %>").value = 1;
                rowNum = 1;
            }


            function ValidateControl(controlType, control) {
                if (controlType == "textbox") {
                    if (control.get_value().trim() != '') {
                        return 0;
                    }
                    else {
                        return 1;
                    }
                }
                else {
                    if (control.get_selectedItem().get_text() != '') {
                        return 0;
                    }
                    else {
                        return 1;
                    }
                }
            }

            function ValidateAudienceMemberValues(sender, args) {

                var ctr = 0;

                var editStr = '';

                if ($("input:hidden[id$=hidOperation]").val() == 'Editing') {
                    editStr = 'Edit';
                }

                var VarCbFilterType1 = $find($("[id$=cbFilterType" + editStr + "1]").attr("id"));
                var VarCbFilterType2 = $find($("[id$=cbFilterType" + editStr + "2]").attr("id"));
                var VarCbFilterType3 = $find($("[id$=cbFilterType" + editStr + "3]").attr("id"));

                var cbFilterType1 = $find('<%=cbFilterType1.ClientID %>');
                var cbFilterType2 = $find('<%=cbFilterType2.ClientID %>');
                var cbFilterType3 = $find('<%=cbFilterType3.ClientID %>');

                var cbFilterValue1 = $find('<%=cbFilterValue1.ClientID %>');
                var cbFilterValue2 = $find('<%=cbFilterValue2.ClientID %>');
                var cbFilterValue3 = $find('<%=cbFilterValue3.ClientID %>');

                var txtFilterValue1 = $find('<%=txtFilterValue1.ClientID %>');
                var txtFilterValue2 = $find('<%=txtFilterValue2.ClientID %>');
                var txtFilterValue3 = $find('<%=txtFilterValue3.ClientID %>');


                switch (rowNum) {
                    case 1:
                        if (cbFilterType1.get_selectedItem().get_value() != 4 || cbFilterType1.get_selectedItem().get_value() != 5) {
                            ctr = ctr + ValidateControl("combobox", cbFilterValue1);
                        }
                        else {
                            ctr = ctr + ValidateControl("textbox", txtFilterValue1);
                        }
                        break;
                    case 2:
                        if (cbFilterType1.get_selectedItem().get_value() != 4 || cbFilterType1.get_selectedItem().get_value() != 5) {
                            ctr = ctr + ValidateControl("combobox", cbFilterValue1);
                        }
                        else {
                            ctr = ctr + ValidateControl("textbox", txtFilterValue1);
                        }
                        if (cbFilterType2.get_selectedItem().get_value() != 4 || cbFilterType2.get_selectedItem().get_value() != 5) {
                            ctr = ctr + ValidateControl("combobox", cbFilterValue2);
                        }
                        else {
                            ctr = ctr + ValidateControl("textbox", txtFilterValue2);
                        }
                    case 3:
                        if (cbFilterType1.get_selectedItem().get_value() != 4 || cbFilterType1.get_selectedItem().get_value() != 5) {
                            ctr = ctr + ValidateControl("combobox", cbFilterValue1);
                        }
                        else {
                            ctr = ctr + ValidateControl("textbox", txtFilterValue1);
                        }
                        if (cbFilterType2.get_selectedItem().get_value() != 4 || cbFilterType2.get_selectedItem().get_value() != 5) {
                            ctr = ctr + ValidateControl("combobox", cbFilterValue2);
                        }
                        else {
                            ctr = ctr + ValidateControl("textbox", txtFilterValue2);
                        }
                        if (cbFilterType3.get_selectedItem().get_value() != 4 || cbFilterType3.get_selectedItem().get_value() != 5) {
                            ctr = ctr + ValidateControl("combobox", cbFilterValue3);
                        }
                        else {
                            ctr = ctr + ValidateControl("textbox", txtFilterValue3);
                        }
                        break;
                }
                if (ctr > 0) {
                    args.IsValid = false;
                }
                else {
                    args.IsValid = true;
                }
                return;
            }

            function CompareComboBoxes(sender, args) {
                var editStr = '';

                if ($("input:hidden[id$=hidOperation]").val() == 'Editing') {
                    editStr = 'Edit';
                }

                var VarCbFilterType1 = $find($("[id$=cbFilterType" + editStr + "1]").attr("id"));
                var VarCbFilterType2 = $find($("[id$=cbFilterType" + editStr + "2]").attr("id"));
                var VarCbFilterType3 = $find($("[id$=cbFilterType" + editStr + "3]").attr("id"));
                var Value1 = VarCbFilterType1.get_selectedItem().get_value();
                var Value2 = VarCbFilterType2.get_selectedItem().get_value();
                var Value3 = VarCbFilterType3.get_selectedItem().get_value();

                switch (rowNum) {
                    case 1:
                        args.IsValid = true;
                        break;
                    case 2:
                        if (Value1 != Value2) {
                            args.IsValid = true;
                        }
                        else {
                            args.IsValid = false;
                        }
                        break;
                    case 3:
                        if (Value1 != Value2 && Value1 != Value3 && Value2 != Value3) {
                            args.IsValid = true;
                        }
                        else {
                            args.IsValid = false;
                        }
                        break;
                }
                return;
            }

            function OnClientSelectedIndexChanged(sender, eventArgs) {
                id = sender.get_id();
                $("input:hidden[id$=hidlblFilterValue" + id.substring(id.length - 1, id.length) + "]").val('');
            }

            function DoValidation(validationGroup) {
                var isValid = Page_ClientValidate(validationGroup);
                if (isValid) {
                    if (validationGroup == 'vgEditAudienceMember') {
                        $("input:hidden[id$=hidOperation]").val('Updating');
                    }
                    else {
                        $("input:hidden[id$=hidOperation]").val('Saving');
                    }
                }
                return isValid;
            }

            function clearAudienceFieldsClient() {
                $("input:hidden[id$=hidlblFilterValue1]").val('');
                $("input:hidden[id$=hidlblFilterValue2]").val('');
                $("input:hidden[id$=hidlblFilterValue3]").val('');
            }

            function ValidateCIMAudience(sender, args) {
                var control;

                var radInput = sender;
                var Input = radInput.get_value();
                var clientID = sender.get_id();
                var controlNum = clientID.substr(clientID.length - 1);
                var editStr = '';

                if ($("input:hidden[id$=hidOperation]").val() != 'Normal') {
                    editStr = 'Edit';
                }
                var hidprevVal = $("input:hidden[id$=hidprevVal" + controlNum + "]");
                var control = $("[id$=lblFilterValueName" + editStr + controlNum + "]");
                var hidControl = $("input:hidden[id$=hidlblFilterValue" + controlNum + "]");

                if (Input != hidprevVal.val()) {
                    GetFullName(Input, control, hidControl);
                }

                hidprevVal.val() = Input;
            }

            function GetFullName(cim, control, hidControl) {
                $.ajax({
                    type: "POST",
                    url: "Admin.aspx/GetFullName",
                    data: '{CIM: "' + cim + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function OnSuccess(response) {

                        name = response.d;
                        if (name == 'Invalid CIM') {
                            name = '<span class="displayerror">' + name + '</span>';
                        }
                        hidControl.val(name);
                        control.html(name);

                    },
                    failure: function (response) {
                        name = 'Invalid CIM';
                        name = '<span class="displayerror">' + name + '</span>';
                        control.html(name);
                        hidControl.val(name);
                    }
                });
            }


            //List box Checkboxes------------------------------------------------------
            function OnClientSelectedIndexChanged(sender, eventArgs) {

                eventArgs.get_item().set_checked(eventArgs.get_item().get_selected());

            }

            function OnClientItemChecked(sender, eventArgs) {

                //                eventArgs.get_item().set_selected(eventArgs.get_item().get_checked());

                var item_g = eventArgs.get_item();
                item_g.set_selected(item_g.get_checked());

                var lstBoxControl;

                var ctr = 0;
                lstBoxControl = $find(sender.get_id());
                var count = lstBoxControl.get_items().get_count();
                var items = lstBoxControl.get_items();

                for (var i = 0; i < count; i++) {

                    if (items.getItem(i).get_checked() == true) {
                        items.getItem(i).set_selected(true);
                        ctr++;
                    }
                }

                if (count == ctr) {
                    if (sender.get_id().substr(sender.get_id().length - ('lbUnassignedCourses').length) == 'lbUnassignedCourses') {
                        $("input[id$=chkAllAssignCourse]").prop('checked', true);
                    }
                    else {
                        $("input[id$=chkAllPrerequisite]").prop('checked', true);
                    }
                }
                else {
                    if (sender.get_id().substr(sender.get_id().length - ('lbUnassignedCourses').length) == 'lbUnassignedCourses') {

                        $("input[id$=chkAllAssignCourse]").prop('checked', false);
                    }
                    else {
                        $("input[id$=chkAllPrerequisite]").prop('checked', false);
                    }
                }

            }

            function OnTransferring(sender, eventArgs) {

                var lstBoxControl;
                lstBoxControl = $find(sender.get_id());

                if (sender.get_id().substr(sender.get_id().length - ('lbUnassignedCourses').length) == 'lbUnassignedCourses') {
                    $("input[id$=chkAllAssignCourse]").prop('checked', false);
                }
                else {
                    $("input[id$=chkAllPrerequisite]").prop('checked', false);
                }

                var items = lstBoxControl.get_items();

                for (var i = 0; i < lstBoxControl.get_items().get_count(); i++) {

                    if (items.getItem(i).get_checked() == true) {
                        items.getItem(i).set_selected(true);
                        items.getItem(i).set_checked(false)
                    }
                }
            }


            function chk(Event) {

                var chkctl;
                var lstBoxControl;
                if (Event == 'AssignCourse') {
                    chkctl = $("input[id$=chkAllAssignCourse]")[0].checked;
                    lstBoxControl = $find($("[id$=lbUnassignedCourses]").attr("id"));
                }
                else {
                    chkctl = $("input[id$=chkAllPrerequisite]")[0].checked;
                    lstBoxControl = $find($("[id$=lbAvailableCourses]").attr("id"));
                }
                var items = lstBoxControl.get_items();

                for (var i = 0; i < lstBoxControl.get_items().get_count(); i++) {
                    items.getItem(i).set_selected(chkctl);
                }
            }

            //---------------------------------------------------

            function autoSizeWithCalendar(oWindow) {
                var iframe = oWindow.get_contentFrame();
                var body = iframe.contentWindow.document.body;

                var height = body.scrollHeight;
                var width = body.scrollWidth;

                var iframeBounds = $telerik.getBounds(iframe);
                var heightDelta = height - iframeBounds.height;
                var widthDelta = width - iframeBounds.width;

                if (heightDelta > 0) oWindow.set_height(oWindow.get_height() + heightDelta);
                if (widthDelta > 0) oWindow.set_width(oWindow.get_width() + widthDelta);
                oWindow.center();
            }

            function chkprerequisite() {


                if ($("input:hidden[id$=ctrPrereq]").val() != "0") {
                    var text = "Course/s selected have prerequisites.</br>Do you want to continue?";
                    return radconfirm(text, callbackFn, 350, 180, null, "Course with Prerequisite");
                }
                else {
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("AddAssignCourse");
                }
            }

            function callbackFn(args) {
                if (args == true) {
                    $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest("AddAssignCourse");
                }
            }

            function confirmCallBackFn(arg) {
                if (arg == true) {
                    chkprerequisite();
                }
            }

            //close radwindow
            var from;
            function onBeforeClose(sender, arg) {
                function callbackFunction(arg) {
                    if (arg) {
                        sender.remove_beforeClose(onBeforeClose);
                        sender.close();
                    }
                }
                arg.set_cancel(true);
                if (from == '1') {
                    sender.remove_beforeClose(onBeforeClose);
                    sender.close();
                }
                else {
                    radconfirm("Are you sure you want to close this window?", callbackFunction, 350, 180, null, "Close Window");
                }
            }
            function View_UploadWindow1(sender, args) {
                var oWindow = window.radopen(null, "windowUpload1");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }
            function Close_UploadWindow1(from1) {
                from = from1;
                var window = $find('<%=windowUpload1.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);
            }
            function View_UploadWindow2(sender, args) {
                var oWindow = window.radopen(null, "windowUpload2");
                oWindow.add_beforeClose(onBeforeClose);
                oWindow.center();
            }
            function Close_UploadWindow2(from1) {
                from = from1;
                var window = $find('<%=windowUpload2.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 100);
            }

            Telerik.Web.UI.RadAsyncUpload.prototype.getUploadedFiles = function () {
                var files = [];
                // debugger;
                $telerik.$(".ruUploadSuccess", this.get_element()).each(function (index, value) {
                    files[index] = $telerik.$(value).text();
                });

                return files;
            }



            function OnClientSelectedIndexChanged(sender, eventArgs) {
                var item = eventArgs.get_item();
                //alert(item.get_text());
                //alert(item.get_value());
                //sender.set_text("You selected " + item.get_text());
                if (item.get_value() == "1") {
                    View_UploadWindow1("1");
                    Close_UploadWindow2("1");

                    //item.selectedIndex = 0;
                    //var combo1 = $find("<%=  cbUploadType1.ClientID %>");
                    //combo1.clearSelection();
                    // $("cbUploadType1").val('0');
                    //$("cbUploadType2").val('1');
                    //combo1.SelectedIndex = "0";
                }
                if (item.get_value() == "2") {
                    View_UploadWindow2("1");
                    Close_UploadWindow1("1");

                    //item.selectedIndex = 1;
                    //var combo2 = $find("<%=  cbUploadType2.ClientID %>");
                    //combo2.clearSelection();
                    //$("cbUploadType2").val('1');
                    //$("cbUploadType1").val('0');
                    //combo1.SelectedIndex = "1" ;
                }
            }
            
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridCareerPath">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridCareerPath" />
                    <telerik:AjaxUpdatedControl ControlID="tsCoursesOnCareerPath" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridCareerPathCourses">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridCareerPathCourses" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tsMenu">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="mpAdmin" />
                    <telerik:AjaxUpdatedControl ControlID="tsMenu" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tsCoursesOnCareerPath">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="mpCoursesOnCareerPath" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAddCareerCourse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="tsCoursesOnCareerPath" />
                    <telerik:AjaxUpdatedControl ControlID="mpCoursesOnCareerPath" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAddSubCategory">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridSubCategory">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="hidCategoryId" />
                    <telerik:AjaxUpdatedControl ControlID="cbCategory_onEdit" />
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbCategory_onEdit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="hidCategoryId" />
                    <telerik:AjaxUpdatedControl ControlID="ddtParentSubcategory_onEdit" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--<telerik:AjaxSetting AjaxControlID="gridCourse">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridCourse" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="gridExternalUsers">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridExternalUsers" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAddExternalUser">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddtSubCategoryOnCourses">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <%--<telerik:AjaxUpdatedControl ControlID="gridCourse" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbCategoryOnCourses">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <%--<telerik:AjaxUpdatedControl ControlID="gridCourse" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--FOR CLASS NAME MAINTENANCE--%>
            <telerik:AjaxSetting AjaxControlID="gridClasses">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridClasses" />
                    <%--<telerik:AjaxUpdatedControl ControlID="lblNote" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAddClass">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                    <%--<telerik:AjaxUpdatedControl ControlID="lblNote" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--END CLASS NAME MAINTENANCE--%>
            <%--FOR PREREQUISITE--%>
            <%--          <telerik:AjaxSetting AjaxControlID="btnPrerequistAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbAvailableCourses" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lbSelectedCourses" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="lbAvailableCourses">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbAvailableCourses" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lbSelectedCourses" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAddCoursePreReq">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="CoursePrerequisiteWindow" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbAvailableCourses" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="lbSelectedCourses" />
                    <telerik:AjaxUpdatedControl ControlID="lbUnassignedCourses" />
                    <telerik:AjaxUpdatedControl ControlID="lblCourseUnassigned" />
                    <telerik:AjaxUpdatedControl ControlID="txtAssignTo" />
                    <telerik:AjaxUpdatedControl ControlID="lbAssignedCourses" />
                    <telerik:AjaxUpdatedControl ControlID="gridAssignedCourses" />
                    <telerik:AjaxUpdatedControl ControlID="AssignCourseWindow" />
                    <telerik:AjaxUpdatedControl ControlID="Panel3" />
                    <telerik:AjaxUpdatedControl ControlID="Panel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridCourse" />
                    <%--SCORM--%>
                    <%--<telerik:AjaxUpdatedControl ControlID="PanelDock" />
                    <telerik:AjaxUpdatedControl ControlID="RadDockLayout1" />
                    <telerik:AjaxUpdatedControl ControlID="RadDockZone1" />--%>
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--END PREREQUISITE--%>
            <%--FOR ASSIGN COURSE--%>
            <telerik:AjaxSetting AjaxControlID="lbUnassignedCourses">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbAssignedCourses" />
                    <telerik:AjaxUpdatedControl ControlID="lbUnassignedCourses" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbAssignTo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lbUnassignedCourses" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridAssignedCourses" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAssign">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AssignCourseWindow" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="Panel3" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--END ASSIGN COURSE--%>
            <%-- AUDIENCE --%>
            <telerik:AjaxSetting AjaxControlID="gridAudience">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridAudienceMembersCIMList" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--END ASSIGN COURSE--%>
            <%-- AUDIENCE --%>
            <telerik:AjaxSetting AjaxControlID="gridAudience">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridAudienceMembersCIMList" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridAudienceMembersCIMList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridAudienceMembersCIMList" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%--END AUDIENCE--%>
            <%--<telerik:AjaxSetting AjaxControlID="btnAddNewPackage">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divPackageControls" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <%--SCORM--%>
            <%--<telerik:AjaxSetting AjaxControlID="cbUploadType1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="panelUpload1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbUploadType2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="panelUpload2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>
            <telerik:AjaxSetting AjaxControlID="btnUpload1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="panelUpload1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnUpload2">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="panelUpload2" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbUploadScormCategory">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="panelUpload2" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbUploadScormSubCategory">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="panelUpload2" />
                    <telerik:AjaxUpdatedControl ControlID="gridScormCourse" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="panelUpload2" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridScormCourse" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="dialogWindow" runat="server" Title="Enter Course Name" Behaviors="Close, Move"
                ShowContentDuringLoad="False" VisibleStatusbar="False" Modal="True" NavigateUrl="~/dialogs/SaveDialog.aspx"
                Width="300px" Animation="Fade" OnClientAutoSizeEnd="autoSizeWithCalendar" />
            <telerik:RadWindow runat="server" Width="550px" Height="560px" VisibleStatusbar="false"
                ShowContentDuringLoad="false" NavigateUrl="~/dialogs/Explorer.aspx" ID="ExplorerWindow"
                Modal="true" Behaviors="Close,Move" Animation="Fade" />
            <telerik:RadWindow ID="CoursePrerequisiteWindow" runat="server" Title="Select Course/s"
                Width="700px" Height="530px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                ReloadOnShow="true" VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <asp:Panel ID="Panel2" runat="server">
                        <div class="insidecontainer2">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div style="background: #e4e4e4; height: 23px; border-style: solid; border-color: #C6C7D2;
                                            border-width: 1px 1px 0 1px">
                                            <asp:CheckBox runat="server" ID="chkAllPrerequisite" Text="All" Checked="false" OnClick="chk('Prerequisite');"
                                                CssClass="lbChk" />
                                        </div>
                                        <telerik:RadListBox ID="lbAvailableCourses" runat="server" Height="200px" Width="100%"
                                            AllowTransfer="true" AutoPostBackOnTransfer="true" TransferMode="Move" EmptyMessage="No Available Courses"
                                            SelectionMode="Multiple" TransferToID="lbSelectedCourses" OnTransferred="lbAvailableCourses_Transferred"
                                            OnInserted="lbAvailableCourses_Inserted" OnClientItemChecked="OnClientItemChecked"
                                            OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" OnClientTransferring="OnTransferring"
                                            CheckBoxes="true">
                                            <ButtonSettings Position="Bottom" HorizontalAlign="Center" RenderButtonText="true" />
                                        </telerik:RadListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadListBox ID="lbSelectedCourses" runat="server" Height="200px" Width="100%"
                                            AutoPostBackOnTransfer="true" EmptyMessage="No Selected Courses">
                                            <ButtonSettings ShowDelete="false" ShowReorder="false" />
                                        </telerik:RadListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <asp:LinkButton runat="server" ID="btnPrerequistAdd" Text="Add" CssClass="linkBtn"
                                            OnClick="btnPrerequistAdd_Click" />
                                        |
                                        <asp:LinkButton runat="server" ID="btnPrerequisiteCancel" Text="Cancel" CssClass="linkBtn"
                                            CausesValidation="False" OnClick="btnPrerequisiteCancel_Click" />
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="AssignCourseWindow" runat="server" Title="Select Course/s"
                Width="700px" Height="530px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                ReloadOnShow="true" VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <asp:Panel ID="Panel4" runat="server" BackColor="White" CssClass="childPanel" DefaultButton="btnAssignCourseAdd">
                        <div class="insidecontainer2">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <div style="background: #e4e4e4; height: 23px; border-style: solid; border-color: #C6C7D2;
                                            border-width: 1px 1px 0 1px">
                                            <asp:CheckBox runat="server" ID="chkAll" Text="All" Checked="false" OnClick="JavaScript:chk();"
                                                CssClass="lbChk" />
                                        </div>
                                        <telerik:RadListBox ID="lbUnassignedCourses" runat="server" Height="200px" Width="100%"
                                            EmptyMessage="No Unassigned Course" TransferToID="lbAssignedCourses" CheckBoxes="true"
                                            AllowTransfer="true" AutoPostBackOnTransfer="true" TransferMode="Move" SelectionMode="Multiple"
                                            OnItemDataBound="lbUnassignedCourses_ItemDataBound" OnTransferred="lbUnassignedCourses_Transferred"
                                            OnInserted="lbUnassignedCourses_Inserted" OnClientItemChecked="OnClientItemChecked"
                                            OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" OnClientTransferring="OnTransferring"
                                            DataKeyField="Prerequisite">
                                            <ButtonSettings Position="Bottom" HorizontalAlign="Center" RenderButtonText="true" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblCourseUnassigned" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"Title\"]")%>'></asp:Label>
                                            </ItemTemplate>
                                        </telerik:RadListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <telerik:RadListBox ID="lbAssignedCourses" runat="server" Height="200px" Width="100%"
                                            AutoPostBackOnTransfer="true" EmptyMessage="No Course Selected" CheckBoxes="false">
                                            <ButtonSettings ShowDelete="false" ShowReorder="false" />
                                            <HeaderTemplate>
                                                <table width="100%" id="tbllbAssignedCourses1" runat="server">
                                                    <tr>
                                                        <td width="60%" align="center" id="td1" runat="server">
                                                            <asp:Label ID="lblCourseName" runat="server" Text="Course Name" Font-Size="9pt"></asp:Label>
                                                        </td>
                                                        <td width="25%" align="center" id="td2" runat="server">
                                                            <asp:Label ID="lblDueDate" runat="server" Text="Due Date" Font-Size="9pt"></asp:Label>
                                                        </td>
                                                        <td width="15%" align="left" id="td3" runat="server">
                                                            <asp:Label ID="lblPrerequisite" runat="server" Text="Prerequisite?" Font-Size="9pt"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table width="100%" runat="server" id="tbllbAssignedCourses2">
                                                    <tr>
                                                        <td width="60%" align="center" id="td1" runat="server">
                                                            <asp:Label ID="lblCourseAssigned" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"Title\"]")%>'></asp:Label>
                                                        </td>
                                                        <td width="30%" align="center" id="td2" runat="server">
                                                            <telerik:RadDatePicker ID="dpCourseAssigned" runat="server" EnableTyping="False"
                                                                SelectedDate='<%# DateTime.Today.AddMonths(1) %>' ShowPopupOnFocus="True" Width="140">
                                                                <DateInput ID="DateInput3" DateFormat="MMMM dd, yyyy" runat="server" Enabled="true" />
                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                        <td width="10%" runat="server" align="center" id="td3">
                                                            <asp:Label ID="lblAssignedPrerequisite" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"Prerequisite\"]")%>'></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </telerik:RadListBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tblfield">
                                        <asp:LinkButton runat="server" ID="btnAssignCourseAdd" Text="Add" CssClass="linkBtn"
                                            OnClientClick="radconfirm('Are you sure you want to add this collection?', confirmCallBackFn, 350, 180, null, 'Close Window'); return false;" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowAudienceMembers" runat="server" Width="650px" Title="Members"
                Height="400px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <div align="right" style="padding: 5px 10px 5px 10px">
                        <asp:LinkButton runat="server" ID="btnExportExcelAudienceMembersCIMList" CssClass="linkBtn"
                            OnClick="btnExportExcel_Click" Height="100%" OnClientClick="if(!confirm('Are you sure you want to export audience member list to excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                    </div>
                    <telerik:RadGrid runat="server" ID="gridAudienceMembersCIMList" AutoGenerateColumns="False"
                        GridLines="None" AllowFilteringByColumn="True" EnableLinqExpressions="true" Skin="Metro"
                        AllowPaging="True" PageSize="15" AllowSorting="True" OnNeedDataSource="gridAudienceMembersCIMList_NeedDataSource"
                        Width="100%" BorderStyle="None" CssClass="RadGrid1">
                        <GroupingSettings CaseSensitive="False" />
                        <FilterItemStyle HorizontalAlign="Center" />
                        <MasterTableView ShowHeader="True" AutoGenerateColumns="False" NoMasterRecordsText="No Audience Members."
                            EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                            HeaderStyle-CssClass="RadWindowGrid" Width="100%" BorderStyle="None" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                            <PagerStyle AlwaysVisible="True" Mode="Slider" />
                            <Columns>
                                <telerik:GridBoundColumn UniqueName="CIMNumber" HeaderText="CIM Number" DataField="CIMNumber"
                                    ShowFilterIcon="false" AllowFiltering="false" />
                                <telerik:GridBoundColumn UniqueName="Name" HeaderText="Name" DataField="Name" ShowFilterIcon="false"
                                    AllowFiltering="false" />
                            </Columns>
                        </MasterTableView>
                        <ClientSettings EnableRowHoverStyle="True">
                            <Selecting AllowRowSelect="True" />
                        </ClientSettings>
                    </telerik:RadGrid>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowUpload1" runat="server" Title="Upload SCO" Behaviors="Close, Move"
                DestroyOnClose="True" ShowContentDuringLoad="True" VisibleStatusbar="False" Modal="True"
                ReloadOnShow="True" Width="280" Height="190">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="panelUpload1">
                        <div class="insidecontainer2">
                            <div style="padding-bottom: 10px">
                                <telerik:RadComboBox runat="server" ID="cbUploadType1" EmptyMessage="- upload type -"
                                    OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" Width="130">
                                    <Items>
                                        <telerik:RadComboBoxItem runat="server" Text="Upload New" Value="1" Selected="True" />
                                        <telerik:RadComboBoxItem runat="server" Text="Add from Catalog" Value="2" />
                                    </Items>
                                </telerik:RadComboBox>
                                <hr />
                            </div>
                            <div runat="server" id="div3">
                                <uc1:UploadContent ID="UploadContent" runat="server" />
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowUpload2" runat="server" Title="Upload SCO" Behaviors="Close, Move"
                DestroyOnClose="True" ShowContentDuringLoad="True" VisibleStatusbar="False" Modal="True"
                ReloadOnShow="True" Width="750" Height="500">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="panelUpload2">
                        <div class="insidecontainer2">
                            <div style="padding-bottom: 10px">
                                <telerik:RadComboBox runat="server" ID="cbUploadType2" EmptyMessage="- upload type -"
                                    OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" Width="130">
                                    <Items>
                                        <telerik:RadComboBoxItem runat="server" Text="Upload New" Value="1" />
                                        <telerik:RadComboBoxItem runat="server" Text="Add from Catalog" Value="2" Selected="True" />
                                    </Items>
                                </telerik:RadComboBox>
                                <hr />
                            </div>
                            <div runat="server" id="divCatalog">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <telerik:RadComboBox ID="cbUploadScormCategory" DataTextField="Category" DataValueField="CategoryID"
                                                runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="CbUploadScormCategorySelectedIndexChanged"
                                                AppendDataBoundItems="true" DropDownAutoWidth="Enabled" MaxHeight="300" AllowCustomText="False"
                                                CausesValidation="False">
                                            </telerik:RadComboBox>
                                            <telerik:RadDropDownTree runat="server" ID="cbUploadScormSubCategory" DefaultMessage="- select subcategory -"
                                                DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                DataFieldParentID="ParentSubcategoryId" TextMode="Default" AutoPostBack="True"
                                                OnEntryAdded="cbUploadScormSubCategory_EntryAdded" OnClientEntryAdding="OnClientEntryAdding"
                                                Skin="Default">
                                                <DropDownSettings Width="400px" Height="250px" />
                                            </telerik:RadDropDownTree>
                                        </td>
                                        <td id="tdAdminSearch" runat="server" style="width: 154px" align="right">
                                            <telerik:RadTextBox ID="txtSearch" runat="server" EmptyMessage="- search key -" Width="150px"
                                                SelectionOnFocus="SelectAll" />
                                        </td>
                                        <td id="tdBtnSearch" runat="server" style="width: 30px" align="right">
                                            <telerik:RadButton ID="btnSearch" runat="server" CssClass="btnSearchImage" OnClick="btnSearch_Click">
                                                <Image EnableImageButton="true" />
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <telerik:RadGrid runat="server" ID="gridScormCourse" AutoGenerateColumns="False"
                                                GridLines="None" OnNeedDataSource="GridScormCourseNeedDataSource" EnableLinqExpressions="True"
                                                AllowSorting="True" AllowFilteringByColumn="False" CssClass="RadGrid1" Skin="Metro"
                                                Height="320px">
                                                <FilterItemStyle HorizontalAlign="Center" />
                                                <ClientSettings EnableRowHoverStyle="True">
                                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" EnableVirtualScrollPaging="false" />
                                                    <Selecting AllowRowSelect="True" />
                                                </ClientSettings>
                                                <MasterTableView ShowHeader="true" AutoGenerateColumns="False" DataKeyNames="CourseID,EncryptedCourseID,EnrolmentRequired,Title,Description"
                                                    NoMasterRecordsText="Course list is empty. Please select a category and subcategory."
                                                    EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" TableLayout="Auto"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                                    <Columns>
                                                        <%--  <telerik:GridTemplateColumn HeaderStyle-Width="30px">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="CheckBoxColumn" runat="server" />
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>--%>
                                                        <telerik:GridBoundColumn UniqueName="CourseID" HeaderText="CourseID" DataField="CourseID"
                                                            AllowFiltering="False" SortExpression="CourseID" Display="False" />
                                                        <telerik:GridBoundColumn UniqueName="Title" HeaderText="Title" DataField="Title"
                                                            AllowFiltering="False" SortExpression="Title" />
                                                        <telerik:GridBoundColumn UniqueName="Description" HeaderText="Description" DataField="Description"
                                                            AllowFiltering="False" SortExpression="Description" />
                                                        <telerik:GridBoundColumn UniqueName="Duration" HeaderText="Duration (min)" DataField="Duration"
                                                            AllowFiltering="False" SortExpression="Duration" HeaderStyle-Width="100" />
                                                        <telerik:GridBoundColumn UniqueName="TargetAudience" HeaderText="Target Audience"
                                                            DataField="TargetAudience" AllowFiltering="False" HeaderStyle-Width="130" SortExpression="TargetAudience" />
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="managecontrol3" colspan="3" align="right">
                                            <div style="width: 100%" align="right">
                                                <br />
                                                <asp:LinkButton runat="server" ID="btnUpload2" Text="Select" CausesValidation="True"
                                                    Font-Size="8" ValidationGroup="upload2" OnClick="btnUpload2_OnClick" OnClientClick="OnClientSelectedIndexChanged" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div id="apDivAdminPageBody" class="divBorder">
                <div class="containerdefault">
                    <div style="padding: 10px 10px 0 10px">
                        <telerik:RadTabStrip ID="tsMenu" runat="server" MultiPageID="mpAdmin" SelectedIndex="0"
                            AutoPostBack="True" ShowBaseLine="True" Width="100%" CausesValidation="False">
                            <Tabs>
                                <telerik:RadTab Text="Category" Selected="True" />
                                <telerik:RadTab Text="Subcategory" />
                                <telerik:RadTab Text="Course" />
                                <telerik:RadTab Text="Career Path" />
                                <telerik:RadTab Text="External Users" />
                                <telerik:RadTab Text="Page Editor" />
                                <telerik:RadTab Text="Audience" />
                                <telerik:RadTab Text="Class Name" />
                                <telerik:RadTab Text="Assign Course" />
                            </Tabs>
                        </telerik:RadTabStrip>
                    </div>
                    <div class="insidecontainer">
                        <telerik:RadMultiPage ID="mpAdmin" runat="server" SelectedIndex="1" RenderSelectedPageOnly="true"
                            Width="100%">
                            <telerik:RadPageView ID="pvCategory" runat="server" Selected="true">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <telerik:RadTabStrip ID="tsCategory" runat="server" SelectedIndex="0" MultiPageID="mpCategory"
                                                Width="100%" CausesValidation="False">
                                                <Tabs>
                                                    <telerik:RadTab Text="Category List" Selected="true" />
                                                    <telerik:RadTab Text="Add Category" />
                                                </Tabs>
                                            </telerik:RadTabStrip>
                                        </td>
                                        <td>
                                            <div runat="server" id="divClassExport" align="right">
                                                <asp:LinkButton runat="server" ID="btnExportExcelCategory" CssClass="linkBtn" OnClick="btnExportExcel_Click"
                                                    Height="100%" OnClientClick="if(!confirm('Are you sure you want to export category list to excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <telerik:RadMultiPage runat="server" ID="mpCategory" SelectedIndex="0" Width="100%">
                                    <telerik:RadPageView runat="server" ID="pvCategoryList" Selected="True">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <telerik:RadGrid runat="server" ID="gridCategory" AutoGenerateColumns="False" GridLines="None"
                                                    OnNeedDataSource="GridCategoryNeedDataSource" OnItemCommand="GridCategoryItemCommand"
                                                    CssClass="RadGrid1" Skin="Metro" AllowSorting="True" AllowFilteringByColumn="True">
                                                    <GroupingSettings CaseSensitive="false" />
                                                    <FilterItemStyle HorizontalAlign="Center" />
                                                    <MasterTableView ShowHeader="true" AutoGenerateColumns="False" DataKeyNames="CategoryID"
                                                        PageSize="10" EditMode="InPlace" EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True"
                                                        AllowPaging="True" TableLayout="Auto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        AlternatingItemStyle-HorizontalAlign="Center">
                                                        <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                        <Columns>
                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                HeaderStyle-Width="70px" EditImageUrl="Images/editpen2.png">
                                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                            </telerik:GridEditCommandColumn>
                                                            <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                                            </telerik:GridButtonColumn>
                                                            <telerik:GridBoundColumn UniqueName="CategoryID" HeaderText="Category ID" DataField="CategoryID"
                                                                SortExpression="CategoryID" AllowFiltering="False" ReadOnly="True">
                                                                <HeaderStyle ForeColor="Silver" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridTemplateColumn UniqueName="Category" HeaderText="Category" DataField="Category"
                                                                SortExpression="Category" EditFormHeaderTextFormat="Category name" AllowFiltering="True"
                                                                FilterControlWidth="150" CurrentFilterFunction="Contains" ShowFilterIcon="False"
                                                                AutoPostBackOnFilter="True">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblDefinition" Text='<%# Eval("Category") %>' />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <telerik:RadTextBox runat="server" ID="txtCategory" Text='<%# Bind("Category") %>'
                                                                        EmptyMessage="- category name -" Width="300" SelectionOnFocus="SelectAll" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                                        ControlToValidate="txtCategory" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" />
                                                                </EditItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridBoundColumn UniqueName="UpdatedBy" HeaderText="Updated By" DataField="UpdatedBy"
                                                                SortExpression="UpdatedBy" AllowFiltering="False" ReadOnly="True" />
                                                            <telerik:GridBoundColumn UniqueName="DateTimeUpdated" HeaderText="Date Updated" DataField="DateTimeUpdated"
                                                                SortExpression="DateTimeUpdated" AllowFiltering="False" ReadOnly="True" />
                                                        </Columns>
                                                    </MasterTableView>
                                                    <ClientSettings EnableRowHoverStyle="True">
                                                        <Selecting AllowRowSelect="True" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView runat="server" ID="pvAddCategory">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <telerik:RadTextBox runat="server" ID="txtAddCategory" Text="" EmptyMessage="- category name -"
                                                    Width="300" ValidationGroup="addCateg" SelectionOnFocus="SelectAll" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                                    ControlToValidate="txtAddCategory" ValidationGroup="addCateg" SetFocusOnError="True"
                                                    Display="Dynamic" CssClass="displayerror" />
                                                <telerik:RadButton runat="server" ID="btnAddCategory" Text="Add" OnClick="BtnAddCategoryClick"
                                                    ValidationGroup="addCateg" Font-Names="Helvetica" />
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvSubCategory" runat="server">
                                <telerik:RadTabStrip ID="tsSubCategory" runat="server" SelectedIndex="0" MultiPageID="mpSubCategory"
                                    Width="100%" CausesValidation="False">
                                    <Tabs>
                                        <telerik:RadTab Text="Subcategory List" Selected="true" />
                                        <telerik:RadTab Text="Add Subcategory" />
                                    </Tabs>
                                </telerik:RadTabStrip>
                                <telerik:RadMultiPage runat="server" ID="mpSubCategory" SelectedIndex="0" Width="100%">
                                    <telerik:RadPageView runat="server" ID="pvSubCategoryList" Selected="True">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <div runat="server" id="divEditNote" visible="False" style="padding-bottom: 5px">
                                                    <label style="font-size: 90%; color: red">
                                                        <strong>Editing note</strong>: When moving Subcategory to different Category please
                                                        make sure that is has no 'child category' or else you might lose it's child data.
                                                        If the subcategory has a child or it has another level of child, please move/edit
                                                        the last level of child first. You can detach child subcategory from parent subcategory
                                                        by selecting '- Select None -' on Parent Subcategory dropdown and it will become
                                                        a parent subcategory on selected Category.</label>
                                                </div>
                                                <div style="padding-bottom: 5px">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <telerik:RadComboBox ID="cbCategoryOnSubCat" DataTextField="Category" DataValueField="CategoryID"
                                                                    runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="CbCategoryOnSubCatSelectedIndexChanged"
                                                                    DropDownAutoWidth="Enabled" MaxHeight="300px" CausesValidation="False" />
                                                            </td>
                                                            <td align="right" style="line-height: 20px; vertical-align: bottom">
                                                                <div style="vertical-align: bottom">
                                                                    <asp:LinkButton runat="server" ID="btnExportExcelSubCategory" CssClass="linkBtn"
                                                                        OnClick="btnExportExcel_Click" Height="100%" OnClientClick="if(!confirm('Are you sure you want to export subcategory list to excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div>
                                                    <telerik:RadTreeList ID="gridSubCategory" runat="server" OnNeedDataSource="GridSubCategoryNeedDataSource"
                                                        ParentDataKeyNames="ParentSubcategoryId" DataKeyNames="SubcategoryId" AutoGenerateColumns="false"
                                                        OnItemCommand="gridSubCategory_ItemCommand" OnItemDataBound="gridSubCategory_ItemDataBound"
                                                        ShowTreeLines="True" Width="100%" OnItemCreated="gridSubCategory_ItemCreated"
                                                        EditMode="InPlace" CssClass="RadGrid1" Skin="Default" GridLines="None">
                                                        <HeaderStyle CssClass="gridheader" />
                                                        <Columns>
                                                            <telerik:TreeListEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                HeaderStyle-Width="70px" ShowAddButton="False" EditImageUrl="Images/editpen2.png">
                                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                            </telerik:TreeListEditCommandColumn>
                                                            <telerik:TreeListButtonColumn ButtonType="ImageButton" CommandName="Delete" Text="Delete"
                                                                UniqueName="DeleteColumn" HeaderStyle-Width="50px" ImageUrl="~/Images/deletesmall.png"
                                                                ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record" ConfirmText="Are you sure you want to delete this record?"
                                                                ConfirmDialogHeight="180" ConfirmDialogWidth="350">
                                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                            </telerik:TreeListButtonColumn>
                                                            <telerik:TreeListBoundColumn DataField="SubcategoryId" UniqueName="SubcategoryId"
                                                                HeaderText="Subcategory ID" ReadOnly="True" Display="False" />
                                                            <telerik:TreeListTemplateColumn DataField="Subcategory" UniqueName="Subcategory"
                                                                HeaderText="Subcategory" EditFormHeaderTextFormat="Subcategory name">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblDefinition" Text='<%# Eval("Subcategory") %>' />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <telerik:RadTextBox runat="server" ID="txtSubCategory" Text='<%# Bind("Subcategory") %>'
                                                                        EmptyMessage="- subcategory name -" SelectionOnFocus="SelectAll" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                                                                        ControlToValidate="txtSubCategory" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" />
                                                                </EditItemTemplate>
                                                            </telerik:TreeListTemplateColumn>
                                                            <telerik:TreeListTemplateColumn DataField="CategoryID" UniqueName="CategoryID" HeaderText="Category ID"
                                                                EditFormHeaderTextFormat="Category name" HeaderStyle-Width="120" Display="False">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblCategoryID" Text='<%# Eval("CategoryID") %>' />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <telerik:RadComboBox runat="server" ID="cbCategory_onEdit" DataTextField="Category"
                                                                        DataValueField="CategoryID" EmptyMessage="- select category -" CausesValidation="False"
                                                                        DropDownAutoWidth="Enabled" MaxHeight="300px" MarkFirstMatch="True" ChangeTextOnKeyBoardNavigation="True"
                                                                        HighlightTemplatedItems="True" AllowCustomText="False" />
                                                                </EditItemTemplate>
                                                            </telerik:TreeListTemplateColumn>
                                                            <telerik:TreeListTemplateColumn DataField="ParentSubcategoryId" UniqueName="ParentSubcategoryId"
                                                                HeaderText="Parent Subcategory ID" EditFormHeaderTextFormat="Parent Subcategory name"
                                                                Display="False" HeaderStyle-Width="320px">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblParentSubcategoryId" Text='<%# Eval("ParentSubcategoryId") %>' />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <telerik:RadDropDownTree runat="server" ID="ddtParentSubcategory_onEdit" DefaultMessage="- select parent -"
                                                                        DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                                        DataFieldParentID="ParentSubcategoryId" TextMode="Default" Width="300px" OnClientEntryAdding="OnClientEntryAdding"
                                                                        Skin="Default">
                                                                        <DropDownSettings Width="300px" Height="200px" />
                                                                    </telerik:RadDropDownTree>
                                                                </EditItemTemplate>
                                                            </telerik:TreeListTemplateColumn>
                                                            <telerik:TreeListBoundColumn DataField="UpdatedBy" UniqueName="UpdatedBy" HeaderText="Updated By"
                                                                ReadOnly="True" />
                                                            <telerik:TreeListBoundColumn DataField="DateTimeUpdated" UniqueName="DateTimeUpdated"
                                                                HeaderText="Date Updated" ReadOnly="True" />
                                                        </Columns>
                                                    </telerik:RadTreeList>
                                                </div>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView runat="server" ID="pvAddSubCategory">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadComboBox ID="cbAddCategoryOnSubCat" DataTextField="Category" DataValueField="CategoryID"
                                                                runat="server" EmptyMessage="- select category -" DropDownAutoWidth="Enabled"
                                                                MaxHeight="300" AllowCustomText="False" AutoPostBack="True" OnSelectedIndexChanged="CbAddCategoryOnSubCatSelectedIndexChanged" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                                                ControlToValidate="cbAddCategoryOnSubCat" ValidationGroup="addSubCateg" SetFocusOnError="True"
                                                                Display="Dynamic" CssClass="displayerror" />
                                                        </td>
                                                    </tr>
                                                    <tr runat="server" id="trParent" visible="False">
                                                        <td>
                                                            <telerik:RadDropDownTree runat="server" ID="ddtAddSubcategoryOnSubCat" DefaultMessage="- select subcategory -"
                                                                DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                                DataFieldParentID="ParentSubcategoryId" TextMode="Default" Width="400px" OnClientEntryAdding="OnClientEntryAdding"
                                                                AutoPostBack="True" Skin="Default">
                                                                <DropDownSettings Width="400px" Height="250px" />
                                                            </telerik:RadDropDownTree>
                                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="validatePage1"
                                                                ErrorMessage="*" Display="Dynamic" CssClass="displayerror" ValidationGroup="addSubCateg"
                                                                SetFocusOnError="True" />
                                                            <asp:LinkButton runat="server" ID="btnCancelParent" Text="Cancel" CausesValidation="False"
                                                                OnClick="btnCancelParent_Click" Visible="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <telerik:RadTextBox runat="server" ID="txtAddSubCategory" Text="" EmptyMessage="- subcategory name -"
                                                                Width="250" ValidationGroup="addSubCateg" SelectionOnFocus="SelectAll" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                                                                ControlToValidate="txtAddSubCategory" ValidationGroup="addSubCateg" SetFocusOnError="True"
                                                                Display="Dynamic" CssClass="displayerror" />
                                                            <asp:LinkButton runat="server" ID="btnAddParent" Text="select a parent" OnClick="btnAddParent_Click"
                                                                ValidationGroup="addSubCateg" CssClass="linkBtn" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <telerik:RadButton runat="server" ID="btnAddSubCategory" Text="Add" OnClick="BtnAddSubCategoryClick"
                                                                ValidationGroup="addSubCateg" Font-Names="Helvetica" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvCourse" runat="server">
                                <%-- <asp:Label ID = "CurrentPackageID" runat = "server" Text = "0" Visible = "false"></asp:Label>--%>
                                <telerik:RadTabStrip ID="tsCourses" runat="server" SelectedIndex="0" MultiPageID="mpCourses"
                                    Width="100%">
                                    <Tabs>
                                        <telerik:RadTab Text="Course List" Selected="true" />
                                        <telerik:RadTab Text="Add Course" />
                                    </Tabs>
                                </telerik:RadTabStrip>
                                <telerik:RadMultiPage ID="mpCourses" runat="server" SelectedIndex="0" Width="100%">
                                    <telerik:RadPageView ID="pvCourseList" runat="server" Selected="true">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <table width="100%" style="padding-bottom: 5px">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadComboBox ID="cbCategoryOnCourses" DataTextField="Category" DataValueField="CategoryID"
                                                                runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="CbCategoryOnCoursesSelectedIndexChanged"
                                                                AppendDataBoundItems="true" DropDownAutoWidth="Enabled" MaxHeight="300" AllowCustomText="False"
                                                                CausesValidation="False">
                                                            </telerik:RadComboBox>
                                                            <telerik:RadDropDownTree runat="server" ID="ddtSubCategoryOnCourses" DefaultMessage="- select subcategory -"
                                                                DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                                DataFieldParentID="ParentSubcategoryId" TextMode="Default" Width="280px" AutoPostBack="True"
                                                                OnEntryAdded="ddtSubCategoryOnCourses_EntryAdded" OnClientEntryAdding="OnClientEntryAdding"
                                                                Skin="Default">
                                                                <DropDownSettings Width="400px" Height="250px" />
                                                            </telerik:RadDropDownTree>
                                                        </td>
                                                        <td align="right" style="line-height: 20px; vertical-align: bottom">
                                                            <div style="vertical-align: bottom">
                                                                <asp:LinkButton runat="server" ID="btnExportExcelCourse" CssClass="linkBtn" OnClick="btnExport_OnClick"
                                                                    Height="100%" OnClientClick="if(!confirm('Are you sure you want to export all course list to excel file?')) return false;">Export all Courses<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <telerik:RadGrid runat="server" ID="gridCourse" AutoGenerateColumns="False" GridLines="None"
                                                        OnNeedDataSource="GridCourseNeedDataSource" OnItemCommand="GridCourseItemCommand"
                                                        AllowFilteringByColumn="True" OnItemDataBound="GridCourseItemDataBound" EnableLinqExpressions="False"
                                                        CssClass="RadGrid1" Skin="Metro" Height="600">
                                                        <ClientSettings EnableRowHoverStyle="True">
                                                            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" EnableVirtualScrollPaging="false" />
                                                            <Selecting AllowRowSelect="True" />
                                                            <ClientEvents OnPopUpShowing="PopUpShowing" />
                                                        </ClientSettings>
                                                        <FilterItemStyle HorizontalAlign="Center" />
                                                        <GroupingSettings CaseSensitive="False" />
                                                        <PagerStyle AlwaysVisible="True" Mode="NextPrevAndNumeric" />
                                                        <MasterTableView ShowHeader="true" AutoGenerateColumns="False" DataKeyNames="CourseID,CategoryID,SubcategoryID,Title,Description,AccessMode,EncryptedCourseID,StartDate,EndDate,
                                                        Duration,Version,Author,DeptOwnership,TrainingCost,TargetAudience,EnrolmentRequired,Tenure,TenureInRole,Legacy,CourseTypeID,PrereqProgramID,Keywords"
                                                            PageSize="10" EditMode="PopUp" NoMasterRecordsText="Course list is empty. Please select a category and subcategory."
                                                            EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" AllowPaging="True"
                                                            TableLayout="Auto" Width="1300px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                            AlternatingItemStyle-HorizontalAlign="Center">
                                                            <EditFormSettings CaptionFormatString="Edit Course ID : {0}" CaptionDataField="CourseID"
                                                                PopUpSettings-Height="650px" PopUpSettings-Width="950px">
                                                                <PopUpSettings Modal="true" ShowCaptionInEditForm="False" ScrollBars="Vertical" />
                                                            </EditFormSettings>
                                                            <Columns>
                                                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                    HeaderStyle-Width="50px" EditImageUrl="Images/editpen2.png">
                                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                </telerik:GridEditCommandColumn>
                                                                <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                    ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                    ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                    ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                                                </telerik:GridButtonColumn>
                                                                <telerik:GridBoundColumn UniqueName="CourseID" HeaderText="ID" DataField="CourseID"
                                                                    AllowFiltering="False" ReadOnly="True">
                                                                    <HeaderStyle ForeColor="Silver" Width="100px" />
                                                                    <ItemStyle ForeColor="Gray" />
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn UniqueName="SubcategoryID" HeaderText="Subcategory ID" DataField="SubcategoryID"
                                                                    AllowFiltering="False" ReadOnly="True" Display="False">
                                                                    <HeaderStyle ForeColor="Silver" />
                                                                    <ItemStyle ForeColor="Gray" />
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn ForceExtractValue="Always" Display="False" HeaderText="Category" />
                                                                <telerik:GridBoundColumn ForceExtractValue="Always" Display="False" HeaderText="Subcategory" />
                                                                <telerik:GridBoundColumn UniqueName="Title" HeaderText="Title" DataField="Title"
                                                                    AllowFiltering="True" FilterControlWidth="150" CurrentFilterFunction="Contains"
                                                                    ShowFilterIcon="False" AutoPostBackOnFilter="True" HeaderStyle-Width="350" />
                                                                <telerik:GridBoundColumn UniqueName="Description" HeaderText="Description" DataField="Description"
                                                                    AllowFiltering="false" ItemStyle-Font-Size="X-Small" HeaderStyle-Width="350" />
                                                                <telerik:GridCheckBoxColumn UniqueName="EnrolmentRequired" DataField="EnrolmentRequired"
                                                                    HeaderText="Enrollment Required?" AllowFiltering="False" SortExpression="EnrolmentRequired"
                                                                    HeaderStyle-Width="100px" />
                                                                <telerik:GridCheckBoxColumn UniqueName="Legacy" DataField="Legacy" HeaderText="Legacy?"
                                                                    AllowFiltering="False" SortExpression="Legacy" HeaderStyle-Width="100px" />
                                                                <telerik:GridBoundColumn UniqueName="CourseType" HeaderText="Course Type" DataField="CourseType"
                                                                    AllowFiltering="False" HeaderStyle-Width="100" />
                                                                <telerik:GridBoundColumn UniqueName="AccessMode" HeaderText="Access Mode" DataField="AccessMode"
                                                                    AllowFiltering="False" HeaderStyle-Width="100" />
                                                                <telerik:GridBoundColumn UniqueName="EncryptedCourseID" HeaderText="Enc. Course ID"
                                                                    DataField="EncryptedCourseID" AllowFiltering="False" HeaderStyle-Width="150" />
                                                                <telerik:GridBoundColumn UniqueName="FeatCourseLnk" HeaderText="Featured Course Link"
                                                                    DataField="FeatCourseLnk" AllowFiltering="False" Display="True" ForceExtractValue="Always"
                                                                    ReadOnly="True">
                                                                    <HeaderStyle Width="550px" />
                                                                    <ItemStyle ForeColor="Green" />
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn UniqueName="StartDate" HeaderText="Start Date" DataField="StartDate"
                                                                    AllowFiltering="False" HeaderStyle-Width="100" />
                                                                <telerik:GridBoundColumn UniqueName="EndDate" HeaderText="End Date" DataField="EndDate"
                                                                    AllowFiltering="False" HeaderStyle-Width="100" />
                                                                <telerik:GridBoundColumn HeaderText="Duration (Mins)" Display="False" />
                                                                <telerik:GridBoundColumn HeaderText="Version" Display="False" />
                                                                <telerik:GridBoundColumn UniqueName="Author" HeaderText="Author" DataField="Author"
                                                                    AllowFiltering="False" HeaderStyle-Width="150" />
                                                                <telerik:GridBoundColumn UniqueName="DeptOwnership" HeaderText="Dept. Ownership"
                                                                    DataField="DeptOwnership" AllowFiltering="False" HeaderStyle-Width="150" />
                                                                <telerik:GridBoundColumn UniqueName="TrainingCost" HeaderText="Training Cost ($)"
                                                                    DataField="TrainingCost" AllowFiltering="False" HeaderStyle-Width="140" />
                                                                <telerik:GridBoundColumn UniqueName="TargetAudience" HeaderText="Target Audience"
                                                                    DataField="TargetAudience" AllowFiltering="False" HeaderStyle-Width="150" />
                                                                <telerik:GridBoundColumn UniqueName="Tenure" HeaderText="Required Tenure (yrs)" DataField="Tenure"
                                                                    AllowFiltering="False" HeaderStyle-Width="150" />
                                                                <telerik:GridBoundColumn UniqueName="TenureInRole" HeaderText="Required Tenure in Role (yrs)"
                                                                    DataField="TenureInRole" AllowFiltering="False" HeaderStyle-Width="150" />
                                                                <telerik:GridBoundColumn UniqueName="PrereqProgram" HeaderText="Required Program"
                                                                    DataField="PrereqProgram" AllowFiltering="False" HeaderStyle-Width="150" />
                                                            </Columns>
                                                            <EditFormSettings EditFormType="Template">
                                                                <FormTemplate>
                                                                    <table id="tblEditMode" class="tabledefault">
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <asp:ValidationSummary ID="validationSummaryEditMode" ShowMessageBox="False" ShowSummary="True"
                                                                                    DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                                    runat="server" ValidationGroup="editCourse" CssClass="displayerror" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Course Type
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadComboBox ID="cbEditCourseType" runat="server" EmptyMessage="- course tpye -"
                                                                                    DropDownAutoWidth="Enabled" MaxHeight="300" DataValueField="coursetypeid" DataTextField="coursetype"
                                                                                    DataSourceID="dsPackageType" OnSelectedIndexChanged="CbEditCourseTypeSelectedIndexChanged"
                                                                                    AutoPostBack="True" OnDataBinding="CbEditCourseTypeDataBinding" />
                                                                                <hr />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Category"
                                                                                    ControlToValidate="cbCategoryOnEditCourse" SetFocusOnError="True" Display="Dynamic"
                                                                                    CssClass="displayerror" ValidationGroup="editCourse" EnableClientScript="False"
                                                                                    Text="*" />
                                                                                Category
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadComboBox runat="server" ID="cbCategoryOnEditCourse" DataTextField="Category"
                                                                                    AutoPostBack="True" DataValueField="CategoryID" EmptyMessage="- select category -"
                                                                                    OnSelectedIndexChanged="CbCategoryOnEditCourseSelectedIndexChanged" CausesValidation="False"
                                                                                    DropDownAutoWidth="Enabled" MaxHeight="300" AllowCustomText="False" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Subcategory"
                                                                                    ControlToValidate="ddtSubCategoryOnEditCourse" SetFocusOnError="True" Display="Dynamic"
                                                                                    CssClass="displayerror" ValidationGroup="editCourse" EnableClientScript="False"
                                                                                    Text="*" />
                                                                                Subcategory
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadDropDownTree runat="server" ID="ddtSubCategoryOnEditCourse" DefaultMessage="- select subcategory -"
                                                                                    DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                                                    DataFieldParentID="ParentSubcategoryId" TextMode="Default" Width="280px" OnClientEntryAdding="OnClientEntryAdding"
                                                                                    AutoPostBack="True" Skin="Default">
                                                                                    <DropDownSettings Width="400px" Height="250px" />
                                                                                </telerik:RadDropDownTree>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Title"
                                                                                    ControlToValidate="txtTitle" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                                    ValidationGroup="editCourse" EnableClientScript="False" Text="*" />
                                                                                Title
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadTextBox runat="server" ID="txtTitle" EmptyMessage="- course name -" Width="400"
                                                                                    SelectionOnFocus="SelectAll" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Description"
                                                                                    ControlToValidate="txtDescription" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                                    ValidationGroup="editCourse" EnableClientScript="False" Text="*" />
                                                                                Description
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadEditor ID="txtDescription" runat="server" EnableResize="false" ToolsFile="~/App_Data/Editor_LimitedTools.xml"
                                                                                    AllowScripts="false" Height="200px" Width="500px" BorderColor="LightGray" BorderStyle="Solid"
                                                                                    BorderWidth="1px">
                                                                                    <CssFiles>
                                                                                        <telerik:EditorCssFile Value="~/Styles/EditorContentArea.css" />
                                                                                    </CssFiles>
                                                                                </telerik:RadEditor>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Enrollment Required?
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <asp:CheckBox runat="server" ID="chkEnrolmentRequired" Text="Enrollment Required?" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Legacy?
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <asp:CheckBox runat="server" ID="chkLegacyEdit" Text="Legacy?" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Access Mode"
                                                                                    ControlToValidate="radioAccessMode" SetFocusOnError="True" Display="Dynamic"
                                                                                    CssClass="displayerror" ValidationGroup="editCourse" EnableClientScript="False"
                                                                                    Text="*" />
                                                                                Access Mode
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <asp:RadioButtonList ID="radioAccessMode" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                                                    RepeatLayout="Flow" CssClass="radio">
                                                                                    <asp:ListItem Value="Internal" Text="Internal" />
                                                                                    <asp:ListItem Value="External" Text="External" />
                                                                                    <asp:ListItem Value="Both" Text="Both" />
                                                                                </asp:RadioButtonList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Encrypted Course Id
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadTextBox runat="server" ID="txtEncryptedCourseID" Width="200" SelectionOnFocus="SelectAll"
                                                                                    EmptyMessage="- enc. course id -" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Start Date
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadDatePicker ID="dpStartDate" runat="server" Width="150" EnableTyping="False"
                                                                                    ShowPopupOnFocus="True">
                                                                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                                </telerik:RadDatePicker>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                End Date
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadDatePicker ID="dpEndDate" runat="server" Width="150" EnableTyping="False"
                                                                                    ShowPopupOnFocus="True">
                                                                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                                </telerik:RadDatePicker>
                                                                                <asp:CompareValidator ID="dateCompareValidator" runat="server" ControlToValidate="dpEndDate"
                                                                                    ControlToCompare="dpStartDate" Operator="GreaterThanEqual" Type="Date" ErrorMessage="The second date must be after the first one"
                                                                                    CssClass="displayerror" ValidationGroup="editCourse" EnableClientScript="False">
                                                                                </asp:CompareValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Duration (Mins)"
                                                                                    ControlToValidate="txtDuration" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                                    ValidationGroup="editCourse" EnableClientScript="False" Text="*" />
                                                                                Duration (Mins)
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadNumericTextBox runat="server" ID="txtDuration" EmptyMessage="- duration -"
                                                                                    Width="100" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Version"
                                                                                    ControlToValidate="txtVersion" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                                    ValidationGroup="editCourse" EnableClientScript="False" Text="*" />
                                                                                Version
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadNumericTextBox runat="server" ID="txtVersion" EmptyMessage="- version -"
                                                                                    Width="100" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Author
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadTextBox runat="server" ID="txtAuthor" EmptyMessage="- author name -"
                                                                                    Width="200" SelectionOnFocus="SelectAll" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Department Ownership
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadTextBox runat="server" ID="txtDeptOwnership" EmptyMessage="- dept. name -"
                                                                                    Width="200" SelectionOnFocus="SelectAll" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Training Cost
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadNumericTextBox ID="txtTrainingCost" runat="server" EmptyMessage="- cost -" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:RequiredFieldValidator ID="rfvTargetAudienceEdit" runat="server" ErrorMessage="Target Audience"
                                                                                    ControlToValidate="txtTargetAudienceEdit" SetFocusOnError="True" Display="Dynamic"
                                                                                    CssClass="displayerror" ValidationGroup="editCourse" EnableClientScript="False"
                                                                                    Text="*" />
                                                                                Target Audience
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadTextBox runat="server" ID="txtTargetAudienceEdit" Width="200" SelectionOnFocus="SelectAll"
                                                                                    EmptyMessage="- audience -" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Required Program
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadComboBox ID="cbProgramPrereqEdit" DataTextField="Client" DataValueField="ClientID"
                                                                                    runat="server" EmptyMessage="- select program -" CausesValidation="False" DropDownAutoWidth="Enabled"
                                                                                    MaxHeight="300px" DataSourceID="dsHarmonyClient" EnableLoadOnDemand="true" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabelRequiredCourse" valign="top">
                                                                                Required Course Taken
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <telerik:RadComboBox ID="cbCategoryEditPrereq" DataTextField="Category" DataValueField="CategoryID"
                                                                                                runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="cbCategoryEditPrereq_SelectedIndexChanged"
                                                                                                CausesValidation="False" DropDownAutoWidth="Enabled" MaxHeight="300px" />
                                                                                            <telerik:RadDropDownTree runat="server" ID="ddtSubcategoryEditPrereq" DefaultMessage="- select subcategory -"
                                                                                                DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                                                                DataFieldParentID="ParentSubcategoryId" TextMode="FullPath" Width="150" AutoPostBack="True"
                                                                                                OnClientEntryAdding="OnClientEntryAdding" Skin="Default" OnEntryAdded="ddtSubcategoryPrereq_EntryAdded">
                                                                                                <DropDownSettings Width="400px" Height="250px" />
                                                                                            </telerik:RadDropDownTree>
                                                                                            <asp:LinkButton runat="server" ID="btnAddCoursePreReqEdit" Text="Choose Course/s"
                                                                                                ToolTip="Assign" CssClass="linkBtn" OnClick="btnAddCoursePreReqEdit_Click" Font-Size="8.5" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <telerik:RadGrid runat="server" ID="gridPrerequisitesEdit" AutoGenerateColumns="False"
                                                                                                AllowPaging="true" GridLines="None" CssClass="RadGrid1 Prereq" Skin="Metro" Width="450px"
                                                                                                OnItemCommand="gridPrerequisitesEdit_ItemCommand" OnNeedDataSource="gridPrerequisitesEdit_NeedDataSource">
                                                                                                <FilterItemStyle HorizontalAlign="Center" />
                                                                                                <MasterTableView DataKeyNames="PrerequisiteID,CourseID" AutoGenerateColumns="False"
                                                                                                    EnableNoRecordsTemplate="True" NoMasterRecordsText="No Course Prerequisite."
                                                                                                    ShowHeadersWhenNoRecords="True" TableLayout="Auto" ShowHeader="false" ItemStyle-HorizontalAlign="Center"
                                                                                                    HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                                                                                    <Columns>
                                                                                                        <telerik:GridBoundColumn UniqueName="Title" HeaderText="Title" DataField="Title"
                                                                                                            AllowFiltering="False" ReadOnly="True">
                                                                                                        </telerik:GridBoundColumn>
                                                                                                        <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                                                            ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                                                            ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                                                            ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                                                                                        </telerik:GridButtonColumn>
                                                                                                    </Columns>
                                                                                                </MasterTableView>
                                                                                            </telerik:RadGrid>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Required Tenure (yrs)
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadNumericTextBox ID="txtYrsEdit" runat="server" DataType="System.Decimal"
                                                                                    Value="0" EmptyMessage="- Tenure in Years -">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Required Tenure in existing Role (yrs)
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadNumericTextBox ID="txtRoleYrsEdit" runat="server" DataType="System.Decimal"
                                                                                    Value="0" EmptyMessage="- Tenure in Years -">
                                                                                </telerik:RadNumericTextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Search keywords (separated by comma)
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <asp:TextBox runat="server" ID="txtKeywords" TextMode="MultiLine" Width="400" Height="100"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:Label ID="lblEditBundle" Text="Course" runat="server" Visible="false" />
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <AddScormCourse:ScormCourse ID="EditScormCourse" ClientIDMode="Static" runat="server"
                                                                                    Visible="false" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <br />
                                                                                <asp:LinkButton ID="bntUpdate" ValidationGroup="editCourse" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                                    runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'>
                                                                                </asp:LinkButton>
                                                                                |
                                                                                <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                                                                                    CommandName="Cancel" />
                                                                                <br />
                                                                                <br />
                                                                                <br />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </FormTemplate>
                                                            </EditFormSettings>
                                                        </MasterTableView>
                                                    </telerik:RadGrid>
                                                </div>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="pvAddCourse" runat="server">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <table width="100%" class="tabledefault">
                                                    <tr>
                                                    <%--Raymark--%>
                                                        <td class="tblLabel">
                                                        </td>                                                           <%--"http://q9vmdevapp06//NucommUniversityAdmin/Login.aspx"--%>
                                                        <td class="tblfield" align="right">                             <%--"http://apac.transcomuniversity.com/NucommUniversityAdmin/Login.aspx"--%>
                                                            <asp:HyperLink runat="server" ID="hplCourseAdd" NavigateUrl="https://applications.transcom.com/TranscomUniversity/NucommUniversityAdmin/Login.aspx"
                                                                Text="Please click here to create Encrypted Course ID" Font-Names="Arial" Font-Size="Small"
                                                                ForeColor="green" Target="_blank" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table runat="server" id="tblAddCourse" width="100%" class="tabledefault">
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Course Type
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbCourseType" runat="server" EmptyMessage="- course tpye -"
                                                                DropDownAutoWidth="Enabled" MaxHeight="300" DataValueField="coursetypeid" DataTextField="coursetype"
                                                                DataSourceID="dsPackageType" OnSelectedIndexChanged="CbCourseTypeSelectedIndexChanged"
                                                                AutoPostBack="True" OnDataBinding="CbCourseTypeDataBinding" />
                                                            <hr />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:ValidationSummary ID="validateAddCourse" ShowMessageBox="False" ShowSummary="True"
                                                                DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                runat="server" ValidationGroup="addCourse" CssClass="displayerror" EnableClientScript="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="Category"
                                                                ControlToValidate="cbAddCategoryOnCourse" SetFocusOnError="True" Display="Dynamic"
                                                                ValidationGroup="addCourse" CssClass="displayerror" EnableClientScript="False"
                                                                Text="*" />
                                                            Category
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbAddCategoryOnCourse" DataTextField="Category" DataValueField="CategoryID"
                                                                runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="CbAddCategoryOnCourseSelectedIndexChanged"
                                                                DropDownAutoWidth="Enabled" MaxHeight="300" AllowCustomText="False" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator34" runat="server" ErrorMessage="Subcategory"
                                                                ControlToValidate="ddtAddSubCategoryOnCourse" SetFocusOnError="True" Display="Dynamic"
                                                                ValidationGroup="addCourse" CssClass="displayerror" EnableClientScript="False"
                                                                Text="*" />
                                                            Subcategory
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadDropDownTree runat="server" ID="ddtAddSubCategoryOnCourse" DefaultMessage="- select subcategory -"
                                                                DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                                DataFieldParentID="ParentSubcategoryId" TextMode="Default" OnClientEntryAdding="OnClientEntryAdding"
                                                                Skin="Default">
                                                                <DropDownSettings Width="400px" Height="250px" />
                                                            </telerik:RadDropDownTree>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Title" ControlToValidate="txtAddTitleOnCourse"
                                                                SetFocusOnError="True" Display="Dynamic" ValidationGroup="addCourse" CssClass="displayerror"
                                                                EnableClientScript="False" Text="*" />
                                                            Title
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtAddTitleOnCourse" EmptyMessage="- course name -"
                                                                Width="500" SelectionOnFocus="SelectAll" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Description" ControlToValidate="txtAddDescriptionOnCourse"
                                                                SetFocusOnError="True" Display="Dynamic" ValidationGroup="addCourse" CssClass="displayerror"
                                                                EnableClientScript="False" Text="*" />
                                                            Description
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadEditor ID="txtAddDescriptionOnCourse" runat="server" EnableResize="false"
                                                                ToolsFile="~/App_Data/Editor_LimitedTools.xml" AllowScripts="false" Width="100%"
                                                                Height="150px" EmptyMessage="- course description -" BorderColor="LightGray"
                                                                BorderStyle="Solid" BorderWidth="1px">
                                                                <CssFiles>
                                                                    <telerik:EditorCssFile Value="~/Styles/EditorContentArea.css" />
                                                                </CssFiles>
                                                            </telerik:RadEditor>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Enrollment Required?
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:CheckBox runat="server" ID="chkEnrolmentRequired" Text="Enrollment Required?" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Legacy?
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:CheckBox runat="server" ID="chkLegacyAdd" Text="Legacy?" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="Access Mode"
                                                                ControlToValidate="radioAccessMode" SetFocusOnError="True" Display="Dynamic"
                                                                ValidationGroup="addCourse" CssClass="displayerror" EnableClientScript="False"
                                                                Text="*" />
                                                            Access Mode
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:RadioButtonList ID="radioAccessMode" runat="server" RepeatDirection="Horizontal"
                                                                AutoPostBack="true" RepeatLayout="Flow" CssClass="radio">
                                                                <asp:ListItem Text="Internal" Value="Internal" />
                                                                <asp:ListItem Text="External" Value="External" />
                                                                <asp:ListItem Text="Both" Value="Both" />
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Encrypted Course ID
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtAddEncryptedCourseIDOnCourse" Width="200"
                                                                SelectionOnFocus="SelectAll" EmptyMessage="- enc. course id -" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Start Date
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadDatePicker ID="dpAddStartDateOnCourses" runat="server" Width="150" EnableTyping="False"
                                                                ShowPopupOnFocus="True">
                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            End Date
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadDatePicker ID="dpAddEndDateOnCourses" runat="server" Width="150" EnableTyping="False"
                                                                ShowPopupOnFocus="True">
                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                            </telerik:RadDatePicker>
                                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="dpAddEndDateOnCourses"
                                                                ControlToCompare="dpAddStartDateOnCourses" Operator="GreaterThanEqual" Type="Date"
                                                                ErrorMessage="The second date must be after the first one" CssClass="displayerror"
                                                                ValidationGroup="reserve" EnableClientScript="False">
                                                            </asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="Duration (Mins)"
                                                                ControlToValidate="txtAddDurationOnCourse" SetFocusOnError="True" Display="Dynamic"
                                                                ValidationGroup="addCourse" CssClass="displayerror" EnableClientScript="False"
                                                                Text="*" />
                                                            Duration (Mins)
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadNumericTextBox runat="server" ID="txtAddDurationOnCourse" EmptyMessage="- duration -"
                                                                Width="100" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ErrorMessage="Version"
                                                                ControlToValidate="txtAddVersionOnCourses" SetFocusOnError="True" Display="Dynamic"
                                                                ValidationGroup="addCourse" CssClass="displayerror" EnableClientScript="False"
                                                                Text="*" />
                                                            Version
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadNumericTextBox runat="server" ID="txtAddVersionOnCourses" EmptyMessage="- version -"
                                                                Width="100" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Author" ControlToValidate="txtAddAuthorOnCourses"
                                                                SetFocusOnError="True" Display="Dynamic" ValidationGroup="addCourse" CssClass="displayerror"
                                                                EnableClientScript="False" Text="*" />
                                                            Author
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtAddAuthorOnCourses" EmptyMessage="- author name -"
                                                                Width="200" SelectionOnFocus="SelectAll" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Dept. Ownership" ControlToValidate="txtAddDeptOwnershipOnCourses"
                                                                SetFocusOnError="True" Display="Dynamic" ValidationGroup="addCourse" CssClass="displayerror"
                                                                EnableClientScript="False" Text="*" />
                                                            Dept. Ownership
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtAddDeptOwnershipOnCourses" EmptyMessage="- dept. name -"
                                                                Width="200" SelectionOnFocus="SelectAll" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Training Cost ($)
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadNumericTextBox ID="txtTrainingCost" runat="server" DataType="System.Decimal"
                                                                Value="0" EmptyMessage="- cost -">
                                                            </telerik:RadNumericTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="rfvTargetAudienceAdd" runat="server" ErrorMessage="Target Audience"
                                                                ControlToValidate="txtTargetAudience" SetFocusOnError="True" Display="Dynamic"
                                                                CssClass="displayerror" ValidationGroup="addCourse" EnableClientScript="False"
                                                                Text="*" />
                                                            Target Audience
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtTargetAudience" Width="200" SelectionOnFocus="SelectAll"
                                                                EmptyMessage="- audience -" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Required Program
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbProgramPrereqAdd" DataTextField="Client" DataValueField="ClientID"
                                                                runat="server" AutoPostBack="True" EmptyMessage="- select program -" CausesValidation="False"
                                                                DropDownAutoWidth="Enabled" MaxHeight="300px" DataSourceID="dsHarmonyClient"
                                                                EnableLoadOnDemand="true" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabelRequiredCourse" valign="top">
                                                            Required Course Taken
                                                        </td>
                                                        <td class="tblfield">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadComboBox ID="cbCategoryAddPrereq" DataTextField="Category" DataValueField="CategoryID"
                                                                            runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="cbCategoryAddPrereq_SelectedIndexChanged"
                                                                            CausesValidation="False" DropDownAutoWidth="Enabled" MaxHeight="300px" />
                                                                        <telerik:RadDropDownTree runat="server" ID="ddtSubcategoryAddPrereq" DefaultMessage="- select subcategory -"
                                                                            DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                                            DataFieldParentID="ParentSubcategoryId" TextMode="FullPath" Width="150" AutoPostBack="True"
                                                                            OnClientEntryAdding="OnClientEntryAdding" Skin="Default" OnEntryAdded="ddtSubcategoryPrereq_EntryAdded">
                                                                            <DropDownSettings Width="400px" Height="250px" />
                                                                        </telerik:RadDropDownTree>
                                                                        <asp:LinkButton runat="server" ID="btnAddCoursePreReq" Text="Choose Course/s" CssClass="linkBtn"
                                                                            OnClick="btnAddCoursePreReq_Click" Font-Size="8.5" ToolTip="Assign" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <telerik:RadGrid runat="server" ID="gridPrerequisites" AutoGenerateColumns="False"
                                                                            AllowPaging="true" PageSize="10" GridLines="None" CssClass="RadGrid2_detailtable"
                                                                            Skin="Metro" Width="550px" OnItemCommand="gridPrerequisites_ItemCommand" OnNeedDataSource="gridPrerequisites_NeedDataSource">
                                                                            <ClientSettings EnableRowHoverStyle="True">
                                                                                <Selecting AllowRowSelect="True" />
                                                                            </ClientSettings>
                                                                            <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                                            <MasterTableView DataKeyNames="CourseID" AutoGenerateColumns="False" EnableNoRecordsTemplate="True"
                                                                                NoMasterRecordsText="No Course Prerequisite." ShowHeadersWhenNoRecords="True"
                                                                                TableLayout="Auto" ShowHeader="false" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                                AlternatingItemStyle-HorizontalAlign="Center">
                                                                                <Columns>
                                                                                    <telerik:GridBoundColumn UniqueName="Title" HeaderText="Title" DataField="Title"
                                                                                        AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="500">
                                                                                    </telerik:GridBoundColumn>
                                                                                    <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                                        ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                                        ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                                        ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="50">
                                                                                    </telerik:GridButtonColumn>
                                                                                </Columns>
                                                                            </MasterTableView>
                                                                        </telerik:RadGrid>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Required Tenure (yrs)
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadNumericTextBox ID="txtYrs" runat="server" DataType="System.Decimal" Value="0"
                                                                EmptyMessage="- Tenure in Years -">
                                                            </telerik:RadNumericTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Required Tenure in existing Role (yrs)
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadNumericTextBox ID="txtRoleYrs" runat="server" DataType="System.Decimal"
                                                                Value="0" EmptyMessage="- Tenure in Years -">
                                                            </telerik:RadNumericTextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Search keywords (separated by comma)
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:TextBox runat="server" ID="txtKeywords" TextMode="MultiLine" Width="400" Height="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:Label ID="lblBundle" Text="Course" runat="server" Visible="false" />
                                                        </td>
                                                        <td class="tblfield">
                                                            <AddScormCourse:ScormCourse ID="AddScormCourse" runat="server" Visible="false" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="100%" class="tabledefault">
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <br />
                                                            <asp:LinkButton runat="server" ID="btnAddCourse" Text="Add" OnClick="BtnAddCourseClick"
                                                                ValidationGroup="addCourse" CssClass="linkBtn" />
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvCareerPath" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <telerik:RadTabStrip ID="tsCareerPath" runat="server" SelectedIndex="0" MultiPageID="mpCareerPath"
                                                Width="100%">
                                                <Tabs>
                                                    <telerik:RadTab Text="Career Path List" Selected="true" />
                                                    <telerik:RadTab Text="Add Career Path" />
                                                </Tabs>
                                            </telerik:RadTabStrip>
                                        </td>
                                        <td>
                                            <div runat="server" id="div1" align="right">
                                                <asp:LinkButton runat="server" ID="btnExportExcelCareerPath" CssClass="linkBtn" OnClick="btnExportExcel_Click"
                                                    Height="100%" OnClientClick="if(!confirm('Are you sure you want to export career path list to excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <telerik:RadMultiPage ID="mpCareerPath" runat="server" SelectedIndex="0" Width="100%">
                                    <telerik:RadPageView ID="pvCareerPaths" runat="server" Selected="true">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <telerik:RadGrid ID="gridCareerPath" runat="server" AutoGenerateColumns="False" GridLines="None"
                                                    EnableLinqExpressions="True" OnNeedDataSource="GridCareerPathNeedDataSource"
                                                    OnPreRender="GridCareerPathPreRender" OnItemCommand="GridCareerPathItemCommand"
                                                    OnItemDataBound="GridCareerPathItemDataBound" CssClass="RadGrid1" Skin="Metro">
                                                    <GroupingSettings CaseSensitive="false" />
                                                    <FilterItemStyle HorizontalAlign="Center" />
                                                    <MasterTableView DataKeyNames="CareerPathID,CareerName,Description,HideFromList"
                                                        AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" PageSize="15"
                                                        GroupLoadMode="Server" HierarchyLoadMode="ServerBind" AllowFilteringByColumn="true"
                                                        EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True" EditMode="EditForms"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                                        <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                        <NestedViewTemplate>
                                                            <div style="padding-top: 4px">
                                                                <telerik:RadTabStrip ID="tsCoursesOnCareerPath" runat="server" SelectedIndex="0"
                                                                    MultiPageID="mpCoursesOnCareerPath" Width="100%">
                                                                    <Tabs>
                                                                        <telerik:RadTab Text="Career Courses" runat="server" Selected="True" />
                                                                        <telerik:RadTab Text="Add Career Course" runat="server" />
                                                                        <telerik:RadTab Text="Employees" runat="server" Visible="false" />
                                                                    </Tabs>
                                                                </telerik:RadTabStrip>
                                                                <telerik:RadMultiPage ID="mpCoursesOnCareerPath" runat="server" SelectedIndex="0"
                                                                    RenderSelectedPageOnly="False" Width="100%">
                                                                    <telerik:RadPageView ID="pvCareerPathCourses" runat="server" Selected="True">
                                                                        <div class="paneldefault">
                                                                            <div class="insidecontainer">
                                                                                <asp:Label ID="lblCareerPathID" Font-Bold="true" Text='<%# Eval("CareerPathID") %>'
                                                                                    Visible="false" runat="server" />
                                                                                <telerik:RadGrid ID="gridCareerPathCourses" runat="server" Width="100%" DataSourceID="dsCareerPathCourses"
                                                                                    EnableLinqExpressions="True" GridLines="None" CssClass="RadGrid1" Skin="Default">
                                                                                    <MasterTableView Name="CareerPathCourses" DataKeyNames="CareerCourseID,CareerPathID"
                                                                                        AutoGenerateColumns="False" AllowSorting="true" PageSize="5" AllowAutomaticUpdates="true"
                                                                                        AllowAutomaticDeletes="true" EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True"
                                                                                        EditMode="EditForms" AllowPaging="True" Font-Names="Helvetica">
                                                                                        <HeaderStyle BackColor="#001538" ForeColor="White" />
                                                                                        <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                                                        <Columns>
                                                                                            <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                                                ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                                                ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                                                ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                                                                            </telerik:GridButtonColumn>
                                                                                            <telerik:GridTemplateColumn UniqueName="CourseName" HeaderText="Course Name" SortExpression="CourseName">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label runat="server" ID="lblCourseName" Text='<%# Eval("CourseName") %>' />
                                                                                                </ItemTemplate>
                                                                                                <EditItemTemplate>
                                                                                                    <telerik:RadTextBox runat="server" ID="txtCourseName" Text='<%# Bind("CourseName") %>'
                                                                                                        EmptyMessage="- career course name -" Width="400" SelectionOnFocus="SelectAll" />
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="*"
                                                                                                        ControlToValidate="txtCourseName" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" />
                                                                                                </EditItemTemplate>
                                                                                            </telerik:GridTemplateColumn>
                                                                                            <telerik:GridDateTimeColumn UniqueName="CreateDate" HeaderText="Date Created" SortExpression="CreateDate"
                                                                                                PickerType="DatePicker" DataField="CreateDate" ReadOnly="True">
                                                                                                <HeaderStyle Width="200px" />
                                                                                            </telerik:GridDateTimeColumn>
                                                                                        </Columns>
                                                                                    </MasterTableView>
                                                                                </telerik:RadGrid>
                                                                                <asp:SqlDataSource ID="dsCareerPathCourses" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
                                                                                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT CC.CareerCourseID, C.CourseName, CP.CareerPathID, CC.SortOrder, CC.HideFromList, CC.CreatedBy, CC.CreateDate, CC.ModifiedBy, CC.ModifyDate FROM dbo.tbl_TranscomUniversity_Cor_CareerCourses AS CC LEFT OUTER JOIN dbo.tbl_TranscomUniversity_Lkp_CareerPath AS CP ON CC.CareerPathID = CP.CareerPathID LEFT OUTER JOIN dbo.tbl_NuCommUniversity_Lkp_Course AS C ON CC.CourseID = C.CourseID WHERE CP.CareerPathID = @CareerPathID"
                                                                                    UpdateCommand="UPDATE tbl_TranscomUniversity_Cor_CareerCourses SET CourseID = @CourseID, SortOrder = @SortOrder, HideFromList = @HideFromList, ModifiedBy = @ModifiedBy, ModifyDate = getdate() WHERE CareerCourseID = @CareerCourseID"
                                                                                    DeleteCommand="DELETE tbl_TranscomUniversity_Cor_CareerCourses WHERE CareerCourseID = @CareerCourseID">
                                                                                    <SelectParameters>
                                                                                        <asp:ControlParameter Name="CareerPathID" ControlID="lblCareerPathID" Type="Int32"
                                                                                            PropertyName="Text" />
                                                                                    </SelectParameters>
                                                                                    <UpdateParameters>
                                                                                        <asp:Parameter Name="CourseID" Type="Int32" />
                                                                                        <asp:Parameter Name="SortOrder" Type="Int32" />
                                                                                        <asp:Parameter Name="HideFromList" Type="Int32" />
                                                                                        <asp:Parameter Name="ModifiedBy" Type="String" />
                                                                                    </UpdateParameters>
                                                                                    <DeleteParameters>
                                                                                        <asp:Parameter Name="CareerCourseID" Type="Int32" />
                                                                                    </DeleteParameters>
                                                                                </asp:SqlDataSource>
                                                                            </div>
                                                                        </div>
                                                                    </telerik:RadPageView>
                                                                    <telerik:RadPageView ID="pvAddCourseOnCareerPath" runat="server">
                                                                        <div class="paneldefault">
                                                                            <div class="insidecontainer">
                                                                                <telerik:RadComboBox runat="server" ID="cbAddCareerCourses" DataTextField="CourseName"
                                                                                    DataSourceID="dsCareerCourse" DataValueField="CourseID" EmptyMessage="- select career course -"
                                                                                    ValidationGroup="addCareerCourse" DropDownAutoWidth="Enabled" MaxHeight="300"
                                                                                    AllowCustomText="False" />
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="*"
                                                                                    ControlToValidate="cbAddCareerCourses" SetFocusOnError="True" Display="Dynamic"
                                                                                    ValidationGroup="addCareerCourse" CssClass="displayerror" />
                                                                                <telerik:RadButton runat="server" ID="btnAddCareerCourse" Text="Add" ValidationGroup="addCareerCourse"
                                                                                    OnClick="BtnAddCareerCourseClick" />
                                                                                <asp:SqlDataSource ID="dsCareerCourse" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
                                                                                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT CourseID, CourseName FROM INTRANET.dbo.tbl_NuCommUniversity_Lkp_Course WHERE HideFromList = 0" />
                                                                            </div>
                                                                        </div>
                                                                    </telerik:RadPageView>
                                                                    <telerik:RadPageView ID="pvEmployeesOnCareerPath" runat="server">
                                                                        <div class="paneldefault">
                                                                            <div class="insidecontainer">
                                                                                <h3>
                                                                                    Under Construction</h3>
                                                                            </div>
                                                                        </div>
                                                                    </telerik:RadPageView>
                                                                </telerik:RadMultiPage>
                                                            </div>
                                                        </NestedViewTemplate>
                                                        <Columns>
                                                            <telerik:GridTemplateColumn UniqueName="CareerName" HeaderText="Career Path Name"
                                                                DataField="CareerName" SortExpression="CareerName" AllowFiltering="True" ShowFilterIcon="False"
                                                                CurrentFilterFunction="Contains" AutoPostBackOnFilter="True" FilterControlWidth="150">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblCareerName" Text='<%# Eval("CareerName") %>' />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <telerik:RadTextBox runat="server" ID="txtCareerName" Text='<%# Bind("CareerName") %>'
                                                                        EmptyMessage="- career path name -" Width="400" SelectionOnFocus="SelectAll" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="*"
                                                                        ControlToValidate="txtCareerName" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" />
                                                                </EditItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn UniqueName="Description" HeaderText="Description" DataField="Description"
                                                                SortExpression="Description" AllowFiltering="True" ShowFilterIcon="False" CurrentFilterFunction="Contains"
                                                                AutoPostBackOnFilter="True" FilterControlWidth="150">
                                                                <ItemTemplate>
                                                                    <asp:Label runat="server" ID="lblDescription" Text='<%# Eval("Description") %>' />
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <telerik:RadTextBox runat="server" ID="txtDescription" Text='<%# Bind("Description") %>'
                                                                        EmptyMessage="- course description -" Width="400" Height="100px" TextMode="MultiLine"
                                                                        CssClass="txtMultiline" Wrap="True" SelectionOnFocus="SelectAll" />
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="*"
                                                                        ControlToValidate="txtDescription" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" />
                                                                </EditItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn UniqueName="HideFromList" HeaderText="Hide From List"
                                                                SortExpression="HideFromList" AllowFiltering="true" ShowFilterIcon="False" CurrentFilterFunction="Contains"
                                                                DataField="HideFromList" AutoPostBackOnFilter="True" FilterControlWidth="50"
                                                                FilterControlToolTip="Enter '0' for No, '1' for Yes">
                                                                <ItemTemplate>
                                                                    <%# DataBinder.Eval(Container.DataItem, "HideFromList").Equals(0) ? "No" : "Yes"%>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <telerik:RadComboBox runat="server" ID="cbCareerHideFromList" EmptyMessage="- select -"
                                                                        Width="100">
                                                                        <Items>
                                                                            <telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
                                                                            <telerik:RadComboBoxItem runat="server" Text="Yes" Value="1" />
                                                                        </Items>
                                                                    </telerik:RadComboBox>
                                                                </EditItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridDateTimeColumn UniqueName="CreateDate" HeaderText="Date Created" SortExpression="CreateDate"
                                                                PickerType="DatePicker" DataField="CreateDate" AllowFiltering="True" ShowFilterIcon="False"
                                                                CurrentFilterFunction="Contains" AutoPostBackOnFilter="True" FilterControlWidth="120"
                                                                ReadOnly="True">
                                                                <HeaderStyle Width="200px" />
                                                            </telerik:GridDateTimeColumn>
                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                HeaderStyle-Width="50px" EditImageUrl="Images/editpen2.png">
                                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                            </telerik:GridEditCommandColumn>
                                                            <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                                            </telerik:GridButtonColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="pvAddCareerPath" runat="server">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <table width="100%">
                                                    <tr>
                                                        <td>
                                                            <telerik:RadTextBox runat="server" ID="txtAddCareerPathName" EmptyMessage="- career path name -"
                                                                Width="400" SelectionOnFocus="SelectAll" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" ErrorMessage="*"
                                                                ControlToValidate="txtAddCareerPathName" SetFocusOnError="True" Display="Dynamic"
                                                                ValidationGroup="addCareerPath" CssClass="displayerror" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <telerik:RadTextBox runat="server" ID="txtAddCareerPathDescription" EmptyMessage="- career path description -"
                                                                Width="400" Height="100px" TextMode="MultiLine" CssClass="txtMultiline" Wrap="True"
                                                                SelectionOnFocus="SelectAll" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" ErrorMessage="*"
                                                                ControlToValidate="txtAddCareerPathDescription" SetFocusOnError="True" Display="Dynamic"
                                                                ValidationGroup="addCareerPath" CssClass="displayerror" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <telerik:RadComboBox runat="server" ID="cbAddCareerHideFromList" EmptyMessage="- hide from list -">
                                                                <Items>
                                                                    <telerik:RadComboBoxItem runat="server" Text="No" Value="0" />
                                                                    <telerik:RadComboBoxItem runat="server" Text="Yes" Value="1" />
                                                                </Items>
                                                            </telerik:RadComboBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="*"
                                                                ControlToValidate="cbAddCareerHideFromList" SetFocusOnError="True" Display="Dynamic"
                                                                ValidationGroup="addCareerPath" CssClass="displayerror" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <telerik:RadButton runat="server" ID="btnAddCareerPath" Text="Add" OnClick="BtnAddCareerPathClick"
                                                                ValidationGroup="addCareerPath" Font-Names="Helvetica" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvExternalUser" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <telerik:RadTabStrip ID="tsExternalUsers" runat="server" SelectedIndex="0" MultiPageID="mpExternalUsers"
                                                Width="100%" CausesValidation="False">
                                                <Tabs>
                                                    <telerik:RadTab Text="External User List" Selected="True" />
                                                    <telerik:RadTab Text="Add External User" />
                                                </Tabs>
                                            </telerik:RadTabStrip>
                                        </td>
                                        <td>
                                            <div runat="server" id="div2" align="right">
                                                <asp:LinkButton runat="server" ID="btnExportExcelExternalUsers" CssClass="linkBtn"
                                                    OnClick="btnExportExcel_Click" Height="100%" OnClientClick="if(!confirm('Are you sure you want to export external user list to excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <telerik:RadMultiPage runat="server" ID="mpExternalUsers" SelectedIndex="0" Width="100%">
                                    <telerik:RadPageView runat="server" ID="pageViewExternalUsersList" Selected="True">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <telerik:RadGrid runat="server" ID="gridExternalUsers" AutoGenerateColumns="False"
                                                    AllowSorting="True" AllowFilteringByColumn="True" EnableLinqExpressions="True"
                                                    GridLines="None" CssClass="RadGrid1" Skin="Metro" OnNeedDataSource="gridExternalUsers_NeedDataSource"
                                                    OnItemCommand="gridExternalUsers_ItemCommand" OnItemDataBound="gridExternalUsers_ItemDataBound"
                                                    Height="550" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                    AlternatingItemStyle-HorizontalAlign="Center">
                                                    <ClientSettings EnableRowHoverStyle="True">
                                                        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" EnableVirtualScrollPaging="false" />
                                                        <Selecting AllowRowSelect="True" />
                                                    </ClientSettings>
                                                    <FilterItemStyle HorizontalAlign="Center" />
                                                    <GroupingSettings CaseSensitive="False" />
                                                    <MasterTableView ShowHeader="true" AutoGenerateColumns="False" DataKeyNames="UserID,CategoryID,SubcategoryID,ClientID,StartDate,EndDate"
                                                        PageSize="15" EditMode="PopUp" EnableNoRecordsTemplate="True" ShowHeadersWhenNoRecords="True"
                                                        AllowPaging="True" TableLayout="Auto" Width="1300">
                                                        <EditFormSettings CaptionFormatString="Edit External User ID : {0}" CaptionDataField="UserID"
                                                            PopUpSettings-Height="330px" PopUpSettings-Width="660px">
                                                            <PopUpSettings Modal="true" ShowCaptionInEditForm="False" ScrollBars="Vertical" />
                                                        </EditFormSettings>
                                                        <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                        <Columns>
                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                HeaderStyle-Width="50px" EditImageUrl="Images/editpen2.png">
                                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                            </telerik:GridEditCommandColumn>
                                                            <telerik:GridButtonColumn UniqueName="ResetPassword" ButtonType="ImageButton" ImageUrl="~/Images/reset.png"
                                                                CommandName="ResetPassword" ConfirmDialogType="Classic" ConfirmText="Are you sure you want to reset password?"
                                                                Text="Reset Password" ConfirmTitle="Reset Password">
                                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                <HeaderStyle Width="30px" />
                                                            </telerik:GridButtonColumn>
                                                            <telerik:GridBoundColumn UniqueName="FirstName" HeaderText="First Name" DataField="FirstName"
                                                                AllowFiltering="True" SortExpression="FirstName" FilterControlWidth="100" AutoPostBackOnFilter="True"
                                                                ShowFilterIcon="False" CurrentFilterFunction="Contains" HeaderStyle-Width="150" />
                                                            <telerik:GridBoundColumn UniqueName="LastName" HeaderText="Last Name" DataField="LastName"
                                                                AllowFiltering="True" SortExpression="LastName" FilterControlWidth="100" AutoPostBackOnFilter="True"
                                                                ShowFilterIcon="False" CurrentFilterFunction="Contains" HeaderStyle-Width="150" />
                                                            <telerik:GridBoundColumn UniqueName="Email" HeaderText="Email Address" DataField="Email"
                                                                AllowFiltering="True" SortExpression="Email" FilterControlWidth="150" AutoPostBackOnFilter="True"
                                                                ShowFilterIcon="False" CurrentFilterFunction="Contains" HeaderStyle-Width="300" />
                                                            <telerik:GridBoundColumn UniqueName="ClientID" HeaderText="Client" DataField="ClientID"
                                                                AllowFiltering="False" HeaderStyle-Width="100" />
                                                            <telerik:GridBoundColumn UniqueName="StartDate" HeaderText="Start Date of Access"
                                                                DataField="StartDate" AllowFiltering="False" DataFormatString="{0:d}" HeaderStyle-Width="100" />
                                                            <telerik:GridBoundColumn UniqueName="EndDate" HeaderText="End Date of Access" DataField="EndDate"
                                                                AllowFiltering="False" DataFormatString="{0:d}" HeaderStyle-Width="100" />
                                                            <telerik:GridBoundColumn UniqueName="CategoryID" HeaderText="Category Access" DataField="CategoryID"
                                                                AllowFiltering="False" HeaderStyle-Width="100" />
                                                            <telerik:GridBoundColumn UniqueName="SubcategoryID" HeaderText="Subcategory Access"
                                                                DataField="SubcategoryID" AllowFiltering="False" HeaderStyle-Width="100" />
                                                            <telerik:GridBoundColumn UniqueName="UpdatedBy" HeaderText="Updated By" DataField="UpdatedBy"
                                                                AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="100" />
                                                        </Columns>
                                                        <EditFormSettings EditFormType="Template">
                                                            <FormTemplate>
                                                                <table id="tblEditMode" class="tabledefault">
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <asp:ValidationSummary ID="validationSummaryEditMode" ShowMessageBox="False" ShowSummary="True"
                                                                                DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                                runat="server" ValidationGroup="edit" CssClass="displayerror" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="First Name" ControlToValidate="txtFirstName"
                                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                                Text="*" />
                                                                            First Name
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadTextBox runat="server" ID="txtFirstName" Text='<%# Bind("FirstName") %>'
                                                                                EmptyMessage="- First Name -" Width="250" SelectionOnFocus="SelectAll" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Last Name" ControlToValidate="txtLastName"
                                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                                Text="*" />
                                                                            Last Name
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadTextBox runat="server" ID="txtLastName" Text='<%# Bind("LastName") %>'
                                                                                EmptyMessage="- Surname -" Width="250" SelectionOnFocus="SelectAll" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Email" ControlToValidate="txtEmail"
                                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                                Text="*" />
                                                                            Email
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadTextBox runat="server" ID="txtEmail" Text='<%# Bind("Email") %>' EmptyMessage="- Email Address -"
                                                                                Width="250" SelectionOnFocus="SelectAll" />
                                                                            <asp:RegularExpressionValidator ID="revEmail" runat="server" Display="Dynamic" ErrorMessage="Invalid email format!"
                                                                                ValidationExpression="^[\w\.\-]+@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]{1,})*(\.[a-zA-Z]{2,3}){1,2}$"
                                                                                ControlToValidate="txtEmail" SetFocusOnError="True" CssClass="displayerror" ValidationGroup="edit" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Client" ControlToValidate="cbClientOnEditExternal"
                                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                                Text="*" />
                                                                            Client
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadComboBox ID="cbClientOnEditExternal" DataTextField="Client" DataValueField="ClientID"
                                                                                runat="server" EmptyMessage="- Select Client -" DropDownAutoWidth="Enabled" MaxHeight="300"
                                                                                AllowCustomText="False" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Start Date of Access" ControlToValidate="dpStartDate"
                                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                                Text="*" />
                                                                            Start Date of Access
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadDatePicker ID="dpStartDate" runat="server" Width="150" EnableTyping="False"
                                                                                ShowPopupOnFocus="True" MaxDate="1/1/3000">
                                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                            </telerik:RadDatePicker>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="End Date of Access" ControlToValidate="dpEndDate"
                                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                                Text="*" />
                                                                            End Date of Access
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadDatePicker ID="dpEndDate" runat="server" Width="150" EnableTyping="False"
                                                                                ShowPopupOnFocus="True" MaxDate="1/1/3000">
                                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                            </telerik:RadDatePicker>
                                                                            <asp:CompareValidator ID="dateCompareValidator" runat="server" ControlToValidate="dpEndDate"
                                                                                ControlToCompare="dpStartDate" Operator="GreaterThan" Type="Date" ErrorMessage="The second date must be after the first one"
                                                                                CssClass="displayerror" ValidationGroup="edit" Text="Invalid date range!" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Category Access" ControlToValidate="cbCategoryOnEditExternal"
                                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                                Text="*" />
                                                                            Category Access
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadComboBox ID="cbCategoryOnEditExternal" DataTextField="Category" DataValueField="CategoryID"
                                                                                runat="server" EmptyMessage="- Select Category -" DropDownAutoWidth="Enabled"
                                                                                MaxHeight="300" AllowCustomText="False" AutoPostBack="True" OnSelectedIndexChanged="cbCategoryOnEditExternal_SelectedIndexChanged"
                                                                                CausesValidation="False" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Subcategory Access" ControlToValidate="ddtSubCategoryOnEditExternal"
                                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="edit"
                                                                                Text="*" />
                                                                            Subcategory Access
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadDropDownTree runat="server" ID="ddtSubCategoryOnEditExternal" DefaultMessage="- select subcategory -"
                                                                                DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                                                DataFieldParentID="ParentSubcategoryId" TextMode="Default" Width="400px" OnClientEntryAdding="OnClientEntryAdding"
                                                                                Skin="Default">
                                                                                <DropDownSettings Width="400px" Height="250px" />
                                                                            </telerik:RadDropDownTree>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <br />
                                                                            <asp:LinkButton ID="bntUpdate" ValidationGroup="edit" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                                runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' />
                                                                            |
                                                                            <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                                                                                CommandName="Cancel" />
                                                                            <br />
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </FormTemplate>
                                                        </EditFormSettings>
                                                    </MasterTableView>
                                                    <ClientSettings>
                                                        <ClientEvents OnPopUpShowing="PopUpShowing" />
                                                    </ClientSettings>
                                                </telerik:RadGrid>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView runat="server" ID="pvAddExternalUsers">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <div>
                                                    <table width="100%" class="tabledefault">
                                                        <tr>
                                                            <td class="tblLabel">
                                                            </td>
                                                            <td class="tblfield">
                                                                <asp:ValidationSummary ID="ValidationSummary1" ShowMessageBox="True" ShowSummary="True"
                                                                    DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                    runat="server" ValidationGroup="addExternalUser" CssClass="displayerror" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ErrorMessage="First Name"
                                                                    ControlToValidate="txtFirstName" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addExternalUser"
                                                                    CssClass="displayerror" Text="*" EnableClientScript="False" />
                                                                First Name
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadTextBox runat="server" ID="txtFirstName" SelectionOnFocus="SelectAll"
                                                                    EmptyMessage="- First Name -" Width="250" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ErrorMessage="Last Name"
                                                                    ControlToValidate="txtLastName" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addExternalUser"
                                                                    CssClass="displayerror" Text="*" EnableClientScript="False" />
                                                                Last Name
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadTextBox runat="server" ID="txtLastName" SelectionOnFocus="SelectAll"
                                                                    EmptyMessage="- Last Name -" Width="250" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ErrorMessage="Email Address"
                                                                    ControlToValidate="txtEmail" SetFocusOnError="True" Display="Dynamic" ValidationGroup="addExternalUser"
                                                                    CssClass="displayerror" Text="*" EnableClientScript="False" />
                                                                Email Address
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadTextBox runat="server" ID="txtEmail" SelectionOnFocus="SelectAll" EmptyMessage="- Email Address -"
                                                                    Width="250" />
                                                                <asp:RegularExpressionValidator ID="emailValidator" runat="server" Display="Dynamic"
                                                                    ErrorMessage="Please enter a valid e-mail address" ValidationExpression="^[\w\.\-]+@[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]{1,})*(\.[a-zA-Z]{2,3}){1,2}$"
                                                                    ControlToValidate="txtEmail" CssClass="displayerror" ValidationGroup="addExternalUser"
                                                                    Text="Please enter a valid e-mail address" EnableClientScript="False" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="rfvClientOnAddExternal" runat="server" ErrorMessage="Client Name"
                                                                    ControlToValidate="cbClientOnAddExternal" SetFocusOnError="True" Display="Dynamic"
                                                                    CssClass="displayerror" ValidationGroup="addExternalUser" Text="*" EnableClientScript="False" />
                                                                Client Name
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadComboBox ID="cbClientOnAddExternal" DataTextField="Client" DataValueField="ClientID"
                                                                    runat="server" EmptyMessage="- Select Client -" DropDownAutoWidth="Enabled" MaxHeight="300"
                                                                    AllowCustomText="False" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ErrorMessage="Start Date of Access"
                                                                    ControlToValidate="dpStartDate" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                    ValidationGroup="addExternalUser" Text="*" EnableClientScript="False" />
                                                                Start Date of Access
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadDatePicker ID="dpStartDate" runat="server" Width="150" EnableTyping="False"
                                                                    ShowPopupOnFocus="True" MinDate='<%# DateTime.Today %>' MaxDate="1/1/3000">
                                                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                </telerik:RadDatePicker>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ErrorMessage="End Date of Access"
                                                                    ControlToValidate="dpEndDate" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                    ValidationGroup="addExternalUser" Text="*" EnableClientScript="False" />
                                                                End Date of Access
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadDatePicker ID="dpEndDate" runat="server" Width="150" EnableTyping="False"
                                                                    ShowPopupOnFocus="True" MinDate='<%# DateTime.Today.AddMonths(1) %>' MaxDate="1/1/3000">
                                                                    <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                </telerik:RadDatePicker>
                                                                <asp:CompareValidator ID="dateCompareValidator" runat="server" ControlToValidate="dpEndDate"
                                                                    ControlToCompare="dpStartDate" Operator="GreaterThan" Type="Date" ErrorMessage="The second date must be after the first one"
                                                                    CssClass="displayerror" ValidationGroup="addExternalUser" Text="The second date must be after the first one"
                                                                    EnableClientScript="False" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="rfvCategoryOnAddExternal" runat="server" ErrorMessage="Category Access"
                                                                    ControlToValidate="cbCategoryOnAddExternal" SetFocusOnError="True" Display="Dynamic"
                                                                    CssClass="displayerror" ValidationGroup="addExternalUser" Text="*" EnableClientScript="False" />
                                                                Category Access
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadComboBox ID="cbCategoryOnAddExternal" DataTextField="Category" DataValueField="CategoryID"
                                                                    runat="server" EmptyMessage="- Select Category -" DropDownAutoWidth="Enabled"
                                                                    MaxHeight="300" AllowCustomText="False" AutoPostBack="True" OnSelectedIndexChanged="cbCategoryOnAddExternal_SelectedIndexChanged" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                                <asp:RequiredFieldValidator ID="rfvSubCategoryOnAddExternal" runat="server" ErrorMessage="Subcategory Access"
                                                                    ControlToValidate="ddtSubCategoryOnAddExternal" SetFocusOnError="True" Display="Dynamic"
                                                                    CssClass="displayerror" ValidationGroup="addExternalUser" Text="*" EnableClientScript="False" />
                                                                Subcategory Access
                                                            </td>
                                                            <td class="tblfield">
                                                                <telerik:RadDropDownTree runat="server" ID="ddtSubCategoryOnAddExternal" DefaultMessage="- select subcategory -"
                                                                    DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                                    DataFieldParentID="ParentSubcategoryId" TextMode="Default" Width="400px" OnClientEntryAdding="OnClientEntryAdding"
                                                                    Skin="Default">
                                                                    <DropDownSettings Width="400px" Height="250px" />
                                                                </telerik:RadDropDownTree>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="tblLabel">
                                                            </td>
                                                            <td class="tblfield">
                                                                <br />
                                                                <asp:LinkButton runat="server" ID="btnAddExternalUser" Text="Add User" ValidationGroup="addExternalUser"
                                                                    OnClick="btnAddExternalUser_Click" CssClass="linkBtn" />
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvEditor" runat="server">
                                <telerik:RadTabStrip ID="tabStripEditor" runat="server" SelectedIndex="0" MultiPageID="multiPageEditor"
                                    ShowBaseLine="True" Width="100%" CausesValidation="False" OnTabClick="tabStripEditor_TabClick">
                                    <Tabs>
                                        <telerik:RadTab Text="Welcome Message" Selected="True" />
                                        <telerik:RadTab Text="Featured Course" />
                                    </Tabs>
                                </telerik:RadTabStrip>
                                <telerik:RadMultiPage runat="server" ID="multiPageEditor" SelectedIndex="0" Width="100%">
                                    <telerik:RadPageView runat="server" ID="pvWelcomeMessage" Selected="True">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="right">
                                                            <asp:Panel ID="panelCourseControls" runat="server" Visible="false">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="font-size: 14pt">
                                                                            <asp:Label ID="lblCourse" Text="Course Name : " runat="server" ForeColor="Black" /><asp:Label
                                                                                ID="lblCourseEditName" runat="server" Text="[Course Name]" ForeColor="Black" />
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:HiddenField ID="hiddenFilePath" Value="" runat="server" />
                                                                            <telerik:RadButton ID="btnNewCourse" runat="server" Text="New Course" OnClientClicked="ShowDialog"
                                                                                AutoPostBack="false" />
                                                                            <telerik:RadButton ID="selectFile" runat="server" Text="Select Course" OnClientClicked="OpenFileExplorerDialog"
                                                                                AutoPostBack="false" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <hr style="margin-bottom: -1px" />
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <telerik:RadEditor ID="editorWelcomeMessage" runat="server" EnableResize="false"
                                                                ToolsFile="~/App_Data/Editor_BasicTools.xml" AllowScripts="false" Width="100%"
                                                                Height="550px" EmptyMessage="- Content Area -" AutoResizeHeight="false" OnClientLoad="initialEditorSize">
                                                                <CssFiles>
                                                                    <telerik:EditorCssFile Value="~/Styles/EditorContentArea.css" />
                                                                </CssFiles>
                                                                <ImageManager ViewPaths="~/PageFiles/Images" UploadPaths="~/PageFiles/Images" DeletePaths="~/PageFiles/Images" />
                                                            </telerik:RadEditor>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-top: 50px;">
                                                            <telerik:RadButton ID="btnSaveWelcomeMsg" runat="server" Text="Save Welcome Message"
                                                                OnClick="btnSaveWelcomeMsg_OnClick" UseSubmitBehavior="False" />
                                                            <telerik:RadButton ID="btnClearWelcomeMsg" runat="server" Text="Clear" OnClick="btnClearWelcomeMsg_OnClick"
                                                                UseSubmitBehavior="False" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvAudience" runat="server">
                                <telerik:RadTabStrip ID="tsAudience" runat="server" SelectedIndex="0" MultiPageID="mpAudience"
                                    ShowBaseLine="True" Width="100%" CausesValidation="False">
                                    <Tabs>
                                        <telerik:RadTab Text="Audience" Selected="True" />
                                        <telerik:RadTab Text="Add Audience" />
                                    </Tabs>
                                </telerik:RadTabStrip>
                                <telerik:RadMultiPage ID="mpAudience" SelectedIndex="0" Width="100%" runat="server">
                                    <telerik:RadPageView ID="pvAudienceList" runat="server" Selected="True">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <table width="100%" style="padding-bottom: 5px">
                                                    <tr>
                                                        <td align="left">
                                                            <asp:CheckBox ID="chkElapsed" runat="server" Text="Include Elapsed" OnCheckedChanged="chkElapsed_CheckedChanged"
                                                                AutoPostBack="true" />
                                                        </td>
                                                        <td align="right" style="line-height: 20px; vertical-align: bottom">
                                                            <div style="vertical-align: bottom">
                                                                <asp:LinkButton runat="server" ID="btnExportExcelAudience" CssClass="linkBtn" OnClick="btnExportExcel_Click"
                                                                    Height="100%" OnClientClick="if(!confirm('Are you sure you want to export audience list to excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <telerik:RadGrid runat="server" ID="gridAudience" AutoGenerateColumns="False" AllowFilteringByColumn="True"
                                                    AllowSorting="True" GridLines="None" CssClass="RadGrid1 gridAudience" Skin="Metro"
                                                    PageSize="15" OnNeedDataSource="gridAudience_NeedDataSource" OnItemCommand="gridAudience_ItemCommand"
                                                    OnItemDataBound="gridAudience_ItemDataBound" OnCancelCommand="gridAudience_CancelCommand"
                                                    OnEditCommand="gridAudience_EditCommand">
                                                    <GroupingSettings CaseSensitive="False" />
                                                    <FilterItemStyle HorizontalAlign="Center" />
                                                    <ClientSettings EnableRowHoverStyle="True">
                                                        <Selecting AllowRowSelect="True" />
                                                        <ClientEvents OnPopUpShowing="PopUpShowing" />
                                                    </ClientSettings>
                                                    <MasterTableView DataKeyNames="AudienceID,AudienceName,DateStart,DateEnd" ShowHeader="true"
                                                        AutoGenerateColumns="False" PageSize="10" EditMode="PopUp" EnableNoRecordsTemplate="True"
                                                        NoMasterRecordsText="No audience record." ShowHeadersWhenNoRecords="True" AllowPaging="True"
                                                        TableLayout="Auto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        AlternatingItemStyle-HorizontalAlign="Center">
                                                        <EditFormSettings CaptionFormatString="Edit Audience ID: {0}" CaptionDataField="AudienceID"
                                                            PopUpSettings-Height="650px" PopUpSettings-Width="990px">
                                                            <PopUpSettings Modal="true" ShowCaptionInEditForm="False" ScrollBars="Vertical" />
                                                        </EditFormSettings>
                                                        <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                        <Columns>
                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                HeaderStyle-Width="50px" EditImageUrl="Images/editpen2.png">
                                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                            </telerik:GridEditCommandColumn>
                                                            <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="40px"
                                                                UniqueName="ParticipantColumn">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnViewAudienceMemberCIMList" runat="server" ToolTip="CIM List"
                                                                        OnClientClick="View_AudienceMembers()" OnClick="btnViewAudienceMemberCIMList_Click">
                                                                            <img style="border:0;" alt="" src="Images/party1.png"/>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                                <ItemStyle CssClass="MyImageButton" />
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridBoundColumn UniqueName="AudienceID" HeaderText="Audience ID" DataField="AudienceID"
                                                                AllowFiltering="False" ReadOnly="True">
                                                                <HeaderStyle ForeColor="Silver" />
                                                                <ItemStyle ForeColor="Gray" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn UniqueName="AudienceName" HeaderText="Audience Name" DataField="AudienceName"
                                                                FilterControlWidth="150" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True"
                                                                ShowFilterIcon="False" />
                                                            <telerik:GridDateTimeColumn UniqueName="DateStart" HeaderText="Date Start" DataField="DateStart"
                                                                DataFormatString="{0:M/d/yyyy}" AllowFiltering="False" DataType="System.DateTime"
                                                                PickerType="DatePicker" />
                                                            <telerik:GridBoundColumn UniqueName="DateEnd" HeaderText="Date End" DataField="DateEnd"
                                                                DataFormatString="{0:M/d/yyyy}" AllowFiltering="False" DataType="System.DateTime" />
                                                            <telerik:GridBoundColumn UniqueName="DateUpdated" HeaderText="Date Updated" DataField="DateUpdated"
                                                                DataFormatString="{0:M/d/yyyy}" DataType="System.DateTime" ReadOnly="True" AllowFiltering="false" />
                                                            <telerik:GridBoundColumn UniqueName="UpdatedBy" HeaderText="Updated By" DataField="UpdatedBy"
                                                                CurrentFilterFunction="Contains" AutoPostBackOnFilter="True" ShowFilterIcon="False"
                                                                ReadOnly="True" />
                                                        </Columns>
                                                        <EditFormSettings EditFormType="Template">
                                                            <FormTemplate>
                                                                <table id="tblEditMode" class="tabledefault">
                                                                    <tr>
                                                                        <td class="tblLabel" style="width: 25%;">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <h3>
                                                                                Audience Details</h3>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <hr />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <asp:ValidationSummary ID="validationSummaryEditAudience" ShowMessageBox="False"
                                                                                ShowSummary="True" DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                                runat="server" ValidationGroup="vgEditAudience" CssClass="displayerror" EnableViewState="False" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="rfvAudienceNameEdit" runat="server" ErrorMessage="Audience Name"
                                                                                ControlToValidate="txtAudienceNameEdit" SetFocusOnError="True" Display="Dynamic"
                                                                                CssClass="displayerror" ValidationGroup="vgEditAudience" EnableClientScript="False"
                                                                                Text="*" />Audience Name
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadTextBox runat="server" ID="txtAudienceNameEdit" EmptyMessage="- audience name -"
                                                                                Width="250" SelectionOnFocus="SelectAll" Text='<%# Eval("AudienceName") %>' />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:CompareValidator ID="dateCompareValidatorEdit" runat="server" ControlToValidate="dpDateEndEdit"
                                                                                ControlToCompare="dpDateStartEdit" Operator="GreaterThan" Type="Date" ErrorMessage="The second date must be after the first one"
                                                                                CssClass="displayerror" ValidationGroup="vgEditAudience" Text="*" EnableClientScript="False" />
                                                                            Date Start
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadDatePicker ID="dpDateStartEdit" runat="server" Width="150" EnableTyping="False"
                                                                                ShowPopupOnFocus="True" SelectedDate='<%# Bind("DateStart") %>'>
                                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                            </telerik:RadDatePicker>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            Date End
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadDatePicker ID="dpDateEndEdit" runat="server" Width="150" EnableTyping="False"
                                                                                ShowPopupOnFocus="True" SelectedDate='<%# Bind("DateEnd") %>'>
                                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                            </telerik:RadDatePicker>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <h3>
                                                                                Add Audience Members</h3>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <hr />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <asp:ValidationSummary ID="vsEditAudienceMember" ShowMessageBox="false" ShowSummary="True"
                                                                                DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                                runat="server" ValidationGroup="vgEditAudienceMember" CssClass="displayerror" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:RequiredFieldValidator ID="rfvAudienceMemberNameEdit" runat="server" ErrorMessage="Audience Name"
                                                                                ControlToValidate="txtAudienceMemberNameEdit" SetFocusOnError="True" Display="Dynamic"
                                                                                ValidationGroup="vgEditAudienceMember" CssClass="displayerror" Text="*" />
                                                                            Audience Member Name
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadTextBox runat="server" ID="txtAudienceMemberNameEdit" SelectionOnFocus="SelectAll"
                                                                                EmptyMessage="- Audience Member Name -" Width="250" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                            <asp:CustomValidator ID="cvComboBoxFilterTypesEdit" runat="server" ErrorMessage="A type of filter can only be used once."
                                                                                ClientValidationFunction="CompareComboBoxes" Display="Dynamic" ValidateEmptyText="true"
                                                                                Text="*" ValidationGroup="vgEditAudienceMember"></asp:CustomValidator>
                                                                            Audience Members Filters
                                                                        </td>
                                                                        <td class="tblfield" colspan="2">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <fieldset>
                                                                                            <legend align="right">Condition/s</legend>
                                                                                            <table id="tblFilter" runat="server" style="padding-bottom: 5px;">
                                                                                                <tr id="rowEdit1" runat="server">
                                                                                                    <td>
                                                                                                        <telerik:RadComboBox ID="cbFilterTypeEdit1" runat="server" DataSourceID="dsAudienceType"
                                                                                                            DataTextField="AudienceType" DataValueField="AudienceTypeID" DropDownAutoWidth="Enabled"
                                                                                                            MaxHeight="300px" AutoPostBack="true" ToolTip="Select Audience Type" OnSelectedIndexChanged="cbGenericFilterType_SelectedIndexChanged"
                                                                                                            CausesValidation="false" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" />
                                                                                                        <telerik:RadComboBox ID="cbFilterValueEdit1" runat="server" DropDownAutoWidth="Enabled"
                                                                                                            EmptyMessage="- select item -" MaxHeight="300px" ToolTip="Select Value" DataSourceID="dsClass"
                                                                                                            DataTextField="ClassID" DataValueField="ClassID" />
                                                                                                        <telerik:RadNumericTextBox ID="txtFilterValueEdit1" runat="server" EmptyMessage=" - cim number - "
                                                                                                            Visible="false" ToolTip="Specify Value" ClientEvents-OnBlur="ValidateCIMAudience">
                                                                                                            <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                                                        </telerik:RadNumericTextBox>
                                                                                                        <asp:Label ID="lblFilterValueNameEdit1" runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="rowEdit2" runat="server">
                                                                                                    <td>
                                                                                                        <telerik:RadComboBox ID="cbFilterTypeEdit2" runat="server" DataSourceID="dsAudienceType"
                                                                                                            DataTextField="AudienceType" DataValueField="AudienceTypeID" DropDownAutoWidth="Enabled"
                                                                                                            MaxHeight="300px" AutoPostBack="true" ToolTip="Select Audience Type" OnSelectedIndexChanged="cbGenericFilterType_SelectedIndexChanged"
                                                                                                            CausesValidation="false" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" />
                                                                                                        <telerik:RadComboBox ID="cbFilterValueEdit2" runat="server" DropDownAutoWidth="Enabled"
                                                                                                            EmptyMessage="- select item -" MaxHeight="300px" ToolTip="Select Value" DataSourceID="dsClass"
                                                                                                            DataTextField="ClassID" DataValueField="ClassID" />
                                                                                                        <telerik:RadNumericTextBox ID="txtFilterValueEdit2" runat="server" EmptyMessage=" - cim number - "
                                                                                                            Visible="false" ToolTip="Specify Value" ClientEvents-OnBlur="ValidateCIMAudience">
                                                                                                            <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                                                        </telerik:RadNumericTextBox>
                                                                                                        <asp:Label ID="lblFilterValueNameEdit2" runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <tr id="rowEdit3" runat="server">
                                                                                                    <td>
                                                                                                        <telerik:RadComboBox ID="cbFilterTypeEdit3" runat="server" DataSourceID="dsAudienceType"
                                                                                                            DataTextField="AudienceType" DataValueField="AudienceTypeID" DropDownAutoWidth="Enabled"
                                                                                                            MaxHeight="300px" AutoPostBack="true" ToolTip="Select Audience Type" OnSelectedIndexChanged="cbGenericFilterType_SelectedIndexChanged"
                                                                                                            CausesValidation="false" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" />
                                                                                                        <telerik:RadComboBox ID="cbFilterValueEdit3" runat="server" DropDownAutoWidth="Enabled"
                                                                                                            EmptyMessage="- select item -" MaxHeight="300px" ToolTip="Select Value" DataSourceID="dsClass"
                                                                                                            DataTextField="ClassID" DataValueField="ClassID" />
                                                                                                        <telerik:RadNumericTextBox ID="txtFilterValueEdit3" runat="server" EmptyMessage=" - cim number - "
                                                                                                            Visible="false" ToolTip="Specify Value" ClientEvents-OnBlur="ValidateCIMAudience">
                                                                                                            <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                                                        </telerik:RadNumericTextBox>
                                                                                                        <asp:Label ID="lblFilterValueNameEdit3" runat="server"></asp:Label>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </fieldset>
                                                                                    </td>
                                                                                    <td style="vertical-align: bottom; padding-bottom: 20px">
                                                                                        <asp:ImageButton ID="btnAddFilterEdit" ImageUrl="Images/addFilter_active.png" runat="server"
                                                                                            ToolTip="Add Filter" OnClientClick="ShowHideFilter('add');return false;" CssClass="imgBtn" />
                                                                                        <asp:ImageButton ID="btnRemoveFilterEdit" ImageUrl="Images/deleteFilter_inactive.png"
                                                                                            runat="server" ToolTip="Delete Filter" OnClientClick="ShowHideFilter('delete');return false;"
                                                                                            CssClass="imgBtn" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <asp:LinkButton ID="btnAddFilterGroupEdit" runat="server" CssClass="linkBtn" OnClick="btnAddFilterGroup_Click"
                                                                                OnClientClick="return DoValidation('vgEditAudienceMember');" ValidationGroup="vgEditAudienceMember"
                                                                                CausesValidation="false" Text="Add Selected Filters >" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <telerik:RadGrid runat="server" ID="rgAudienceMembersEdit" AutoGenerateColumns="False"
                                                                                AllowPaging="False" GridLines="None" CssClass="RadGrid1" Skin="Metro" OnNeedDataSource="rgAudienceMembers_NeedDataSource"
                                                                                OnItemDataBound="rgAudienceMembers_ItemDataBound" OnItemCommand="rgAudienceMembers_ItemCommand"
                                                                                Width="700">
                                                                                <ClientSettings EnableRowHoverStyle="True">
                                                                                    <Selecting AllowRowSelect="True" />
                                                                                </ClientSettings>
                                                                                <FilterItemStyle HorizontalAlign="Center" />
                                                                                <MasterTableView ShowHeader="true" DataKeyNames="AudienceMemberID,FilterType,FilterTypeVal,FilterName,FilterNameVal"
                                                                                    AutoGenerateColumns="False" EnableNoRecordsTemplate="True" NoMasterRecordsText="No filter Added."
                                                                                    ShowHeadersWhenNoRecords="True" TableLayout="Auto" ItemStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                                                                    <Columns>
                                                                                        <telerik:GridBoundColumn UniqueName="AudienceMemberName" DataField="AudienceMemberName"
                                                                                            HeaderText="Audience Member Name">
                                                                                        </telerik:GridBoundColumn>
                                                                                        <telerik:GridTemplateColumn UniqueName="Filters" DataField="Filters" HeaderText="Filters">
                                                                                            <ItemTemplate>
                                                                                                <telerik:RadListBox ID="lbFiltersEdit" runat="server" OnItemDataBound="lbFiltersAdd_ItemDataBound"
                                                                                                    Width="100%">
                                                                                                    <ItemTemplate>
                                                                                                        <table width="100%" class="tabledefault">
                                                                                                            <tr>
                                                                                                                <td width="40%">
                                                                                                                    <asp:Label ID="lblFilterTypeEdit" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"FilterType\"]")%>'></asp:Label>
                                                                                                                </td>
                                                                                                                <td width="4%">
                                                                                                                    <asp:Label ID="Label1" runat="server" Text=":"></asp:Label>
                                                                                                                </td>
                                                                                                                <td width="56%">
                                                                                                                    <asp:Label ID="lblFilterNameEdit" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"FilterName\"]")%>'></asp:Label>
                                                                                                                </td>
                                                                                                            </tr>
                                                                                                        </table>
                                                                                                    </ItemTemplate>
                                                                                                </telerik:RadListBox>
                                                                                            </ItemTemplate>
                                                                                        </telerik:GridTemplateColumn>
                                                                                        <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                                            ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                                            ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                                            ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="50">
                                                                                        </telerik:GridButtonColumn>
                                                                                    </Columns>
                                                                                </MasterTableView>
                                                                            </telerik:RadGrid>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="tblLabel">
                                                                        </td>
                                                                        <td class="tblfield">
                                                                            <br />
                                                                            <asp:LinkButton runat="server" ID="btnUpdate" Text="Update" CommandName="Update"
                                                                                OnClientClick="EndEditMode();" ValidationGroup="vgEditAudience" />
                                                                            |
                                                                            <asp:LinkButton runat="server" ID="btnClose" Text="Cancel" CommandName="Cancel" OnClientClick="EndEditMode();" />
                                                                            <br />
                                                                            <br />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </FormTemplate>
                                                        </EditFormSettings>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView ID="pvAudienceMembers" runat="server">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <table width="100%" class="tabledefault">
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <h3>
                                                                Audience</h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <hr />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:ValidationSummary ID="vsAddAudience" ShowMessageBox="True" ShowSummary="True"
                                                                DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                runat="server" ValidationGroup="vgAddAudience" CssClass="displayerror" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="rfvAudienceName" runat="server" ErrorMessage="Audience Name"
                                                                ControlToValidate="txtAudienceNameAdd" SetFocusOnError="True" Display="Dynamic"
                                                                ValidationGroup="vgAddAudience" CssClass="displayerror" Text="*" EnableClientScript="False" />Audience
                                                            Name
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtAudienceNameAdd" SelectionOnFocus="SelectAll"
                                                                EmptyMessage="- Audience Name -" Width="250" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="dpDateEndAdd"
                                                                ControlToCompare="dpDateStartAdd" Operator="GreaterThan" Type="Date" ErrorMessage="The second date must be after the first one"
                                                                CssClass="displayerror" ValidationGroup="vgAddAudience" Text="*" EnableClientScript="False" />
                                                            Date Start
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadDatePicker ID="dpDateStartAdd" runat="server" Width="150" EnableTyping="False"
                                                                ShowPopupOnFocus="True" MinDate='<%# DateTime.Today %>' MaxDate="1/1/3000" SelectedDate='<%# DateTime.Today %>'>
                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            Date End
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadDatePicker ID="dpDateEndAdd" runat="server" Width="150" EnableTyping="False"
                                                                ShowPopupOnFocus="True" MinDate='<%# DateTime.Today.AddMonths(1) %>' MaxDate="1/1/3000"
                                                                SelectedDate='<%# DateTime.Today.AddMonths(1) %>'>
                                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <h3>
                                                                Audience Member</h3>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <hr />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:ValidationSummary ID="vsAddAudienceMember" ShowMessageBox="false" ShowSummary="True"
                                                                DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                runat="server" ValidationGroup="vgAddAudienceMember" CssClass="displayerror" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="rfvAudienceMemberAdd" runat="server" ErrorMessage="Audience Name"
                                                                ControlToValidate="txtAudienceMemberName" SetFocusOnError="True" Display="Dynamic"
                                                                ValidationGroup="vgAddAudienceMember" CssClass="displayerror" Text="*" />
                                                            Audience Member Name
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtAudienceMemberName" SelectionOnFocus="SelectAll"
                                                                EmptyMessage="- Audience Member Name -" Width="250" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:CustomValidator ID="cvComboBoxFilterTypes" runat="server" ErrorMessage="A type of filter can only be used once."
                                                                ClientValidationFunction="CompareComboBoxes" Display="Dynamic" ValidateEmptyText="true"
                                                                Text="*" ValidationGroup="vgAddAudienceMember"></asp:CustomValidator>
                                                            Audience Members Filters
                                                        </td>
                                                        <td class="tblfield" colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <fieldset>
                                                                            <legend align="right">Condition/s</legend>
                                                                            <table id="tblFilter" runat="server" style="padding-bottom: 5px;">
                                                                                <tr id="row1" runat="server">
                                                                                    <td>
                                                                                        <telerik:RadComboBox ID="cbFilterType1" runat="server" DataSourceID="dsAudienceType"
                                                                                            DataTextField="AudienceType" DataValueField="AudienceTypeID" DropDownAutoWidth="Enabled"
                                                                                            MaxHeight="300px" AutoPostBack="true" ToolTip="Select Audience Type" OnSelectedIndexChanged="cbGenericFilterType_SelectedIndexChanged"
                                                                                            CausesValidation="false" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" />
                                                                                        <telerik:RadComboBox ID="cbFilterValue1" runat="server" DropDownAutoWidth="Enabled"
                                                                                            EmptyMessage="- select item -" MaxHeight="300px" ToolTip="Select Value" DataSourceID="dsClass"
                                                                                            DataTextField="ClassID" DataValueField="ClassID" />
                                                                                        <telerik:RadNumericTextBox ID="txtFilterValue1" runat="server" EmptyMessage=" - cim number - "
                                                                                            Visible="false" ToolTip="Specify Value" ClientEvents-OnBlur="ValidateCIMAudience">
                                                                                            <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                                        </telerik:RadNumericTextBox>
                                                                                        <asp:Label ID="lblFilterValueName1" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="row2" runat="server">
                                                                                    <td>
                                                                                        <telerik:RadComboBox ID="cbFilterType2" runat="server" DataSourceID="dsAudienceType"
                                                                                            DataTextField="AudienceType" DataValueField="AudienceTypeID" DropDownAutoWidth="Enabled"
                                                                                            MaxHeight="300px" AutoPostBack="true" ToolTip="Select Audience Type" OnSelectedIndexChanged="cbGenericFilterType_SelectedIndexChanged"
                                                                                            CausesValidation="false" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" />
                                                                                        <telerik:RadComboBox ID="cbFilterValue2" runat="server" DropDownAutoWidth="Enabled"
                                                                                            EmptyMessage="- select item -" MaxHeight="300px" ToolTip="Select Value" DataSourceID="dsClass"
                                                                                            DataTextField="ClassID" DataValueField="ClassID" />
                                                                                        <telerik:RadNumericTextBox ID="txtFilterValue2" runat="server" EmptyMessage=" - cim number - "
                                                                                            Visible="false" ToolTip="Specify Value" ClientEvents-OnBlur="ValidateCIMAudience">
                                                                                            <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                                        </telerik:RadNumericTextBox>
                                                                                        <asp:Label ID="lblFilterValueName2" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr id="row3" runat="server">
                                                                                    <td>
                                                                                        <telerik:RadComboBox ID="cbFilterType3" runat="server" DataSourceID="dsAudienceType"
                                                                                            DataTextField="AudienceType" DataValueField="AudienceTypeID" DropDownAutoWidth="Enabled"
                                                                                            MaxHeight="300px" AutoPostBack="true" ToolTip="Select Audience Type" OnSelectedIndexChanged="cbGenericFilterType_SelectedIndexChanged"
                                                                                            CausesValidation="false" OnClientSelectedIndexChanged="OnClientSelectedIndexChanged" />
                                                                                        <telerik:RadComboBox ID="cbFilterValue3" runat="server" DropDownAutoWidth="Enabled"
                                                                                            EmptyMessage="- select item -" MaxHeight="300px" ToolTip="Select Value" DataSourceID="dsClass"
                                                                                            DataTextField="ClassID" DataValueField="ClassID" />
                                                                                        <telerik:RadNumericTextBox ID="txtFilterValue3" runat="server" EmptyMessage=" - cim number - "
                                                                                            Visible="false" ToolTip="Specify Value" ClientEvents-OnBlur="ValidateCIMAudience">
                                                                                            <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                                                        </telerik:RadNumericTextBox>
                                                                                        <asp:Label ID="lblFilterValueName3" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </fieldset>
                                                                    </td>
                                                                    <td style="vertical-align: bottom; padding-bottom: 20px">
                                                                        <div>
                                                                            <asp:ImageButton ID="btnAddFilter" ImageUrl="Images/addFilter_active.png" runat="server"
                                                                                ToolTip="Add Filter" OnClientClick="ShowHideFilter('add');return false;" CssClass="imgBtn" />
                                                                            <asp:ImageButton ID="btnRemoveFilter" ImageUrl="Images/deleteFilter_inactive.png"
                                                                                runat="server" ToolTip="Delete Filter" OnClientClick="ShowHideFilter('delete');return false;"
                                                                                CssClass="imgBtn" />
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:LinkButton ID="btnAddFilterGroup" runat="server" CssClass="linkBtn" OnClick="btnAddFilterGroup_Click"
                                                                ValidationGroup="vgAddAudienceMember" CausesValidation="false" OnClientClick="return DoValidation('vgAddAudienceMember');"
                                                                Text="Add Selected Filters >" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadGrid runat="server" ID="rgAudienceMembers" AutoGenerateColumns="False"
                                                                AllowPaging="False" GridLines="None" CssClass="RadGrid1" Skin="Metro" OnNeedDataSource="rgAudienceMembers_NeedDataSource"
                                                                OnItemDataBound="rgAudienceMembers_ItemDataBound" OnItemCommand="rgAudienceMembers_ItemCommand"
                                                                Width="100%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                AlternatingItemStyle-HorizontalAlign="Center">
                                                                <ClientSettings EnableRowHoverStyle="True">
                                                                    <Selecting AllowRowSelect="True" />
                                                                </ClientSettings>
                                                                <FilterItemStyle HorizontalAlign="Center" />
                                                                <MasterTableView ShowHeader="true" DataKeyNames="AudienceMemberID,FilterType,FilterTypeVal,FilterName,FilterNameVal"
                                                                    AutoGenerateColumns="False" EnableNoRecordsTemplate="True" NoMasterRecordsText="No filter Added."
                                                                    ShowHeadersWhenNoRecords="True" TableLayout="Auto">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn UniqueName="AudienceMemberName" DataField="AudienceMemberName"
                                                                            HeaderText="Audience Member Name">
                                                                        </telerik:GridBoundColumn>
                                                                        <telerik:GridTemplateColumn UniqueName="Filters" DataField="Filters" HeaderText="Filters">
                                                                            <ItemTemplate>
                                                                                <telerik:RadListBox ID="lbFiltersAdd" runat="server" OnItemDataBound="lbFiltersAdd_ItemDataBound"
                                                                                    Width="100%">
                                                                                    <ItemTemplate>
                                                                                        <table width="100%" class="tabledefault">
                                                                                            <tr>
                                                                                                <td width="40%">
                                                                                                    <asp:Label ID="lblFilterTypeAdd" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"FilterType\"]")%>'></asp:Label>
                                                                                                </td>
                                                                                                <td width="4%">
                                                                                                    <asp:Label ID="Label2" runat="server" Text=":"></asp:Label>
                                                                                                </td>
                                                                                                <td width="56%">
                                                                                                    <asp:Label ID="lblFilterNameAdd" runat="server" Text='<%# DataBinder.Eval(Container, "Attributes[\"FilterName\"]")%>'></asp:Label>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </ItemTemplate>
                                                                                </telerik:RadListBox>
                                                                            </ItemTemplate>
                                                                        </telerik:GridTemplateColumn>
                                                                        <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                            ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                            ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                            ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="50">
                                                                        </telerik:GridButtonColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <br />
                                                            <asp:LinkButton runat="server" ID="btnAddAudience" Text="Add" ValidationGroup="vgAddAudience"
                                                                CssClass="linkBtn" OnClick="btnAddAudience_Click" />
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvClassNames" runat="server">
                                <telerik:RadTabStrip ID="tsClassNameMaintenance" runat="server" SelectedIndex="0"
                                    MultiPageID="mpClassNameMaintenance" ShowBaseLine="True" Width="100%" CausesValidation="False">
                                    <Tabs>
                                        <telerik:RadTab Text="Class Name List" Selected="True" />
                                        <telerik:RadTab Text="Add Class Name" />
                                    </Tabs>
                                </telerik:RadTabStrip>
                                <telerik:RadMultiPage runat="server" ID="mpClassNameMaintenance" SelectedIndex="0"
                                    Width="100%">
                                    <telerik:RadPageView runat="server" ID="pvViewClass" Selected="True">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <table width="100%" style="padding-bottom: 5px">
                                                    <tr>
                                                        <td style="width: 164px">
                                                            <telerik:RadComboBox ID="cbClient" DataTextField="ClientName" DataValueField="Id"
                                                                runat="server" EmptyMessage="- select client -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                                DataSourceID="dsClient" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="cbClient_SelectedIndexChanged" />
                                                        </td>
                                                        <td>
                                                            <telerik:RadComboBox ID="cbTrainingType" DataTextField="TrainingType" DataValueField="TrainingTypeId"
                                                                runat="server" EmptyMessage="- select training type -" DropDownAutoWidth="Enabled"
                                                                MaxHeight="300px" AutoPostBack="True" OnSelectedIndexChanged="cbTrainingType_SelectedIndexChanged" />
                                                        </td>
                                                        <td align="right" style="line-height: 20px; vertical-align: bottom">
                                                            <div style="vertical-align: bottom">
                                                                <asp:LinkButton runat="server" ID="btnExportExcelClasses" CssClass="linkBtn" OnClick="btnExportExcel_Click"
                                                                    Height="100%" OnClientClick="if(!confirm('Are you sure you want to export class list to excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div>
                                                    <telerik:RadGrid runat="server" ID="gridClasses" AutoGenerateColumns="False" AllowSorting="True"
                                                        GridLines="None" CssClass="RadGrid1" Skin="Metro" PageSize="15" OnNeedDataSource="gridClasses_NeedDataSource"
                                                        OnItemCommand="gridClasses_ItemCommand" OnItemDataBound="gridClasses_ItemDataBound">
                                                        <GroupingSettings CaseSensitive="False" />
                                                        <FilterItemStyle HorizontalAlign="Center" />
                                                        <MasterTableView DataKeyNames="ID,ClassName,ClientID,TrainingTypeID,Active,TrainingType,Client"
                                                            AutoGenerateColumns="False" ShowHeader="True" PageSize="10" EditMode="PopUp"
                                                            EnableNoRecordsTemplate="True" NoMasterRecordsText="No class name/s to display."
                                                            ShowHeadersWhenNoRecords="True" AllowPaging="True" TableLayout="Auto" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center" AlternatingItemStyle-HorizontalAlign="Center">
                                                            <EditFormSettings CaptionFormatString="Edit Class Name ID : {0}" CaptionDataField="ID"
                                                                PopUpSettings-Height="215px" PopUpSettings-Width="650px">
                                                                <PopUpSettings Modal="true" ShowCaptionInEditForm="False" ScrollBars="Vertical" />
                                                            </EditFormSettings>
                                                            <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                                            <Columns>
                                                                <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                    HeaderStyle-Width="50px" EditImageUrl="Images/editpen2.png">
                                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                </telerik:GridEditCommandColumn>
                                                                <telerik:GridBoundColumn UniqueName="ID" HeaderText="ID" DataField="ID" AllowFiltering="False"
                                                                    Display="False">
                                                                    <HeaderStyle ForeColor="Silver" />
                                                                    <ItemStyle ForeColor="Gray" />
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn UniqueName="ClassName" HeaderText="Class Name" DataField="ClassName"
                                                                    AllowFiltering="False" />
                                                                <telerik:GridBoundColumn UniqueName="ClientID" HeaderText="Client ID" DataField="ClientID"
                                                                    AllowFiltering="False" Display="False" />
                                                                <telerik:GridBoundColumn UniqueName="Client" HeaderText="Client" DataField="Client"
                                                                    AllowFiltering="False" />
                                                                <telerik:GridBoundColumn UniqueName="TrainingTypeID" HeaderText="Training Type ID"
                                                                    DataField="TrainingTypeID" AllowFiltering="False" Display="False" />
                                                                <telerik:GridBoundColumn UniqueName="TrainingType" HeaderText="Training Type" DataField="TrainingType"
                                                                    AllowFiltering="False" />
                                                                <telerik:GridBoundColumn UniqueName="Active" HeaderText="Active" DataField="Active"
                                                                    AllowFiltering="False" />
                                                            </Columns>
                                                            <EditFormSettings EditFormType="Template">
                                                                <FormTemplate>
                                                                    <table id="tblEditMode" class="tabledefault">
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <asp:ValidationSummary ID="validationSummaryEditMode" ShowMessageBox="False" ShowSummary="True"
                                                                                    DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                                    runat="server" ValidationGroup="edit" CssClass="displayerror" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Client"
                                                                                    ControlToValidate="cbClient_edit" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                                    ValidationGroup="edit" Text="*" EnableClientScript="False" />
                                                                                Client
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadComboBox ID="cbClient_edit" DataTextField="ClientName" DataValueField="Id"
                                                                                    runat="server" EmptyMessage="- client -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                                                    DataSourceID="dsClient" AutoPostBack="True" OnSelectedIndexChanged="cbClientEdit_SelectedIndexChanged" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Training Type"
                                                                                    ControlToValidate="cbTrainingType_edit" SetFocusOnError="True" Display="Dynamic"
                                                                                    CssClass="displayerror" ValidationGroup="edit" Text="*" EnableClientScript="False" />
                                                                                Training Type
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadComboBox ID="cbTrainingType_edit" DataTextField="TrainingType" DataValueField="TrainingTypeId"
                                                                                    runat="server" EmptyMessage="- training type -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                                                    AutoPostBack="True" OnSelectedIndexChanged="cbTrainingTypeEdit_SelectedIndexChanged" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Class Name"
                                                                                    ControlToValidate="cbClassName_edit" SetFocusOnError="True" Display="Dynamic"
                                                                                    CssClass="displayerror" ValidationGroup="edit" Text="*" EnableClientScript="False" />
                                                                                Class Name
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <telerik:RadComboBox ID="cbClassName_edit" runat="server" EmptyMessage="- select -"
                                                                                    DropDownAutoWidth="Enabled" MaxHeight="300px" DataTextField="ClassName" DataValueField="ClassName"
                                                                                    EnableViewState="False">
                                                                                </telerik:RadComboBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                                Active
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <asp:CheckBox ID="chkActive" runat="server" Text="Active" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="tblLabel">
                                                                            </td>
                                                                            <td class="tblfield">
                                                                                <br />
                                                                                <asp:LinkButton ID="bntUpdate" ValidationGroup="edit" Text='<%# (Container is GridEditFormInsertItem) ? "Insert" : "Update" %>'
                                                                                    runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'>
                                                                                </asp:LinkButton>
                                                                                |
                                                                                <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CausesValidation="False"
                                                                                    CommandName="Cancel" />
                                                                                <br />
                                                                                <br />
                                                                                <br />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </FormTemplate>
                                                            </EditFormSettings>
                                                        </MasterTableView>
                                                        <ClientSettings EnableRowHoverStyle="True">
                                                            <ClientEvents OnPopUpShowing="PopUpShowing" />
                                                        </ClientSettings>
                                                    </telerik:RadGrid>
                                                </div>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                    <telerik:RadPageView runat="server" ID="pvAddClass">
                                        <div class="paneldefault">
                                            <div class="insidecontainer">
                                                <table class="tabledefault">
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:LinkButton ID="btnClear" runat="server" Text="Clear Form" OnClientClick="if (!confirm('Clear form? Please confirm.')) return false;"
                                                                CssClass="linkBtn" OnClick="btnClear_Click" /><br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <asp:ValidationSummary ID="ValidationSummary2" ShowMessageBox="True" ShowSummary="True"
                                                                DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                                runat="server" ValidationGroup="addClass" CssClass="displayerror" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Client"
                                                                ControlToValidate="cbClient_Add" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                                ValidationGroup="addClass" Text="*" EnableClientScript="False" />
                                                            Client
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbClient_Add" DataTextField="ClientName" DataValueField="Id"
                                                                runat="server" EmptyMessage="- client -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                                DataSourceID="dsClient" AutoPostBack="True" OnSelectedIndexChanged="cbClient_Add_SelectedIndexChanged" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Training Type" ControlToValidate="cbTrainingType_Add"
                                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="addClass"
                                                                Text="*" EnableClientScript="False" />
                                                            Training Type
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadComboBox ID="cbTrainingType_Add" DataTextField="TrainingType" DataValueField="TrainingTypeId"
                                                                runat="server" EmptyMessage="- training type -" DropDownAutoWidth="Enabled" MaxHeight="300px" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Class Name"
                                                                ControlToValidate="txtClassName_Add" SetFocusOnError="True" Display="Dynamic"
                                                                CssClass="displayerror" ValidationGroup="addClass" Text="*" EnableClientScript="False" />
                                                            Class Name
                                                        </td>
                                                        <td class="tblfield">
                                                            <telerik:RadTextBox runat="server" ID="txtClassName_Add" EmptyMessage="- name -"
                                                                SelectionOnFocus="SelectAll" Width="200" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="tblLabel">
                                                        </td>
                                                        <td class="tblfield">
                                                            <br />
                                                            <asp:LinkButton ID="btnAddClass" runat="server" Text="Add" CssClass="linkBtn" ValidationGroup="addClass"
                                                                OnClick="btnAddClass_Click" />
                                                            <br />
                                                            <br />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </telerik:RadPageView>
                                </telerik:RadMultiPage>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvAssignCourse" runat="server">
                                <div class="paneldefault">
                                    <div class="insidecontainer">
                                        <table class="tabledefault">
                                            <tr>
                                                <td class="tblLabel">
                                                </td>
                                                <td class="tblfield" colspan="2">
                                                    <asp:ValidationSummary ID="vsAssign" ShowMessageBox="True" ShowSummary="True" DisplayMode="BulletList"
                                                        HeaderText="Please complete the following field/s:" runat="server" ValidationGroup="vgAssign"
                                                        CssClass="displayerror" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tblLabel">
                                                    Type Of Assignment
                                                </td>
                                                <td class="tblfield">
                                                    <telerik:RadComboBox ID="cbAssignmentType" runat="server" AutoPostBack="True" EmptyMessage="- select assignment type -"
                                                        DropDownAutoWidth="Enabled" MaxHeight="300px" Width="200px" CausesValidation="False"
                                                        DataSourceID="dsAssignmentTypes" DataTextField="CourseAssignmentType" DataValueField="CourseAssignmentTypeID"
                                                        OnSelectedIndexChanged="cbAssignmentType_SelectedIndexChanged">
                                                    </telerik:RadComboBox>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tblLabel">
                                                    <asp:RequiredFieldValidator ID="rfvAssignTo" runat="server" ErrorMessage="Assign To"
                                                        InitialValue="" ControlToValidate="cbAssignTo" SetFocusOnError="True" Display="Dynamic"
                                                        CssClass="displayerror" ValidationGroup="vgAssign" Text="*" EnableClientScript="false"
                                                        Enabled="false" />
                                                    <asp:RequiredFieldValidator ID="rfvAssignToText" runat="server" ErrorMessage="Assign To 2"
                                                        ControlToValidate="txtAssignTo" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                        ValidationGroup="vgAssign" Text="*" EnableClientScript="false" Enabled="false" />
                                                    Assign To
                                                </td>
                                                <td class="tblfield" colspan="2">
                                                    <div style="float: left;">
                                                        <telerik:RadComboBox ID="cbAssignTo" runat="server" AutoPostBack="True" EmptyMessage="- select assign to -"
                                                            DropDownAutoWidth="Enabled" MaxHeight="300px" Width="200px" CausesValidation="False"
                                                            OnSelectedIndexChanged="cbAssignTo_SelectedIndexChanged">
                                                        </telerik:RadComboBox>
                                                        <telerik:RadNumericTextBox ID="txtAssignTo" runat="server" EmptyMessage=" - cim number -"
                                                            Visible="false" ToolTip="Specify Value" ClientEvents-OnBlur="ValidateCIM">
                                                            <NumberFormat GroupSeparator="" DecimalDigits="0" />
                                                        </telerik:RadNumericTextBox>
                                                        <asp:Label ID="lblEmployeeName" runat="server"></asp:Label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tblLabel">
                                                    Assignment Filters
                                                </td>
                                                <td class="tblfield" colspan="2">
                                                    <telerik:RadComboBox ID="cbCategoryAssign" DataTextField="Category" DataValueField="CategoryID"
                                                        runat="server" AutoPostBack="True" EmptyMessage="- select category -" OnSelectedIndexChanged="cbCategoryAssignSelectedIndexChanged"
                                                        CausesValidation="False" DropDownAutoWidth="Enabled" MaxHeight="300px" />
                                                    <telerik:RadDropDownTree runat="server" ID="ddtSubcategoryAssign" DefaultMessage="- select subcategory -"
                                                        DefaultValue="0" DataTextField="Subcategory" DataValueField="SubcategoryId" DataFieldID="SubcategoryId"
                                                        DataFieldParentID="ParentSubcategoryId" TextMode="FullPath" Width="150" AutoPostBack="True"
                                                        OnClientEntryAdding="OnClientEntryAdding" Skin="Default" OnEntryAdded="ddtSubcategoryAssign_EntryAdded"
                                                        OnEntryRemoved="ddtSubcategoryAssign_EntryRemoved">
                                                        <DropDownSettings Width="400px" Height="250px" />
                                                    </telerik:RadDropDownTree>
                                                    <asp:LinkButton ID="btnAssign" runat="server" Text="Choose Course/s" ToolTip="Assign"
                                                        CssClass="linkBtn" OnClick="btnAssign_Click" ValidationGroup="vgAssign" Visible="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="tblLabel">
                                                    Currently Assigned
                                                </td>
                                                <td class="tblfield">
                                                    <hr />
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <div style="vertical-align: bottom" align="right">
                                                                    <asp:LinkButton runat="server" ID="btnExportExcelAssignedCourses" CssClass="linkBtn"
                                                                        OnClick="btnExportExcel_Click" Height="100%" OnClientClick="if(!confirm('Are you sure you want to export assign courses list to excel file?')) return false;">Export to Excel<img style="border:0; padding-left: 3px; height: 20px" alt="" src="Images/excel.png"/></asp:LinkButton>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <telerik:RadGrid runat="server" ID="gridAssignedCourses" AutoGenerateColumns="False"
                                                                    AllowPaging="true" GridLines="None" CssClass="RadGrid1" Skin="Metro" Width="100%"
                                                                    OnItemCommand="gridAssignedCourses_ItemCommand" OnNeedDataSource="gridAssignedCourses_NeedDataSource">
                                                                    <ClientSettings EnableRowHoverStyle="True">
                                                                        <Selecting AllowRowSelect="True" />
                                                                    </ClientSettings>
                                                                    <FilterItemStyle HorizontalAlign="Center" />
                                                                    <MasterTableView ShowHeader="true" DataKeyNames="CourseAssignmentID" AutoGenerateColumns="False"
                                                                        EnableNoRecordsTemplate="True" NoMasterRecordsText="No assigned course" ShowHeadersWhenNoRecords="True"
                                                                        TableLayout="Auto" EditMode="InPlace" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                        AlternatingItemStyle-HorizontalAlign="Center">
                                                                        <Columns>
                                                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                                                                HeaderStyle-Width="70px" EditImageUrl="Images/editpen2.png">
                                                                                <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                                            </telerik:GridEditCommandColumn>
                                                                            <telerik:GridButtonColumn ConfirmDialogType="RadWindow" ConfirmTitle="Delete Record"
                                                                                ButtonType="ImageButton" CommandName="Delete" Text="Delete Record" UniqueName="DeleteColumn"
                                                                                ConfirmText="Are you sure you want to delete this record?" ImageUrl="~/Images/deletesmall.png"
                                                                                ConfirmDialogHeight="180" ConfirmDialogWidth="350" HeaderStyle-Width="30">
                                                                            </telerik:GridButtonColumn>
                                                                            <telerik:GridBoundColumn UniqueName="Title" HeaderText="Title" DataField="Title"
                                                                                AllowFiltering="False" ReadOnly="True">
                                                                                <HeaderStyle Width="72%" />
                                                                            </telerik:GridBoundColumn>
                                                                            <telerik:GridTemplateColumn UniqueName="DueDate" HeaderText="Due Date" DataField="DueDate"
                                                                                SortExpression="DueDate" EditFormHeaderTextFormat="Due Date" ShowFilterIcon="False">
                                                                                <ItemTemplate>
                                                                                    <asp:Label runat="server" ID="lblDueDate" Text='<%# Eval("DueDate" , "{0:d}") %>' />
                                                                                </ItemTemplate>
                                                                                <EditItemTemplate>
                                                                                    <telerik:RadDatePicker ID="dpDueDateEdit" runat="server" Width="150" EnableTyping="False"
                                                                                        SelectedDate='<%# Bind("DueDate") %>' ShowPopupOnFocus="True">
                                                                                        <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                                                                    </telerik:RadDatePicker>
                                                                                </EditItemTemplate>
                                                                            </telerik:GridTemplateColumn>
                                                                        </Columns>
                                                                    </MasterTableView>
                                                                </telerik:RadGrid>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:HiddenField ID="ctrPrereq" runat="server" />
        <asp:HiddenField runat="server" ID="hidCategoryId" Value="0" />
        <asp:HiddenField ID="hiddenCourseName" Value="" runat="server" />
        <asp:HiddenField ID="hidCourseID" Value="" runat="server" />
        <asp:HiddenField ID="HiddenField1" Value="" runat="server" />
        <asp:HiddenField ID="hidTRClientID2" Value="" runat="server" />
        <asp:HiddenField ID="hidTRClientID3" Value="" runat="server" />
        <asp:HiddenField ID="hidlblFilterValue1" Value="" runat="server" />
        <asp:HiddenField ID="hidlblFilterValue2" Value="" runat="server" />
        <asp:HiddenField ID="hidlblFilterValue3" Value="" runat="server" />
        <asp:HiddenField ID="hidlblFilterValueEdit1" Value="" runat="server" />
        <asp:HiddenField ID="hidlblFilterValueEdit2" Value="" runat="server" />
        <asp:HiddenField ID="hidlblFilterValueEdit3" Value="" runat="server" />
        <asp:HiddenField ID="hidprevVal1" Value="" runat="server" />
        <asp:HiddenField ID="hidprevVal2" Value="" runat="server" />
        <asp:HiddenField ID="hidprevVal3" Value="" runat="server" />
        <asp:HiddenField ID="hidRowNum" Value="1" runat="server" />
        <asp:HiddenField ID="hidGridAudClientID" Value="" runat="server" />
        <asp:HiddenField ID="hidOperation" Value="Normal" runat="server" />
    </div>
    <div>
        <asp:SqlDataSource ID="dsClient" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT [Id], [ClientName], [Active] FROM [INTRANET].[dbo].[ref_TranscomUniversity_Class_Client] WHERE [Active] = 1 ORDER BY [ClientName]"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsAudienceType" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            ProviderName="System.Data.SqlClient" SelectCommand="  SELECT [AudienceTypeID],[AudienceType] FROM [dbo].[tbl_TranscomUniversity_Lkp_AudienceType] WHERE [Active] = 1" />
        <asp:SqlDataSource ID="dsSite" runat="server" ConnectionString="<%$ ConnectionStrings:Cimenterprise %>"
            SelectCommand="SELECT CompanySiteID,CompanySite FROM CIMEnterprise.dbo.tbl_Personnel_Lkp_CompanySite WHERE HideFromList=0 ORDER BY COMPANYSITE" />
        <asp:SqlDataSource ID="dsRoleGroup" runat="server" ConnectionString="<%$ ConnectionStrings:Cimenterprise %>"
            SelectCommand="SELECT  RoleGroupID,RoleGroup FROM [CIMEnterprise].dbo.tbl_personnel_lkp_rolegroup WHERE HideFromList=0 ORDER BY RoleGroup" />
        <asp:SqlDataSource ID="dsRole" runat="server" ConnectionString="<%$ ConnectionStrings:Cimenterprise %>"
            SelectCommand="SELECT RoleID,[Role] FROM [CIMEnterprise].dbo.tbl_personnel_cor_role WHERE GETDATE() BETWEEN StartDate and EndDate AND CompanyGroupID =2 ORDER BY Role" />
        <asp:SqlDataSource ID="dsDeparment" runat="server" ConnectionString="<%$ ConnectionStrings:Cimenterprise %>"
            SelectCommand="SELECT 
                        DISTINCT D.AccountID, D.Account 
                    FROM     
                    CIMEnterprise.dbo.tbl_Personnel_Cor_Employee AS E     
                    INNER JOIN CIMEnterprise.dbo.tbl_Personnel_Cor_EmployeeCompany AS EC     
                    ON E.EmployeeID = EC.EmployeeID     AND GETDATE() BETWEEN EC.StartDate AND EC.EndDate    
                    INNER JOIN CIMEnterprise.dbo.tbl_Business_Cor_Account AS C     
                    ON E.DepartmentID = C.AccountID     
                    INNER JOIN CIMEnterprise.dbo.tbl_Hierarchy_Hld_HierarchyFramework as HF    
                    ON HF.ElementID = C.AccountID    
                    AND HF.HierarchyTypeID = 2    
                    INNER JOIN CIMEnterprise.dbo.tbl_Hierarchy_Hld_HierarchyFramework HF1  WITH (READUNCOMMITTED)    
                    ON HF1.HierarchyID = HF.ParentHierarchyID    
                    AND HF1.HierarchyTypeID = 2    
                    INNER JOIN CIMEnterprise.dbo.tbl_Business_Cor_Account D WITH (READUNCOMMITTED)    
                    ON D.AccountID = HF1.ElementID
					ORDER BY D.Account" />
        <asp:SqlDataSource ID="dsClass" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT ClassID FROM tbl_Infinity_Cor_Class WHERE (GETDATE() &lt;= ClosedDate)" />
        <asp:SqlDataSource ID="dsHarmonyClient" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_Lkp_Client" SelectCommandType="StoredProcedure" />
        <asp:SqlDataSource ID="dsCareerPath" runat="server" ConnectionString="<%$ ConnectionStrings:NuSkillCheck %>"
            ProviderName="System.Data.SqlClient" SelectCommand="SELECT [ID], [Description] FROM [refCareerPath] WHERE ([Active] = 1) ORDER BY [Description]" />
        <asp:SqlDataSource ID="dsAssignmentTypes" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT CourseAssignmentTypeID,CourseAssignmentType FROM tbl_TranscomUniversity_lkp_CourseAssignmentType WHERE Active = 1
            AND CourseAssignmentTypeID NOT IN (5)" />
        <asp:SqlDataSource ID="dsPackageType" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="select CourseTypeID,CourseType from tbl_Scorm_Lkp_CourseType where inactive = 0">
        </asp:SqlDataSource>
    </div>
</asp:Content>
