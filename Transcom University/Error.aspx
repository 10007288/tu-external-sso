<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Error.aspx.cs" Inherits="Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <asp:Table runat="server" HorizontalAlign="Center" Width="100%">
            <asp:TableRow runat="server" VerticalAlign="Middle">
                <asp:TableCell runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                    <hr />
                    <br />
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Font-Size="Large" Text="An error was encountered while processing your request.<br />Please contact Service Desk to report this incident.<br />Thank you."></asp:Label>
                    <br />
                    <br />
                </asp:TableCell>
            </asp:TableRow>
            <%--<asp:TableRow runat="server" VerticalAlign="Bottom">
                <asp:TableCell runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                    <asp:LinkButton ID="btnExit" runat="server" Text="Home" OnClick="btnExit_Click" CssClass="linkBtn" />
                    <br />
                </asp:TableCell>
            </asp:TableRow>--%>
        </asp:Table>
    </div>
</asp:Content>
