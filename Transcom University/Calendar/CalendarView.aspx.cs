﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using TranscomUniversityUI;

namespace Calendar
{
    public partial class CalendarCalendarView : Page
    {
        int _month = DateTime.Now.Month;
        int _view;
        //int _clientId;
        //int _siteId;
        //short _roomId;
        int _keyId;

        protected void Page_Init(object sender, EventArgs e)
        {
            if (Request.QueryString["month"] != "")
                _month = Convert.ToInt32(Request.QueryString["month"]);

            if (Request.QueryString["view"] != "")
                _view = Convert.ToInt32(Request.QueryString["view"]);

            if (Request.QueryString["id"] != "")
                _keyId = Convert.ToInt32(Request.QueryString["id"]);
            
            //elementh2.InnerText = _view == 1 ? "Professional Development Calendar" : "Program Specific Calendar";

            switch (_view)
            {
                case 1:
                    elementh2.InnerText = "Professional Development Calendar";
                    break;
                case 2:
                    elementh2.InnerText = "Program Specific Calendar";
                    break;
                case 3:
                    elementh2.InnerText = "Site Specific Calendar";
                    break;
                case 4:
                    elementh2.InnerText = "Room Specific Calendar";
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var year = DateTime.Now.Year;
            //var numberOfDays = DateTime.DaysInMonth(year, _month);

            if (!Page.IsPostBack)
            {
                string table = ApplyCalenderHeader(year, _month, _view, _keyId);

                int firstDay = SetFirstRowCells(year, _month);

                for (int i = 1; i < firstDay; i++)
                    table += "<td><div class='calendar-non-date'></div></td>";

                var taken = CalendarOnClass.GetDateTaken(_month, _view);

                var date = GetDates(year, _month);

                foreach (var item in date)
                {
                    if (item.DayOfWeek == DayOfWeek.Monday)
                        table += string.Format("<tr>", item.Day);

                    string _details;
                    if (taken.Any(t => t.Date == item.Date))
                    {
                        _details = "";

                        switch (_view)
                        {
                            case 1:
                                var professionalview = CalendarOnClass.ProfessionalView(item.Date);

                                if (professionalview != null && professionalview.Count != 0)
                                { _details = SetPopProfessionalDetails(_details, item, professionalview); }
                                else { _details = ""; }

                                break;
                            case 2:
                                var clientview = CalendarOnClass.ClientView(item.Date, _keyId);

                                if (clientview != null && clientview.Count != 0)
                                { _details += SetPopClientDetails(_details, item, clientview); }
                                else { _details = ""; }

                                break;
                            case 3:
                                var siteview = CalendarOnClass.SiteView(item.Date, _keyId);

                                if (siteview != null && siteview.Count != 0)
                                { _details += SetPopSiteDetails(_details, item, siteview); }
                                else { _details = ""; }

                                break;
                            case 4:
                                var roomview = CalendarOnClass.RoomView(item.Date, Convert.ToInt16(_keyId));

                                if (roomview != null && roomview.Count != 0)
                                { _details += SetPopRoomDetails(_details, item, roomview); }
                                else { _details = ""; }

                                break;
                            default:
                                _details = "";
                                break;
                        }
                    }
                    else
                    {
                        _details = "";
                    }

                    table += string.Format(@"<td>
                            <div class='calendar-date{2}{3}'>
                                <span class='calendar-day'><label>{0}</label></span>
                                {4}
                                <div class='calendar-icon'>                                    
                                </div>
                                {1}
                            </div>
                        </td>",
                              item.Day,
                              _details,
                              _details.Length != 0 ? " marked" : "",
                              item.Date.ToShortDateString() == DateTime.Now.ToShortDateString() ? " today" : "",
                              _details.Length != 0 ? "<span class='calendar-clock'></span>" : "");

                    if (item.DayOfWeek == DayOfWeek.Sunday)
                        table += string.Format("</tr>", item.Day);
                }

                table += "</tbody>";

                table += "</table>";

                calendar.InnerHtml = table;
            }
        }

        private static string SetPopProfessionalDetails(string _details, DateTime item, List<CalendarProfessionalOnModel> model)
        {
            string desc = string.Empty;
            //string _editbutton = string.Empty;

            int index = 1;
            int lastindex = model.Count();

            bool isCoordinator = (Roles.IsUserInRole("Coordinator") || Roles.IsUserInRole("Admin"));

            foreach (var view in model)
            {
                desc += string.Format(
                        @"<div id='{9}' class='desc' style='display:{8}'>
                            <span class='next-prev' {15}>
                                {18}
                                <a title='Previous' class='gotoprev' showobject='date{7}-{10}' {12} {13} {14}>&ltPrev</a> 
                                <a title='Next' class='gotonext' showobject='date{7}-{11}' {12} {16}>Next&gt</a>
                                {17}
                            </span>
                            <div><label>Course Name</label></div>
                            <div class='desc-item'>{0}</div>
                            <div><label>Site</label></div>
                            <div class='desc-item'>{1}</div>
                            <div class='detail-room'><label>Room</label></div>
                            <div class='desc-item'>{2}</div>
                            <div class='detail-timer'><label>Time</label></div>
                            <div class='desc-item'>{3}</div>
                            <div><label>Facilitator</label></div>
                            <div class='desc-item-bottom'>{4}</div>
                            <div class='buttons'>
                                <a class='btnenrollnow' href='#' modal='{7}'>Enroll Now</a>
                                <a class='btndescription' href='#' modal='{7}'>Description</a>
                                {19}
                            </div>
                            <div id='divenroll{7}' class='modal' style='display: none;'>
                                <div class='layer'>
                                    <a class='modal-close' onclick='$(this).parent().parent().hide();' href='#'>Close</a>
                                    <div class='modal-header'>How to Enroll</div>
                                    <div>
                                    {6}
                                    </div>
                                </div>
                            </div>
                            <div id='divdesc{7}' class='modal' style='display: none;'>
                                <div class='layer'>
                                    <a class='modal-close' onclick='$(this).parent().parent().hide();' href='#'>Close</a>
                                    <div class='modal-header'>{0}</div>
                                    <div>
                                    {5}
                                    </div>
                                </div>
                            </div>
                        </div>",
                    view.CourseName, //0
                    view.Site, //1
                    view.Room, //2
                    view.Time, //3
                    view.Facilitator, //4
                    view.Description, //5                                       
                    view.Instruction, //6
                    item.Day, //7
                    index == 1 ? "block" : "none", //8
                    "date" + item.Day + "-" + index, //9
                    index - 1, //10
                    index + 1, //11
                    index != lastindex ? "hideme='date" + item.Day + "-" + index + "'" : "", //12
                    index == lastindex ? "hideme='date" + item.Day + "-" + index + "'" : "", //13
                    index == 1 ? "style='display:none;'" : "", //14
                    lastindex == 1 ? "style='visibility: hidden;'" : "", //15
                    index == lastindex ? "style='display:none;'" : "", //16
                    index == lastindex ? "<a style='text-decoration: none; color: gray; cursor: default;'>Next&gt</a>" : "", //17
                    index == 1 ? "<a style='text-decoration: none; color: gray; cursor: default;'>&ltPrev</a>" : "", //18
                    isCoordinator ? string.Format("<a class='btnedit' {0}>Edit</a>",
                        item.Date < DateTime.Now.Date ? " onclick='alert(\"This event is already lapsed. Editing is not allowed.\");return false;' "
                        : string.Format("target='_blank' href='../CalendarEvents.aspx?ReservId={0}'", Security.Encrypt(view.ID.ToString())))
                    : ""); //19

                index++;
            }

            _details = string.Format(@"<div class='details' id='date{2}'>
                    <div class='date'>
                        <a class='home-right' title='Go to Home' href='../Default.aspx'></a>                                        
                        <div class='day'>{0}</div>
                        <div class='year'>{1}</div>
                        <div class='number'>{2}</div>
                    </div>
                    {3}
                </div>",
                item.DayOfWeek,
                string.Format("{0:MMMM} {0:yyyy}", item.Date),
                item.Day,
                desc);

            return _details;
        }

        private static string SetPopClientDetails(string _details, DateTime item, List<CalendarClientOnModel> model)
        {
            var desc = string.Empty;
            //string _editbutton = string.Empty;

            var index = 1;
            var lastindex = model.Count();

            var isCoordinator = (Roles.IsUserInRole("Coordinator") || Roles.IsUserInRole("Admin"));

            foreach (var view in model)
            {
                desc += string.Format(
                        @"<div id='{5}' class='desc' style='display:{4}'>
                            <span class='next-prev' {12}>
                                {15}
                                <a title='Previous' class='gotoprev' showobject='date{6}-{7}' {9} {10} {11}>&ltPrev</a> 
                                <a title='Next' class='gotonext' showobject='date{6}-{8}' {9} {13}>Next&gt</a>
                                {14}
                            </span>
                            <div><label>Class Name</label></div>
                            <div class='desc-item'>{0}</div>
                            <div class='detail-room'><label>Room</label></div>
                            <div class='desc-item'>{1}</div>
                            <div class='detail-timer'><label>Time</label></div>
                            <div class='desc-item'>{2}</div>
                            <div><label>Trainer</label></div>
                            <div class='desc-item'>{3}</div>
                            {16}
                        </div>",
                    view.TraningClassName, //0
                    view.Room, //1
                    view.Time, //2
                    view.Facilitator, //3
                    index == 1 ? "block" : "none", //4
                    "date" + item.Day + "-" + index, //5
                    item.Day, //6 
                    index - 1, //7
                    index + 1, //8
                    index != lastindex ? "hideme='date" + item.Day + "-" + index + "'" : "", //9
                    index == lastindex ? "hideme='date" + item.Day + "-" + index + "'" : "", //10
                    index == 1 ? "style='display:none;'" : "", //11
                    lastindex == 1 ? "style='visibility: hidden;'" : "", //12
                    index == lastindex ? "style='display:none;'" : "", //13
                    index == lastindex ? "<a style='text-decoration: none; color: gray; cursor: default;'>Next&gt</a>" : "", //14
                    index == 1 ? "<a style='text-decoration: none; color: gray; cursor: default;'>&ltPrev</a>" : "", //15
                    isCoordinator ?
                    string.Format(@"<div class='desc-item-editbottom'></div><div class='buttons'><a class='btnedit' {0}>Edit</a></div>",
                            item.Date < DateTime.Now.Date ? " onclick='alert(\"This event is already lapsed. Editing is not allowed.\");return false;' "
                            : string.Format("target='_blank' href='../CalendarEvents.aspx?ReservId={0}'", Security.Encrypt(view.ID.ToString())))
                        : ""); //16

                index++;
            }

            _details = string.Format(@"
                                <div class='details' id='date{2}'>
                                    <div class='date'>
                                        <a class='home-right' title='Go to Home' href='../Default.aspx'></a>                                        
                                        <div class='day'>{0}</div>
                                        <div class='year'>{1}</div>
                                        <div class='number'>{2}</div>                                        
                                    </div>
                                    {3}
                                </div>",
                    item.DayOfWeek,
                    string.Format("{0:MMMM} {0:yyyy}", item.Date),
                    item.Day,
                    desc);

            return _details;
        }

        private static string SetPopSiteDetails(string _details, DateTime item, List<CalendarSiteOnModel> model)
        {
            var desc = string.Empty;
            //string _editbutton = string.Empty;

            var index = 1;
            var lastindex = model.Count();

            var isCoordinator = (Roles.IsUserInRole("Coordinator") || Roles.IsUserInRole("Admin"));

            foreach (var view in model)
            {
                desc += string.Format(
                        @"<div id='{5}' class='desc' style='display:{4}'>
                            <span class='next-prev' {12}>
                                {15}
                                <a title='Previous' class='gotoprev' showobject='date{6}-{7}' {9} {10} {11}>&ltPrev</a> 
                                <a title='Next' class='gotonext' showobject='date{6}-{8}' {9} {13}>Next&gt</a>
                                {14}
                            </span>
                            <div><label>Class Name</label></div>
                            <div class='desc-item'>{0}</div>
                            <div class='detail-room'><label>Room</label></div>
                            <div class='desc-item'>{1}</div>
                            <div class='detail-timer'><label>Time</label></div>
                            <div class='desc-item'>{2}</div>
                            <div><label>Trainer</label></div>
                            <div class='desc-item'>{3}</div>
                            {16}
                        </div>",
                    view.TraningClassName, //0
                    view.Room, //1
                    view.Time, //2
                    view.Facilitator, //3
                    index == 1 ? "block" : "none", //4
                    "date" + item.Day + "-" + index, //5
                    item.Day, //6 
                    index - 1, //7
                    index + 1, //8
                    index != lastindex ? "hideme='date" + item.Day + "-" + index + "'" : "", //9
                    index == lastindex ? "hideme='date" + item.Day + "-" + index + "'" : "", //10
                    index == 1 ? "style='display:none;'" : "", //11
                    lastindex == 1 ? "style='visibility: hidden;'" : "", //12
                    index == lastindex ? "style='display:none;'" : "", //13
                    index == lastindex ? "<a style='text-decoration: none; color: gray; cursor: default;'>Next&gt</a>" : "", //14
                    index == 1 ? "<a style='text-decoration: none; color: gray; cursor: default;'>&ltPrev</a>" : "", //15
                    isCoordinator ?
                    string.Format(@"<div class='desc-item-editbottom'></div><div class='buttons'><a class='btnedit' {0}>Edit</a></div>",
                            item.Date < DateTime.Now.Date ? " onclick='alert(\"This event is already lapsed. Editing is not allowed.\");return false;' "
                            : string.Format("target='_blank' href='../CalendarEvents.aspx?ReservId={0}'", Security.Encrypt(view.ID.ToString())))
                        : ""); //16

                index++;
            }

            _details = string.Format(@"
                                <div class='details' id='date{2}'>
                                    <div class='date'>
                                        <a class='home-right' title='Go to Home' href='../Default.aspx'></a>                                        
                                        <div class='day'>{0}</div>
                                        <div class='year'>{1}</div>
                                        <div class='number'>{2}</div>                                        
                                    </div>
                                    {3}
                                </div>",
                    item.DayOfWeek,
                    string.Format("{0:MMMM} {0:yyyy}", item.Date),
                    item.Day,
                    desc);

            return _details;
        }

        private static string SetPopRoomDetails(string _details, DateTime item, List<CalendarRoomOnModel> model)
        {
            var desc = string.Empty;
            //string _editbutton = string.Empty;

            var index = 1;
            var lastindex = model.Count();

            var isCoordinator = (Roles.IsUserInRole("Coordinator") || Roles.IsUserInRole("Admin"));

            foreach (var view in model)
            {
                desc += string.Format(
                        @"<div id='{5}' class='desc' style='display:{4}'>
                            <span class='next-prev' {12}>
                                {15}
                                <a title='Previous' class='gotoprev' showobject='date{6}-{7}' {9} {10} {11}>&ltPrev</a> 
                                <a title='Next' class='gotonext' showobject='date{6}-{8}' {9} {13}>Next&gt</a>
                                {14}
                            </span>
                            <div><label>Class Name</label></div>
                            <div class='desc-item'>{0}</div>
                            <div class='detail-room'><label>Room</label></div>
                            <div class='desc-item'>{1}</div>
                            <div class='detail-timer'><label>Time</label></div>
                            <div class='desc-item'>{2}</div>
                            <div><label>Trainer</label></div>
                            <div class='desc-item'>{3}</div>
                            {16}
                        </div>",
                    view.TraningClassName, //0
                    view.Room, //1
                    view.Time, //2
                    view.Facilitator, //3
                    index == 1 ? "block" : "none", //4
                    "date" + item.Day + "-" + index, //5
                    item.Day, //6 
                    index - 1, //7
                    index + 1, //8
                    index != lastindex ? "hideme='date" + item.Day + "-" + index + "'" : "", //9
                    index == lastindex ? "hideme='date" + item.Day + "-" + index + "'" : "", //10
                    index == 1 ? "style='display:none;'" : "", //11
                    lastindex == 1 ? "style='visibility: hidden;'" : "", //12
                    index == lastindex ? "style='display:none;'" : "", //13
                    index == lastindex ? "<a style='text-decoration: none; color: gray; cursor: default;'>Next&gt</a>" : "", //14
                    index == 1 ? "<a style='text-decoration: none; color: gray; cursor: default;'>&ltPrev</a>" : "", //15
                    isCoordinator ?
                    string.Format(@"<div class='desc-item-editbottom'></div><div class='buttons'><a class='btnedit' {0}>Edit</a></div>",
                            item.Date < DateTime.Now.Date ? " onclick='alert(\"This event is already lapsed. Editing is not allowed.\");return false;' "
                            : string.Format("target='_blank' href='../CalendarEvents.aspx?ReservId={0}'", Security.Encrypt(view.ID.ToString())))
                        : ""); //16

                index++;
            }

            _details = string.Format(@"
                                <div class='details' id='date{2}'>
                                    <div class='date'>
                                        <a class='home-right' title='Go to Home' href='../Default.aspx'></a>                                        
                                        <div class='day'>{0}</div>
                                        <div class='year'>{1}</div>
                                        <div class='number'>{2}</div>                                        
                                    </div>
                                    {3}
                                </div>",
                    item.DayOfWeek,
                    string.Format("{0:MMMM} {0:yyyy}", item.Date),
                    item.Day,
                    desc);

            return _details;
        }

        private static int SetFirstRowCells(int year, int month)
        {
            var first = GetDates(year, month).First();

            int firstDay = first.DayOfWeek == DayOfWeek.Monday ? 1 :
                first.DayOfWeek == DayOfWeek.Tuesday ? 2 :
                first.DayOfWeek == DayOfWeek.Wednesday ? 3 :
                first.DayOfWeek == DayOfWeek.Thursday ? 4 :
                first.DayOfWeek == DayOfWeek.Friday ? 5 :
                first.DayOfWeek == DayOfWeek.Saturday ? 6 :
                first.DayOfWeek == DayOfWeek.Sunday ? 7 :
                0;

            return firstDay;
        }

        private static string ApplyCalenderHeader(int year, int month, int viewType, int client)
        {
            string onTheMonthOf = string.Format("{0:MMMM yyyy}", new DateTime(year, month, 1));

            int prev = month - 1;
            int next = month + 1;

            if (month == 12)
                next = 1;

            if (month == 1)
                prev = 12;

            return string.Format(@"<table id='table-calendar' border='0' cellpadding='0' cellspacing='0'>
                    <thead>
                        <tr>
                            <th colspan='7' class='month-header'>
                                <a href='?view={10}&month={8}&id={11}' class='prev'></a>
                                <label>{7}</label>
                                <a href='?view={10}&month={9}&id={11}' class='next'></a>
                            </th>
                        </tr>
                        <tr>
                            <th>{0}</th><th>{1}</th><th>{2}</th><th>{3}</th><th>{4}</th><th>{5}</th><th>{6}</th>
                        </tr>
                    </thead><tbody><tr>",
                "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun", onTheMonthOf, prev, next, viewType, client);
        }

        public static List<DateTime> GetDates(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))
                             .Select(day => new DateTime(year, month, day))
                             .ToList();
        }
    }
}