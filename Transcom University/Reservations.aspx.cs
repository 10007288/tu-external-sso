﻿using System;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class Reservations : Page
{
    public static int[] StatusIds
    {
        get
        {
            var value = HttpContext.Current.Session["StatusIdsSession"];
            return value == null ? new int[] { } : (int[])value;
        }
        set
        {
            HttpContext.Current.Session["StatusIdsSession"] = value;
        }
    }

    public static string TwwId
    {
        get
        {
            var value = HttpContext.Current.Session["TwwIdSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["TwwIdSession"] = value;
        }
    }

    public static int ReservationId
    {
        get
        {
            var value = HttpContext.Current.Session["ReservationIdSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["ReservationIdSession"] = value;
        }
    }

    public static string EventId
    {
        get
        {
            var value = HttpContext.Current.Session["EventIdSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["EventIdSession"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            SetPage_RolePreviledges();

            TwwId = null;
            StatusIds = new[] { 0, 1, 2 };
        }
    }

    private void SetPage_RolePreviledges()
    {
        if (!Roles.IsUserInRole("Trainer") && !Roles.IsUserInRole("Coordinator") && !Roles.IsUserInRole("Admin"))
        {
            Response.Redirect("NoAccess.aspx");
        }
        
        if (Roles.IsUserInRole("Trainer"))
        {
            btnGotoEvents.Visible = false;
            radioFilter.Visible = false;

            TwwId = User.Identity.Name;
            gridReservations.Rebind();
        }
        if (Roles.IsUserInRole("Coordinator") || Roles.IsUserInRole("Admin"))
        {
            btnGotoEvents.Visible = true;
            radioFilter.Visible = true;
        }
        else
        {
            btnGotoEvents.Visible = true;
            radioFilter.Visible = true;
        }
    }

    private void InitializeReservations_Tab()
    {
        radioFilter.SelectedValue = "0";
        gridReservations.Rebind();
    }

    private void InitializeReservationsRequest_Tab()
    {
        ClearControls();
    }

    private void LoadGridReservations()
    {
        var list = DataHelper.GetReservations_ByStatus(StatusIds, TwwId);
        gridReservations.DataSource = list;
    }

    protected void tsReservations_TabClick(object sender, RadTabStripEventArgs e)
    {
        if (tsReservations.SelectedIndex == 0)
        {
            if (!Roles.IsUserInRole("Trainer"))
            {
                TwwId = null;
            }
            InitializeReservations_Tab();
        }
        if (tsReservations.SelectedIndex == 1)
        {
            TwwId = User.Identity.Name;
            InitializeReservationsRequest_Tab();
        }
    }

    protected void cbTrainingType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        cbClassName.Text = "";
        cbClassName.ClearSelection();
        
        cbClassName.DataBind();
    }

    protected void cbTrainingTypeEdit_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        var combo = sender as RadComboBox;

        if (combo != null)
        {
            var editedItem = combo.NamingContainer as GridEditableItem;
            if (editedItem != null)
            {
                var txtTrainingTypeOtherEdit = editedItem.FindControl("txtTrainingTypeOther_edit") as RadTextBox;
                var rfvOtherTrainTypeEdit = editedItem.FindControl("rfvOtherTrainType_edit") as RequiredFieldValidator;

                if (combo.SelectedValue == "6")
                {
                    txtTrainingTypeOtherEdit.Visible = true;
                    rfvOtherTrainTypeEdit.Enabled = true;
                    txtTrainingTypeOtherEdit.Focus();
                }
                else
                {
                    txtTrainingTypeOtherEdit.Visible = false;
                    rfvOtherTrainTypeEdit.Enabled = false;
                }
            }
        }
    }

    protected void cbClient_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        cbTrainingType.Text = "";
        cbTrainingType.ClearSelection();

        ComboBoxHelper.Populate_TrainingType(cbTrainingType, cbClient.Text);

        cbClassName.Text = "";
        cbClassName.ClearSelection();

        cbClassName.DataBind();
    }

    protected void btnAddReservation_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            const string trainingTypeOther = ""; 

            //INSERT DRAFT RESERVATION
            var retVal = DataHelper.InsertRoomReservation(Convert.ToInt16(cbTrainingType.SelectedValue), trainingTypeOther,
                                                          Convert.ToInt16(cbAttritionGrowth.SelectedValue),
                                                          Convert.ToInt32(txtFteCount.Text), txtRtpTandim.Text, Convert.ToInt32(txtWaveNo.Text),
                                                          Convert.ToInt32(cbSite.SelectedValue),
                                                          Convert.ToInt32(cbClient.SelectedValue), null,
                                                          Convert.ToDateTime(dpDate.SelectedDate),
                                                          Convert.ToDateTime(dpDateTo.SelectedDate),
                                                          Convert.ToInt16(cbTimeSlot.SelectedValue), txtTrainerName.Text,
                                                          cbClassName.Text, Convert.ToInt16(txtWeekNo.Text),
                                                          txtAdvisory.Text, null, null, 1, User.Identity.Name, DateTime.Now);

            if (retVal)
            {
                var trainingClassName = cbClassName.Text;
                var trainerName = txtTrainerName.Text;
                var companySite = cbSite.Text;
                var timeSlot = cbTimeSlot.Text;
                var dateFrom = dpDate.SelectedDate.ToString();
                var dateTo = dpDateTo.SelectedDate.ToString();
                var message = txtAdvisory.Text;

                var recipient = ConfigurationManager.AppSettings["EmailItTo"];
                var email = SendEmail.SendEmail_ReservationEvent(recipient, message, cbTrainingType.Text, trainingClassName, trainerName, companySite, timeSlot,
                                           Convert.ToDateTime(dateFrom).ToShortDateString(),
                                           Convert.ToDateTime(dateTo).ToShortDateString(), "", "", "");

                var alert = string.Format("alert('Your reservation request has been succesfully submitted and waiting for approval. Email: {0}');", email);

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", alert, true);

                if (!Roles.IsUserInRole("Trainer"))
                {
                    TwwId = null;
                }

                gridReservations.Rebind();

                ClearControls();

                //new
                tsReservations.SelectedIndex = 0;
                mpReservations.SelectedIndex = 0;
                pvReservations.Selected = true;
                radioFilter.SelectedValue = "0";
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                        "alert('Error processing your request! Please review your input fields.');",
                                                        true);
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ClearControls();
    }

    private void ClearControls()
    {
        cbAttritionGrowth.Text = "";
        cbClient.Text = "";
        cbSite.Text = "";
        cbTimeSlot.Text = "";

        cbAttritionGrowth.ClearSelection();
        cbClient.ClearSelection();
        cbSite.ClearSelection();
        cbTimeSlot.ClearSelection();

        txtFteCount.Text = "";
        txtRtpTandim.Text = "";
        txtTrainerName.Text = "";
        txtWeekNo.Text = "";
        txtAdvisory.Text = "";

        dpDate.Clear();
        dpDateTo.Clear();

        txtWaveNo.Text = "";

        cbTrainingType.Text = "";
        cbTrainingType.ClearSelection();

        cbClassName.Text = "";
        cbClassName.ClearSelection();
    }

    protected void gridReservations_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LoadGridReservations();
    }

    protected void gridReservations_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataItem = e.Item as GridDataItem;
            var client = dataItem.GetDataKeyValue("Client").ToString();
            var trainingTypeId = dataItem.GetDataKeyValue("TrainingTypeID").ToString();

            var cancelColumn = dataItem.OwnerTableView.Columns.FindByUniqueName("CancelColumn");

            if (!Roles.IsUserInRole("Admin") && !Roles.IsUserInRole("Non-APAC Admin"))
            {
                if (dataItem["CreatedByReserv"].Text == User.Identity.Name)
                {
                    dataItem.BackColor = System.Drawing.Color.Beige;
                    cancelColumn.Display = true;
                }
                else
                {
                    cancelColumn.Display = false;
                }
            }
            else
            {
                if (dataItem["CreatedByReserv"].Text == User.Identity.Name)
                {
                    dataItem.BackColor = System.Drawing.Color.Beige;
                }

                cancelColumn.Display = true;
            }

            if (client == "OD")
            {
                switch (trainingTypeId)
                {
                    case "1":
                        dataItem["TrainingType"].Text = "Agent Training";
                        break;
                    case "2":
                        dataItem["TrainingType"].Text = "Supervisor Training";
                        break;
                    case "3":
                        dataItem["TrainingType"].Text = "Manager Training";
                        break;
                    case "4":
                        dataItem["TrainingType"].Text = "Off Phone Employees (All)";
                        break;
                }
            }
        }
    }

    protected void gridReservations_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "Cancel")
        {
            hidQueryStr.Value = "";
        }
    }

    protected void btnSendForApproval_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        var reservationId = item.GetDataKeyValue("ReservationReqId").ToString();

        var retVal = DataHelper.UpdateReservation_Status_ById(1, null, Convert.ToInt32(reservationId)); //FOR APPROVAL

        if (retVal)
        {
            var trainingType = (string)item.GetDataKeyValue("TrainingType");
            var trainingClassName = (string)item.GetDataKeyValue("TrainingClassName");
            var trainerName = (string)item.GetDataKeyValue("TrainerName");
            var companySite = (string)item.GetDataKeyValue("CompanySite");
            var timeSlot = (string)item.GetDataKeyValue("TimeSlot");
            var date = item.GetDataKeyValue("Date");
            var message = txtAdvisory.Text;

            var recipient = ConfigurationManager.AppSettings["EmailItTo"];
            var email = SendEmail.SendEmail_ReservationEvent(recipient, message, trainingType, trainingClassName, trainerName, companySite, timeSlot,
                                       Convert.ToDateTime(date).ToShortDateString(), "", "", "", "");

            var alert = string.Format("alert('Your reservation request has been succesfully submitted and waiting for approval. Email: {0}');", email);

            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", alert, true);

            gridReservations.Rebind();
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Warning! Error processing your request.');", true);
        }
    }

    protected void radioFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (radioFilter.SelectedValue == "0")
        {
            TwwId = null;
            gridReservations.Rebind();
        }
        else
        {
            TwwId = User.Identity.Name;
            gridReservations.Rebind();
        }
    }

    public void ClearPreviewForm()
    {
        vwlblAdvisory.Text = "";
        vwlblApprove.Text = "";
        vwlblAttrition.Text = "";
        vwlblClassname.Text = "";
        vwlblClient.Text = "";
        vwlblCreate.Text = "";
        vwlblDate.Text = "";
        vwlblDateTo.Text = "";
        vwlblFteCount.Text = "";
        vwlblRtpTandim.Text = "";
        vwlblWaveNo.Text = "";
        vwlblSite.Text = "";
        vwlblStat.Text = "";
        vwlblTime.Text = "";
        vwlblTrainType.Text = "";
        vwlblTrainer.Text = "";
        vwlblWeek.Text = "";
        vwlblUpdatedReserv.Text = "";

        vwlblRoom.Text = "";
        vwlblCoorAdvise.Text = "";
        vwlblEnrolmentIns.Text = "";
        vwlvlCourseDesc.Text = "";
        vwlblCreatedEvent.Text = "";
        vwlblUpdatedEvent.Text = "";
    }

    protected void btnPreview_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        var reservationId = item.GetDataKeyValue("ReservationReqId");

        ClearPreviewForm();

        var result = DataHelper.GetReservations_ById(Convert.ToInt32(reservationId)).FirstOrDefault();
        if (result != null)
        {
            //RESERVATION DETAILS
            vwlblAdvisory.Text = result.Advisory;
            vwlblApprove.Text = result.ApproveByCoordinator;
            vwlblAttrition.Text = result.AttritionGrowth;
            vwlblClassname.Text = result.TrainingClassName;
            vwlblClient.Text = string.IsNullOrEmpty(result.Client) ? "Shared" : result.Client;

            if (result.Client == "OD")
            {
                switch (result.TrainingTypeId)
                {
                    case 1:
                        vwlblTrainType.Text = "Agent Training";
                        break;
                    case 2:
                        vwlblTrainType.Text = "Supervisor Training";
                        break;
                    case 3:
                        vwlblTrainType.Text = "Manager Training";
                        break;
                    case 4:
                        vwlblTrainType.Text = "Off Phone Employees (All)";
                        break;
                }
            }
            else
            {
                vwlblTrainType.Text = result.TrainingType;
            }

            vwlblCreate.Text = result.CreatedByReserv;
            vwlblDate.Text = result.Date.ToString();
            vwlblDateTo.Text = result.DateTo.ToString();
            vwlblFteCount.Text = result.FteCount.ToString();
            vwlblRtpTandim.Text = result.RtpTandim;
            vwlblWaveNo.Text = result.WaveNo.ToString();
            vwlblSite.Text = result.CompanySite;
            vwlblStat.Text = result.ReservationStatus;
            vwlblTime.Text = result.TimeSlot;
            vwlblTrainer.Text = result.TrainerName;
            vwlblWeek.Text = result.WeekNo.ToString();
            vwlblUpdatedReserv.Text = result.UpdatedByReserv;
            //EVENT DETAILS
            vwlblRoom.Text = result.RoomAssignment;
            vwlblCoorAdvise.Text = result.CoordinatorAdvisory;
            vwlblEnrolmentIns.Text = result.EnrollmentInstruction;
            vwlvlCourseDesc.Text = result.CourseDescription;
            vwlblCreatedEvent.Text = result.CreatedByEvent;
            vwlblUpdatedEvent.Text = result.UpdatedByEvent;
        }
    }

    protected void btnCancelEvent_Click(object sender, EventArgs e)
    {
        var retVal = DataHelper.UpdateReservation_Status_ById(3, null, ReservationId);

        if (!string.IsNullOrEmpty(EventId))
        {
            DataHelper.DeactivateEvent_ById(Convert.ToInt32(EventId), User.Identity.Name);
        }

        if (retVal)
        {
            var msg = "YOUR RESERVATION HAS BEEN CANCELLED! " + txtCancelAdvisory.Text;

            var result = DataHelper.GetReservations_ById(Convert.ToInt32(ReservationId)).FirstOrDefault();
            if (result != null)
            {
                var trainingType = result.TrainingTypeId == 6 ? result.TrainingTypeOther : result.TrainingType;
                var recipient = DataHelper.GetUserEmailAddress(result.CreatedByReserv);
                if (!string.IsNullOrEmpty(recipient))
                {
                    recipient += ";" + ConfigurationManager.AppSettings["EmailItTo"];
                }

                var email = SendEmail.SendEmail_ReservationEvent(recipient, msg, trainingType, result.TrainingClassName,
                                       result.TrainerName, result.CompanySite, result.TimeSlot,
                                       Convert.ToDateTime(result.Date).ToShortDateString(),
                                       Convert.ToDateTime(result.DateTo).ToShortDateString(), result.RoomAssignment,
                                       result.CoordinatorAdvisory, result.CourseDescription);
                var alert = string.Format("alert('Reservation has been cancelled. Email: {0}');", email);
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", alert, true);
                ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseCancelForm();", true);
            }

            gridReservations.Rebind();
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                        "alert('Error! Request failed.');",
                                                        true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        txtCancelAdvisory.Text = "";

        var reservationId = item.GetDataKeyValue("ReservationReqId");
        var eventId = item.GetDataKeyValue("EventId");

        ReservationId = Convert.ToInt32(reservationId.ToString());
        EventId = eventId.ToString();
    }
}