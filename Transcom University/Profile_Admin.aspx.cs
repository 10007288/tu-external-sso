﻿using System;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.Security;
using NuComm;
using Telerik.Web.UI.GridExcelBuilder;
using System.Text;

public partial class Profile_Admin : Page
{
    private DbHelper _db;
    private DataSet _dsGrd;
    private const string INFO_EMPNOTFOUND = "Employee/s Not Found";
    private const string INFO_SEARCHNOTALLOWED =
        "The employee you are searching for is not under you in Harmony. Please contact HR if this is an error and you need to be able to search for this employee. HR will update Harmony accordingly";

    protected void Page_Load(object sender, EventArgs e)
    {
        //Disable Page Cache 
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!Roles.IsUserInRole("Admin"))
            Response.Redirect("NoAccess.aspx");

        gridProfiles.Page.Response.ClearHeaders();
        gridProfiles.Page.Response.Cache.SetCacheability(HttpCacheability.Private);

        if (!Page.IsPostBack)
        {
            gridProfiles.Rebind();
        }

        _db = new DbHelper("NuSkillCheck");
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtSearch.Text.Trim() == "")
            {
                _dsGrd = GetUsersInfo(txtSearch.Text.Replace("'", "").Trim());
                gridProfiles.DataSource = GetUsersInfo(txtSearch.Text.Replace("'", "").Trim());
            }
            else
            {
                gridProfiles.Visible = true;

                _db = new DbHelper("Intranet");
                //var ds = new DataSet();
                var searchee = txtSearch.Text.Split(","[0]);

                var emp = new Employee();

                //New Code For Multiple Cim Query Tandim #2566672
                var ctrEmp = 0;

                for (Int16 i = 0; i < searchee.Length; i++)
                {
                    if (emp.LoadEmployeeByCIM(Convert.ToInt32(searchee[i]), DateTime.Now)) ctrEmp++;
                }

                if (ctrEmp == searchee.Length) //if employee/s exists
                {
                    var userId = Convert.ToInt32(Membership.GetUser().UserName);

                    _db = new DbHelper("NuSkillCheck");
                    
                    try
                    {
                        if (Roles.IsUserInRole("Admin"))
                        {
                            gridProfiles.Rebind();
                        }

                        else if (_db.IsTrainer(userId))
                        {
                            gridProfiles.Rebind();
                        }
                        else
                        {
                            var ds1 = _db.GetSupHeirarchy(userId);
                            var isBelongToHeirarchy = false;

                            ctrEmp = 0;
                            foreach (DataRow item in ds1.Tables[0].Rows)
                            {
                                for (Int16 i = 0; i < searchee.Length; i++)
                                {
                                    if ((int) item["CIMNumber"] == Convert.ToInt32(searchee[i])) ctrEmp++;

                                }
                                if (ctrEmp == searchee.Length) isBelongToHeirarchy = true;
                            }

                            if (isBelongToHeirarchy)
                            {
                                gridProfiles.Rebind();
                            }
                            else
                            {
                                gridProfiles.Visible = false;
                                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('" + INFO_SEARCHNOTALLOWED + "');", true);
                            }
                        }
                    }
                    catch
                    {
                        var tu = new TranscomUniversity();
                        tu.Exit();
                    }
                }
                else
                {
                    gridProfiles.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('" + INFO_EMPNOTFOUND + "');", true);
                }
            }
        }
        catch (Exception ex)
        {
            var tu = new TranscomUniversity();
            tu.Exit();
        }
    }

    protected void gridProfiles_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        _dsGrd = GetUsersInfo(txtSearch.Text.Replace("'", "").Trim());
        gridProfiles.DataSource = GetUsersInfo(txtSearch.Text.Replace("'", "").Trim());
    }

    private DataSet GetUsersInfo(string userIDs)
    {
        _db = new DbHelper("NuSkillCheck");
        var ds = _db.GetUsersInfo(userIDs);
        return ds;
    }

    private static TableElement ParseTable(DataTable dt)
    {
        var ssTable = new TableElement();
        CellElement ssCell;

        var ssRow = new RowElement();

        for (var iCol = 0; iCol < dt.Columns.Count; iCol++)
        {
            string cellWidth;
            switch (iCol)
            {
                case 0:
                    cellWidth = "100";
                    break;
                case 1:
                    cellWidth = "100";
                    break;
                case 2:
                    cellWidth = "200";
                    break;
                default:
                    cellWidth = "105";
                    break;
            }

            var ssColumn = new ColumnElement();
            ssColumn.Attributes.Add("ss:Width", cellWidth); //set column width - col1 = 100pt, col2 = 200pt and so on.
            ssTable.Columns.Add(ssColumn);
            ssCell = new CellElement();
            ssCell.Data.DataItem = dt.Columns[iCol].ColumnName;
            ssRow.Cells.Add(ssCell);
        }

        ssTable.Rows.Add(ssRow);

        for (var iRow = 0; iRow < dt.Rows.Count; iRow++)
        {
            ssRow = new RowElement();
            for (var iCol = 0; iCol < dt.Columns.Count; iCol++)
            {
                ssCell = new CellElement();
                ssCell.Data.DataItem = dt.Rows[iRow][iCol];
                ssRow.Cells.Add(ssCell);
            }
            ssTable.Rows.Add(ssRow);
        }
        return ssTable;
    }

    protected DataTable GetSearchees(string filter)
    {
        var dt = new DataTable();
        var gridDr = _dsGrd.Tables[0].Select("CIMNumber = " + filter);

        dt.Columns.Add("CourseID", typeof (String));
        dt.Columns.Add("CIMNumber", typeof (String));
        dt.Columns.Add("CourseName", typeof (String));
        dt.Columns.Add("DateCourseTaken", typeof (String));
        dt.Columns.Add("Score", typeof (String));
        dt.Columns.Add("DateTestTaken", typeof (String));

        foreach (DataRow dr in gridDr)
        {
            dt.ImportRow(dr);
        }
        return dt;
    }

    protected void gridProfiles_GridExporting(object sender, Telerik.Web.UI.GridExportingArgs e)
    {
        var searchee = txtSearch.Text.Split(","[0]);
        if (searchee.Length > 1)
        {
            var sb = new StringBuilder();
            for (Int16 i = 0; i < searchee.Length; i++)
            {
                var workSheet = new WorksheetElement(searchee[i]) {Table = ParseTable(GetSearchees(searchee[i]))};
                workSheet.AutoFilter.Range = "R1C1:R1C6";
                workSheet.Render(sb);

            }
            e.ExportOutput = e.ExportOutput.Replace("</Styles>", "</Styles>" + sb);
                //add the rendered worksheet to the output 
        }

        if (HttpContext.Current.Request.IsSecureConnection
            && HttpContext.Current.Request.Browser.Type.ToUpper().Contains("IE") //fix for IE 8 below when exporting.
            && HttpContext.Current.Request.Browser.MajorVersion < 9)
        {
            gridProfiles.Page.Response.ClearHeaders();
            gridProfiles.Page.Response.Cache.SetCacheability(HttpCacheability.Private);
            gridProfiles.Page.Response.ContentType = "application/vnd.ms-excel";
            gridProfiles.Page.Response.AddHeader("Content-Disposition",
                                                     "attachment;filename=" + txtSearch.Text + ".xls");
        }
    }

    protected void gridProfiles_ExcelMLExportRowCreated(object sender, GridExportExcelMLRowCreatedArgs e)
    {
        e.Worksheet.Name = "GridContents";
    }
}