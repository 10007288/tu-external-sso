﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Reservations.aspx.cs" Inherits="Reservations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript" language="javascript">
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("btnExport") >= 0) {
                    args.set_enableAjax(false);
                }

                var loadingPanel = $get("<%= RadAjaxLoadingPanel1.ClientID %>");
                var pageHeight = document.documentElement.scrollHeight;
                var viewportHeight = document.documentElement.clientHeight;

                if (pageHeight > viewportHeight) {
                    loadingPanel.style.height = pageHeight + "px";
                }

                var pageWidth = document.documentElement.scrollWidth;
                var viewportWidth = document.documentElement.clientWidth;

                if (pageWidth > viewportWidth) {
                    loadingPanel.style.width = pageWidth + "px";
                }
                var scrollTopOffset;
                var scrollLeftOffset;
                if ($telerik.isSafari) {
                    scrollTopOffset = document.body.scrollTop;
                    scrollLeftOffset = document.body.scrollLeft;
                }
                else {
                    scrollTopOffset = document.documentElement.scrollTop;
                    scrollLeftOffset = document.documentElement.scrollLeft;
                }

                var loadingImageWidth = 55;
                var loadingImageHeight = 55;

                loadingPanel.style.backgroundPosition = (parseInt(scrollLeftOffset) + parseInt(viewportWidth / 2) - parseInt(loadingImageWidth / 2)) + "px " + (parseInt(scrollTopOffset) + parseInt(viewportHeight / 2) - parseInt(loadingImageHeight / 2)) + "px";
            }
            var popUp;
            function PopUpShowing(sender, eventArgs) {
                popUp = eventArgs.get_popUp();
                var gridWidth = sender.get_element().offsetWidth;
                var gridHeight = sender.get_element().offsetHeight;
                var popUpWidth = popUp.style.width.substr(0, popUp.style.width.indexOf("px"));
                var popUpHeight = popUp.style.height.substr(0, popUp.style.height.indexOf("px"));
                popUp.style.left = ((gridWidth - popUpWidth) / 2 + sender.get_element().offsetLeft).toString() + "px";
                popUp.style.top = ((gridHeight - popUpHeight) / 2 + sender.get_element().offsetTop).toString() + "px";
            }
            function Cancel_Reservation(sender, args) {
                setTimeout(function () { $find("<%=windowCancelReservation.ClientID %>").show(); }, 800);
            }
            function CloseCancelForm(sender, args) {
                var window = $find('<%=windowCancelReservation.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 800);
            }
            function PreviewDetails(sender, args) {
                setTimeout(function () {
                    $find("<%=windowPreviewDetails.ClientID %>").show();
                }, 800);
            }
            function ClosePreviewForm(sender, args) {
                var window = $find('<%=windowPreviewDetails.ClientID %>');
                setTimeout(function () {
                    window.close();
                }, 800);
            }
        </script>
    </telerik:RadCodeBlock>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ClientEvents OnRequestStart="onRequestStart" />
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="gridReservations">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridReservations" />
                    <telerik:AjaxUpdatedControl ControlID="tblEditMode" />
                    <telerik:AjaxUpdatedControl ControlID="previewFields" />
                    <telerik:AjaxUpdatedControl ControlID="hidQueryStr" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="tsReservations">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="mpReservations" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbClient">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="cbClient" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbClient_edit">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="cbClient_edit" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAddReservation">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                    <telerik:AjaxUpdatedControl ControlID="tsReservations" />
                    <telerik:AjaxUpdatedControl ControlID="mpReservations" />
                    <telerik:AjaxUpdatedControl ControlID="radioFilter" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="radioFilter">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridReservations" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSendForApproval">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="gridReservations" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnGotoEvents">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnClosePreview">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCloseCancel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" CssClass="MyModalPanel"
        IsSticky="true" BackgroundPosition="Center" EnableEmbeddedSkins="False" Transparency="50"
        Style="z-index: 99999" />
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <Windows>
            <telerik:RadWindow ID="windowCancelReservation" runat="server" Title="Cancel Reservation"
                Width="560px" Height="200px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <table runat="server" id="tblCancel" class="tabledefault">
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <h3>
                                    Cancel Reservation</h3>
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Advisory
                            </td>
                            <td class="tblfield">
                                <telerik:RadTextBox runat="server" ID="txtCancelAdvisory" EmptyMessage="- cancel advisory -"
                                    Width="300" Height="60px" TextMode="MultiLine" CssClass="txtMultiline" Wrap="True"
                                    SelectionOnFocus="SelectAll" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <asp:LinkButton runat="server" ID="btnCancelEvent" Text="Cancel Reservation" CssClass="linkBtn"
                                    ValidationGroup="cancel" OnClick="btnCancelEvent_Click" />
                                |
                                <asp:LinkButton runat="server" ID="btnCloseCancel" Text="Close" CssClass="linkBtn"
                                    OnClientClick="CloseCancelForm()" CausesValidation="False" />
                                <br />
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
            <telerik:RadWindow ID="windowPreviewDetails" runat="server" Title="Reservation/Event Details"
                Width="800px" Height="570px" Behaviors="Close, Move" DestroyOnClose="True" ShowContentDuringLoad="False"
                VisibleStatusbar="False" Modal="True">
                <ContentTemplate>
                    <table runat="server" id="previewFields" class="tabledefault">
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <h3>
                                    Reservation Details</h3>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Attrition Growth
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblAttrition" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                FTE Count
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblFteCount" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                RTP Tandim
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblRtpTandim" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Wave #
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblWaveNo" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Site
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblSite" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Client
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblClient" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Training Type
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblTrainType" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Class Name
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblClassname" Font-Bold="True" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Date from
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblDate" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Date to
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblDateTo" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Time Slot
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblTime" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Trainer Name
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblTrainer" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Week #
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblWeek" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Advisory
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblAdvisory" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Created By
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblCreate" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Approved By
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblApprove" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Updated By
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblUpdatedReserv" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Status
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblStat" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <h3>
                                    Event Details</h3>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Room
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblRoom" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Coordinator Advisory
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblCoorAdvise" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Enrolment Instruction
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblEnrolmentIns" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Course Description
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlvlCourseDesc" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Created By
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblCreatedEvent" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                                Updated By
                            </td>
                            <td class="tblfield">
                                <asp:Label runat="server" ID="vwlblUpdatedEvent" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tblLabel">
                            </td>
                            <td class="tblfield">
                                <br />
                                <asp:LinkButton runat="server" ID="btnClosePreview" Text="Close" CssClass="linkBtn"
                                    OnClientClick="ClosePreviewForm()" CausesValidation="False" />
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <div id="ParentDivElement">
        <asp:Panel ID="Panel1" runat="server" BackColor="White" CssClass="childPanel">
            <div class="containerdefault">
                <div>
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadTabStrip ID="tsReservations" runat="server" SelectedIndex="0" MultiPageID="mpReservations"
                                    ShowBaseLine="true" Width="100%" CausesValidation="False" AutoPostBack="True"
                                    OnTabClick="tsReservations_TabClick" RenderMode="Lightweight">
                                    <Tabs>
                                        <telerik:RadTab Text="Reservations" Selected="true" />
                                        <telerik:RadTab Text="Request for Room Reservation" />
                                    </Tabs>
                                </telerik:RadTabStrip>
                            </td>
                            <td align="right" style="width: 300px">
                                <asp:LinkButton runat="server" ID="btnGotoEvents" CssClass="linkBtn" Text="Manage Calendar Events"
                                    PostBackUrl="CalendarEvents.aspx" />
                            </td>
                        </tr>
                    </table>
                    <div class="insidecontainer">
                        <telerik:RadMultiPage ID="mpReservations" runat="server" SelectedIndex="0" Width="100%">
                            <telerik:RadPageView ID="pvReservations" runat="server" Selected="true">
                                <div>
                                    <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="radioFilter"
                                        CssClass="radio" AutoPostBack="True" OnSelectedIndexChanged="radioFilter_SelectedIndexChanged">
                                        <asp:ListItem Selected="True" Value="0">All Requests</asp:ListItem>
                                        <asp:ListItem Value="1">My Requests</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div>
                                    <telerik:RadGrid runat="server" ID="gridReservations" AutoGenerateColumns="False"
                                        AllowFilteringByColumn="True" AllowSorting="True" GridLines="None" CssClass="RadGrid1"
                                        Skin="Default" OnItemCommand="gridReservations_ItemCommand" OnItemDataBound="gridReservations_ItemDataBound"
                                        OnNeedDataSource="gridReservations_NeedDataSource" PageSize="15">
                                        <GroupingSettings CaseSensitive="False" />
                                        <MasterTableView DataKeyNames="ReservationReqId,StatusId,TrainingTypeID,TrainingType,TrainingClassName,TrainerName,CompanySite,TimeSlot,Date,DateTo,EventId,Client"
                                            ShowHeader="true" AutoGenerateColumns="False" PageSize="10" EditMode="PopUp"
                                            EnableNoRecordsTemplate="True" NoMasterRecordsText="No reservations to display."
                                            ShowHeadersWhenNoRecords="True" AllowPaging="True" TableLayout="Auto">
                                            <EditFormSettings CaptionFormatString="Edit Reservation ID : {0}" CaptionDataField="ReservationReqId"
                                                PopUpSettings-Height="600px" PopUpSettings-Width="800px">
                                                <PopUpSettings Modal="true" ShowCaptionInEditForm="False" ScrollBars="Vertical" />
                                            </EditFormSettings>
                                            <PagerStyle AlwaysVisible="True" Mode="Slider" />
                                            <Columns>
                                                <telerik:GridBoundColumn UniqueName="ReservationReqId" HeaderText="ID" DataField="ReservationReqId"
                                                    AllowFiltering="False">
                                                    <HeaderStyle ForeColor="Silver" />
                                                    <ItemStyle ForeColor="Gray" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn UniqueName="TrainingClassName" HeaderText="Class Name" DataField="TrainingClassName"
                                                    FilterControlWidth="150" CurrentFilterFunction="Contains" AutoPostBackOnFilter="True"
                                                    ShowFilterIcon="False" />
                                                <telerik:GridBoundColumn UniqueName="TrainingType" HeaderText="Training Type" DataField="TrainingType"
                                                    AllowFiltering="False" />
                                                <telerik:GridBoundColumn UniqueName="Date" HeaderText="Date from" DataField="Date"
                                                    DataFormatString="{0:M/d/yyyy}" AllowFiltering="False" />
                                                <telerik:GridBoundColumn UniqueName="DateTo" HeaderText="Date to" DataField="DateTo"
                                                    DataFormatString="{0:M/d/yyyy}" AllowFiltering="False" />
                                                <telerik:GridBoundColumn UniqueName="CompanySite" HeaderText="Site" DataField="CompanySite"
                                                    AllowFiltering="False" />
                                                <telerik:GridBoundColumn UniqueName="TimeSlot" HeaderText="Time Slot" DataField="TimeSlot"
                                                    AllowFiltering="False" />
                                                <telerik:GridBoundColumn UniqueName="CreatedByReserv" HeaderText="Created By" DataField="CreatedByReserv"
                                                    AllowFiltering="False" Display="False" />
                                                <telerik:GridBoundColumn UniqueName="ReservationStatus" HeaderText="Status" DataField="ReservationStatus"
                                                    AllowFiltering="False" />
                                                <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="50px"
                                                    UniqueName="ViewDetails">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnPreview" runat="server" ToolTip="View Complete Details" OnClientClick="PreviewDetails()"
                                                            OnClick="btnPreview_Click"><img style="border:0;" alt="" src="Images/more2.png"/></asp:LinkButton></ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                </telerik:GridTemplateColumn>
                                                <%--<telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="30px"
                                                    UniqueName="SendForApproval" Display="False">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnSendForApproval" runat="server" ToolTip="Send for Approval"
                                                            OnClientClick="javascript:return confirm('Do you want to send this Room Reservation for Approval? Once clicked OK, this request will be locked and no further editing allowed.')"
                                                            OnClick="btnSendForApproval_Click"><img style="border:0;" alt="" src="Images/next.png"/></asp:LinkButton></ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                </telerik:GridTemplateColumn>--%>
                                                <telerik:GridTemplateColumn AllowFiltering="False" ReadOnly="True" HeaderStyle-Width="50px"
                                                    UniqueName="CancelColumn">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnCancel" runat="server" ToolTip="Cancel" OnClientClick="Cancel_Reservation()"
                                                            OnClick="btnCancel_Click"><img style="border:0;" alt="" src="Images/discard2.png"/></asp:LinkButton></ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" CssClass="MyImageButton" />
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings>
                                            <ClientEvents OnPopUpShowing="PopUpShowing" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </div>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="pvReservationRequest" runat="server">
                                <table class="tabledefault">
                                    <tr>
                                        <td class="tblLabel">
                                        </td>
                                        <td class="tblfield">
                                            <asp:LinkButton ID="btnClear" runat="server" Text="Clear Form" OnClientClick="if (!confirm('Clear Reservation form? Please confirm.')) return false;"
                                                CssClass="linkBtn" OnClick="btnClear_Click" /><br />
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                        </td>
                                        <td class="tblfield">
                                            <asp:ValidationSummary ID="ValidationSummary1" ShowMessageBox="True" ShowSummary="True"
                                                DisplayMode="BulletList" HeaderText="Please complete the following field/s:"
                                                runat="server" ValidationGroup="reserve" CssClass="displayerror" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Attrition/Growth" ControlToValidate="cbAttritionGrowth"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />Attrition/Growth
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadComboBox ID="cbAttritionGrowth" DataTextField="Desc" DataValueField="Id"
                                                runat="server" EmptyMessage="- attrition/growth -" DropDownAutoWidth="Enabled"
                                                MaxHeight="300px" DataSourceID="dsAttritionGrowth" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="FTE Count" ControlToValidate="txtFteCount"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />FTE Count
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadNumericTextBox runat="server" ID="txtFteCount" EmptyMessage="- fte count -">
                                                <NumberFormat DecimalDigits="0" GroupSeparator="" />
                                            </telerik:RadNumericTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="RTP Tandim" ControlToValidate="txtRtpTandim"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />RTP Tandim
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadTextBox runat="server" ID="txtRtpTandim" EmptyMessage="- rtp tandim -"
                                                SelectionOnFocus="SelectAll" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Wave #" ControlToValidate="txtWaveNo"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />Wave #
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadNumericTextBox runat="server" ID="txtWaveNo" EmptyMessage="- wave no. -">
                                                <NumberFormat DecimalDigits="0" GroupSeparator="" />
                                            </telerik:RadNumericTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Site" ControlToValidate="cbSite"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />Site
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadComboBox ID="cbSite" DataTextField="CompanySite" DataValueField="CompanySiteID"
                                                runat="server" EmptyMessage="- site -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                DataSourceID="dsSite" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Client" ControlToValidate="cbClient"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />Client
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadComboBox ID="cbClient" DataTextField="ClientName" DataValueField="Id"
                                                runat="server" EmptyMessage="- client -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                DataSourceID="dsClient" AutoPostBack="True" OnSelectedIndexChanged="cbClient_SelectedIndexChanged">
                                                <%--<Items>
                                                    <telerik:RadComboBoxItem runat="server" Text="Shared" Value="0" AppendDataBoundItems="True" />
                                                </Items>--%>
                                            </telerik:RadComboBox>
                                        </td>
                                    </tr>
                                    <%-- <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Campaign" ControlToValidate="cbCampaign"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />Campaign
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadComboBox ID="cbCampaign" DataTextField="campaign" DataValueField="campaignID"
                                                runat="server" EmptyMessage="- campaign -" DropDownAutoWidth="Enabled" MaxHeight="300px" />
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Training Type" ControlToValidate="cbTrainingType"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />
                                            <%--<asp:RequiredFieldValidator runat="server" ID="rfvOtherTrainType" ErrorMessage="Training Type"
                                                ControlToValidate="txtTrainingTypeOther" SetFocusOnError="True" Display="Dynamic"
                                                CssClass="displayerror" ValidationGroup="reserve" Text="*" EnableClientScript="False"
                                                Enabled="False" />--%>Training Type
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadComboBox ID="cbTrainingType" DataTextField="TrainingType" DataValueField="TrainingTypeId"
                                                runat="server" EmptyMessage="- training type -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                AutoPostBack="True" OnSelectedIndexChanged="cbTrainingType_SelectedIndexChanged" />
                                            <%--<telerik:RadTextBox runat="server" ID="txtTrainingTypeOther" EmptyMessage="- training type -"
                                                SelectionOnFocus="SelectAll" Visible="False" DataSourceID="dsTrainingType"/>--%>
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Class Name" ControlToValidate="txtClassName"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />Class Name
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadTextBox runat="server" ID="txtClassName" EmptyMessage="- class name -"
                                                SelectionOnFocus="SelectAll" Width="350" Font-Bold="True" />
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Class Name" ControlToValidate="cbClassName"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />
                                            <%--<asp:RequiredFieldValidator runat="server" ID="rfvOtherClassName" ErrorMessage="Class Name"
                                                ControlToValidate="txtClassName" SetFocusOnError="True" Display="Dynamic" CssClass="displayerror"
                                                ValidationGroup="reserve" Text="*" EnableClientScript="False" Enabled="False" />--%>
                                            Class Name
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadComboBox ID="cbClassName" runat="server" EmptyMessage="- select -" DropDownAutoWidth="Enabled"
                                                MaxHeight="300px" DataTextField="ClassName" DataValueField="ClassName" DataSourceID="dsClassNames"
                                                EnableViewState="False">
                                                <%--<Items>
                                                    <telerik:RadComboBoxItem runat="server" Text="Other (Please Specify)" Value="" AutoPostBack="True" OnSelectedIndexChanged="cbClassName_SelectedIndexChanged"
                                                AppendDataBoundItems="True" />
                                                </Items>--%>
                                            </telerik:RadComboBox>
                                            <%--<telerik:RadTextBox runat="server" ID="txtClassName" EmptyMessage="- class name -"
                                                SelectionOnFocus="SelectAll" Visible="False" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Date from" ControlToValidate="dpDate"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />Date from
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadDatePicker ID="dpDate" runat="server" Width="150" EnableTyping="False"
                                                ShowPopupOnFocus="True">
                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Date to" ControlToValidate="dpDateTo"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />Date to
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadDatePicker ID="dpDateTo" runat="server" Width="150" EnableTyping="False"
                                                ShowPopupOnFocus="True">
                                                <DatePopupButton HoverImageUrl="Images/btnCalendar.gif" ImageUrl="Images/btnCalendar.gif" />
                                            </telerik:RadDatePicker>
                                            <asp:CompareValidator ID="dateCompareValidator" runat="server" ControlToValidate="dpDateTo"
                                                ControlToCompare="dpDate" Operator="GreaterThanEqual" Type="Date" ErrorMessage="The second date must be after the first one"
                                                CssClass="displayerror" ValidationGroup="reserve" EnableClientScript="False">
                                            </asp:CompareValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Time Slot" ControlToValidate="cbTimeSlot"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />Time Slot
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadComboBox ID="cbTimeSlot" DataTextField="TimeSlot" DataValueField="TimeSlotId"
                                                runat="server" EmptyMessage="- time slot -" DropDownAutoWidth="Enabled" MaxHeight="300px"
                                                DataSourceID="dsTimeSlot" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Trainer Name" ControlToValidate="txtTrainerName"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />Trainer Name
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadTextBox runat="server" ID="txtTrainerName" EmptyMessage="- trainer name -"
                                                SelectionOnFocus="SelectAll" Width="200" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            <asp:RequiredFieldValidator runat="server" ErrorMessage="Week #" ControlToValidate="txtWeekNo"
                                                SetFocusOnError="True" Display="Dynamic" CssClass="displayerror" ValidationGroup="reserve"
                                                Text="*" EnableClientScript="False" />Week #
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadNumericTextBox runat="server" ID="txtWeekNo" EmptyMessage="- week no. -">
                                                <NumberFormat DecimalDigits="0" GroupSeparator="" />
                                            </telerik:RadNumericTextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                            Advisory
                                        </td>
                                        <td class="tblfield">
                                            <telerik:RadTextBox runat="server" ID="txtAdvisory" EmptyMessage="- advisory -" Width="400"
                                                Height="100px" TextMode="MultiLine" CssClass="txtMultiline" Wrap="True" SelectionOnFocus="SelectAll" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tblLabel">
                                        </td>
                                        <td class="tblfield">
                                            <br />
                                            <asp:LinkButton ID="btnAddReservation" runat="server" Text="Add" CssClass="linkBtn"
                                                ValidationGroup="reserve" OnClick="btnAddReservation_Click" />
                                            <br />
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <div>
        <asp:HiddenField runat="server" ID="hidQueryStr" Value="0" />
    </div>
    <div>
        <%--<asp:SqlDataSource ID="dsTrainingType" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT TOP 5 [TrainingTypeId], [TrainingType] FROM [ref_TranscomUniversity_TrainingType] WHERE ([Active] = 1) ORDER BY [TrainingTypeId]">
        </asp:SqlDataSource>--%>
        <asp:SqlDataSource ID="dsAttritionGrowth" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT [Id], [Desc] FROM [ref_TranscomUniversity_AttritionGrowth] WHERE ([Active] = 1) ORDER BY [Id]">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsTimeSlot" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT [TimeSlotId], [TimeSlot] FROM [ref_TranscomUniversity_TimeSlot] WHERE ([Active] = 1) ORDER BY [TimeSlotId]">
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsSite" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_Lkp_PhilippineSites" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="0" Name="CompanySiteID" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="dsClient" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT [Id], [ClientName], [Active] FROM [INTRANET].[dbo].[ref_TranscomUniversity_Class_Client] WHERE [Active] = 1 ORDER BY [ClientName]"
            SelectCommandType="Text" />
        <asp:SqlDataSource ID="dsRoomAsignment" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT [RoomId], [Room] FROM [ref_TranscomUniversity_RoomAssignment] WHERE ([Active] = 1) ORDER BY [Room]">
        </asp:SqlDataSource>
        <%--<asp:SqlDataSource ID="dsCampaign" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="pr_TranscomUniversity_Lkp_Campaign" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="cbClient" DefaultValue="0" Name="ClientID" PropertyName="SelectedValue"
                    Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>--%>
        <asp:SqlDataSource ID="dsClassNames" runat="server" ConnectionString="<%$ ConnectionStrings:Intranet %>"
            SelectCommand="SELECT DISTINCT [ID], [ClassName] FROM [ref_TranscomUniversity_Class] WHERE (([Active] = 1) AND ([ClientID] = @ClientID) AND ([TrainingTypeID] = @TrainingTypeID))">
            <SelectParameters>
                <asp:ControlParameter ControlID="cbClient" DefaultValue="0" Name="ClientID" PropertyName="SelectedValue"
                    Type="Int32" />
                <asp:ControlParameter ControlID="cbTrainingType" DefaultValue="0" Name="TrainingTypeID"
                    PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>
