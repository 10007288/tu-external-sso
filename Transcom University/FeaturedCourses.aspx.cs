﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using Telerik.Web.UI;
using System.IO;

public partial class FeaturedCourses : Page
{
    protected string FeaturedFilesPath = "~/PageFiles/Featured Courses";
    protected string FeaturedCourseUrl = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            var filename = string.Empty;
            var filepath = string.Empty;
            var files = new List<string>(Directory.GetFiles(Server.MapPath(FeaturedFilesPath), "*.html", SearchOption.AllDirectories));

            hiddenMaxCount.Value = (files.Count - 1).ToString();

            foreach (var file in files)
            {
                filename = Path.GetFileNameWithoutExtension(file);
                filepath = FeaturedFilesPath + "/" + filename + ".html";
                AddPageView(filename, filepath);
            }

        }
    }

    private void AddPageView(string pageName, string path)
    {
        var pageView = new RadPageView {ID = pageName, ContentUrl = path};
        RadMultiPage1.PageViews.Add(pageView);
    }

}