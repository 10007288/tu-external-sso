﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" enableSessionState="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <asp:Panel ID="pnlMain" runat="server" CssClass="pnlMain">
            <asp:Table runat="server" ID="tblLogin" HorizontalAlign="center" Width="350px" defaultbutton="btnLogin" CssClass="tblLogin">
                <asp:TableRow>
                    <asp:TableCell>
                        <table width="100%" align="center">
                            <tr style="height: 50px;">
                                <td colspan="2" style="color: #FFF; text-align: center;">
                                    <asp:Label ID="Label1" runat="server" Text="Label">Welcome to Transcom University! To log in, use your Trancom Google account</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:LoginView ID="LoginView2" runat="server">
                                        <AnonymousTemplate>
                                            <asp:Label ID="ModelErrorMessage1" runat="server"></asp:Label>
                                            <div style="text-align: center;">
                                                <asp:ImageButton ID="btnRequestLogin" runat="server" Text="Google Login" 
                                                    OnClick="btnRequestLogin_Click" ImageUrl="~/Images/btn_google_signin_dark_normal_web@2x.png" 
                                                    ToolTip="Sign in with your Google Account" style="height: 60px;" />
                                            </div>
                                        </AnonymousTemplate>
                                   </asp:LoginView>
                                </td>
                            </tr>
                            <tr style="height: 40px;">
                                <td colspan="2" style="color: #FFF; text-align: center;">
                                    <asp:Label ID="Label2" runat="server" Text="Label">or use your External account.</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    External Email
                                    &nbsp;&nbsp;
                                    <telerik:RadTextBox ID="txtUsername" runat="server" LabelWidth="64px" Width="160px" />&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtUsername" Font-Size="Medium" Display="Dynamic"
                                        ErrorMessage="*" runat="server" SetFocusOnError="True" ValidationGroup="login"
                                        CssClass="displayerror" />
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    Password
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <telerik:RadTextBox ID="txtPassword" TextMode="Password" runat="server" />&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtPassword" Font-Size="Medium" ErrorMessage="*"
                                        runat="server" SetFocusOnError="True" ValidationGroup="login" CssClass="displayerror" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right" style="padding: 10px 38px 15px 0">
                                    <div id="Div1" style="width: 100%">
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Login" OnClick="btnLogin_Click"
                                            ValidationGroup="login" />
                                    </div>
                                    <p>
                                        <asp:Label ID="lblNotify" ForeColor="red" runat="server" />
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <div runat="server" id="ctrTULinkInternal">
            <div id="footerLink">
                <label>
                    To access this outside the Transcom University Network, please use this address:
                </label>
                <asp:LinkButton runat="server" ID="lbnTUInternal" Text="https://applications.transcom.com/transcomuniversity"
                    PostBackUrl="https://applications.transcom.com/transcomuniversity" />&nbsp;
                <asp:GridView ID="gvLocation" runat="server" AutoGenerateColumns = "false" Visible="false">
                    <Columns>
                        <asp:BoundField DataField="CountryName" HeaderText="" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
