﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CourseRedirect.aspx.cs" Inherits="CourseRedirect" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <asp:Table ID="Table1" runat="server" HorizontalAlign="Center" Width="100%">
            <asp:TableRow ID="TableRow1" runat="server" VerticalAlign="Middle">
                <asp:TableCell ID="TableCell1" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                    <hr />
                    <asp:Label ID="lblNotes" runat="server" ForeColor="Red" />
                    <br />
                    <br />
                </asp:TableCell></asp:TableRow><asp:TableRow ID="TableRow2" runat="server" VerticalAlign="Bottom">
                <asp:TableCell ID="TableCell2" runat="server" HorizontalAlign="Center" VerticalAlign="Middle">
                    <asp:LinkButton ID="btnExit" runat="server" Text="Home" OnClick="btnExit_Click" CssClass="linkBtn" />
                    <br />
                </asp:TableCell></asp:TableRow></asp:Table>
</div></asp:Content><asp:Content ID="Content3" runat="server" 
    contentplaceholderid="head"></asp:Content>