using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;
using NuComm.Security.Encryption;
using System.Configuration;
using System.Data.SqlClient;
using TWW.Data.Intranet;

public partial class Courses : Page
{
    private DbHelper _db;
    private MasterPage _master;

    public static string AccessMode
    {
        get
        {
            var value = HttpContext.Current.Session["AccessModeSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["AccessModeSession"] = value;
        }
    }

    public static string TwwId
    {
        get
        {
            var value = HttpContext.Current.Session["TwwIdSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["TwwIdSession"] = value; }
    }

    public static int CourseId
    {
        get
        {
            var value = HttpContext.Current.Session["CourseIdSession"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["CourseIdSession"] = value; }
    }

    public static string StrCourseIDs
    {
        get
        {
            var val = HttpContext.Current.Session["strCourseIDs"] == null ? string.Empty : HttpContext.Current.Session["strCourseIDs"].ToString();
            return val;
        }
        set { HttpContext.Current.Session["strCourseIDs"] = value; }
    }

    public static bool IsSearching
    {
        get
        {
            var val = HttpContext.Current.Session["IsSearching"] != null && Convert.ToBoolean(HttpContext.Current.Session["IsSearching"]);
            return val;
        }
        set { HttpContext.Current.Session["IsSearching"] = value; }
    }

    public static string CourseLinkTitleToLearner
    {
        get
        {
            var value = HttpContext.Current.Session["CourseLinkTitleToLearnerSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseLinkTitleToLearnerSession"] = value; }
    }

    public static string CourseLinkTitleToApprover
    {
        get
        {
            var value = HttpContext.Current.Session["CourseLinkTitleToApproverSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseLinkTitleToApproverSession"] = value; }
    }

    public static string CourseDescription
    {
        get
        {
            var value = HttpContext.Current.Session["CourseDescriptionSession"];
            return value == null ? "" : (string)value;
        }
        set { HttpContext.Current.Session["CourseDescriptionSession"] = value; }
    }

    private DataSet GetSubcategory_Courses_Active(int? categoryId, string accessmode2)
    {
        _db = new DbHelper("Intranet");
        var ds = _db.GetSubcategory_CoursesActive(categoryId, accessmode2);
        return ds;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!IsPostBack)
        {
            GetGlobals();
            PopulateComboBoxCategory(cbCategory);
            StrCourseIDs = Request.QueryString["c"] ?? string.Empty;
            btnExport.Visible = false;
            if (StrCourseIDs != string.Empty)
            {
                gridCourse.Rebind();
                StrCourseIDs = string.Empty;
            }
            if (AccessMode == "External")
            {
                RestrictExternalUser();
            }
        }
    }

    private void GetGlobals()
    {
        if (!string.IsNullOrEmpty(Context.User.Identity.Name))
        {
            TwwId = Context.User.Identity.Name;
            CourseLinkTitleToLearner = "";
            CourseLinkTitleToApprover = "";
            CourseDescription = "";
            AccessMode = "";

            if (Page.User.Identity.Name.Contains("@"))
            {
                AccessMode = "External";
            }
            else
            {
                AccessMode = System.Configuration.ConfigurationManager.AppSettings["AccessMode"] == "Admin" ? "Admin" : "Internal";
            }
        }
        else
        {
            Response.Redirect("Login.aspx");
        }
    }

    protected void PopulateComboBoxCategory(RadComboBox combo)
    {
        combo.Text = "";
        combo.Items.Clear();

        var ds = DataHelper.GetCategories(AccessMode);
        combo.DataSource = ds;
        combo.DataBind();
    }

    protected void PopulateComboBoxSubCategory(RadComboBox combo, string selectedVal)
    {
        combo.Text = "";
        combo.Items.Clear();

        var categoryId = !string.IsNullOrEmpty(selectedVal) ? Convert.ToInt32(selectedVal) : (int?)null;

        var ds = DataHelper.GetSubCategoriesByCategoryId(categoryId, AccessMode);
        combo.DataSource = ds;
        combo.DataBind();
    }

    public void PopulateDropdownTreeSubcategories(RadComboBox cbCategory1, RadDropDownTree ddtSubcategory1)
    {
        var categoryId = !string.IsNullOrEmpty(cbCategory1.SelectedValue)
                             ? Convert.ToInt32(cbCategory1.SelectedValue)
                             : (int?)null;
        ddtSubcategory1.DataSource = GetSubcategory_Courses_Active(categoryId, AccessMode);
        ddtSubcategory1.DataBind();
    }

    //protected void LoadgridCourses()
    //{
    //    var categoryId = !string.IsNullOrEmpty(cbCategory.SelectedValue)
    //                         ? Convert.ToInt32(cbCategory.SelectedValue)
    //                         : (int?)null;
    //    var subcategoryId = !string.IsNullOrEmpty(ddtSubcategory.SelectedValue)
    //                            ? Convert.ToInt32(ddtSubcategory.SelectedValue)
    //                            : (int?)null;

    //    var ds = DataHelper.GetCourses(categoryId, subcategoryId, AccessMode, StrCourseIDs);
    //    //var ds = DataHelper.GetCourses(categoryId, subcategoryId, AccessMode);
    //    gridCourse.DataSource = ds;
    //    gridCourse.MasterTableView.GetColumn("Subcategory").Display = false;
    //}

    //protected void DoSearch(string searchString)
    //{
    //    var ds = DataHelper.DoSearch(searchString, AccessMode);

    //    gridCourse.DataSource = ds;
    //    gridCourse.MasterTableView.GetColumn("Subcategory").Display = true;
    //    //gridCourse.Rebind();
    //}

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        IsSearching = true;
        gridCourse.Rebind();

        cbCategory.Text = "";
        cbCategory.ClearSelection();
        PopulateDropdownTreeSubcategories(cbCategory, ddtSubcategory);
    }

    protected void CbCategorySelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbCategory, ddtSubcategory);
    }

    protected void ddtSubcategory_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
    {
        IsSearching = false;
        gridCourse.Rebind();

        txtSearch.Text = "";
    }

    protected void GridCourseNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (IsSearching)
        {
            var val = Server.HtmlEncode(Microsoft.Security.Application.AntiXss.HtmlEncode(txtSearch.Text.Trim()));

            if (!string.IsNullOrEmpty(val))
            {
                var ds = DataHelper.DoSearch(val, AccessMode);
                gridCourse.DataSource = ds;
                gridCourse.MasterTableView.GetColumn("Subcategory").Display = true;

                btnExport.Visible = ds.Any();
            }
        }
        else
        {
            var categoryId = !string.IsNullOrEmpty(cbCategory.SelectedValue)
                     ? Convert.ToInt32(cbCategory.SelectedValue)
                     : (int?)null;
            var subcategoryId = !string.IsNullOrEmpty(ddtSubcategory.SelectedValue)
                                    ? Convert.ToInt32(ddtSubcategory.SelectedValue)
                                    : (int?)null;

            var ds = DataHelper.GetCourses(categoryId, subcategoryId, AccessMode, StrCourseIDs);
            gridCourse.DataSource = ds;
            gridCourse.MasterTableView.GetColumn("Subcategory").Display = false;

            btnExport.Visible = ds.Any();
        }
    }

    protected void gridCourse_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataItem = (GridDataItem)e.Item;

            var courseId = dataItem.GetDataKeyValue("CourseID").ToString();
            var encCourseId = Server.UrlEncode(UTF8.EncryptText(dataItem.GetDataKeyValue("CourseID").ToString()));
            var sTestCategoryId = dataItem.GetDataKeyValue("EncryptedCourseID").ToString();
            var enrolmentRequired = dataItem.GetDataKeyValue("EnrolmentRequired") ?? string.Empty;
            var title = dataItem.GetDataKeyValue("Title").ToString();
            var courseTypeID = dataItem.GetDataKeyValue("CourseTypeID") ?? 1;

            string urlWithParams;

            if (Convert.ToInt32(courseTypeID) == 1)
            {
                urlWithParams = string.Format("CourseRedirect.aspx?CourseID={0}&TestCategoryID={1}", encCourseId,
                                              sTestCategoryId);
            }
            else
            {
                urlWithParams = string.Format("CourseLaunch.aspx?CourseID={0}", encCourseId);
            }
            //var urlWithParams = string.Format("CourseRedirect.aspx?CourseID={0}&TestCategoryID={1}", encCourseId,
            //                                  sTestCategoryId);

            var linkTitle = dataItem.FindControl("linkCourseTitle") as LinkButton;

            if (linkTitle != null)
                linkTitle.Attributes["onclick"] =
                    string.Format("javascript: return LaunchCourse('{0}', '{1}', '{2}', '{3}', '{4}');", urlWithParams,
                                  enrolmentRequired, HttpUtility.JavaScriptStringEncode(title), courseId, HttpContext.Current.User.Identity.Name);
            var IT = dataItem.ItemType;
        }
    }

    protected void RestrictExternalUser()
    {
        var list = DataHelper.GetCourseAccess(Page.User.Identity.Name);
        cbCategory.SelectedValue = list[0].CategoryIDAccess.ToString();
        PopulateDropdownTreeSubcategories(cbCategory, ddtSubcategory);
        ddtSubcategory.SelectedValue = list[0].SubCategoryIDAccess.ToString();

        tblDefault2.Visible = false;
        tblExternal.Visible = true;

        lblCourse.Text = cbCategory.Text + " > " + ddtSubcategory.SelectedText;
    }

    protected void btnEnrolCourse_Click(object sender, EventArgs e)
    {
        var enrolmentStatus = DataHelper.ChkUserEnrolmentStatusToCourse(Convert.ToInt32(hidCourseId.Value), Convert.ToInt32(TwwId));
        _master = Master;

        switch (enrolmentStatus)
        {
            case 0:
            case 3:
            case 4: //0 = New, 3 = Denied, 4 = Cancelled by user
                var supQry = DataHelper.GetSupervisorDetails(Convert.ToInt32(TwwId));

                if (supQry != null)
                {
                    var retVal = DataHelper.InsertCourseEnrolment(Convert.ToInt32(hidCourseId.Value),
                                                                  Convert.ToInt32(TwwId), supQry.Cim1);

                    if (retVal)
                    {
                        const string msgToLearner = "Your course enrollment has been received and is going through approval process.";
                        const string msgToApprover = "Course registration for your approval.";

                        var owner = SendEmail.ValidateAddress(supQry.Email) ? supQry.Email : supQry.Email_fallback;
                        var recipient = SendEmail.ValidateAddress(supQry.Email1)
                                            ? supQry.Email1
                                            : supQry.Email1_fallback;

                        SendEmail.SendEmail_CourseEnrolment(owner, owner, msgToLearner, "", CourseLinkTitleToLearner, CourseDescription);
                        SendEmail.SendEmail_CourseEnrolment(owner, recipient, msgToApprover, "", CourseLinkTitleToApprover, CourseDescription);

                        const string alertMsg = "Your request for enrollment has been sent to your manager for approval. Please check your email for updates on the status of your enrollment.";
                        _master.AlertMessage(true, alertMsg);
                        ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseEnrollCourseWindow('1');", true);
                    }
                    else
                    {
                        _master.AlertMessage(false, "Request failed. Please try again.");
                    }
                }
                else
                {
                    _master.AlertMessage(false, "Request failed. Please try again.");
                }
                break;
            case 1: //For Approval
                _master.AlertMessage(false,
                                     "Your enrollment request is still subject for approval. Please wait for the email notification that will be sent to your registered email.");
                ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseEnrollCourseWindow('1');", true);
                break;
            case 2: //Approved
                var url = hidUrlWithParams.Value;
                var s = "window.open('" + url + "', 'popup_window', 'width=700,height=550,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=yes,resizable=yes');";
                ClientScript.RegisterStartupScript(GetType(), "script", s, true);
                ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseEnrollCourseWindow('1');", true);
                break;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        gridCourse.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        gridCourse.ExportSettings.IgnorePaging = true;
        gridCourse.ExportSettings.ExportOnlyData = true;
        gridCourse.ExportSettings.OpenInNewWindow = true;
        gridCourse.ExportSettings.FileName = "Courses_" + DateTime.Now;
        gridCourse.MasterTableView.ExportToExcel();
    }

    protected void linkCourseTitle_Click(object sender, EventArgs e)
    {
        var btn = sender as LinkButton;
        var item = btn.NamingContainer as GridDataItem;

        var title = item.GetDataKeyValue("Title").ToString();
        var desc = item.GetDataKeyValue("Description").ToString();

        const string url1 = "http://transcomuniversity.com/MyTeam.aspx";
        var titleLink1 = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url1, title);

        const string url2 = "http://transcomuniversity.com/MyPlan.aspx";
        var titleLink2 = string.Format("<a href=\"{0}\" target=\"_blank\">{1}</a>", url2, title);

        CourseLinkTitleToApprover = titleLink1;
        CourseLinkTitleToLearner = titleLink2;
        CourseDescription = desc;
    }
}
