using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Text;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Text.RegularExpressions;
using NuComm.Security.Encryption;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;

using ScormCourse;

public partial class Admin : Page
{

     ASP.scorm_dnscontrols_uploadcontent_ascx test = new ASP.scorm_dnscontrols_uploadcontent_ascx();

    #region .VARIABLES

    private string _employeeId = "";
    private MasterPage _master;
    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;
    readonly SCORM_UserControls_AddScormCourse _asc = new SCORM_UserControls_AddScormCourse();


    private DbHelper _db;

    public static bool IncludeElapsed
    {
        get
        {
            var value = HttpContext.Current.Session["includeElapsed"];
            return value != null && (bool)value;
        }
        set { HttpContext.Current.Session["includeElapsed"] = value; }
    }

    public static int AudienceId
    {
        get
        {
            var value = HttpContext.Current.Session["audienceID"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["audienceID"] = value; }
    }

    public static int PreReqCourseId
    {
        get
        {
            var value = HttpContext.Current.Session["preReqCourseID"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["preReqCourseID"] = value; }
    }

    private DataTable DtAudienceFilter
    {
        get
        {
            return ViewState["dtAudienceFilter"] as DataTable;
        }
        set
        {
            ViewState["dtAudienceFilter"] = value;
        }
    }

    private DataTable DtAudienceFilterSave
    {
        get
        {
            return ViewState["dtAudienceFilterSave"] as DataTable;
        }
        set
        {
            ViewState["dtAudienceFilterSave"] = value;
        }
    }

    private DataTable DtAudienceFilterEdit
    {
        get
        {
            return ViewState["DtAudienceFilterEdit"] as DataTable;
        }
        set
        {
            ViewState["DtAudienceFilterEdit"] = value;
        }
    }

    private DataTable DtToAssign
    {
        get
        {
            return ViewState["dtToAssign"] as DataTable;
        }
        set
        {
            ViewState["dtToAssign"] = value;
        }
    }

    private DataTable DtCoursePrereq
    {
        get
        {
            return ViewState["dtCoursePrereq"] as DataTable;
        }
        set
        {
            ViewState["dtCoursePrereq"] = value;
        }
    }

    private DataTable DtCoursePrereqEdit
    {
        get
        {
            return ViewState["dtCoursePrereqEdit"] as DataTable;
        }
        set
        {
            ViewState["dtCoursePrereqEdit"] = value;
        }
    }

    private string Operation
    {
        get
        {
            return ViewState["operation"] as string;
        }
        set
        {
            ViewState["operation"] = value;
        }
    }

    private string OperationPrereq
    {
        get
        {
            return ViewState["operationPrereq"] as string;
        }
        set
        {
            ViewState["operationPrereq"] = value;
        }
    }

    public DateTime? DpStartDateSelectedDate
    {
        get
        {
            if (ViewState["dpStartDateSelectedDate"] == null)
            {
                ViewState["dpStartDateSelectedDate"] = DateTime.Now;
            }

            return (DateTime)ViewState["dpStartDateSelectedDate"];
        }
        set
        {
            ViewState["dpStartDateSelectedDate"] = value;
        }
    }

    public DateTime? DpEndDateSelectedDate
    {
        get
        {
            if (ViewState["dpEndDateSelectedDate"] == null)
            {
                ViewState["dpEndDateSelectedDate"] = DateTime.Now;
            }

            return (DateTime)ViewState["dpEndDateSelectedDate"];
        }
        set
        {
            ViewState["dpEndDateSelectedDate"] = value;
        }
    }

    public static int ClientId
    {
        get
        {
            var value = HttpContext.Current.Session["ClientIdSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["ClientIdSession"] = value;
        }
    }

    public static int TrainTypeId
    {
        get
        {
            var value = HttpContext.Current.Session["TrainTypeIdSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["TrainTypeIdSession"] = value;
        }
    }

    public static int CtrPrerequisite
    {
        get
        {
            var value = HttpContext.Current.Session["ctrPrerequisite"];
            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["ctrPrerequisite"] = value; }
    }

    //SCORM
    public static string StrCourseIDs
    {
        get
        {
            var val = HttpContext.Current.Session["strCourseIDs"] == null ? string.Empty : HttpContext.Current.Session["strCourseIDs"].ToString();
            return val;
        }
        set { HttpContext.Current.Session["strCourseIDs"] = value; }
    }

    public static bool IsSearching
    {
        get
        {
            var val = HttpContext.Current.Session["IsSearching"] != null && Convert.ToBoolean(HttpContext.Current.Session["IsSearching"]);
            return val;
        }
        set { HttpContext.Current.Session["IsSearching"] = value; }
    }

    public DataTable DtBundle
    {
        get
        {
            return ViewState["DtBundle"] as DataTable;
        }
        set
        {
            ViewState["DtBundle"] = value;
        }
    }

    public DataTable DtOverAllBundle
    {
        get
        {
            return ViewState["DtOverAllBundle"] as DataTable;
        }
        set
        {
            ViewState["DtOverAllBundle"] = value;
        }
    }

    #endregion

    #region .INITIALIZE

    protected void Page_Load(object sender, EventArgs e)
    {

        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!Roles.IsUserInRole("Admin") && !Roles.IsUserInRole("Non-APAC Admin"))
        {
            Response.Redirect("NoAccess.aspx");
        }

        if (Roles.IsUserInRole("Non-APAC Admin"))
        {
            HideApacOnlyTabs();
        }

        GetGlobals();

        if (!Page.IsPostBack)
        {
            //PREREQUISITE
            CtrPrerequisite = 0;
            InitCoursePrereqDT();
            SetAvailableCourses(DtCoursePrereq, 0);

            //FOR CLASS NAME MAINTENANCE
            ClientId = 0;
            TrainTypeId = 0;
            //END CLASS NAME MAINTENANCE

            //FOR AUDIENCE MAINTENANCE
            dpDateEndAdd.SelectedDate = DateTime.Now.AddMonths(1);
            dpDateStartAdd.SelectedDate = DateTime.Now;

            Operation = "new";
            OperationPrereq = "new";
            //END AUDIENCE MAINTENANCE

            RefreshComboBoxesOnCategorySubcategory();
            editorWelcomeMessage.Content = ReadFile(Server.MapPath(WelcomePath));
            dpEndDate.SelectedDate = DateTime.Now.AddMonths(1);
            dpStartDate.SelectedDate = DateTime.Now;

            //SCORM
            StrCourseIDs = Request.QueryString["c"] ?? string.Empty;
            if (StrCourseIDs != string.Empty)
            {
                gridScormCourse.Rebind();
                StrCourseIDs = string.Empty;
            }
        }
    }

    private void GetGlobals()
    {
        _employeeId = Context.User.Identity.Name;
    }

    public void HideApacOnlyTabs()
    {
        var tab1 = tsMenu.FindTabByText("External Users");
        var tab2 = tsMenu.FindTabByText("Page Editor");
        tab1.Visible = false;
        tab2.Visible = false;
    }

    protected void RefreshComboBoxesOnCategorySubcategory()
    {
        PopulateComboBoxCategory(cbCategoryOnSubCat, "Admin");
        PopulateComboBoxCategory(cbAddCategoryOnSubCat, "Admin");
        PopulateComboBoxCategory(cbCategoryOnCourses, "Admin");
        PopulateComboBoxCategory(cbAddCategoryOnCourse, "Admin");
        PopulateComboBoxClient(cbClientOnAddExternal);
        PopulateComboBoxCategory(cbCategoryOnAddExternal, "Internal");
        PopulateComboBoxCategory(cbCategoryAssign, "Internal");
        PopulateComboBoxCategory(cbCategoryAddPrereq, "Internal");
        //SCORM
        PopulateComboBoxCategory(cbUploadScormCategory, "Internal");
    }

    #endregion

    #region .HTML

    public string StripHtml(string arg)
    {
        return HtmlRemoval.StripTagsRegex(arg);
    }

    public static class HtmlRemoval
    {
        /// <summary>
        /// Remove HTML from string with Regex.
        /// </summary>
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static readonly Regex HtmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string StripTagsRegexCompiled(string source)
        {
            return HtmlRegex.Replace(source, string.Empty);
        }

        /// <summary>
        /// Remove HTML tags from string using char array.
        /// </summary>
        public static string StripTagsCharArray(string source)
        {
            var array = new char[source.Length];
            var arrayIndex = 0;
            var inside = false;

            foreach (var @let in source)
            {
                if (@let == '<')
                {
                    inside = true;
                    continue;
                }
                if (@let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = @let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }
    }

    #endregion

    #region .CATEGORY

    protected void GridCategoryNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var ds = DataHelper.GetCategories("Admin");
        gridCategory.DataSource = ds;
    }

    protected void GridCategoryItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                var txtCategory = (RadTextBox)editedItem.FindControl("txtCategory");
                var categId = (int)editedItem.GetDataKeyValue("CategoryID");
                var categTxt = txtCategory.Text;

                var retVal = DataHelper.UpdateCategory(categId, categTxt, int.Parse(_employeeId));

                switch (retVal)
                {
                    case true:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('The Category has been updated.');", true);
                        RefreshComboBoxesOnCategorySubcategory();
                        break;
                    case false:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('The Category update failed.');", true);
                        break;
                }
            }
        }
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var categId = (int)deletedItem.GetDataKeyValue("CategoryID");

                var retVal = DataHelper.DeleteCategory(categId, int.Parse(_employeeId));

                switch (retVal)
                {
                    case true:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('The Category has been deleted.');", true);
                        RefreshComboBoxesOnCategorySubcategory();
                        break;
                    case false:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Category deletion failed.');", true);
                        break;
                }
            }
        }
    }

    protected void BtnAddCategoryClick(object sender, EventArgs e)
    {
        var retVal = DataHelper.InsertCategory(txtAddCategory.Text, int.Parse(_employeeId));

        switch (retVal)
        {
            case true:
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                            "alert('The Category has been added.');", true);
                gridCategory.Rebind();

                txtAddCategory.Text = string.Empty;
                RefreshComboBoxesOnCategorySubcategory();
                break;
            case false:
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                            "alert('Category insertion failed.');", true);
                break;
        }
    }

    #endregion

    #region .SUB-CATEGORY

    private static List<DataHelperModel.SubCategories> GetSubCategoriesByCategory_Edit(int? categoryId)
    {
        var db = new IntranetDBDataContext();
        var info = new List<DataHelperModel.SubCategories>
            {
                new DataHelperModel.SubCategories {SubcategoryId = null, Subcategory = "- Select None -"}
            };
        var result = info.Concat((from a in db.tbl_TranscomUniversity_Lkp_Subcategories
                                  join b in db.tbl_TranscomUniversity_Lkp_Categories on a.CategoryID equals b.CategoryID
                                  where b.CategoryID == categoryId && a.HideFromList == 0
                                  orderby a.Subcategory
                                  select new DataHelperModel.SubCategories
                                  {
                                      SubcategoryId = a.SubCategoryID,
                                      Subcategory = a.Subcategory,
                                      CategoryId = a.CategoryID,
                                      UpdatedBy = a.UpdatedBy,
                                      DateTimeUpdated = a.DateTimeUpdated,
                                      ParentSubcategoryId = a.ParentSubcategoryID
                                  })).ToList();
        return result;
    }

    private static List<DataHelperModel.SubCategories> GetSubCategoriesByCategory_Edit_No_SelectNone(int? categoryId)
    {
        var db = new IntranetDBDataContext();
        var result = (from a in db.tbl_TranscomUniversity_Lkp_Subcategories
                      join b in db.tbl_TranscomUniversity_Lkp_Categories on a.CategoryID equals b.CategoryID
                      where b.CategoryID == categoryId && a.HideFromList == 0
                      orderby a.Subcategory
                      select new DataHelperModel.SubCategories
                      {
                          SubcategoryId = a.SubCategoryID,
                          Subcategory = a.Subcategory,
                          CategoryId = a.CategoryID,
                          UpdatedBy = a.UpdatedBy,
                          DateTimeUpdated = a.DateTimeUpdated,
                          ParentSubcategoryId = a.ParentSubcategoryID
                      }).ToList();
        return result;
    }

    protected void GridSubCategoryNeedDataSource(object sender, TreeListNeedDataSourceEventArgs e)
    {
        LoadGridSubCategory();
    }

    private void LoadGridSubCategory()
    {
        var categoryId = !string.IsNullOrEmpty(cbCategoryOnSubCat.SelectedValue)
                             ? Convert.ToInt32(cbCategoryOnSubCat.SelectedValue)
                             : (int?)null;

        var ds = DataHelper.GetSubCategoriesByCategoryId(categoryId, "Admin");
        gridSubCategory.DataSource = ds;
    }

    private void GridSubcategoryDefaultRender()
    {
        gridSubCategory.GetColumnSafe("UpdatedBy").Display = true;
        gridSubCategory.GetColumnSafe("DateTimeUpdated").Display = true;
        gridSubCategory.GetColumnSafe("ParentSubcategoryId").Display = false;
    }

    protected void gridSubCategory_ItemCommand(object sender, TreeListCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as TreeListDataItem;

            if (editedItem != null)
            {
                var txtSubCategory = (RadTextBox)editedItem.FindControl("txtSubCategory");
                var cbCategory = (RadComboBox)editedItem.FindControl("cbCategory_onEdit");
                var cbParentSubcategory = (RadDropDownTree)editedItem.FindControl("ddtParentSubcategory_onEdit");
                var subCategId = (int)editedItem.GetDataKeyValue("SubcategoryId");
                var subCategTxt = txtSubCategory.Text;
                var parentSubcategId = !string.IsNullOrEmpty(cbParentSubcategory.SelectedValue)
                                           ? Convert.ToInt32(cbParentSubcategory.SelectedValue)
                                           : (int?)null;

                var retVal = DataHelper.UpdateSubCategory(subCategId, subCategTxt, Convert.ToInt32(cbCategory.SelectedValue),
                                                          int.Parse(_employeeId), parentSubcategId);

                switch (retVal)
                {
                    case true:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('The Sub-category has been updated.');", true);
                        txtSubCategory.Text = string.Empty;
                        cbCategory.ClearSelection();
                        GridSubcategoryDefaultRender();
                        break;
                    case false:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Sub-category update failed.');", true);
                        GridSubcategoryDefaultRender();
                        break;
                }
            }
        }
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as TreeListDataItem;
            if (deletedItem != null)
            {
                var subCategId = (int)deletedItem.GetDataKeyValue("SubcategoryId");
                var retVal = DataHelper.DeleteSubCategory(subCategId, int.Parse(_employeeId));

                switch (retVal)
                {
                    case true:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('The Sub-category has been deleted.');", true);
                        break;
                    case false:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Sub-category deletion failed.');", true);
                        break;
                }
            }
        }
        if (e.CommandName == RadGrid.CancelCommandName)
        {
            GridSubcategoryDefaultRender();
            gridSubCategory.DataBind();
        }
    }

    protected void gridSubCategory_ItemDataBound(object sender, TreeListItemDataBoundEventArgs e)
    {
        if (e.Item is TreeListDataItem)
        {
            var item = (e.Item as TreeListDataItem);

            if (item.IsInEditMode)
            {
                if (!(e.Item is ITreeListInsertItem))
                {
                    var cbCategory = (RadComboBox)item.FindControl("cbCategory_onEdit");
                    var cbParentSubcategory = (RadDropDownTree)item.FindControl("ddtParentSubcategory_onEdit");
                    var txtSubCateg = (RadTextBox)item.FindControl("txtSubCategory");

                    var subcategoryId = item.GetDataKeyValue("SubcategoryId");

                    var result = DataHelper.GetSubCategoryById(Convert.ToInt32(subcategoryId));

                    if (result != null)
                    {
                        txtSubCateg.Text = result.Subcategory;

                        PopulateComboBoxCategory(cbCategory, "Admin");
                        cbCategory.SelectedValue = result.CategoryId.ToString();
                        hidCategoryId.Value = result.CategoryId.ToString();

                        PopulateDropdownTreeSubcategory_EditMode(result.CategoryId.ToString(), cbParentSubcategory);
                        cbParentSubcategory.SelectedValue = result.ParentSubcategoryId.ToString();

                        gridSubCategory.GetColumnSafe("UpdatedBy").Display = false;
                        gridSubCategory.GetColumnSafe("DateTimeUpdated").Display = false;
                        gridSubCategory.GetColumnSafe("ParentSubcategoryId").Display = true;
                    }
                }
            }
        }
    }

    protected void gridSubCategory_ItemCreated(object sender, TreeListItemCreatedEventArgs e)
    {
        if (e.Item is TreeListDataItem)
        {
            var item = e.Item as TreeListDataItem;

            if (item.IsInEditMode)
            {
                var combo = (RadComboBox)item.FindControl("cbCategory_onEdit");
                combo.AutoPostBack = true;
                combo.SelectedIndexChanged += CbCategoryOnEditModeSubCatSelectedIndexChanged;
            }
        }
    }

    protected void CbCategoryOnEditModeSubCatSelectedIndexChanged(object sender, EventArgs e)
    {
        var cbCategoryOnEdit = (RadComboBox)sender;
        hidCategoryId.Value = cbCategoryOnEdit.SelectedValue;

        var editedItem = cbCategoryOnEdit.NamingContainer as TreeListDataItem;
        if (editedItem != null)
        {
            var cbParentSubcategoryOnEdit = editedItem.FindControl("ddtParentSubcategory_onEdit") as RadDropDownTree;
            PopulateDropdownTreeSubcategory_EditMode(hidCategoryId.Value, cbParentSubcategoryOnEdit);
        }
    }

    protected void cbParentSubcategory_ItemsRequested(object sender, RadComboBoxItemsRequestedEventArgs e)
    {
        var cbParentSubcategoryOnEdit = (RadComboBox)sender;

        var categoryId = !string.IsNullOrEmpty(hidCategoryId.Value)
                             ? Convert.ToInt32(hidCategoryId.Value)
                             : (int?)null;
        var result = DataHelper.GetSubCategoriesByCategoryId(categoryId, "Admin");
        cbParentSubcategoryOnEdit.DataSource = result;
        cbParentSubcategoryOnEdit.DataBind();
    }

    protected void btnAddParent_Click(object sender, EventArgs e)
    {
        trParent.Visible = true;
        btnAddParent.Visible = false;
        btnCancelParent.Visible = true;

        PopulateDropdownTreeSubcategories(cbAddCategoryOnSubCat, ddtAddSubcategoryOnSubCat);
    }

    public void PopulateDropdownTreeSubcategories_Active(RadComboBox cbCategory1, RadDropDownTree ddtSubcategory1)
    {
        var db = new DbHelper("Intranet");
        var categoryId = !string.IsNullOrEmpty(cbCategory1.SelectedValue)
                             ? Convert.ToInt32(cbCategory1.SelectedValue)
                             : (int?)null;

        ddtSubcategory1.DataSource = db.GetSubcategory_CoursesActive(categoryId, "Internal");

        ddtSubcategory1.DataBind();
    }

    public void PopulateDropdownTreeSubcategories(RadComboBox cbCategory1, RadDropDownTree ddtSubcategory1)
    {
        var categoryId = !string.IsNullOrEmpty(cbCategory1.SelectedValue) ? Convert.ToInt32(cbCategory1.SelectedValue) : (int?)null;

        var ds1 = DataHelper.GetSubCategoriesByCategoryId(categoryId, "Admin");
        ddtSubcategory1.DataSource = ds1;
        ddtSubcategory1.DataBind();
    }

    public void PopulateDropdownTreeSubcategory_EditMode(string selectedVal, RadDropDownTree ddtSubcategory1)
    {
        var categoryId = !string.IsNullOrEmpty(selectedVal) ? Convert.ToInt32(selectedVal) : (int?)null;
        var ds1 = ddtSubcategory1.ID == "ddtSubCategoryOnEditCourse" ? GetSubCategoriesByCategory_Edit_No_SelectNone(categoryId) : GetSubCategoriesByCategory_Edit(categoryId);

        ddtSubcategory1.DataSource = ds1;
        ddtSubcategory1.DataBind();
    }

    protected void btnCancelParent_Click(object sender, EventArgs e)
    {
        ddtAddSubcategoryOnSubCat.Entries.Clear();

        trParent.Visible = false;
        btnAddParent.Visible = true;
        btnCancelParent.Visible = false;
    }

    protected void BtnAddSubCategoryClick(object sender, EventArgs e)
    {
        int? parentSubCategId;
        if (!string.IsNullOrEmpty(ddtAddSubcategoryOnSubCat.SelectedValue))
        {
            parentSubCategId = Convert.ToInt32(ddtAddSubcategoryOnSubCat.SelectedValue);
        }
        else
        {
            parentSubCategId = null;
        }

        var retVal = DataHelper.InsertSubCategory(Convert.ToString(txtAddSubCategory.Text),
                                                  int.Parse(cbAddCategoryOnSubCat.SelectedValue),
                                                  int.Parse(_employeeId), parentSubCategId);

        switch (retVal)
        {
            case true:
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('The Sub-category has been added.');", true);
                gridSubCategory.Rebind();
                PopulateComboBoxCategory(cbAddCategoryOnSubCat, "Admin");
                txtAddSubCategory.Text = string.Empty;
                ddtAddSubcategoryOnSubCat.DataBindings.Clear();
                ddtAddSubcategoryOnSubCat.Entries.Clear();
                trParent.Visible = false;
                btnAddParent.Visible = true;
                btnCancelParent.Visible = false;
                break;
            case false:
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Sub-category insertion failed.');", true);
                break;
        }
    }

    protected void CbAddCategoryOnSubCatSelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        trParent.Visible = false;
        btnAddParent.Visible = true;
    }

    #endregion

    #region .CATEGORY SUBCATEGORY COMBOs

    protected void PopulateComboBoxCategory(RadComboBox combo, string accessMode)
    {
        combo.Text = "";

        var ds = DataHelper.GetCategories(accessMode);
        combo.DataSource = ds;
        combo.DataBind();
    }

    //protected void PopulateComboBoxSubCategory(RadComboBox combo, string selectedVal)
    //{
    //    combo.Text = "";
    //    combo.Items.Clear();

    //    var categoryId = !string.IsNullOrEmpty(selectedVal) ? Convert.ToInt32(selectedVal) : (int?)null;

    //    var ds = DataHelper.GetSubCategoriesByCategoryId(categoryId, "Admin");
    //    combo.DataSource = ds;
    //    combo.DataBind();
    //}

    protected void CbCategoryOnSubCatSelectedIndexChanged(object sender, EventArgs e)
    {
        GridSubcategoryDefaultRender();
        gridSubCategory.Rebind();
    }

    #endregion

    #region .COURSES

    private void LoadGridCourse()
    {
        var categoryId = !string.IsNullOrEmpty(cbCategoryOnCourses.SelectedValue) ? Convert.ToInt32(cbCategoryOnCourses.SelectedValue) : (int?)null;
        var subcategoryId = !string.IsNullOrEmpty(ddtSubCategoryOnCourses.SelectedValue) ? Convert.ToInt32(ddtSubCategoryOnCourses.SelectedValue) : (int?)null;

        var ds = DataHelper.GetCourses(categoryId, subcategoryId, "Admin", string.Empty);

        gridCourse.DataSource = ds;
    }

    public void CreatePackageDataTableColumns()
    {
        DtBundle = new DataTable();
        var column = new DataColumn
        {
            DataType = Type.GetType("System.Int32"),
            ColumnName = "ScoId",
            AutoIncrement = true,
            AutoIncrementSeed = 1,
            AutoIncrementStep = 1
        };

        DtBundle.Columns.Add(column);
        // DtBundle.Columns.Add("ScoId", typeof(int));
        DtBundle.Columns.Add("ScoPackageId", typeof(int));
        DtBundle.Columns.Add("ScoTitle", typeof(String));
        DtBundle.Columns.Add("ScoTypeId", typeof(int));
        DtBundle.Columns.Add("ScoType", typeof(String));
        DtBundle.Columns.Add("CourseTypeID", typeof(int));
        DtBundle.Columns.Add("curriculumcourseID", typeof(int));
    }

    public void CreateOverAllPackageDataTableColumns()
    {
        DtOverAllBundle = new DataTable();
        var column = new DataColumn
        {
            DataType = Type.GetType("System.Int32"),
            ColumnName = "ScoId",
            AutoIncrement = true,
            AutoIncrementSeed = 1,
            AutoIncrementStep = 1
        };
        DtOverAllBundle.Columns.Add(column);
        DtOverAllBundle.Columns.Add("ScoPackageId", typeof(int));
        DtOverAllBundle.Columns.Add("ScoTitle", typeof(String));
        DtOverAllBundle.Columns.Add("ScoTypeId", typeof(int));
        DtOverAllBundle.Columns.Add("ScoType", typeof(String));
        DtOverAllBundle.Columns.Add("CourseTypeID", typeof(int));
        DtOverAllBundle.Columns.Add("curriculumcourseID", typeof(int));
    }

    public void GetDBPackages(int CurriculumID, SCORM_UserControls_AddScormCourse EditScormCourse)
    {
        int counter = 1;
        SCORM_UserControls_AddScormCourse asc1 = new SCORM_UserControls_AddScormCourse();
        RadPanelBar RadPanelBar1 = (RadPanelBar)EditScormCourse.FindControl("pnl").FindControl("RadPanelBar1");
           
        Dbconn("Intranet");
        _ocmd = new SqlCommand("pr_Scorm_Cor_GetCurriculumPackages", _oconn)
        {
            CommandType = CommandType.StoredProcedure
        };

        _ocmd.Parameters.AddWithValue("@CurriculumID", CurriculumID);
        _ocmd.Parameters.AddWithValue("@isBundle", 1);
        SqlDataReader oDataReader = _ocmd.ExecuteReader();

        if (oDataReader.Read())
        {
            CreateOverAllPackageDataTableColumns();
            ImageButton btnDeletePackage = (ImageButton)(EditScormCourse.FindControl("pnl").FindControl("btnDeletePackage"));
            btnDeletePackage.Visible = true;


            int i = 0;
            do
            {
                Label lblPackageTitle = (Label)((RadPanelItem)((RadPanelBar)EditScormCourse.FindControl("pnl").FindControl("RadPanelBar1")).FindItemByValue(Convert.ToString(counter))).FindControl("lblPackageTitle" + Convert.ToString(counter));
                
                RadTextBox PackageTitle = (RadTextBox)((RadPanelItem)((RadPanelBar)EditScormCourse.FindControl("pnl").FindControl("RadPanelBar1")).FindItemByValue(Convert.ToString(counter))).FindControl("PackageTitle" + Convert.ToString(counter));
                RadTextBox PackageDesc = (RadTextBox)((RadPanelItem)((RadPanelBar)EditScormCourse.FindControl("pnl").FindControl("RadPanelBar1")).FindItemByValue(Convert.ToString(counter))).FindControl("PackageDesc" + Convert.ToString(counter));
                   i += 1;

                //RadPanelBar1.FindItemByValue(Convert.ToString(counter)).Text = oDataReader["Course"].ToString();

                RadPanelBar1.FindItemByValue(Convert.ToString(counter)).Text = "Course " +  i.ToString(); ;
                RadPanelBar1.FindItemByValue(Convert.ToString(counter)).Visible = true;

                PackageTitle.Text = oDataReader["Course"].ToString();
                lblPackageTitle.Text = oDataReader["Course"].ToString(); 
                PackageDesc.Text = oDataReader["PackageDesc"].ToString();

                RadGrid gridPackage = (RadGrid)((RadPanelItem)((RadPanelBar)EditScormCourse.FindControl("pnl").FindControl("RadPanelBar1")).FindItemByValue(Convert.ToString(counter))).FindControl("gridPackage" + Convert.ToString(counter));

              
                DataTable dt1 = GetCurriculumPackages(CurriculumID, Convert.ToInt32(oDataReader["curriculumcourseID"].ToString())).Tables["GetCurriculumPackages"];

                //add/append to temp datatable here===============
                foreach (DataRow row in dt1.Rows)
                {
                    DataRow dr = DtOverAllBundle.NewRow();
                    dr["ScoId"] = row["ScoId"].ToString();
                    dr["ScoPackageId"] = row["ScoPackageId"].ToString();
                    dr["ScoTitle"] = row["ScoTitle"].ToString();
                    dr["ScoTypeId"] = row["ScoTypeId"].ToString();
                    dr["ScoType"] = row["ScoType"].ToString();
                    dr["CourseTypeID"] = row["CourseTypeID"].ToString();
                    dr["curriculumcourseID"] = row["curriculumcourseID"].ToString();
                    DtOverAllBundle.Rows.Add(dr);
                }

                gridPackage.DataSource = dt1;
                gridPackage.DataBind();
                //==================================================

                counter += 1;

            }
            while (oDataReader.Read());
        }
        //TotalBundles = counter - 1;
        oDataReader.Close();
        _oconn.Close();
    }

    protected void CbCourseTypeSelectedIndexChanged(object sender, EventArgs e)
    {
        if (cbCourseType.Text == "Non-SCORM")
        {
            txtAddEncryptedCourseIDOnCourse.Enabled = true;
            lblBundle.Visible = false;
            AddScormCourse.Visible = false;
        }
        else
        {
            txtAddEncryptedCourseIDOnCourse.Enabled = false;
            lblBundle.Visible = true;
            AddScormCourse.Visible = true;
        }
    }

    protected void CbEditCourseTypeDataBinding(object sender, EventArgs e)
    {
        var CbEditCourseType = sender as RadComboBox;
        var editedItem = CbEditCourseType.NamingContainer as GridEditableItem;
        var ddtSubCategoryOnEditCourse = editedItem.FindControl("ddtSubCategoryOnEditCourse") as RadDropDownTree;
        var txtEditEncryptedCourseID = editedItem.FindControl("txtEncryptedCourseID") as RadTextBox;

        var lblEditBundle = editedItem.FindControl("lblEditBundle") as Label;
        var EditScormCourse = editedItem.FindControl("EditScormCourse") as SCORM_UserControls_AddScormCourse;

        if (CbEditCourseType.Text == "Non-SCORM")
        {
            txtEditEncryptedCourseID.Enabled = true;

            lblEditBundle.Visible = false;
            EditScormCourse.Visible = false;

        }
        else
        {
            txtEditEncryptedCourseID.Enabled = false;
            lblEditBundle.Visible = true;
            EditScormCourse.Visible = true;
        }
    }

    protected void CbEditCourseTypeSelectedIndexChanged(object sender, EventArgs e)
    {
        var CbEditCourseType = sender as RadComboBox;
        var editedItem = CbEditCourseType.NamingContainer as GridEditableItem;
        var ddtSubCategoryOnEditCourse = editedItem.FindControl("ddtSubCategoryOnEditCourse") as RadDropDownTree;
        var txtEditEncryptedCourseID = editedItem.FindControl("txtEncryptedCourseID") as RadTextBox;

        var lblEditBundle = editedItem.FindControl("lblEditBundle") as Label;
        var EditScormCourse = editedItem.FindControl("EditScormCourse") as SCORM_UserControls_AddScormCourse;

        if (CbEditCourseType.Text == "Non-SCORM")
        {
            txtEditEncryptedCourseID.Enabled = true;

            lblEditBundle.Visible = false;
            EditScormCourse.Visible = false;

        }
        else
        {
            txtEditEncryptedCourseID.Enabled = false;
            lblEditBundle.Visible = true;
            EditScormCourse.Visible = true;
        }
    }

    protected void CbCategoryOnCoursesSelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbCategoryOnCourses, ddtSubCategoryOnCourses);
    }

    protected void CbSubCategoryOnCoursesSelectedIndexChanged(object sender, EventArgs e)
    {
        gridCourse.Rebind();
    }

    protected void ddtSubCategoryOnCourses_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
    {
        gridCourse.Rebind();
    }

    protected void GridCourseNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LoadGridCourse();
    }

    protected void GridCourseItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            if (Page.IsValid)
            {
                var editedItem = e.Item as GridEditableItem;

                if (editedItem != null)
                {
                    var courseId = editedItem.GetDataKeyValue("CourseID").ToString();
                    var cbCategoryOnEditCourse = (RadComboBox)editedItem.FindControl("cbCategoryOnEditCourse");
                    var ddtSubCategoryOnEditCourse =
                        (RadDropDownTree)editedItem.FindControl("ddtSubCategoryOnEditCourse");

                    var txtTitle = (RadTextBox)editedItem.FindControl("txtTitle");
                    var txtDescription = (RadEditor)editedItem.FindControl("txtDescription");
                    var chkEnrolmentRequired = (CheckBox)editedItem.FindControl("chkEnrolmentRequired");
                    var txtEncryptedCourseId = (RadTextBox)editedItem.FindControl("txtEncryptedCourseID");
                    var txtTargetAudienceEdit = (RadTextBox)editedItem.FindControl("txtTargetAudienceEdit");
                    var dpStartDate = (RadDatePicker)editedItem.FindControl("dpStartDate");

                    var dpEndDate = (RadDatePicker)editedItem.FindControl("dpEndDate");
                    var txtDuration = (RadNumericTextBox)editedItem.FindControl("txtDuration");
                    var txtVersion = (RadNumericTextBox)editedItem.FindControl("txtVersion");
                    var txtAuthor = (RadTextBox)editedItem.FindControl("txtAuthor");
                    var txtDeptOwnership = (RadTextBox)editedItem.FindControl("txtDeptOwnership");
                    var txtTrainingCost = (RadNumericTextBox)editedItem.FindControl("txtTrainingCost");
                    var txtYrsEdit = (RadNumericTextBox)editedItem.FindControl("txtYrsEdit");
                    var txtRoleYrsEdit = (RadNumericTextBox)editedItem.FindControl("txtRoleYrsEdit");
                    var radioAccessMode = (RadioButtonList)editedItem.FindControl("radioAccessMode");
                    var chkLegacyEdit = (CheckBox)editedItem.FindControl("chkLegacyEdit");
                    var cbProgramPrereqEdit = (RadComboBox)editedItem.FindControl("cbProgramPrereqEdit");
                    var txtEncryptedCourseID = (RadTextBox)editedItem.FindControl("txtEncryptedCourseID");
                    var cbEditCourseType = (RadComboBox)editedItem.FindControl("cbEditCourseType");
                    var txtKeywords = (TextBox)editedItem.FindControl("txtKeywords");

                    var qry1 = DtCoursePrereq.AsEnumerable().Select(a => new { PrerequisiteID = a["PrerequisiteID"].ToString() });
                    var qry2 =
                        DtCoursePrereqEdit.AsEnumerable().Select(b => new { PrerequisiteID = b["PrerequisiteID"].ToString() });

                    var toBeDeleted = qry1.Except(qry2);
                    var toBeAdded = qry2.Except(qry1);

                    DataTable dtDelete = null;
                    DataTable dtAdd = null;

                    dtDelete = (from a in DtCoursePrereq.AsEnumerable()
                                join ab in toBeDeleted on a["PrerequisiteID"].ToString() equals ab.PrerequisiteID
                                select a).Any()
                                   ? (from a in DtCoursePrereq.AsEnumerable()
                                      join ab in toBeDeleted on a["PrerequisiteID"].ToString() equals
                                          ab.PrerequisiteID
                                      select a).CopyToDataTable()
                                   : dtDelete;

                    dtAdd = (from a in DtCoursePrereqEdit.AsEnumerable()
                             join ab in toBeAdded on a["PrerequisiteID"].ToString() equals ab.PrerequisiteID
                             select a).Any()
                                ? (from a in DtCoursePrereqEdit.AsEnumerable()
                                   join ab in toBeAdded on a["PrerequisiteID"].ToString() equals ab.PrerequisiteID
                                   select a).CopyToDataTable()
                                : dtAdd;

                    OperationPrereq = "new";
                    int? nucommcourseID = null;
                    if (txtEncryptedCourseID.Text != string.Empty)
                    {
                        nucommcourseID = Convert.ToInt32(UTF8.DecryptText(HttpUtility.UrlDecode(txtEncryptedCourseID.Text)));
                    }
                    var retVal = DataHelper.UpdateCourse(Convert.ToInt32(courseId),
                                                         Convert.ToInt32(cbCategoryOnEditCourse.SelectedValue),
                                                         Convert.ToInt32(ddtSubCategoryOnEditCourse.SelectedValue),
                                                         txtTitle.Text, txtDescription.Content, chkEnrolmentRequired.Checked,
                                                         txtEncryptedCourseId.Text, radioAccessMode.SelectedItem.Text,
                                                         dpStartDate.SelectedDate, dpEndDate.SelectedDate,
                                                         int.Parse(_employeeId), Convert.ToInt32(txtDuration.Text),
                                                         Convert.ToInt32(txtVersion.Text),
                                                         txtAuthor.Text, txtDeptOwnership.Text, txtTrainingCost.Text,
                                                         txtTargetAudienceEdit.Text, dtAdd, dtDelete, Convert.ToDecimal(txtYrsEdit.Text),
                                                         Convert.ToDecimal(txtRoleYrsEdit.Text), chkLegacyEdit.Checked, cbProgramPrereqEdit.SelectedValue == string.Empty ? 0 : Convert.ToDecimal(cbProgramPrereqEdit.SelectedValue),
                                                         nucommcourseID, Convert.ToInt32(cbEditCourseType.SelectedValue), txtKeywords.Text);


                    //update bundle
                    try
                    {
                        foreach (DataRow row in DtOverAllBundle.Rows) //update to 1 --contains original list
                        {
                            Dbconn("Intranet");
                            _ocmd = new SqlCommand("pr_Scorm_SaveUpdate_ScoDetails", _oconn)
                            {
                                CommandType = CommandType.StoredProcedure
                            };
                            _ocmd.Parameters.AddWithValue("@ScoPackageId", row["ScoPackageId"].ToString());
                            _ocmd.Parameters.AddWithValue("@isUpdate", "1");
                            _ocmd.ExecuteReader();
                            _oconn.Close();
                        }


                        //scan the current table
                        var EditScormCourse = (SCORM_UserControls_AddScormCourse)editedItem.FindControl("EditScormCourse");
                       

                        for (int i = 1; i <= 10; i++)
                        {
                            

                            RadPanelItem RadPanelItem1 =  (RadPanelItem)((RadPanelBar)EditScormCourse.FindControl("pnl").FindControl("RadPanelBar1")).FindItemByValue(Convert.ToString(i));
                            RadGrid gridPackage = (RadGrid)RadPanelItem1.FindControl("gridPackage" + Convert.ToString(i));

                            RadTextBox PackageDesc = (RadTextBox)RadPanelItem1.FindControl("PackageDesc" + Convert.ToString(i));
                            RadTextBox PackageTitle = (RadTextBox)RadPanelItem1.FindControl("PackageTitle" + Convert.ToString(i));

                            Label lblPackageTitle = (Label)RadPanelItem1.FindControl("lblPackageTitle" + Convert.ToString(i));

                            int curriculumcourseID;

                            if (RadPanelItem1.Visible == true)
                            {

                                //if bundle is new
                                //save bundle detail first

                                Dbconn("Intranet");
                                _ocmd = new SqlCommand("pr_Scorm_Sav_CourseDetails", _oconn)
                                {
                                    CommandType = CommandType.StoredProcedure
                                };
                                //_ocmd.Parameters.AddWithValue("@Course", RadPanelItem1.Text);
                                _ocmd.Parameters.AddWithValue("@Course", PackageTitle.Text);

                                _ocmd.Parameters.AddWithValue("@OldCourse", lblPackageTitle.Text);

                                _ocmd.Parameters.AddWithValue("@Description", PackageDesc.Text);
                                _ocmd.Parameters.AddWithValue("@isEdit", "1");
                                _ocmd.Parameters.AddWithValue("@Curriculumid", courseId);
                                _ocmd.Parameters.Add("@curriculumcourseid", SqlDbType.Int);
                                _ocmd.Parameters["@curriculumcourseid"].Direction = ParameterDirection.Output;

                                _ocmd.ExecuteReader();
                                _oconn.Close();
                          
                            //=======================================================================


                            curriculumcourseID = Convert.ToInt32(_ocmd.Parameters["@curriculumcourseid"].Value.ToString());

                            foreach (GridDataItem row in gridPackage.Items)
                            {
                                var ScoId = (int)row.GetDataKeyValue("ScoId");
                                var ScoPackageId = (int)row.GetDataKeyValue("ScoPackageId");
                                var ScoTitle = (string)row.GetDataKeyValue("ScoTitle");
                                var ScoTypeId = (int)row.GetDataKeyValue("ScoTypeId");
                                var CourseTypeID = (int)row.GetDataKeyValue("CourseTypeID");
                                //curriculumcourseID = (int)row.GetDataKeyValue("curriculumcourseID");

                                Dbconn("Intranet");
                                _ocmd = new SqlCommand("pr_Scorm_SaveUpdate_ScoDetails", _oconn)
                                {
                                    CommandType = CommandType.StoredProcedure
                                };

                                _ocmd.Parameters.AddWithValue("@ScoTypeId", ScoTypeId);
                                _ocmd.Parameters.AddWithValue("@ScoPackageId", ScoPackageId);
                                _ocmd.Parameters.AddWithValue("@CourseTypeID", CourseTypeID);
                                _ocmd.Parameters.AddWithValue("@ScoTitle", ScoTitle);
                                _ocmd.Parameters.AddWithValue("@CreatedBy", User.Identity.Name);
                                _ocmd.Parameters.AddWithValue("@ScoID", ScoId);
                                _ocmd.Parameters.AddWithValue("@isUpdate", "0");
                                _ocmd.Parameters.AddWithValue("@curriculumcourseID", curriculumcourseID);
                                _ocmd.ExecuteReader();
                                _oconn.Close();

                                }
                                
                            }
                        }

                    }
                    catch (Exception)
                    {

                    }


                    switch (retVal)
                    {
                        case true:
                            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                    "alert('The Course has been updated.');", true);
                            break;
                        case false:
                            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                    "alert('Course update failed.');", true);
                            break;
                    }
                }
            }
        }
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var courseId = deletedItem.GetDataKeyValue("CourseID").ToString();


                var retVal = DataHelper.DeleteCourse(Convert.ToInt32(courseId), int.Parse(_employeeId));

                switch (retVal)
                {
                    case true:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('The Course has been deleted.');", true);
                        break;
                    case false:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Course deletion failed.');", true);
                        break;
                }
            }
        }
        if (e.CommandName == RadGrid.CancelCommandName)
        {
            OperationPrereq = "new";
            DtCoursePrereqEdit.Clear();
            DtCoursePrereq.Clear();
            gridPrerequisites.Rebind();
        }
    }

    protected void GridCourseItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var gridItem = e.Item as GridDataItem;
            var title = gridItem.GetDataKeyValue("Title").ToString();
            gridItem.ToolTip = "Course Title: " + title;

            //new 
            var encryptedCourseId = gridItem.GetDataKeyValue("EncryptedCourseID").ToString();
            var courseId = gridItem.GetDataKeyValue("CourseID").ToString();
            string urlWithParams = null;
            if (!string.IsNullOrEmpty(encryptedCourseId) && !string.IsNullOrEmpty(courseId))
            {
                var baseUrl = ConfigurationManager.AppSettings["FeatCourseLink"];

                urlWithParams = string.Format("{0}CourseRedirect.aspx?CourseID={1}&TestCategoryID={2}", baseUrl,
                                                  HttpUtility.UrlEncode(UTF8.EncryptText(courseId)), encryptedCourseId);
            }
            gridItem["FeatCourseLnk"].Text = urlWithParams;

            ((ImageButton)gridItem["EditCommandColumn"].Controls[0]).Attributes.Add("onclick", "PassID('" + courseId + "')");
            //}
        }
        else if (e.Item.IsInEditMode)
        {
            var item = (GridEditableItem)e.Item;

            if (!(e.Item is IGridInsertItem))
            {
                PreReqCourseId = Convert.ToInt32(item.GetDataKeyValue("CourseID"));
                var categoryId = item.GetDataKeyValue("CategoryID").ToString();
                var subCategoryId = item.GetDataKeyValue("SubcategoryID").ToString();
                var title = item.GetDataKeyValue("Title").ToString();
                var desc = item.GetDataKeyValue("Description").ToString();
                var enrolmentReq = item.GetDataKeyValue("EnrolmentRequired") ?? "false";
                var legacy = item.GetDataKeyValue("Legacy") ?? "false";
                var access = item.GetDataKeyValue("AccessMode").ToString();
                var programId = item.GetDataKeyValue("PrereqProgramID") != null ? Convert.ToInt32(item.GetDataKeyValue("PrereqProgramID")) : 0;
                var encCourseId = item.GetDataKeyValue("EncryptedCourseID") != null ? item.GetDataKeyValue("EncryptedCourseID").ToString() : null;

                DateTime? start;
                if (item.GetDataKeyValue("StartDate") != null)
                {
                    start = Convert.ToDateTime(item.GetDataKeyValue("StartDate").ToString());
                }
                else
                {
                    start = null;
                }

                DateTime? end;
                if (item.GetDataKeyValue("EndDate") != null)
                {
                    end = Convert.ToDateTime(item.GetDataKeyValue("EndDate").ToString());
                }
                else
                {
                    end = null;
                }

                var duration = item.GetDataKeyValue("Duration").ToString();
                var version = item.GetDataKeyValue("Version").ToString();

                var author = item.GetDataKeyValue("Author") != null ? item.GetDataKeyValue("Author").ToString() : null;
                var deptOwner = item.GetDataKeyValue("DeptOwnership") != null ? item.GetDataKeyValue("DeptOwnership").ToString() : null;

                var cost = item.GetDataKeyValue("TrainingCost") ?? 0;
                var tenure = item.GetDataKeyValue("Tenure") ?? 0;
                var tenureInRole = item.GetDataKeyValue("TenureInRole") ?? 0;
                var audience = item.GetDataKeyValue("TargetAudience") != null ? item.GetDataKeyValue("TargetAudience").ToString() : null;


                var CourseTypeID = item.GetDataKeyValue("CourseTypeID").ToString();
                var keywords = item.GetDataKeyValue("Keywords") != null ? item.GetDataKeyValue("Keywords").ToString() : null;

                var cbCategoryOnEditCourse = (RadComboBox)item.FindControl("cbCategoryOnEditCourse");
                var ddtSubCategoryOnEditCourse = (RadDropDownTree)item.FindControl("ddtSubCategoryOnEditCourse");
                var txtTitle = (RadTextBox)item.FindControl("txtTitle");
                var txtDescription = (RadEditor)item.FindControl("txtDescription");
                var chkEnrolmentRequired = (CheckBox)item.FindControl("chkEnrolmentRequired");
                var chkLegacyEdit = (CheckBox)item.FindControl("chkLegacyEdit");
                var radioAccessMode = (RadioButtonList)item.FindControl("radioAccessMode");
                var txtEncryptedCourseID = (RadTextBox)item.FindControl("txtEncryptedCourseID");
                var dpStartDate = (RadDatePicker)item.FindControl("dpStartDate");
                var dpEndDate = (RadDatePicker)item.FindControl("dpEndDate");
                var txtDuration = (RadNumericTextBox)item.FindControl("txtDuration");
                var txtVersion = (RadNumericTextBox)item.FindControl("txtVersion");
                var txtAuthor = (RadTextBox)item.FindControl("txtAuthor");
                var txtDeptOwnership = (RadTextBox)item.FindControl("txtDeptOwnership");
                var txtTrainingCost = (RadNumericTextBox)item.FindControl("txtTrainingCost");
                var txtTargetAudienceEdit = (RadTextBox)item.FindControl("txtTargetAudienceEdit");
                var gridPrerequisitesEdit = (RadGrid)item.FindControl("gridPrerequisitesEdit");
                var cbCategoryEditPrereq = (RadComboBox)item.FindControl("cbCategoryEditPrereq");
                PopulateComboBoxCategory(cbCategoryEditPrereq, "Internal");
                var txtYrsEdit = (RadNumericTextBox)item.FindControl("txtYrsEdit");
                var txtRoleYrsEdit = (RadNumericTextBox)item.FindControl("txtRoleYrsEdit");
                var cbProgramPrereqEdit = (RadComboBox)item.FindControl("cbProgramPrereqEdit");

                var cbEditCourseType = (RadComboBox)item.FindControl("cbEditCourseType");
                var EditScormCourse = (SCORM_UserControls_AddScormCourse)item.FindControl("EditScormCourse");
                var txtKeywords = (TextBox)item.FindControl("txtKeywords");


                var lblEditBundle = (Label)item.FindControl("lblEditBundle");

                gridPrerequisitesEdit.Rebind();

                PopulateComboBoxCategory(cbCategoryOnEditCourse, "Admin");
                PopulateDropdownTreeSubcategory_EditMode(categoryId, ddtSubCategoryOnEditCourse);

                cbCategoryOnEditCourse.SelectedValue = categoryId;
                ddtSubCategoryOnEditCourse.SelectedValue = subCategoryId;
                txtTitle.Text = title;
                txtDescription.Content = desc;
                chkEnrolmentRequired.Checked = Convert.ToBoolean(enrolmentReq.ToString());
                radioAccessMode.SelectedValue = access;
                txtEncryptedCourseID.Text = encCourseId;
                dpStartDate.SelectedDate = start;
                dpEndDate.SelectedDate = end;
                txtDuration.Text = duration;
                txtVersion.Text = version;
                txtAuthor.Text = author;
                txtDeptOwnership.Text = deptOwner;
                txtTrainingCost.Text = cost.ToString();
                txtTargetAudienceEdit.Text = audience;
                chkLegacyEdit.Checked = Convert.ToBoolean(legacy.ToString());
                txtYrsEdit.Text = tenure.ToString();
                txtRoleYrsEdit.Text = tenureInRole.ToString();
                cbEditCourseType.SelectedValue = CourseTypeID;

                txtKeywords.Text = keywords;
                if (programId != 0)
                {
                    cbProgramPrereqEdit.SelectedValue = programId.ToString();
                }
                else
                {
                    cbProgramPrereqEdit.SelectedIndex = -1;
                }


                if (cbEditCourseType.Text == "Non-SCORM")
                {
                    txtEncryptedCourseID.Enabled = true;

                    lblEditBundle.Visible = false;
                    EditScormCourse.Visible = false;

                }
                else
                {
                    txtEncryptedCourseID.Enabled = false;
                    lblEditBundle.Visible = true;
                    EditScormCourse.Visible = true;
                }

                //load packages here
                GetDBPackages(PreReqCourseId, EditScormCourse);
            }
        }
    }

    protected void BtnAddCourseClick(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int? nucommcourseId = null;
            try
            {
                if (txtAddEncryptedCourseIDOnCourse.Text != string.Empty)
                {
                    nucommcourseId = Convert.ToInt32(UTF8.DecryptText(HttpUtility.UrlDecode(txtAddEncryptedCourseIDOnCourse.Text)));
                }
            }
            catch
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", "alert('Invalid Encrypted ID.');", true);
            return;
            }
            if (txtAddEncryptedCourseIDOnCourse.Text != string.Empty)
            {
                nucommcourseId = Convert.ToInt32(UTF8.DecryptText(HttpUtility.UrlDecode(txtAddEncryptedCourseIDOnCourse.Text)));
            }
            var retVal = DataHelper.InsertCourse(Convert.ToInt32(cbAddCategoryOnCourse.SelectedValue),
                                                 Convert.ToInt32(ddtAddSubCategoryOnCourse.SelectedValue),
                                                 txtAddTitleOnCourse.Text, txtAddDescriptionOnCourse.Content, chkEnrolmentRequired.Checked,
                                                 txtAddEncryptedCourseIDOnCourse.Text, radioAccessMode.SelectedItem.Text,
                                                 dpAddStartDateOnCourses.SelectedDate,
                                                 dpAddEndDateOnCourses.SelectedDate,
                                                 int.Parse(_employeeId), Convert.ToInt32(txtAddDurationOnCourse.Text),
                                                 Convert.ToInt32(txtAddVersionOnCourses.Text),
                                                 txtAddAuthorOnCourses.Text, txtAddDeptOwnershipOnCourses.Text,
                                                 txtTrainingCost.Text, null, txtTargetAudience.Text, DtCoursePrereq, Convert.ToDecimal(txtYrs.Text),
                                                 Convert.ToDecimal(txtRoleYrs.Text), chkLegacyAdd.Checked,
                                                 cbProgramPrereqAdd.SelectedValue == string.Empty ? 0 : Convert.ToInt32(cbProgramPrereqAdd.SelectedValue),
                                                 nucommcourseId, Convert.ToInt32(cbCourseType.SelectedValue), txtKeywords.Text);
            switch (retVal)
            {
                case true:

                    //add bundle details

                    if (cbCourseType.Text == "SCORM")
                    {
                        for (int i = 1; i <= 10; i++)
                        {
                            RadGrid gridPackage = (RadGrid)((RadPanelBar)((Panel)((UserControl)((RadPageView)btnAddCourse.NamingContainer.FindControl("pvCourse")).FindControl("AddScormCourse")).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(i.ToString()).FindControl("gridPackage" + i.ToString());
                            RadPanelItem RadPanelItem1 = (RadPanelItem)((RadPanelBar)((Panel)((UserControl)((RadPageView)btnAddCourse.NamingContainer.FindControl("pvCourse")).FindControl("AddScormCourse")).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(i.ToString());
                            RadTextBox PackageDesc = (RadTextBox)((RadPanelItem)((RadPanelBar)((Panel)((UserControl)((RadPageView)btnAddCourse.NamingContainer.FindControl("pvCourse")).FindControl("AddScormCourse")).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(i.ToString())).FindControl("PackageDesc" + i.ToString());
                            RadTextBox PackageTitle = (RadTextBox)((RadPanelItem)((RadPanelBar)((Panel)((UserControl)((RadPageView)btnAddCourse.NamingContainer.FindControl("pvCourse")).FindControl("AddScormCourse")).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(i.ToString())).FindControl("PackageTitle" + i.ToString());

                            int curriculumcourseid = 0;

                            if (RadPanelItem1.Visible)
                            //if (RadPanelItem1.Text != "")
                            {
                                Dbconn("Intranet");
                                _ocmd = new SqlCommand("pr_Scorm_Sav_CourseDetails", _oconn)
                                {
                                    CommandType = CommandType.StoredProcedure
                                };
                                _ocmd.Parameters.AddWithValue("@Course", PackageTitle.Text);
                                _ocmd.Parameters.AddWithValue("@Description", PackageDesc.Text);
                                _ocmd.Parameters.Add("@curriculumcourseid", SqlDbType.Int);
                                _ocmd.Parameters["@curriculumcourseid"].Direction = ParameterDirection.Output;

                                _ocmd.ExecuteReader();
                                _oconn.Close();
                                curriculumcourseid = Convert.ToInt32(_ocmd.Parameters["@curriculumcourseid"].Value.ToString());
                            }
                            int a;
                            a = 0;

                            if (gridPackage.Items.Count != 0)
                            {

                                foreach (GridDataItem row in gridPackage.Items)
                                {
                                    a += 1;

                                    var ScoTypeId = (Int32)row.GetDataKeyValue("ScoTypeId");
                                    var ScoPackageId = (Int32)row.GetDataKeyValue("ScoPackageId");
                                    var CourseTypeID = (Int32)row.GetDataKeyValue("CourseTypeID");
                                    var ScoTitle = (string)row.GetDataKeyValue("ScoTitle");

                                    Dbconn("Intranet");
                                    _ocmd = new SqlCommand("pr_Scorm_SaveUpdate_ScoDetails", _oconn)
                                    {
                                        CommandType = CommandType.StoredProcedure
                                    };
                                    _ocmd.Parameters.AddWithValue("@ScoTypeId", ScoTypeId);
                                    _ocmd.Parameters.AddWithValue("@ScoPackageId", ScoPackageId);
                                    _ocmd.Parameters.AddWithValue("@CourseTypeID", CourseTypeID);
                                    _ocmd.Parameters.AddWithValue("@ScoTitle", ScoTitle);
                                    _ocmd.Parameters.AddWithValue("@CreatedBy", User.Identity.Name);
                                    _ocmd.Parameters.AddWithValue("@curriculumcourseid", curriculumcourseid);
                                    _ocmd.ExecuteReader();
                                    _oconn.Close();
                                }
                            }
                            
                            //gridPackage.DataSource = null;
                            //gridPackage.DataBind();

                            gridPackage.DataSource = new string[] { };
                            gridPackage.DataBind();

                            gridPackage.Visible = false;
                            RadPanelItem1.Text = string.Empty;
                            RadPanelItem1.Visible = false;

                            _asc.CreatePackageDataTableColumns();
                        }

                    }

                    //=================================================================================

                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                            "alert('The Course has been added.');", true);
                    gridCourse.Rebind();
                    InitCoursePrereqDT();
                    gridPrerequisites.Rebind();
                    cbAddCategoryOnCourse.Text = "";
                    cbAddCategoryOnCourse.ClearSelection();
                    txtYrs.Text = "0";
                    txtRoleYrs.Text = "0";
                    PopulateDropdownTreeSubcategories(cbAddCategoryOnCourse, ddtAddSubCategoryOnCourse);

                    txtAddTitleOnCourse.Text = string.Empty;
                    txtAddDescriptionOnCourse.Content = string.Empty;
                    chkEnrolmentRequired.Checked = false;
                    txtAddEncryptedCourseIDOnCourse.Text = string.Empty;
                    txtAddDurationOnCourse.Text = string.Empty;
                    txtAddVersionOnCourses.Text = string.Empty;
                    txtAddAuthorOnCourses.Text = string.Empty;
                    txtAddDeptOwnershipOnCourses.Text = string.Empty;
                    txtTargetAudience.Text = string.Empty;
                    dpAddStartDateOnCourses.Clear();
                    dpAddEndDateOnCourses.Clear();
                    txtTrainingCost.Text = string.Empty;
                    radioAccessMode.ClearSelection();
                    cbProgramPrereqAdd.Text = string.Empty;
                    cbProgramPrereqAdd.SelectedIndex = -1;
                    cbAddCategoryOnCourse.Focus();
                    tsCourses.SelectedIndex = 2;
                    mpCourses.SelectedIndex = 0;
                    txtKeywords.Text = string.Empty;

                    break;
                case false:
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                            "alert(Course insertion failed.');", true);
                    break;
            }
        }
    }

    protected void CbAddCategoryOnCourseSelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbAddCategoryOnCourse, ddtAddSubCategoryOnCourse);
    }

    protected void CbCategoryOnEditCourseSelectedIndexChanged(object sender, EventArgs e)
    {
        var cbCategoryOnEditCourse = sender as RadComboBox;

        if (cbCategoryOnEditCourse != null)
        {
            var editedItem = cbCategoryOnEditCourse.NamingContainer as GridEditableItem;
            if (editedItem != null)
            {
                var ddtSubCategoryOnEditCourse = editedItem.FindControl("ddtSubCategoryOnEditCourse") as RadDropDownTree;

                PopulateDropdownTreeSubcategories(cbCategoryOnEditCourse, ddtSubCategoryOnEditCourse);
            }
        }
    }

    protected void btnExport_OnClick(object sender, EventArgs e)
    {
        var dt = DataHelper.GetCoursesForExport(0, 0, "Admin");
        ExportToExcel(dt);
    }

    public void ExportToExcel(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            dt.Columns.Remove("CategoryID");
            dt.Columns.Remove("SubcategoryID");
            dt.Columns.Remove("Duration");
            dt.Columns.Remove("UpdatedBy");
            dt.Columns.Remove("LastUpdated");
            dt.Columns.Remove("EncryptedCourseID1");
            dt.Columns.Remove("Category");
            dt.Columns["Subcategory"].ColumnName = "Course Path";
            var sbDocBody = new StringBuilder();

            try
            {
                sbDocBody.Append("<style>");
                sbDocBody.Append(".Header {  background-color:Navy; color:#ffffff; font-weight:bold;font-family:Verdana; font-size:12px;}");
                sbDocBody.Append(".SectionHeader { background-color:#8080aa; color:#ffffff; font-family:Verdana; font-size:12px;font-weight:bold;}");
                sbDocBody.Append(".Content { background-color:#ccccff; color:#000000; font-family:Verdana; font-size:12px;text-align:left}");
                sbDocBody.Append(".Label { background-color:#ccccee; color:#000000; font-family:Verdana; font-size:12px; text-align:right;}");
                sbDocBody.Append("</style>");

                sbDocBody.Append("<br><table align=\"center\" cellpadding=1 cellspacing=0 style=\"background-color:#000000;\">");
                sbDocBody.Append("<tr><td width=\"500\">");
                sbDocBody.Append("<table width=\"100%\" cellpadding=1 cellspacing=2 style=\"background-color:#ffffff;\">");

                if (dt.Rows.Count > 0)
                {
                    sbDocBody.Append("<tr><td>");
                    sbDocBody.Append("<table width=\"600\" cellpadding=\"0\" cellspacing=\"2\"><tr><td>");

                    sbDocBody.Append("<tr><td width=\"25\"> </td></tr>");
                    sbDocBody.Append("<tr>");
                    sbDocBody.Append("<td> </td>");
                    for (var i = 0; i < dt.Columns.Count; i++)
                    {
                        sbDocBody.Append("<td class=\"Header\" width=\"120\">" + dt.Columns[i].ToString().Replace(".", "<br>") + "</td>");
                    }
                    sbDocBody.Append("</tr>");

                    for (var i = 0; i < dt.Rows.Count; i++)
                    {
                        sbDocBody.Append("<tr>");
                        sbDocBody.Append("<td> </td>");
                        for (var j = 0; j < dt.Columns.Count; j++)
                        {
                            sbDocBody.Append("<td class=\"Content\">" + dt.Rows[i][j] + "</td>");
                        }
                        sbDocBody.Append("</tr>");
                    }
                    sbDocBody.Append("</table>");
                    sbDocBody.Append("</td></tr></table>");
                    sbDocBody.Append("</td></tr></table>");
                }

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;

                HttpContext.Current.Response.AppendHeader("Content-Type", "application/ms-excel");
                HttpContext.Current.Response.AppendHeader("Content-disposition", "attachment; filename=Courses.xls");
                HttpContext.Current.Response.Write(sbDocBody.ToString());
                HttpContext.Current.Response.End();
            }
            catch (Exception)
            { }
        }
    }

    #endregion

    #region .SCORM

    protected void GridScormCourseNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (IsSearching)
        {
            var val = Server.HtmlEncode(Microsoft.Security.Application.AntiXss.HtmlEncode(txtSearch.Text.Trim()));

            if (!string.IsNullOrEmpty(val))
            {
                var ds = DataHelper.DoSearch(val, "Admin");
                gridScormCourse.DataSource = ds;
            }
        }
        else
        {
            var categoryId = !string.IsNullOrEmpty(cbUploadScormCategory.SelectedValue)
                     ? Convert.ToInt32(cbUploadScormCategory.SelectedValue)
                     : (int?)null;
            var subcategoryId = !string.IsNullOrEmpty(cbUploadScormSubCategory.SelectedValue)
                                    ? Convert.ToInt32(cbUploadScormSubCategory.SelectedValue)
                                    : (int?)null;

            var ds = DataHelper.GetNonScormCourses(categoryId, subcategoryId, "Admin", StrCourseIDs);

            gridScormCourse.DataSource = ds;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        IsSearching = true;
        gridScormCourse.Rebind();

        cbUploadScormCategory.Text = "";
        cbUploadScormCategory.ClearSelection();
        PopulateDropdownTreeSubcategories(cbUploadScormCategory, cbUploadScormSubCategory);
    }

    protected void CbUploadScormCategorySelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbUploadScormCategory, cbUploadScormSubCategory);
    }

    protected void cbUploadScormSubCategory_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
    {
        IsSearching = false;
        gridScormCourse.Rebind();

        txtSearch.Text = "";
    }

    protected void cbUploadType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //SCORM_UserControls_Test.PersistUploadDock_OnPostback();
        var combo = sender as RadComboBox;

        if (combo != null && combo.SelectedValue == "1")
        {
            //string script = "function f(){$find(\"" + windowUpload1.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
            //ScriptManager.RegisterStartupScript(this, GetType(), "close", "Close_UploadWindow2('1');", true);
        }
        else
        {
            //string script = "function f(){$find(\"" + windowUpload2.ClientID + "\").show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, true);
            //ScriptManager.RegisterStartupScript(this, GetType(), "close", "Close_UploadWindow1('1');", true);
        }
    }

    public static List<Control> GetAllControls(List<Control> controls, Type t, Control parent)
        {
            foreach (Control c in parent.Controls)
            {
                if (c.GetType()== t)
                    controls.Add(c);
                if (c.HasControls())
                    controls = GetAllControls(controls, t, c);
            }
            return controls;
        }

    protected void btnUpload2_OnClick(object sender, EventArgs e)
    {
        string key = "";
        string title = "";

        foreach (GridDataItem item in gridScormCourse.MasterTableView.Items)
        {
            if (item.Selected)

            //if (cb != null && cb.Checked)
            {
                key = item.GetDataKeyValue("CourseID").ToString();
                title = item.GetDataKeyValue("Title").ToString();
            }
        }

        //add non scorm course =====================================================

        SCORM_UserControls_AddScormCourse asc1 = new SCORM_UserControls_AddScormCourse();

        Label AddButtonSenderID;
        RadComboBox cbSCOType;
        RadGrid gridPackage;

        if (mpCourses.SelectedIndex == 1) //if for addding
        {
            AddButtonSenderID = (Label)((Panel)(AddScormCourse).FindControl("pnl")).FindControl("AddButtonSenderID");
            cbSCOType = (RadComboBox)((RadPanelItem)((RadPanelBar)((Panel)(AddScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("cbSCOType" + AddButtonSenderID.Text);
            gridPackage = (RadGrid)((RadPanelItem)((RadPanelBar)((Panel)(AddScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("gridPackage" + AddButtonSenderID.Text);
        }

        else //if for updating
        {


           // List<ScormCourse.SCORM_UserControls_AddScormCourse> cntrls = GetAllControls(new List<ScormCourse.SCORM_UserControls_AddScormCourse>(), ScormCourse.SCORM_UserControls_AddScormCourse, this);


        
            ASP.scorm_usercontrols_addscormcourse_ascx EditScormCourse = LogicHelper.FindControl<ASP.scorm_usercontrols_addscormcourse_ascx>(gridCourse.Controls, "EditScormCourse");

            //SCORM_UserControls_AddScormCourse EditScormCourse = LogicHelper.FindControl<SCORM_UserControls_AddScormCourse>(gridCourse.Controls, "EditScormCourse");


            AddButtonSenderID = (Label)((Panel)(EditScormCourse).FindControl("pnl")).FindControl("AddButtonSenderID");
            cbSCOType = (RadComboBox)((RadPanelItem)((RadPanelBar)((Panel)(EditScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("cbSCOType" + AddButtonSenderID.Text);
            gridPackage = (RadGrid)((RadPanelItem)((RadPanelBar)((Panel)(EditScormCourse).FindControl("pnl")).FindControl("RadPanelBar1")).FindItemByValue(AddButtonSenderID.Text)).FindControl("gridPackage" + AddButtonSenderID.Text);
       
        }

        asc1.CreatePackageDataTableColumns();
        foreach (GridDataItem row in gridPackage.Items)
        {
            var ScoTypeId = (Int32)row.GetDataKeyValue("ScoTypeId");
            var ScoPackageId = (Int32)row.GetDataKeyValue("ScoPackageId");
            var ScoType = (string)row.GetDataKeyValue("ScoType");
            var ScoTitle = (string)row.GetDataKeyValue("ScoTitle");
            var CourseTypeID = (Int32)row.GetDataKeyValue("CourseTypeID");
            var curriculumcourseID = (Int32)row.GetDataKeyValue("curriculumcourseID");

            DataRow dr = asc1.DtPackage1.NewRow();
            dr["ScoPackageId"] = ScoPackageId;
            dr["ScoTitle"] = ScoTitle;
            dr["ScoType"] = ScoType;
            dr["ScoTypeId"] = ScoTypeId;
            dr["CourseTypeID"] = CourseTypeID;
            dr["curriculumcourseID"] = curriculumcourseID;
            asc1.DtPackage1.Rows.Add(dr);
        }


        DataRow drPackage = asc1.DtPackage1.NewRow();
        drPackage["ScoPackageId"] =  Convert.ToInt32(key.ToString()); //courseid
        drPackage["ScoTitle"] = title; //coursetitle

        drPackage["ScoType"] = cbSCOType.SelectedItem.Text;
        drPackage["ScoTypeId"] = cbSCOType.SelectedValue;

        drPackage["CourseTypeID"] = 1; //1 - non scorm, 2- scorm
        drPackage["curriculumcourseID"] = AddButtonSenderID.Text; //id of grid

        asc1.DtPackage1.Rows.Add(drPackage);

        gridPackage.DataSource = asc1.DtPackage1;
        gridPackage.DataBind();

        //====================================================================


        //close window
        ScriptManager.RegisterStartupScript(this, GetType(), "close", "Close_UploadWindow2('1');", true);


    }

    #endregion

    #region .SCORM COURSES

    private DataSet GetCurriculumPackages(int CurriculumID, int CurriculumCourseID)
    {
        _db = new DbHelper("Intranet");
        var ds = _db.GetCurriculumPackages(CurriculumID, CurriculumCourseID);
        return ds;
    }

    protected void CbCourseTypeListSelectedIndexChanged(object sender, EventArgs e)
    {

        gridCourse.Visible = true;
        gridCourse.Rebind();

    }

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
        {
            ConnectionString = ConfigurationManager.ConnectionStrings[connStr].ConnectionString
        };
        _oconn.Open();
    }

    protected void CbCourseTypeDataBinding(object sender, EventArgs e)
    {
        cbCourseType.SelectedIndex = 0;
    }

    #endregion

    #region .CAREER PATH

    protected void GridCareerPathNeedDataSource(object source, GridNeedDataSourceEventArgs e)
    {
        var ds = DataHelper.GetCareerPath();
        gridCareerPath.DataSource = ds;
    }

    protected void BtnAddCareerPathClick(object sender, EventArgs e)
    {
        var retVal = DataHelper.InsertCareerPath(txtAddCareerPathName.Text, txtAddCareerPathDescription.Text,
                             Convert.ToByte(cbAddCareerHideFromList.SelectedValue), _employeeId);

        switch (retVal)
        {
            case true:
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('The Career Path has been added.');", true);
                txtAddCareerPathName.Text = string.Empty;
                txtAddCareerPathDescription.Text = string.Empty;
                cbAddCareerHideFromList.ClearSelection();
                break;
            case false:
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Career Path insertion failed.');", true);
                break;
        }
    }

    protected void GridCareerPathItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.IsInEditMode)
        {
            var item = (GridEditableItem)e.Item;

            if (!(e.Item is IGridInsertItem))
            {
                var careerName = item.GetDataKeyValue("CareerName");
                var description = item.GetDataKeyValue("Description").ToString();
                var hidefromlist = item.GetDataKeyValue("HideFromList").ToString();

                var txtCareerName = (RadTextBox)item.FindControl("txtCareerName");
                var txtDescription = (RadTextBox)item.FindControl("txtDescription");
                var cbCareerHideFromList = (RadComboBox)item.FindControl("cbCareerHideFromList");

                txtCareerName.Text = (string)careerName;
                txtDescription.Text = description;
                cbCareerHideFromList.SelectedValue = hidefromlist;
            }
        }
    }

    protected void GridCareerPathItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.ExpandCollapseCommandName)
        {
            foreach (GridItem item in e.Item.OwnerTableView.Items)
            {
                if (item.Expanded && item != e.Item)
                {
                    item.Expanded = false;
                }
            }
        }
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                var careerPathId = editedItem.GetDataKeyValue("CareerPathID").ToString();

                var txtCareerName = (RadTextBox)editedItem.FindControl("txtCareerName");
                var txtDescription = (RadTextBox)editedItem.FindControl("txtDescription");
                var cbCareerHideFromList = (RadComboBox)editedItem.FindControl("cbCareerHideFromList");

                var retVal = DataHelper.UpdateCareerPath(Convert.ToInt32(careerPathId), txtCareerName.Text, txtDescription.Text,
                                                         Convert.ToByte(cbCareerHideFromList.SelectedValue), _employeeId);

                switch (retVal)
                {
                    case true:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('The Career Path has been updated.');", true);
                        gridCareerPath.Rebind();
                        break;
                    case false:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Career Path update failed.');", true);
                        break;
                }
            }
        }
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var careerPathId = deletedItem.GetDataKeyValue("CareerPathID").ToString();
                DataHelper.DeleteCareerPath(Convert.ToInt32(careerPathId));
            }
        }
    }

    protected void GridCareerPathPreRender(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            foreach (GridDataItem item in gridCareerPath.Items)
            {
                if (!item.Expanded || item.Selected) continue;
                item.Expanded = true;
                item.Selected = true;
            }
        }
    }

    protected void BtnAddCareerCourseClick(object sender, EventArgs e)
    {
        var btnAddCareerCourse = sender as RadButton;
        if (btnAddCareerCourse != null)
        {
            var item = (GridNestedViewItem)btnAddCareerCourse.NamingContainer;

            var gridCareerPathCourses = (RadGrid)item.FindControl("gridCareerPathCourses");
            var cbAddCareerCourses = (RadComboBox)item.FindControl("cbAddCareerCourses");
            var tsCoursesOnCareerPath = (RadTabStrip)item.FindControl("tsCoursesOnCareerPath");
            var mpCoursesOnCareerPath = (RadMultiPage)item.FindControl("mpCoursesOnCareerPath");

            var keyCareerPathId = item.ParentItem.GetDataKeyValue("CareerPathID").ToString();

            var retVal = DataHelper.InsertCareerCourse(Convert.ToInt32(keyCareerPathId),
                                                       Convert.ToInt32(cbAddCareerCourses.SelectedValue), _employeeId);

            switch (retVal)
            {
                case true:
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                            "alert('The Career Course has been added.');", true);
                    gridCareerPathCourses.Rebind();
                    cbAddCareerCourses.ClearSelection();
                    tsCoursesOnCareerPath.SelectedIndex = 0;
                    mpCoursesOnCareerPath.SelectedIndex = 0;
                    break;
                case false:
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                            "alert('Career Course insertion failed.');", true);
                    break;
            }
        }
    }

    #endregion

    #region .EXTERNAL USER MANAGEMENT

    protected void gridExternalUsers_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var ds = DataHelper.GetExternalUsers(0);
        gridExternalUsers.DataSource = ds;
    }

    protected void gridExternalUsers_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                var txtFirstName = (RadTextBox)editedItem.FindControl("txtFirstName");
                var txtLastName = (RadTextBox)editedItem.FindControl("txtLastName");
                var txtEmail = (RadTextBox)editedItem.FindControl("txtEmail");
                var cbClientOnEditExternal = (RadComboBox)editedItem.FindControl("cbClientOnEditExternal");
                var dpEndDate = (RadDatePicker)editedItem.FindControl("dpEndDate");
                var dpStartDate = (RadDatePicker)editedItem.FindControl("dpStartDate");
                var cbCategoryOnEditExternal = (RadComboBox)editedItem.FindControl("cbCategoryOnEditExternal");
                var ddtSubCategoryOnEditExternal = (RadDropDownTree)editedItem.FindControl("ddtSubCategoryOnEditExternal");

                var UserID = (int)editedItem.GetDataKeyValue("UserID");

                var FirstNameTxt = txtFirstName.Text;
                var LastNameTxt = txtLastName.Text;
                var EmailTxt = txtEmail.Text;
                var StartDateVal = dpStartDate.DbSelectedDate;
                var EndDateVal = dpEndDate.DbSelectedDate;
                var cbClientOnEditExternalVal = Convert.ToInt32(cbClientOnEditExternal.SelectedValue);
                var cbCategoryOnEditExternalVal = Convert.ToInt32(cbCategoryOnEditExternal.SelectedValue);
                var ddtSubCategoryOnEditExternalVal = Convert.ToInt32(ddtSubCategoryOnEditExternal.SelectedValue);

                var retVal = DataHelper.UpdateExternalUser(UserID, FirstNameTxt, LastNameTxt, EmailTxt,
                                                           cbClientOnEditExternalVal, Convert.ToDateTime(StartDateVal),
                                                           Convert.ToDateTime(EndDateVal), cbCategoryOnEditExternalVal,
                                                           ddtSubCategoryOnEditExternalVal, int.Parse(_employeeId));

                switch (retVal)
                {
                    case true:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('The External User has been updated.');", true);
                        break;
                    case false:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('The External User update failed.');", true);
                        break;
                }
            }
        }
        else if (e.CommandName == "ResetPassword")
        {
            var item = e.Item as GridDataItem;

            var userId = (int)item.GetDataKeyValue("UserID");


            var retVal = DataHelper.ResetExternalUserPassword(userId, int.Parse(_employeeId));

            switch (retVal)
            {
                case true:
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                            "alert('Password has been reset.');", true);
                    break;
                case false:
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                            "alert('Password reset has failed.');", true);
                    break;
            }
        }
    }

    protected void btnAddExternalUser_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (DataHelper.ExternalUserExists(txtEmail.Text))
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                           "alert('Email already exists. Just change the end date of the email to activate the account.');", true);
                return;
            }

            var retVal = DataHelper.InsertExternalUser(txtFirstName.Text, txtLastName.Text, txtEmail.Text,
                                                       Convert.ToInt32(cbClientOnAddExternal.SelectedValue),
                                                       dpStartDate.SelectedDate, dpEndDate.SelectedDate,
                                                       Convert.ToInt32(cbCategoryOnAddExternal.SelectedValue),
                                                       Convert.ToInt32(ddtSubCategoryOnAddExternal.SelectedValue),
                                                       int.Parse(_employeeId));


            switch (retVal)
            {
                case true:
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                            "alert('The External User has been added.');", true);
                    gridExternalUsers.Rebind();

                    txtFirstName.Text = string.Empty;
                    txtLastName.Text = string.Empty;
                    txtEmail.Text = string.Empty;
                    cbClientOnAddExternal.ClearSelection();
                    cbClientOnAddExternal.Text = string.Empty;
                    dpStartDate.SelectedDate = DateTime.Now;
                    dpEndDate.SelectedDate = DateTime.Now.AddMonths(1);
                    cbCategoryOnAddExternal.ClearSelection();
                    cbCategoryOnAddExternal.Text = string.Empty;
                    ddtSubCategoryOnAddExternal.DataSource = null;
                    ddtSubCategoryOnAddExternal.DataBind();
                    break;
                case false:
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                            "alert('External User addition has failed.');", true);
                    break;
            }
        }
    }

    protected void gridExternalUsers_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item.IsInEditMode && e.Item.IsInEditMode)
        {
            var item = (GridEditableItem)e.Item;

            if (!(e.Item is IGridInsertItem))
            {
                var categoryId = item.GetDataKeyValue("CategoryID").ToString();
                var subCategoryId = item.GetDataKeyValue("SubcategoryID").ToString();
                var clientId = item.GetDataKeyValue("ClientID").ToString();

                var startDate = item.GetDataKeyValue("StartDate").ToString();
                var endDate = item.GetDataKeyValue("EndDate").ToString();

                var cbCategoryOnEditExternal = (RadComboBox)item.FindControl("cbCategoryOnEditExternal");
                var ddtSubCategoryOnEditExternal = (RadDropDownTree)item.FindControl("ddtSubCategoryOnEditExternal");
                var cbClientOnEditExternal = (RadComboBox)item.FindControl("cbClientOnEditExternal");

                var dpStartDate = (RadDatePicker)item.FindControl("dpStartDate");
                var dpEndDate = (RadDatePicker)item.FindControl("dpEndDate");

                dpStartDate.SelectedDate = Convert.ToDateTime(startDate);
                dpEndDate.SelectedDate = Convert.ToDateTime(endDate);

                PopulateComboBoxCategory(cbCategoryOnEditExternal, "Internal");
                PopulateDropdownTreeSubcategory_EditMode(categoryId, ddtSubCategoryOnEditExternal);
                PopulateComboBoxClient(cbClientOnEditExternal);
                cbCategoryOnEditExternal.SelectedValue = categoryId;
                ddtSubCategoryOnEditExternal.SelectedValue = subCategoryId;
                cbClientOnEditExternal.SelectedValue = clientId;
            }
        }
    }

    protected void cbCategoryOnAddExternal_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        PopulateDropdownTreeSubcategories_Active(cbCategoryOnAddExternal, ddtSubCategoryOnAddExternal);
    }

    protected void cbCategoryOnEditExternal_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        var cbCategoryOnEditExternal = sender as RadComboBox;

        if (cbCategoryOnEditExternal != null)
        {
            var editedItem = cbCategoryOnEditExternal.NamingContainer as GridEditableItem;
            if (editedItem != null)
            {
                var ddtSubCategoryOnEditExternal = editedItem.FindControl("ddtSubCategoryOnEditExternal") as RadDropDownTree;

                PopulateDropdownTreeSubcategories(cbCategoryOnEditExternal, ddtSubCategoryOnEditExternal);
            }
        }
    }

    protected void PopulateComboBoxClient(RadComboBox combo)
    {
        combo.Text = "";
        combo.ClearSelection();

        var ds = DataHelper.GetClients();
        combo.DataSource = ds;
        combo.DataBind();
    }

    public void ConfigureExport(RadGrid grid)
    {
        grid.ExportSettings.HideStructureColumns = true;
        grid.ExportSettings.ExportOnlyData = true;
        grid.ExportSettings.IgnorePaging = true;
        grid.ExportSettings.OpenInNewWindow = true;
        grid.ExportSettings.FileName = grid.ID.Replace("grid", "") + DateTime.Today.ToShortDateString();

        if (grid.ID != "gridAudienceMembersCIMList")
        {
            grid.MasterTableView.Columns.FindByUniqueName("EditCommandColumn").Display = false;
            if (grid.ID == "gridExternalUsers")
            {
                grid.MasterTableView.Columns.FindByUniqueName("ResetPassword").Display = false;
            }
            else if (grid.ID != "gridExternalUsers" && grid.ID != "gridAudience" && grid.ID != "gridClasses")
            {
                grid.MasterTableView.Columns.FindByUniqueName("DeleteColumn").Display = false;
            }
        }
        grid.MasterTableView.ExportToExcel();
    }
    public void ConfigureExport(RadTreeList grid)
    {
        grid.ExportSettings.IgnorePaging = true;
        grid.ExportSettings.ExportMode = TreeListExportMode.RemoveAll;
        grid.ExportSettings.FileName = grid.ID.Replace("grid", "") + DateTime.Today.ToShortDateString();
        grid.Rebind();
        grid.ExportToExcel();
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        var gridID = ((LinkButton)sender).ID.Replace("btnExportExcel", "grid");
        if (gridID == "gridSubCategory")
        {
            var grid = LogicHelper.FindControl<RadTreeList>(((LinkButton)sender).Parent.Controls, gridID);

            if (grid.Items.Count > 0)
            {
                ConfigureExport(grid);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Warning! No record available for your request.');", true);
            }
        }
        else
        {
            var grid = LogicHelper.FindControl<RadGrid>(((LinkButton)sender).Parent.Parent.Controls, gridID);

            if (grid.Items.Count > 0)
            {
                ConfigureExport(grid);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Warning! No record available for your request.');", true);
            }
        }

    }

    #endregion

    #region .PAGE EDITOR

    protected string WelcomePath = "~/PageFiles/Welcome Message/Welcome.html";
    protected string FeaturedPath = string.Empty;
    protected string FeaturedFilesPath = "~/PageFiles/Featured Courses";

    protected string ReadFile(string path)
    {
        if (!File.Exists(path))
        {
            return string.Empty;
        }
        using (var sr = new StreamReader(path))
        {
            return sr.ReadToEnd();
        }
    }

    protected void btnSaveWelcomeMsg_OnClick(object sender, EventArgs e)
    {
        if (tabStripEditor.SelectedIndex == 0)
        {
            using (var externalFile = new StreamWriter(Server.MapPath(WelcomePath), false, Encoding.UTF8))
            {
                externalFile.Write(editorWelcomeMessage.Content);
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                             "alert('Welcome Message succesfully saved!');", true);
            }
        }
        else
        {
            using (var externalFile = new StreamWriter(Server.MapPath(hiddenFilePath.Value), false, Encoding.UTF8))
            {
                externalFile.Write(editorWelcomeMessage.Content);
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                             "alert('Featured Course succesfully saved!');", true);
                lblCourseEditName.Text = hiddenCourseName.Value;
            }
        }
    }

    protected void btnClearWelcomeMsg_OnClick(object sender, EventArgs e)
    {
        editorWelcomeMessage.Content = string.Empty;
    }

    protected void tabStripEditor_TabClick(object sender, RadTabStripEventArgs e)
    {
        if (tabStripEditor.SelectedIndex == 0)
        {
            editorWelcomeMessage.Content = ReadFile(Server.MapPath(WelcomePath));
            panelCourseControls.Visible = false;

            btnSaveWelcomeMsg.Text = "Save Welcome Message";
            btnClearWelcomeMsg.Text = "Clear";
        }
        else
        {
            {
                hiddenFilePath.Value = string.Empty;
                lblCourseEditName.Text = "[Course Name]";
                editorWelcomeMessage.Content = ReadFile(Server.MapPath(hiddenFilePath.Value));
                panelCourseControls.Visible = true;

                btnSaveWelcomeMsg.Text = "Save Course";
                btnClearWelcomeMsg.Text = "Clear & Reset";
            }
        }
    }

    #endregion

    #region .AUDIENCE MAINTENANCE

    #region .Audience

    protected void chkElapsed_CheckedChanged(object sender, EventArgs e)
    {
        IncludeElapsed = chkElapsed.Checked;
        gridAudience.Rebind();
    }

    protected void btnAddAudience_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            var retVal = AddAudience();
            switch (retVal)
            {
                case true:
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "AlertBox",
                                                        "alert('The audience has been added.');", true);
                    ClearAddForm();
                    clearAudienceFilterForm(btnAddAudience, "");
                    tsAudience.SelectedIndex = 0;
                    mpAudience.SelectedIndex = 0;
                    break;
                case false:
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "AlertBox",
                                                        "alert('Addition of audience has failed.');", true);
                    break;
            }
        }
    }

    private void InitDtAudienceFilter()
    {
        DtAudienceFilter = new DataTable();

        var column = new DataColumn
        {
            DataType = Type.GetType("System.Int32"),
            ColumnName = "AudienceMemberID",
            AutoIncrement = true,
            AutoIncrementSeed = 1,
            AutoIncrementStep = 1
        };

        DtAudienceFilter.Columns.Add(column);
        DtAudienceFilter.Columns.Add("AudienceMemberName", typeof(string));
        DtAudienceFilter.Columns.Add("FilterType", typeof(String));
        DtAudienceFilter.Columns.Add("FilterTypeVal", typeof(String));
        DtAudienceFilter.Columns.Add("FilterName", typeof(String));
        DtAudienceFilter.Columns.Add("FilterNameVal", typeof(String));
    }


    private void InitDtAudienceFilterSave()
    {
        DtAudienceFilterSave = new DataTable();

        var column = new DataColumn
        {
            DataType = Type.GetType("System.Int32"),
            ColumnName = "AudienceMemberID",
            AutoIncrement = true,
            AutoIncrementSeed = 1,
            AutoIncrementStep = 1
        };

        DtAudienceFilterSave.Columns.Add(column);
        DtAudienceFilterSave.Columns.Add("AudienceMemberName", typeof(String));
        DtAudienceFilterSave.Columns.Add("AudienceTypeID", typeof(int));
        DtAudienceFilterSave.Columns.Add("Value", typeof(int));
    }

    private bool AddAudience()
    {
        InitDtAudienceFilterSave();
        var retVal = DataHelper.BulkAddAudience(txtAudienceNameAdd.Text,
                                                     Convert.ToDateTime(dpDateStartAdd.SelectedDate),
                                                     Convert.ToDateTime(dpDateEndAdd.SelectedDate),
                                                     Convert.ToInt32(Context.User.Identity.Name), processAudienceDTForSaving(DtAudienceFilter));

        return retVal;
    }

    private DataTable processAudienceDTForSaving(DataTable dt)
    {
        InitDtAudienceFilterSave();

        foreach (DataRow row in dt.Rows)
        {
            string[] filterTypesArr = row["FilterTypeVal"].ToString().Split(',');
            string[] filterTypesName = row["FilterNameVal"].ToString().Split(',');

            for (int i = 0; i <= filterTypesArr.Length - 1; i++)
            {
                var dr = DtAudienceFilterSave.NewRow();
                dr["AudienceMemberName"] = row["AudienceMemberName"].ToString();
                dr["AudienceTypeID"] = Convert.ToInt32(filterTypesArr[i]);
                dr["Value"] = Convert.ToInt32(filterTypesName[i]);
                DtAudienceFilterSave.Rows.Add(dr);
            }
        }

        return DtAudienceFilterSave;
    }

    protected void gridAudience_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            if (Page.IsValid)
            {
                var editedItem = e.Item as GridEditableItem;

                if (editedItem != null)
                {
                    var txtAudienceName = (RadTextBox)editedItem.FindControl("txtAudienceNameEdit");
                    var dpStartDate = (RadDatePicker)editedItem.FindControl("dpDateStartEdit");
                    var dpEndDate = (RadDatePicker)editedItem.FindControl("dpDateEndEdit");
                    var audienceId = (int)editedItem.GetDataKeyValue("AudienceID");
                    var currAudienceName = editedItem.GetDataKeyValue("AudienceName").ToString();

                    var qry1 = DtAudienceFilter.AsEnumerable().Select(a => new { AudienceMemberID = a["AudienceMemberID"].ToString() });
                    var qry2 =
                        DtAudienceFilterEdit.AsEnumerable().Select(b => new { AudienceMemberID = b["AudienceMemberID"].ToString() });

                    var toBeDeleted = qry1.Except(qry2);
                    var toBeAdded = qry2.Except(qry1);

                    DataTable dtDelete = null;
                    DataTable dtAdd = null;

                    dtDelete = (from a in DtAudienceFilter.AsEnumerable()
                                join ab in toBeDeleted on a["AudienceMemberID"].ToString() equals ab.AudienceMemberID
                                select a).Any()
                                   ? (from a in DtAudienceFilter.AsEnumerable()
                                      join ab in toBeDeleted on a["AudienceMemberID"].ToString() equals
                                          ab.AudienceMemberID
                                      select a).CopyToDataTable()
                                   : dtDelete;

                    dtAdd = (from a in DtAudienceFilterEdit.AsEnumerable()
                             join ab in toBeAdded on a["AudienceMemberID"].ToString() equals ab.AudienceMemberID
                             select a).Any()
                                ? (from a in DtAudienceFilterEdit.AsEnumerable()
                                   join ab in toBeAdded on a["AudienceMemberID"].ToString() equals ab.AudienceMemberID
                                   select a).CopyToDataTable()
                                : dtAdd;
                    if (dtAdd != null)
                    {
                        dtAdd = processAudienceDTForSaving(dtAdd);
                    }

                    var retVal = DataHelper.UpdateAudience(audienceId, currAudienceName, txtAudienceName.Text,
                                                           Convert.ToDateTime(dpStartDate.SelectedDate),
                                                           Convert.ToDateTime(dpEndDate.SelectedDate),
                                                           Convert.ToInt32(Context.User.Identity.Name), dtAdd, dtDelete);

                    InitDtAudienceFilter();

                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "AlertBox",
                                                        retVal
                                                            ? "alert('The Audience item has been updated.');"
                                                            : "alert('Audience item failed to update.');", true);
                }
            }
        }
    }

    protected void gridAudience_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        gridAudience.DataSource = DataHelper.GetAudience(IncludeElapsed).OrderByDescending(p => p.DateUpdated).ToList();
    }

    protected void gridAudience_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridEditableItem && e.Item.IsInEditMode)
        {
            var item = (GridEditableItem)e.Item;

            AudienceId = (int)item.GetDataKeyValue("AudienceID");

            var dateStart = (DateTime)item.GetDataKeyValue("DateStart");
            var dateEnd = (DateTime)item.GetDataKeyValue("DateEnd");

            gridAudience.MasterTableView.GetItems(GridItemType.FilteringItem)[0].Visible = false;

            DpStartDateSelectedDate = dateStart;
            DpEndDateSelectedDate = dateEnd;
        }
        else if (e.Item is GridDataItem)
        {
            var item = (GridDataItem)e.Item;

            ((ImageButton)(e.Item as GridDataItem)["EditCommandColumn"].Controls[0]).Attributes["onclick"] =
                string.Format("javascript: setAudienceEditMode();");
        }
    }


    protected void gridAudience_EditCommand(object sender, GridCommandEventArgs e)
    {
        var item = (GridEditableItem)e.Item;

        AudienceId = (int)item.GetDataKeyValue("AudienceID");
        clearAudienceFilterForm(btnAddAudience, "");
        Operation = "new";
    }

    protected void gridAudience_CancelCommand(object sender, GridCommandEventArgs e)
    {
        InitDtAudienceFilter();
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "AlertBox", "clearAudienceFieldsClient();", true);

    }

    #endregion

    #region .Audience Members

    protected void gridAudienceMembersCIMList_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (AudienceId != 0)
        {
            var qry = DataHelper.GetMembersCimList(AudienceId);
            if (qry != null && qry.Count > 0)
            {
                gridAudienceMembersCIMList.DataSource = qry;
            }
            else
            {
                gridAudienceMembersCIMList.DataSource = new int[] { };
            }

        }
    }

    protected void btnViewAudienceMemberCIMList_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;

        var item = btn.NamingContainer as GridDataItem;

        AudienceId = Convert.ToInt32(item.GetDataKeyValue("AudienceID"));

        gridAudienceMembersCIMList.Rebind();
    }



    [System.Web.Services.WebMethod]
    public static string GetFullName(string CIM)
    {
        var name = string.Empty;
        if (CIM != string.Empty)
        {
            name = DataHelper.IsValidCim(Convert.ToInt32(CIM)).Column1;
            if (name == string.Empty)
            {
                name = "Invalid CIM";
            }
        }
        return name;
    }

    protected void cbGenericFilterType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {

        var cbFilterType = (RadComboBox)sender;
        var cbFilterTypeID = ((RadComboBox)sender).ID;
        var num = Regex.Match(cbFilterTypeID, @"\d+").Value;
        var editStr = string.Empty;
        if (cbFilterTypeID.IndexOf("Edit") > 0)
        {
            editStr = "Edit";
        }
        var cbFilterValue = (RadComboBox)((RadComboBox)sender).Parent.FindControl("cbFilterValue" + editStr + num);
        var txtFilterValue = (RadNumericTextBox)((RadComboBox)sender).Parent.FindControl("txtFilterValue" + editStr + num);
        var lblFilterValue = (Label)((RadComboBox)sender).Parent.FindControl("lblFilterValueName" + editStr + num);
        var hidFilterValue = (HiddenField)btnAddFilterGroup.Parent.FindControl("hidlblFilterValue" + num);
        cbFilterValue.Text = string.Empty;

        prepareControl(cbFilterValue, txtFilterValue, cbFilterType.SelectedItem.Text, lblFilterValue, hidFilterValue);


    }

    protected void rgAudienceMembers_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var gridMembers = (RadGrid)sender;

        if (gridMembers.ID == "rgAudienceMembers")
        {
            if (DtAudienceFilter == null)
            {
                InitDtAudienceFilter();
            }
            gridMembers.DataSource = DtAudienceFilter;
        }
        else
        {
            if (Operation == "new")
            {
                var qryList = DataHelper.GetAudienceMembers(AudienceId);

                DtAudienceFilter = LogicHelper.ConvertToDataTable(qryList.ToList());
                DtAudienceFilterEdit = DtAudienceFilter;

                if (qryList != null)
                {
                    gridMembers.DataSource = DtAudienceFilterEdit;
                    Operation = "edit";
                }
            }
            else
            {
                gridMembers.DataSource = DtAudienceFilterEdit;
            }

        }

    }

    protected void rgAudienceMembers_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var audienceMemberID = (int)deletedItem.GetDataKeyValue("AudienceMemberID");

                var rgAudienceMembers = (RadGrid)(sender);
                if (rgAudienceMembers.ID.IndexOf("Edit") > 0)
                {
                    Operation = "edit";
                    var rows = DtAudienceFilterEdit.Select("AudienceMemberID = '" + audienceMemberID + "'");
                    foreach (var row in rows)
                        row.Delete();
                }
                else
                {
                    var rows = DtAudienceFilter.Select("AudienceMemberID = '" + audienceMemberID + "'");
                    foreach (var row in rows)
                        row.Delete();
                }
            }
        }
    }

    protected void btnAddFilterGroup_Click(object sender, EventArgs e)
    {

        var strFilterTypes = string.Empty;
        var strFilterTypesVal = string.Empty;
        var strFilterNames = string.Empty;
        var strFilterNamesVal = string.Empty;
        var btn = (LinkButton)(sender);
        var editStr = string.Empty;
        if (btn.ID.IndexOf("Edit") > 0)
        {
            editStr = "Edit";
        }

        for (int i = 1; i <= Convert.ToInt32(hidRowNum.Value); i++)
        {

            var cbFilterType = (RadComboBox)btn.Parent.FindControl("cbFilterType" + editStr + i);
            var cbFilterValue = (RadComboBox)btn.Parent.FindControl("cbFilterValue" + editStr + i);
            var txtFilterValue = (RadNumericTextBox)btn.Parent.FindControl("txtFilterValue" + editStr + i);
            //var lblFilterValueName = (Label)btn.Parent.FindControl("lblFilterValueName" + editStr + i);
            var hidlblFilterValue = (HiddenField)btnAddFilterGroup.Parent.FindControl("hidlblFilterValue" + i);
            strFilterTypes += cbFilterType.SelectedItem.Text + ",";
            strFilterTypesVal += cbFilterType.SelectedValue + ",";

            if (Convert.ToInt32(cbFilterType.SelectedValue).In(4, 5))
            {
                strFilterNames += hidlblFilterValue.Value + ",";
                strFilterNamesVal += txtFilterValue.Text + ",";
            }
            else
            {
                strFilterNames += cbFilterValue.SelectedItem.Text + ",";
                strFilterNamesVal += cbFilterValue.SelectedValue + ",";
            }
        }

        strFilterTypes = strFilterTypes.Substring(0, strFilterTypes.Length - 1);
        strFilterTypesVal = strFilterTypesVal.Substring(0, strFilterTypesVal.Length - 1);
        strFilterNames = strFilterNames.Substring(0, strFilterNames.Length - 1);
        strFilterNamesVal = strFilterNamesVal.Substring(0, strFilterNamesVal.Length - 1);

        if (btn.ID.IndexOf("Edit") > 0)
        {
            var txtAudienceMemberNametxt = (RadTextBox)btn.Parent.FindControl("txtAudienceMemberNameEdit");
            var dr = DtAudienceFilterEdit.NewRow();
            dr["AudienceMemberName"] = txtAudienceMemberNametxt.Text;
            dr["FilterType"] = strFilterTypes;
            dr["FilterTypeVal"] = strFilterTypesVal;
            dr["FilterName"] = strFilterNames;
            dr["FilterNameVal"] = strFilterNamesVal;
            DtAudienceFilterEdit.Rows.Add(dr);
            var gridMembersEdit = (RadGrid)((LinkButton)sender).Parent.FindControl("rgAudienceMembersEdit");
            gridMembersEdit.Rebind();
        }
        else
        {
            var dr = DtAudienceFilter.NewRow();
            dr = DtAudienceFilter.NewRow();
            dr["AudienceMemberName"] = txtAudienceMemberName.Text;
            dr["FilterType"] = strFilterTypes;
            dr["FilterTypeVal"] = strFilterTypesVal;
            dr["FilterName"] = strFilterNames;
            dr["FilterNameVal"] = strFilterNamesVal;
            DtAudienceFilter.Rows.Add(dr);
            rgAudienceMembers.Rebind();
        }

        clearAudienceFilterForm(btn, editStr);


    }
    private void clearAudienceFilterForm(LinkButton btn, string editStr)
    {
        var varCbFilterType1 = (RadComboBox)btn.Parent.FindControl("cbFilterType" + editStr + "1");
        var varCbFilterType2 = (RadComboBox)btn.Parent.FindControl("cbFilterType" + editStr + "2");
        var varCbFilterType3 = (RadComboBox)btn.Parent.FindControl("cbFilterType" + editStr + "3");

        var varlblFilterValueName1 = (Label)btn.Parent.FindControl("lblFilterValueName" + editStr + "1");
        var varlblFilterValueName2 = (Label)btn.Parent.FindControl("lblFilterValueName" + editStr + "2");
        var varlblFilterValueName3 = (Label)btn.Parent.FindControl("lblFilterValueName" + editStr + "3");

        var varCbFilterValue1 = (RadComboBox)btn.Parent.FindControl("cbFilterValue" + editStr + "1");
        var varCbFilterValue2 = (RadComboBox)btn.Parent.FindControl("cbFilterValue" + editStr + "2");
        var varCbFilterValue3 = (RadComboBox)btn.Parent.FindControl("cbFilterValue" + editStr + "3");

        var vartxtFilterValue1 = (RadNumericTextBox)btn.Parent.FindControl("txtFilterValue" + editStr + "1");
        var vartxtFilterValue2 = (RadNumericTextBox)btn.Parent.FindControl("txtFilterValue" + editStr + "2");
        var vartxtFilterValue3 = (RadNumericTextBox)btn.Parent.FindControl("txtFilterValue" + editStr + "3");


        var varTxtAudienceMemberName = (RadTextBox)btn.Parent.FindControl("txtAudienceMemberName" + editStr);

        var varRow2 = (HtmlTableRow)btn.Parent.FindControl("row" + editStr + "2");
        var varRow3 = (HtmlTableRow)btn.Parent.FindControl("row" + editStr + "3");

        //hidlblFilterValue1.Value = string.Empty;
        //hidlblFilterValue2.Value = string.Empty;
        //hidlblFilterValue3.Value = string.Empty;
        varCbFilterType1.SelectedIndex = 0;
        varCbFilterType2.SelectedIndex = 0;
        varCbFilterType3.SelectedIndex = 0;
        //varlblFilterValueName1.Text = string.Empty;
        //varlblFilterValueName2.Text = string.Empty;
        //varlblFilterValueName3.Text = string.Empty;

        prepareControl(varCbFilterValue1, vartxtFilterValue1, varCbFilterType1.SelectedItem.Text ?? string.Empty, varlblFilterValueName1, hidlblFilterValue1);
        prepareControl(varCbFilterValue2, vartxtFilterValue2, varCbFilterType2.SelectedItem.Text ?? string.Empty, varlblFilterValueName2, hidlblFilterValue2);
        prepareControl(varCbFilterValue3, vartxtFilterValue3, varCbFilterType3.SelectedItem.Text ?? string.Empty, varlblFilterValueName3, hidlblFilterValue3);

        cbFilterValue1.SelectedIndex = -1;
        cbFilterValue2.SelectedIndex = -1;
        cbFilterValue3.SelectedIndex = -1;

        cbFilterValue1.Text = string.Empty;
        cbFilterValue2.Text = string.Empty;
        cbFilterValue3.Text = string.Empty;

        varTxtAudienceMemberName.Text = string.Empty;
        varRow2.Attributes.Clear();
        varRow3.Attributes.Clear();
        varRow2.Attributes["style"] = "display:none";
        varRow3.Attributes["style"] = "display:none";
        hidRowNum.Value = "1";
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "AlertBox", "clearAudienceFieldsClient();", true);
    }


    protected void btnExportAudience_Click(object sender, EventArgs e)
    {
        gridAudience.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        gridAudience.ExportSettings.IgnorePaging = true;
        gridAudience.ExportSettings.ExportOnlyData = true;
        gridAudience.ExportSettings.OpenInNewWindow = true;
        gridAudience.ExportSettings.FileName = "Audience_" + DateTime.Now;
        gridAudience.MasterTableView.ExportToExcel();
    }

    protected void btnExportAudienceMembers_Click(object sender, EventArgs e)
    {
        gridAudienceMembersCIMList.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;
        gridAudienceMembersCIMList.ExportSettings.IgnorePaging = true;
        gridAudienceMembersCIMList.ExportSettings.ExportOnlyData = true;
        gridAudienceMembersCIMList.ExportSettings.OpenInNewWindow = true;
        gridAudienceMembersCIMList.ExportSettings.FileName = "AudienceMembers_" + DateTime.Now;
        gridAudienceMembersCIMList.MasterTableView.ExportToExcel();
    }

    private List<DataHelperModel.FilterCol> filterListSource(string filterTypes, string filterNames)
    {
        string[] filterTypesArr = filterTypes.Split(',');
        string[] filterNamesArr = filterNames.Split(',');
        var ListSource = new List<DataHelperModel.FilterCol>();

        for (int i = 0; i < filterTypesArr.Length; i++)
        {
            var Source = new DataHelperModel.FilterCol();
            Source.FilterName = filterNamesArr[i];
            Source.FilterType = filterTypesArr[i];
            ListSource.Add(Source);
        }
        return ListSource;
    }

    protected void lbFiltersAdd_ItemDataBound(object sender, RadListBoxItemEventArgs e)
    {

        e.Item.Text = ((DataHelperModel.FilterCol)e.Item.DataItem).FilterType;

        e.Item.Value = ((DataHelperModel.FilterCol)e.Item.DataItem).FilterName;

        e.Item.Attributes.Add("FilterType", "<b>" + ((DataHelperModel.FilterCol)e.Item.DataItem).FilterType + "</b>");
        e.Item.Attributes.Add("FilterName", "<b>" + ((DataHelperModel.FilterCol)e.Item.DataItem).FilterName + "</b>");
        e.Item.DataBind();
    }

    protected void rgAudienceMembers_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {

            var dataItem = (GridDataItem)e.Item;

            var FilterType = dataItem.GetDataKeyValue("FilterType").ToString();
            var FilterName = dataItem.GetDataKeyValue("FilterName").ToString();

            var lbFilters = new RadListBox();
            if (((RadGrid)(sender)).ID == "rgAudienceMembers")
            {
                lbFilters = (RadListBox)dataItem["Filters"].FindControl("lbFiltersAdd");

            }
            else
            {
                lbFilters = (RadListBox)dataItem["Filters"].FindControl("lbFiltersEdit");
            }
            lbFilters.DataSource = filterListSource(FilterType, FilterName);
            lbFilters.DataBind();

        }
    }

    private void prepareControl(RadComboBox combo, RadNumericTextBox numtxt, string filterType, Label lblValue, HiddenField hidValue)
    {
        switch (filterType)
        {
            case "":
            case "Class":
                combo.Visible = true;
                numtxt.Visible = false;
                combo.DataSourceID = "dsClass";
                combo.DataTextField = "ClassID";
                combo.DataValueField = "ClassID";
                break;
            case "Client":
                combo.Visible = true;
                numtxt.Visible = false;
                combo.DataSourceID = "dsHarmonyClient";
                combo.DataTextField = "Client";
                combo.DataValueField = "ClientID";
                break;
            case "Department":
                combo.Visible = true;
                numtxt.Visible = false;
                combo.DataSourceID = "dsDeparment";
                combo.DataTextField = "Account";
                combo.DataValueField = "AccountID";
                break;

            case "Individual Users (Cim)":
            case "Reporting Manager":
                combo.Visible = false;
                numtxt.Visible = true;
                numtxt.Text = string.Empty;
                break;

            case "Role":
                combo.Visible = true;
                numtxt.Visible = false;
                combo.DataSourceID = "dsRole";
                combo.DataTextField = "Role";
                combo.DataValueField = "RoleID";
                break;
            case "Role Group":
                combo.Visible = true;
                numtxt.Visible = false;
                combo.DataSourceID = "dsRoleGroup";
                combo.DataTextField = "RoleGroup";
                combo.DataValueField = "RoleGroupID";
                break;
            case "Site":
                combo.Visible = true;
                numtxt.Visible = false;
                combo.DataSourceID = "dsSite";
                combo.DataTextField = "CompanySite";
                combo.DataValueField = "CompanySiteID";
                break;
            case "Career Path":
                combo.Visible = true;
                numtxt.Visible = false;
                combo.DataSourceID = "dsCareerPath";
                combo.DataTextField = "Description";
                combo.DataValueField = "ID";
                break;
        }
        if (filterType != string.Empty)
        {
            combo.SelectedIndex = -1;
            combo.Text = string.Empty;
            combo.DataBind();
            lblValue.Text = string.Empty;
            hidValue.Value = string.Empty;
        }
    }

    protected void gridMembersEdit_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var gridMembersEdit = (RadGrid)sender;

        if (Operation == "new")
        {
            var qryList = DataHelper.GetAudienceMembers(AudienceId);

            DtAudienceFilter = LogicHelper.ConvertToDataTable(qryList.ToList());
            DtAudienceFilterEdit = DtAudienceFilter;

            if (qryList != null)
            {
                gridMembersEdit.DataSource = DtAudienceFilterEdit;
                Operation = "edit";
            }
        }
        else
        {
            gridMembersEdit.DataSource = DtAudienceFilterEdit;
        }
    }

    protected void gridMembersEdit_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            Operation = "edit";

            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var audienceMemberId = (int)deletedItem.GetDataKeyValue("AudienceMemberID");

                var rows = DtAudienceFilterEdit.Select("AudienceMemberID = '" + audienceMemberId + "'");
                try
                {
                    foreach (var row in rows)
                        row.Delete();
                }
                catch
                {
                }
            }
        }
    }

    #endregion

    private void ClearAddForm()
    {
        gridAudience.Rebind();
        DtAudienceFilterSave.Clear();
        DtAudienceFilter.Clear();
        //cbAudienceTypeAdd.SelectedIndex = 0;
        //cbMemberValue.SelectedIndex = -1;
        rgAudienceMembers.Rebind();
        txtAudienceNameAdd.Text = string.Empty;
        dpDateEndAdd.SelectedDate = DateTime.Now.AddMonths(1);
        dpDateStartAdd.SelectedDate = DateTime.Now;
    }

    #endregion

    #region .CLASS NAME MAINTENANCE

    public void LoadClasses(int? clientId, int? trainTypeId)
    {
        var list = DataHelper.GetClassNames(clientId, trainTypeId);
        gridClasses.DataSource = list;
    }

    protected void gridClasses_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LoadClasses(ClientId, TrainTypeId);
    }

    protected void cbClient_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        ClientId = Convert.ToInt32(cbClient.SelectedValue);

        TrainTypeId = 0;
        cbTrainingType.Text = "";
        cbTrainingType.ClearSelection();

        ComboBoxHelper.Populate_TrainingType(cbTrainingType, cbClient.Text);

        gridClasses.Rebind();
    }

    //public void PopulateTrainingType(RadComboBox combo, string client)
    //{
    //    combo.DataSource = client == "OD" ? DataHelper.GetTrainingTypes_OD() : DataHelper.GetTrainingTypes();
    //    combo.DataBind();
    //}

    protected void cbTrainingType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        TrainTypeId = Convert.ToInt32(cbTrainingType.SelectedValue);

        gridClasses.Rebind();
    }

    protected void btnAddClass_Click(object sender, EventArgs e)
    {
        _master = Master;

        if (Page.IsValid)
        {
            var exist = DataHelper.ChkIfClassExist(txtClassName_Add.Text, Convert.ToInt32(cbClient_Add.SelectedValue),
                                                   Convert.ToInt32(cbTrainingType_Add.SelectedValue));

            if (exist)
            {
                _master.AlertMessage(false, "Duplicate entry denied. Record already exists.");
            }
            else
            {
                var retVal = DataHelper.InsertClass(txtClassName_Add.Text, Convert.ToInt32(cbClient_Add.SelectedValue),
                                                    Convert.ToInt32(cbTrainingType_Add.SelectedValue),
                                                    true);

                if (retVal)
                {
                    _master.AlertMessage(true, "Successfully saved.");
                    ClearAddClassForm();
                }
                else
                {
                    _master.AlertMessage(false, "Request error! Please review your input fields.");
                }
            }
        }
    }

    public void ClearAddClassForm()
    {
        cbClient_Add.Text = "";
        cbClient_Add.ClearSelection();

        cbTrainingType_Add.Text = "";
        cbTrainingType_Add.ClearSelection();

        txtClassName_Add.Text = "";
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ClearAddClassForm();
    }

    protected void gridClasses_ItemCommand(object sender, GridCommandEventArgs e)
    {
        _master = Master;

        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                if (Page.IsValid)
                {
                    var id = editedItem.GetDataKeyValue("ID").ToString();

                    var cbClient_edit = (RadComboBox)editedItem.FindControl("cbClient_edit");
                    var cbTrainingType_edit = (RadComboBox)editedItem.FindControl("cbTrainingType_edit");
                    var cbClassName_edit = (RadComboBox)editedItem.FindControl("cbClassName_edit");
                    var chkActive = (CheckBox)editedItem.FindControl("chkActive");

                    var exist = DataHelper.ChkIfClassExist(cbClassName_edit.Text, Convert.ToInt32(cbClient_edit.SelectedValue),
                                                           Convert.ToInt32(cbTrainingType_edit.SelectedValue),
                                                           chkActive.Checked);

                    if (exist)
                    {
                        _master.AlertMessage(false, "Duplicate entry denied. Record already exists.");
                    }
                    else
                    {
                        var retVal = DataHelper.UpdateClass(Convert.ToInt32(id), cbClassName_edit.Text,
                                                            Convert.ToInt32(cbClient_edit.SelectedValue),
                                                            Convert.ToInt32(cbTrainingType_edit.SelectedValue),
                                                            chkActive.Checked);

                        if (retVal)
                        {
                            _master.AlertMessage(true, "Successfully saved.");
                            ClearAddClassForm();
                        }
                        else
                        {
                            _master.AlertMessage(false, "Request error! Please review your input fields.");
                        }
                    }
                }
            }
        }
    }

    protected void gridClasses_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var gridItem = e.Item as GridDataItem;

            var client = gridItem.GetDataKeyValue("Client").ToString();
            var trainingTypeId = gridItem.GetDataKeyValue("TrainingTypeID").ToString();

            if (client == "OD")
            {
                switch (trainingTypeId)
                {
                    case "1":
                        gridItem["TrainingType"].Text = "Agent Training";
                        break;
                    case "2":
                        gridItem["TrainingType"].Text = "Supervisor Training";
                        break;
                    case "3":
                        gridItem["TrainingType"].Text = "Manager Training";
                        break;
                    case "4":
                        gridItem["TrainingType"].Text = "Off Phone Employees (All)";
                        break;
                }
            }
        }
        if (e.Item.IsInEditMode)
        {
            var item = (GridEditableItem)e.Item;

            if (!(e.Item is IGridInsertItem))
            {
                var className = item.GetDataKeyValue("ClassName").ToString();
                var clientId = item.GetDataKeyValue("ClientID").ToString();
                var client = item.GetDataKeyValue("Client").ToString();
                var trainTypeId = item.GetDataKeyValue("TrainingTypeID").ToString();
                var active = item.GetDataKeyValue("Active").ToString();

                var cbClient_edit = (RadComboBox)item.FindControl("cbClient_edit");
                var cbTrainingType_edit = (RadComboBox)item.FindControl("cbTrainingType_edit");
                var cbClassName_edit = (RadComboBox)item.FindControl("cbClassName_edit");
                var chkActive = (CheckBox)item.FindControl("chkActive");

                cbClient_edit.SelectedValue = clientId;

                ComboBoxHelper.Populate_TrainingType(cbTrainingType_edit, client);
                cbTrainingType_edit.SelectedValue = trainTypeId;

                chkActive.Checked = Convert.ToBoolean(active);

                GetClassNamesOnEdit(Convert.ToInt32(clientId),
                                                       Convert.ToInt32(trainTypeId), cbClassName_edit);

                cbClassName_edit.Text = className;
            }
        }
    }

    public void GetClassNamesOnEdit(int clientId, int trainTypeid, RadComboBox cbClassNameEdit)
    {
        var list = DataHelper.GetClassNames_Active(clientId, trainTypeid);
        cbClassNameEdit.DataSource = list;
        cbClassNameEdit.DataBind();
    }

    protected void cbClientEdit_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        var combo = sender as RadComboBox;

        if (combo != null)
        {
            var editedItem = combo.NamingContainer as GridEditableItem;
            if (editedItem != null)
            {
                var cbTrainingType_edit = editedItem.FindControl("cbTrainingType_edit") as RadComboBox;
                var cbClassName_edit = editedItem.FindControl("cbClassName_edit") as RadComboBox;

                cbTrainingType_edit.Text = "";
                cbTrainingType_edit.ClearSelection();

                ComboBoxHelper.Populate_TrainingType(cbTrainingType_edit, combo.Text);

                cbClassName_edit.Text = "";
                cbClassName_edit.ClearSelection();

                if (!string.IsNullOrEmpty(cbTrainingType_edit.SelectedValue))
                {
                    GetClassNamesOnEdit(Convert.ToInt32(combo.SelectedValue),
                                                        Convert.ToInt32(cbTrainingType_edit.SelectedValue), cbClassName_edit);
                }
            }
        }
    }

    protected void cbTrainingTypeEdit_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        var combo = sender as RadComboBox;

        if (combo != null)
        {
            var editedItem = combo.NamingContainer as GridEditableItem;
            if (editedItem != null)
            {
                var cbClient_edit = editedItem.FindControl("cbClient_edit") as RadComboBox;
                var cbClassName_edit = editedItem.FindControl("cbClassName_edit") as RadComboBox;

                cbClassName_edit.Text = "";
                cbClassName_edit.ClearSelection();

                if (!string.IsNullOrEmpty(cbClient_edit.SelectedValue))
                {
                    GetClassNamesOnEdit(Convert.ToInt32(cbClient_edit.SelectedValue),
                                                        Convert.ToInt32(combo.SelectedValue), cbClassName_edit);
                }
            }
        }
    }

    protected void cbClient_Add_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        cbTrainingType_Add.Text = "";
        cbTrainingType_Add.ClearSelection();

        ComboBoxHelper.Populate_TrainingType(cbTrainingType_Add, cbClient_Add.Text);

        txtClassName_Add.Text = "";
    }

    #endregion

    #region .ASSIGN COURSE

    protected void ddtSubcategoryAssign_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
    {
        if (cbAssignmentType.SelectedIndex != -1 && cbAssignTo.SelectedIndex != -1)
        {
            setAssignToSource();
        }

    }

    protected void ddtSubcategoryAssign_EntryRemoved(object sender, DropDownTreeEntryEventArgs e)
    {
        btnAssign.Visible = false;

    }

    protected void setAssignToSource()
    {

        if (cbAssignmentType.SelectedIndex != 2)
        {
            lbUnassignedCourses.DataSource = DataHelper.GetUnassignedCourses(Convert.ToInt32(cbAssignmentType.SelectedValue),
                Convert.ToInt32(cbAssignTo.SelectedValue),
                cbCategoryAssign.SelectedValue == string.Empty ? 0 : Convert.ToInt32(cbCategoryAssign.SelectedValue),
                ddtSubcategoryAssign.SelectedValue == string.Empty ? 0 : Convert.ToInt32(ddtSubcategoryAssign.SelectedValue));
        }
        else
        {
            lbUnassignedCourses.DataSource = DataHelper.GetUnassignedCourses(Convert.ToInt32(cbAssignmentType.SelectedValue),
                Convert.ToInt32(txtAssignTo.Text),
                cbCategoryAssign.SelectedValue == string.Empty ? 0 : Convert.ToInt32(cbCategoryAssign.SelectedValue),
                ddtSubcategoryAssign.SelectedValue == string.Empty ? 0 : Convert.ToInt32(ddtSubcategoryAssign.SelectedValue));
        }
        btnAssign.Visible = true;
        lbUnassignedCourses.Sort = RadListBoxSort.Ascending;
        lbUnassignedCourses.DataBind();
        lbUnassignedCourses.SortItems();
        lbAssignedCourses.Items.Clear();

        if (lbUnassignedCourses.Items.Count > 0)
        {
            btnAssign.Visible = true;
        }
        else
        {
            btnAssign.Visible = false;
        }
    }

    protected void setAudienceListSource()
    {
        cbAssignTo.Visible = true;
        rfvAssignTo.Enabled = true;
        rfvAssignToText.Enabled = false;
        txtAssignTo.Visible = false;
        txtAssignTo.Text = string.Empty;
        cbAssignTo.DataSource = DataHelper.GetAudience(false);
        cbAssignTo.DataTextField = "AudienceName";
        cbAssignTo.DataValueField = "AudienceID";
        cbAssignTo.DataBind();
        lblEmployeeName.Text = string.Empty;
    }

    protected void setClassSource()
    {
        cbAssignTo.Visible = true;
        rfvAssignTo.Enabled = true;
        rfvAssignToText.Enabled = false;
        txtAssignTo.Visible = false;
        txtAssignTo.Text = string.Empty;
        cbAssignTo.DataSource = dsClass;
        cbAssignTo.DataTextField = "ClassID";
        cbAssignTo.DataValueField = "ClassID";
        cbAssignTo.DataBind();
        lblEmployeeName.Text = string.Empty;
    }

    protected void setTeamAssign()
    {
        cbAssignTo.Visible = false;
        rfvAssignTo.Enabled = false;
        rfvAssignToText.Enabled = true;
        txtAssignTo.Visible = true;
        txtAssignTo.Text = string.Empty;
        lblEmployeeName.Text = string.Empty;

    }

    protected void validateCIM()
    {
        if (txtAssignTo.Text != string.Empty)
        {
            var name = DataHelper.IsValidCim(Convert.ToInt32(txtAssignTo.Text)).Column1;

            if (name != string.Empty)
            {
                lblEmployeeName.Text = name;
                setAssignToSource();
                gridAssignedCourses.Rebind();

            }
            else
            {
                btnAssign.Visible = false;
                gridAssignedCourses.Rebind();
                txtAssignTo.Focus();
                txtAssignTo.Text = string.Empty;
                lblEmployeeName.Text = string.Empty;
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                            "alert('Invalid CIM Number.');", true);
            }
        }
        else
        {
            btnAssign.Visible = false;
            gridAssignedCourses.Rebind();
            lblEmployeeName.Text = string.Empty;
        }

    }

    protected void cbAssignmentType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        switch (Convert.ToInt32(cbAssignmentType.SelectedValue))
        {
            case 1:
                setAudienceListSource();
                break;
            case 2:
                setClassSource();
                break;
            case 3:
                setTeamAssign();
                break;
        }
        cbAssignTo.Text = string.Empty;
        btnAssign.Visible = false;
        gridAssignedCourses.Rebind();

    }


    protected void lbUnassignedCourses_ItemDataBound(object sender, RadListBoxItemEventArgs e)
    {
        e.Item.Text = ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).Title;
        e.Item.Value = ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).CourseID.ToString();
        e.Item.Attributes.Add("Title", ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).Title);
        e.Item.Attributes.Add("Prerequisite", ((pr_TranscomUniversity_lkp_UnassignedCourseResult)e.Item.DataItem).Prerequisite);
        e.Item.DataBind();
    }

    protected void lbUnassignedCourses_Transferred(object sender, RadListBoxTransferredEventArgs e)
    {
        foreach (RadListBoxItem item in e.Items)
        {
            item.DataBind();
            if (item.Attributes["Prerequisite"] == "Yes")
            {
                if (e.DestinationListBox.ID == "lbAssignedCourses")
                {
                    CtrPrerequisite++;
                }
                else
                {
                    CtrPrerequisite--;
                }
            }
        }

        lbUnassignedCourses.SelectedIndex = -1;
        ctrPrereq.Value = CtrPrerequisite.ToString();
    }

    protected void lbUnassignedCourses_Inserted(object sender, RadListBoxEventArgs e)
    {
        lbUnassignedCourses.SortItems();
    }

    protected void lbUnassignedCourses_Transferring(object sender, RadListBoxTransferringEventArgs e)
    {

    }

    protected void btnAssign_Click(object sender, EventArgs e)
    {

        if (Page.IsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                         "OpenAssignForm();", true);
        }
    }

    protected void btnAssignCourseAdd_Click(object sender, EventArgs e)
    {

        if (Page.IsValid)
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                         "chkprerequisite();", true);
        }
    }

    protected void gridAssignedCourses_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (cbAssignmentType.SelectedIndex == 2 && txtAssignTo.Text != string.Empty)
        {
            gridAssignedCourses.DataSource = DataHelper.GetAssignedCourses(Convert.ToInt32(cbAssignmentType.SelectedValue), Convert.ToInt32(txtAssignTo.Text));
        }
        else
        {
            if (cbAssignTo.SelectedIndex != -1)
            {
                gridAssignedCourses.DataSource = DataHelper.GetAssignedCourses(Convert.ToInt32(cbAssignmentType.SelectedValue), Convert.ToInt32(cbAssignTo.SelectedValue));
            }
            else
            {
                gridAssignedCourses.DataSource = new int[] { };
            }
        }

    }

    protected void gridAssignedCourses_ItemCommand(object sender, GridCommandEventArgs e)
    {



        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var courseAssignmentID = (int)deletedItem.GetDataKeyValue("CourseAssignmentID");

                var retVal = DataHelper.DeleteCourseAssignment(courseAssignmentID, Convert.ToInt32(_employeeId));

                if (retVal)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                        "radalert('Assignment Removed.', 250, 150);", true);
                    setAssignToSource();
                    gridAssignedCourses.Rebind();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                   "radalert('Failed to remove assignement.', 250, 150);", true);
                }

            }
        }
        else if (e.CommandName == RadGrid.UpdateCommandName)
        {
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                var courseAssignmentID = (int)editedItem.GetDataKeyValue("CourseAssignmentID");

                var dpDueDateEdit = (RadDatePicker)editedItem.FindControl("dpDueDateEdit");

                var retVal = DataHelper.UpdateCourseAssignment(Convert.ToDateTime(dpDueDateEdit.SelectedDate), Convert.ToInt32(courseAssignmentID), int.Parse(_employeeId));

                switch (retVal)
                {
                    case true:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Assignment record has been updated.');", true);
                        RefreshComboBoxesOnCategorySubcategory();
                        break;
                    case false:
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Assignment update has failed.');", true);
                        break;
                }
            }
        }
    }

    protected void btnAssignCourseCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "CloseAssignForm(false,'')", true);
    }

    private static string ConvertToDelimited(DataTable dt, string delimeter, int colNum)
    {
        var retStr = "";

        foreach (DataRow row in dt.Rows)
        {
            retStr = row[colNum].ToString() + delimeter;
        }

        return retStr.Substring(0, retStr.Length - 1);

    }



    protected void AssignCourseAdd()
    {
        var msg = string.Empty;
        DtToAssign = new DataTable();
        DtToAssign.Columns.Add("CourseID", typeof(int));
        DtToAssign.Columns.Add("DueDate", typeof(DateTime));

        foreach (RadListBoxItem item in lbAssignedCourses.Items)
        {
            var dr = DtToAssign.NewRow();
            RadDatePicker dpCourseAssigned = (RadDatePicker)item.FindControl("dpCourseAssigned");
            dr["CourseID"] = item.Value;
            dr["DueDate"] = dpCourseAssigned.SelectedDate;
            DtToAssign.Rows.Add(dr);
        }
        var retVal = true;


        //var coursesWithPrereq = 

        //if (!coursesWithPrereq.Any())
        //{

        if (cbAssignmentType.SelectedIndex != 2)
        {
            retVal = DataHelper.BulkAddAssignCourse(Convert.ToInt32(cbAssignmentType.SelectedValue), Convert.ToInt32(cbAssignTo.SelectedValue), Convert.ToInt32(_employeeId), DtToAssign);
        }
        else
        {
            retVal = DataHelper.BulkAddAssignCourse(Convert.ToInt32(cbAssignmentType.SelectedValue), Convert.ToInt32(txtAssignTo.Text), Convert.ToInt32(_employeeId), DtToAssign);
        }



        if (retVal)
        {
            msg = "Courses has been successfully assigned.";
        }
        else
        {
            msg = "Assignment of courses failed.";
        }

        setAssignToSource();
        gridAssignedCourses.Rebind();

        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "CloseAssignForm(true,'" + msg + "')", true);
    }




    protected void cbCategoryAssignSelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbCategoryAssign, ddtSubcategoryAssign);
        if (cbAssignmentType.SelectedIndex != -1 && cbAssignTo.SelectedIndex != -1)
        {
            setAssignToSource();
        }
    }

    protected void cbAssignTo_SelectedIndexChanged(object sender, EventArgs e)
    {
        gridAssignedCourses.Rebind();
        setAssignToSource();
    }

    #endregion

    #region .PREREQUISITE

    private void InitCoursePrereqDT()
    {
        DtCoursePrereq = new DataTable();

        var column = new DataColumn
        {
            DataType = Type.GetType("System.Int32"),
            ColumnName = "PrerequisiteID",
            AutoIncrement = true,
            AutoIncrementSeed = 1,
            AutoIncrementStep = 1
        };

        DtCoursePrereq.Columns.Add(column);
        DtCoursePrereq.Columns.Add("CourseID", typeof(int));
        DtCoursePrereq.Columns.Add("Title", typeof(String));
    }

    protected void gridPrerequisites_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (OperationPrereq == "new")
        {

            gridPrerequisites.DataSource = DtCoursePrereq;

        }
        else
        {
            gridPrerequisites.DataSource = DtCoursePrereqEdit;

        }
    }
    protected void gridPrerequisites_ItemCommand(object sender, GridCommandEventArgs e)
    {

        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;

            if (deletedItem != null)
            {
                var CourseID = Convert.ToInt32(deletedItem.GetDataKeyValue("CourseID"));
                var rows = DtCoursePrereq.Select("CourseID= '" + CourseID + "'");
                foreach (var row in rows)
                    row.Delete();

            }
            SetAvailableCourses(DtCoursePrereq, 0);
        }
    }

    protected void lbAvailableCourses_Transferred(object sender, RadListBoxTransferredEventArgs e)
    {
        foreach (RadListBoxItem item in e.Items)
        {
            item.DataBind();

        }
        lbAssignedCourses.SelectedIndex = -1;
    }

    protected void lbAvailableCourses_Inserted(object sender, RadListBoxEventArgs e)
    {
        lbAvailableCourses.SortItems();
    }

    protected void btnPrerequistAdd_Click(object sender, EventArgs e)
    {
        if (OperationPrereq == "new")
        {
            foreach (RadListBoxItem item in lbSelectedCourses.Items)
            {
                DataRow dr = DtCoursePrereq.NewRow();
                dr["CourseID"] = item.Value;
                dr["Title"] = item.Text;
                DtCoursePrereq.Rows.Add(dr);
            }
            gridPrerequisites.Rebind();
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                  "ClosePrerequisiteForm('RebindPrereq')", true);
        }
        else
        {
            foreach (RadListBoxItem item in lbSelectedCourses.Items)
            {
                DataRow dr = DtCoursePrereqEdit.NewRow();
                dr["CourseID"] = item.Value;
                dr["Title"] = item.Text;
                DtCoursePrereqEdit.Rows.Add(dr);
            }

            var gridPrerequisitesEdit = LogicHelper.FindControl<RadGrid>(gridCourse.Controls, "gridPrerequisitesEdit");
            gridPrerequisitesEdit.Rebind();
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                  "ClosePrerequisiteForm('RebindPrereqEdit')", true);
        }
    }

    private void SetAvailableCourses(DataTable dtCourse, int operation)
    {
        var lstAvailableCourses = new List<pr_TranscomUniversity_lkp_AvailableCourseResult>();

        if (OperationPrereq != "edit")
        {
            if (operation == 0)
            {
                lstAvailableCourses = DataHelper.GetAvailableCourses(0, cbCategoryAddPrereq.SelectedValue == string.Empty ? 0 : Convert.ToInt32(cbCategoryAddPrereq.SelectedValue), ddtSubcategoryAddPrereq.SelectedValue == string.Empty ? 0 : Convert.ToInt32(ddtSubcategoryAddPrereq.SelectedValue));
            }
            else
            {
                lstAvailableCourses = DataHelper.GetAvailableCourses(0, cbCategoryAddPrereq.SelectedValue == string.Empty ? 0 : Convert.ToInt32(cbCategoryAddPrereq.SelectedValue), ddtSubcategoryAddPrereq.SelectedValue == string.Empty ? 0 : Convert.ToInt32(ddtSubcategoryAddPrereq.SelectedValue));
            }
        }
        else
        {
            var ddtSubcategoryEditPrereq = LogicHelper.FindControl<RadDropDownTree>(gridCourse.Controls, "ddtSubcategoryEditPrereq");
            var cbCategoryEditPrereq = LogicHelper.FindControl<RadComboBox>(gridCourse.Controls, "cbCategoryEditPrereq");

            if (ddtSubcategoryEditPrereq != null && cbCategoryEditPrereq != null)
            {
                if (operation == 0)
                {
                    lstAvailableCourses = DataHelper.GetAvailableCourses(0, cbCategoryEditPrereq.SelectedValue == string.Empty ? 0 : Convert.ToInt32(cbCategoryEditPrereq.SelectedValue), ddtSubcategoryEditPrereq.SelectedValue == string.Empty ? 0 : Convert.ToInt32(ddtSubcategoryEditPrereq.SelectedValue));
                }
                else
                {
                    lstAvailableCourses = DataHelper.GetAvailableCourses(0, cbCategoryEditPrereq.SelectedValue == string.Empty ? 0 : Convert.ToInt32(cbCategoryEditPrereq.SelectedValue), ddtSubcategoryEditPrereq.SelectedValue == string.Empty ? 0 : Convert.ToInt32(ddtSubcategoryEditPrereq.SelectedValue));
                }
            }
        }

        if (dtCourse.AsEnumerable().Any())
        {
            foreach (DataRow row in dtCourse.Rows)
            {
                lstAvailableCourses.RemoveAll(x => x.CourseID == Convert.ToInt64(row["CourseID"]));
            }
        }

        lbAvailableCourses.DataSource = lstAvailableCourses;
        lbAvailableCourses.DataValueField = "CourseID";
        lbAvailableCourses.DataTextField = "Title";
        lbAvailableCourses.Sort = RadListBoxSort.Ascending;
        lbAvailableCourses.DataBind();
        lbAvailableCourses.SortItems();
        lbSelectedCourses.Items.Clear();
    }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument == "RebindPrereq")
        {
            SetAvailableCourses(DtCoursePrereq, 0);
        }
        else if (e.Argument == "RebindPrereqEdit")
        {
            SetAvailableCourses(DtCoursePrereqEdit, 1);
        }
        else if (e.Argument == "ValidateCIM")
        {
            validateCIM();
        }
        else if (e.Argument == "AddAssignCourse")
        {
            AssignCourseAdd();
        }

    }

    protected void btnPrerequisiteCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "ClosePrerequisiteForm('RebindPrereq')", true);
    }

    protected void btnAddCoursePreReq_Click(object sender, EventArgs e)
    {

        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                     "OpenPrerequisiteForm();", true);
    }

    protected void btnAddCoursePreReqEdit_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                 "OpenPrerequisiteForm();", true);

    }

    protected void gridPrerequisitesEdit_ItemCommand(object sender, GridCommandEventArgs e)
    {

        if (e.CommandName == RadGrid.DeleteCommandName)
        {
            var deletedItem = e.Item as GridEditableItem;
            if (deletedItem != null)
            {
                var CourseID = Convert.ToInt32(deletedItem.GetDataKeyValue("CourseID"));
                var rows = DtCoursePrereqEdit.Select("CourseID= '" + CourseID + "'");
                foreach (var row in rows)
                    row.Delete();
            }
        }
    }

    protected void gridPrerequisitesEdit_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        PreReqCourseId = Convert.ToInt32(hidCourseID.Value);

        var gridPrerequisitesEdit = (RadGrid)sender;

        if (OperationPrereq != "edit")
        {
            if (PreReqCourseId != 0)
            {
                var qryList = DataHelper.GetPrerequisiteCourses(PreReqCourseId);

                DtCoursePrereq = LogicHelper.ConvertToDataTable(qryList);
                DtCoursePrereqEdit = DtCoursePrereq;

                if (qryList != null)
                {
                    gridPrerequisitesEdit.DataSource = DtCoursePrereqEdit;
                    OperationPrereq = "edit";
                }
            }
        }
        else
        {
            gridPrerequisitesEdit.DataSource = DtCoursePrereqEdit;
        }
        SetAvailableCourses(DtCoursePrereqEdit, 1);
    }

    protected void cbCategoryAddPrereq_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateDropdownTreeSubcategories(cbCategoryAddPrereq, ddtSubcategoryAddPrereq);
        SetAvailableCourses(DtCoursePrereq, 0);
    }

    protected void ddtSubcategoryPrereq_EntryAdded(object sender, DropDownTreeEntryEventArgs e)
    {
        if (((RadDropDownTree)(sender)).ID.Contains("Edit"))
        {
            SetAvailableCourses(DtCoursePrereqEdit, 1);
        }
        else
        {
            SetAvailableCourses(DtCoursePrereq, 0);
        }
    }

    protected void cbCategoryEditPrereq_SelectedIndexChanged(object sender, EventArgs e)
    {
        var cbCategoryEditPrereq = (RadComboBox)(sender);
        var ddtSubcategoryEditPrereq = (RadDropDownTree)(cbCategoryEditPrereq.Parent.FindControl("ddtSubcategoryEditPrereq"));

        PopulateDropdownTreeSubcategories(cbCategoryEditPrereq, ddtSubcategoryEditPrereq);
        SetAvailableCourses(DtCoursePrereqEdit, 1);
    }

    #endregion
}
