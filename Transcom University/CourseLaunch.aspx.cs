﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using NuComm.Security.Encryption;
using System.Web.Security;
using System.Configuration;
using System.Web.Configuration;
using iTextSharp.text;
using iTextSharp.text.pdf;

public partial class CourseLaunch : BasicWebPlayerBase
{
    public static string EncCourseId
    {
        get
        {
            var value = HttpContext.Current.Session["EncCourseIdSession"];
            return value.ToString();
        }
        set { HttpContext.Current.Session["EncCourseIdSession"] = value; }
    }

    public static int CourseId //equivalent to CurriculumId
    {
        get
        {
            var value = HttpContext.Current.Session["CourseIdSession"];

            return value == null ? 0 : (int)value;
        }
        set { HttpContext.Current.Session["CourseIdSession"] = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BindControls();
            SetPage_RolePreviledges();
        }
    }

    private void SetPage_RolePreviledges()
    {
        if (Roles.IsUserInRole("Admin") || Roles.IsUserInRole("Trainer"))
        {
            divTrainer.Visible = true;
        }
        else
        {
            divTrainer.Visible = false;
        }
    }

    void BindControls()
    {
        EncCourseId = Request.QueryString["CourseID"] ?? string.Empty;
        if (EncCourseId != string.Empty)
        {
            CourseId = Convert.ToInt32(Server.UrlDecode(UTF8.DecryptText(EncCourseId)));
        }
        SetBundleDetails();
        gridBundle.Rebind();
    }

    public void SetBundleDetails()
    {
        var bundle = ScormDataHelper.Get_ScormCourse_ByCourseId(CourseId);
        var overProgress = ScormDataHelper.GetOverallProgressByCourseID(HttpContext.Current.User.Identity.Name, CourseId);

        lblBundleTitle.Text = bundle.Title;
        lblBundleDesc.Text = bundle.Description;
        hdnOverall.Value = overProgress;
    }

    protected void GridBundlePreRender(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            foreach (GridDataItem item in gridBundle.Items)
            {
                if (!item.Expanded || item.Selected) continue;
                item.Expanded = true;
                item.Selected = true;
            }
        }
    }

    protected void GridTrainerPreRender(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            foreach (GridDataItem item in gridBundle.Items)
            {
                if (!item.Expanded || item.Selected) continue;
                item.Expanded = true;
                item.Selected = true;
            }
        }
    }

    protected void gridBundle_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var scormCourses = ScormDataHelper.Get_ScormPackage_ByCurriculumId(CourseId, Context.User.Identity.Name);

        gridBundle.DataSource = scormCourses;
    }

    protected void GridTrainer_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        var scormCourses = ScormDataHelper.Get_TrainerResource(CourseId);

        gridTrainer.DataSource = scormCourses;
    }

    protected void gridPackageScosItemCreated(object sender, GridItemEventArgs e)
    {

    }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument == "Rebind")
        {
            BindControls();
        }
    }

    protected void gridPackageScosItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataItem = (GridDataItem)e.Item;

            var FileName = dataItem.GetDataKeyValue("FileName").ToString();
            var scoTypeID = Convert.ToInt32(dataItem.GetDataKeyValue("SCOTYPEID"));
            var AttemptID = dataItem.GetDataKeyValue("AttemptID").ToString() ?? "0";
            var RedirectID = dataItem.GetDataKeyValue("RedirectID") ?? 0;
            var CompletionStatus = dataItem.GetDataKeyValue("AttemptStatus").ToString() ?? "0";
            var SuccessStatus = dataItem.GetDataKeyValue("SuccessStatus").ToString();
            var ScoPackageID = dataItem.GetDataKeyValue("ScoPackageID").ToString() ?? "0";
            var encCourseId = Server.UrlEncode(UTF8.EncryptText(ScoPackageID));
            var coursetypeID = dataItem.GetDataKeyValue("coursetypeID").ToString() ?? "0";
            var EncryptedOrManifest = dataItem.GetDataKeyValue("EncryptedOrManifest").ToString() ?? string.Empty;
            var Score = dataItem.GetDataKeyValue("Score").ToString() ?? "0";
            var LessonStatus = dataItem.GetDataKeyValue("LessonStatus").ToString();
            int? packageFormat = dataItem.GetDataKeyValue("PackageFormat") != DBNull.Value ? Convert.ToInt32(dataItem.GetDataKeyValue("PackageFormat")) : 2;

            var linkTitle = dataItem["Launch"].FindControl("btnLaunch") as Button;
            var lblScore = dataItem["Progress"].FindControl("lblScore") as Label;
            var lblScoreMarker = dataItem["Progress"].FindControl("lblScoreMarker") as Label;
            var lblStatus = dataItem["Progress"].FindControl("lblStatus") as Label;
            var lblStatusMarker = dataItem["Progress"].FindControl("lblStatusMarker") as Label;

            Session["Launch"] = linkTitle;

            if (Convert.ToInt32(AttemptID == "" ? "0" : AttemptID) == 0)
            {
                lblStatus.Text = "Not Started";
            }
            else
            {
                if (packageFormat == 2) // SCORM 2004
                {
                    switch (Convert.ToInt32(CompletionStatus))
                    {
                        case 0:
                            lblStatus.Text = "In Progress";
                            linkTitle.ToolTip = "Continue Training";
                            break;
                        case 1:
                            if (scoTypeID == 1 || scoTypeID == 3)
                            {
                                lblStatus.Text = "Completed";

                                foreach (string item in WebConfigurationManager.AppSettings["gdprCourseId"].Split(','))
                                {
                                    if (item == CourseId.ToString())
                                    {
                                        ((ImageButton)dataItem["DownloadCert"].FindControl("btnDownloadCert")).Visible = true;
                                        ((ImageButton)dataItem["DownloadCert"].FindControl("btnDownloadCert")).OnClientClick = string.Format("window.open('CertDownload.aspx?AttemptID={0}')", AttemptID);
                                    }
                                }
                            }
                            else if (scoTypeID == 2)
                            {
                                lblStatus.Text = SuccessStatus;
                            }
                            else
                            {
                                lblStatus.Text = "Completed";
                            }

                            linkTitle.ToolTip = "Training Already Completed";
                            break;
                        case 2:
                            lblStatus.Text = "In Progress";
                            linkTitle.ToolTip = "Continue Training";
                            break;
                        case 3:
                            lblStatus.Text = "Not Attempted";
                            linkTitle.ToolTip = "Launch Course";
                            break;
                    }
                }
                else if (packageFormat == 1) // SCORM 1.2
                {
                    switch (LessonStatus)
                    {
                        case "Incomplete":
                            lblStatus.Text = "In Progress";
                            linkTitle.ToolTip = "Continue Training";
                            break;
                        case "Completed":
                            lblStatus.Text = "Completed";
                            linkTitle.ToolTip = "Review Training";

                            foreach (string item in WebConfigurationManager.AppSettings["gdprCourseId"].Split(','))
                            {
                                if (item == CourseId.ToString())
                                {
                                    ((ImageButton)dataItem["DownloadCert"].FindControl("btnDownloadCert")).Visible = true;
                                    ((ImageButton)dataItem["DownloadCert"].FindControl("btnDownloadCert")).OnClientClick = string.Format("window.open('CertDownload.aspx?AttemptID={0}')", AttemptID);
                                }
                            }

                            break;
                        case "Passed":
                            lblStatus.Text = "Passed";
                            linkTitle.ToolTip = "Review Training";

                            foreach (string item in WebConfigurationManager.AppSettings["gdprCourseId"].Split(','))
                            {
                                if (item == CourseId.ToString())
                                {
                                    ((ImageButton)dataItem["DownloadCert"].FindControl("btnDownloadCert")).Visible = true;
                                    ((ImageButton)dataItem["DownloadCert"].FindControl("btnDownloadCert")).OnClientClick = string.Format("window.open('CertDownload.aspx?AttemptID={0}')", AttemptID);
                                }
                            }

                            break;
                        case "Failed":
                            lblStatus.Text = "Failed";
                            linkTitle.ToolTip = "Review Training";
                            break;
                        default:
                            lblStatus.Text = "In Progress";
                            linkTitle.ToolTip = "Continue Training";
                            break;
                    }
                }
            }

            if (linkTitle != null)
            {
                if (scoTypeID != 5)
                {
                    if (Convert.ToInt32(coursetypeID) == 1)
                    {
                        var urlWithParams = string.Format("CourseRedirect.aspx?CourseID={0}&TestCategoryID={1}", encCourseId,
                                             EncryptedOrManifest);
                        linkTitle.Attributes["onclick"] = string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return window.open('{0}', '_blank');", urlWithParams);
                    }
                    else
                    {
                        if (Convert.ToInt32(AttemptID == "" ? "0" : AttemptID) == 0)
                        {
                            linkTitle.Attributes["onclick"] =
                            string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return OpenTraining('Pkg:{0}');", RedirectID);
                        }
                        else
                        {
                            linkTitle.Attributes["onclick"] =
                            string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return OpenTraining('Att:{0}');", AttemptID);
                        }
                    }
                }
            }

            if (scoTypeID != 2)
            {
                lblScore.Visible = false;
                lblScoreMarker.Visible = false;
            }
            else
            {
                lblScore.Text = Score;
            }

            if (scoTypeID == 3 || scoTypeID == 5 || scoTypeID == 6 || scoTypeID == 7 || scoTypeID == 8)
            {
                lblStatusMarker.Visible = false;
                lblStatus.Visible = false;

                var targetFolder = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"].ToString();
                string completePath = Page.ResolveClientUrl(targetFolder.Replace("\\", "//") + "/" + FileName);
                linkTitle.Attributes["onclick"] = string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return window.open('{0}', '_blank');", completePath);
            }
        }
    }

    protected void gridTrainerResourcesItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataItem = (GridDataItem)e.Item;

            var FileName = dataItem.GetDataKeyValue("FileName").ToString() ?? "";
            var linkTitle = dataItem["Launch"].FindControl("btnLaunch") as Button;

            if (linkTitle != null)
            {
                var targetFolder = ConfigurationManager.AppSettings["OtherResourcesDirectoryPath"].ToString();
                string completePath = Page.ResolveClientUrl(targetFolder.Replace("\\", "//") + "/" + FileName);
                linkTitle.Attributes["onclick"] = string.Format("javascript: this.value = 'Launching...'; this.disabled = true; this.disabled = 'disabled'; return window.open('{0}', '_blank');", completePath);
            }
        }
    }

    protected void GridBundleItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridNestedViewItem)
        {
            var db = new SCORMDBDataContext();
            var dataItem = (GridNestedViewItem)e.Item;
            var dataItem2 = (pr_TranscomUniversity_lkp_GetScormPackageResult)dataItem.DataItem;
            var lblDesc1 = (Label)dataItem.FindControl("lblDesc1");
            var lblTitle1 = (Label)dataItem.FindControl("lblTitle1");

            lblDesc1.Text = dataItem2.Description;
            lblTitle1.Text = dataItem2.Title;
        }
    }

    protected void GridBundleItemCommand(object sender, GridCommandEventArgs e)
    {

    }

    protected void dsPackageScosr_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.Parameters["@CIM"].Value = User.Identity.Name;
    }

    protected void btnDownloadCert_Click(object sender, EventArgs e)
    {
        
    }
}