﻿using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

public partial class ViewProfile : Page
{
    private MasterPage _master;
    private string _employeeId = "";

    private static SqlConnection _oconn;
    private static SqlCommand _ocmd;

    public string SearchValue = String.Empty;

    public string GetComboBoxSelectedItems(RadComboBox combo)
    {
        string collection = null;
        if (combo.CheckedItems.Count > 0)
        {
            var sb = new StringBuilder();

            foreach (var item in combo.CheckedItems)
            {
                sb.Append(item.Value + ",");
            }

            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                collection = sb.ToString().TrimEnd(',');
            }
        }
        return collection;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Form.Attributes.Add("enctype", "multipart/form-data");
        Page.Form.Enctype = "multipart/form-data";

        if (User.Identity.Name.Contains("@"))
        {
            Response.Redirect("NoAccess.aspx");
        }

        if (!IsPostBack)
        {
            hidCIM.Value = User.Identity.Name;
            SetPageOnViewing();
            HideEditColumn();

            if (
                (int)
                TWW.Data.Intranet.SPs.PrTranscomUniversityRstCheckAccount(Convert.ToInt32(User.Identity.Name))
                   .ExecuteScalar() == 0)
            {
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                        "alert('Please complete your profile.');",
                                                        true);

                foreach (HtmlTableRow thisRow in tblProfile.Rows)
                {
                    thisRow.Cells[1].Visible = false;
                    thisRow.Cells[2].Visible = true;
                }

                if (_employeeId == "")
                {
                    _employeeId = lblTWWID.Text;
                }

                SetDetails(false, true);
            }

            if (Roles.IsUserInRole("Admin") || Roles.IsUserInRole("Non-APAC Admin"))
            {
                tblAdmin.Visible = true;
                //txtSearch.Enabled = true;
                //btnViewProfileReport.Visible = true;
            }
            else if (Roles.IsUserInRole("Manager") || Roles.IsUserInRole("Trainer"))
            {
                tblAdmin.Visible = true;
                //txtSearch.Enabled = false;
                //btnViewProfileReport.Visible = false;
            }
            else
            {
                tblAdmin.Visible = false;
            }
        }
    }

    private void SetPageOnViewing()
    {
        _employeeId = User.Identity.Name;
        SetDetails(true, false);
        //txtSearch.Text = _employeeId;
        SearchValue = _employeeId;
    }

    private static void Dbconn(string connStr)
    {
        _oconn = new SqlConnection
            {
                ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connStr].ConnectionString
            };
        _oconn.Open();
    }

    private void SetDetails(bool isView, bool isEdit)
    {
        _master = Master;
        Dbconn("NuskillCheck");
        _ocmd = new SqlCommand("sp_Get_User_ByTWWID", _oconn);
        _ocmd.Parameters.AddWithValue("@EmpID", _employeeId);
        _ocmd.Parameters.AddWithValue("@IsReport", "No");
        _ocmd.CommandType = CommandType.StoredProcedure;
        var oDataReader = _ocmd.ExecuteReader();

        if (isView)
        {
            btnEditProfile.Visible = true;
            btnUpdateProfile.Visible = false;
            btnCancelUpdate.Visible = false;
            divUpload.Visible = false;

            if (oDataReader.Read())
            {
                lblName.Text = oDataReader[0] + " " + oDataReader[1];
                lblTWWID.Text = oDataReader[2].ToString();
                lblGender.Text = oDataReader[3].ToString();
                lblDateOfBirth.Text = oDataReader[4].ToString();
                lblRole.Text = oDataReader[5].ToString();
                lblStartDateWithTranscom.Text = oDataReader[6].ToString();
                lblEmailAddress.Text = oDataReader[7].ToString();
                lblEmailAddress2.Text = oDataReader[7].ToString();
                lblCountry.Text = oDataReader[8].ToString();
                lblSite.Text = oDataReader[9].ToString();

                lblDepartment.Text = oDataReader["departmentid"].ToString() == "0"
                                         ? oDataReader["otherdepartment"].ToString()
                                         : oDataReader["departmentname"].ToString();
                lblCampaign.Text = oDataReader["campaignid"].ToString() == "0"
                                       ? oDataReader["othercampaign"].ToString()
                                       : oDataReader["campaignname"].ToString();
                lblClient.Text = oDataReader["clientid"].ToString() == "0"
                                     ? oDataReader["otherclient"].ToString()
                                     : oDataReader["clientname"].ToString();
                lblCareerPath.Text = oDataReader["careerpathid"].ToString() == "0"
                                         ? oDataReader["othercareerpath"].ToString()
                                         : oDataReader["careerpath"].ToString();

                cbEditReceiveUpdates.DataBind();
                lblReceiveUpdates.Text = "";

                List<String> names1 = oDataReader["updatevia"].ToString().Split(',').ToList();

                foreach (RadComboBoxItem itm in cbEditReceiveUpdates.Items)
                {
                    if (names1.Contains(itm.Value))
                    {
                        lblReceiveUpdates.Text = itm.Text + ", " + lblReceiveUpdates.Text;
                    }
                }

                if (lblReceiveUpdates.Text.Length > 0)
                {
                    char[] myChar = { ',', ' ' };
                    lblReceiveUpdates.Text = lblReceiveUpdates.Text.TrimEnd(myChar);
                }

                lblLanguage.Text = oDataReader["languageid"].ToString() == "0"
                                       ? oDataReader["otherlanguage"].ToString()
                                       : oDataReader["languagename"].ToString();

                lblRegion.Text = oDataReader["region"].ToString();
                lblSupervisor.Text = oDataReader["TLName"].ToString();

                lblMobileNo.Text = oDataReader["mobileno"].ToString();
                lblCoursesInterested.Text = oDataReader["courseinterests"].ToString();

                profilePhoto.ImageUrl = "~/Resources/PhotoHandler.ashx?id=" + _employeeId;
            }
            else
            {
                _master.AlertMessage(false, "Search key returned no result. Maybe CIM# is invalid or not active.");
            }
        }
        if (isEdit)
        {
            btnEditProfile.Visible = false;
            btnUpdateProfile.Visible = true;
            btnCancelUpdate.Visible = true;
            divUpload.Visible = true;

            if (oDataReader.Read())
            {
                lblEditTWWID.Text = oDataReader[2].ToString();
                lblEditGender.Text = oDataReader[3].ToString();
                lblEditDateOfBirth.Text = oDataReader[4].ToString();
                lblEditStartDateWithTranscom.Text = oDataReader[6].ToString();
                txtEditEmailAddress.Text = oDataReader[7].ToString();
                txtEditEmailAddress2.Text = oDataReader[7].ToString();
                lblEditCountry.Text = oDataReader[8].ToString();

                if (oDataReader["regionid"].ToString() == "0")
                {
                    cbEditRegion.SelectedIndex = 0;
                }
                else
                {
                    cbEditRegion.SelectedValue = oDataReader["regionid"].ToString();
                }

                if (oDataReader["departmentid"].ToString() == "0")
                {
                    txtEditDepartment.Visible = true;
                    rfvOtherDepartment.Visible = true;
                    cbEditDepartment.SelectedIndex = 0;
                    txtEditDepartment.Text = oDataReader["otherdepartment"].ToString();
                }
                else
                {
                    txtEditDepartment.Visible = false;
                    rfvOtherDepartment.Visible = false;
                    cbEditDepartment.SelectedValue = oDataReader["departmentid"].ToString();
                }

                if (oDataReader["campaignid"].ToString() == "0")
                {
                    if (oDataReader["othercampaign"].ToString() == "Shared")
                    {
                        cbEditCampaign.SelectedIndex = 1;
                        txtEditCampaign.Text = "";
                        txtEditCampaign.Visible = false;
                        rfvOtherCampaign.Enabled = false;
                    }
                    else
                    {
                        cbEditCampaign.SelectedIndex = 0;
                        txtEditCampaign.Text = oDataReader["othercampaign"].ToString();
                        txtEditCampaign.Visible = true;
                        rfvOtherCampaign.Enabled = true;
                    }

                }
                else
                {
                    txtEditCampaign.Visible = false;
                    rfvOtherCampaign.Enabled = false;
                    cbEditCampaign.SelectedValue = oDataReader["campaignid"].ToString();
                }

                if (oDataReader["clientid"].ToString() == "0")
                {
                    if (oDataReader["otherclient"].ToString() == "Shared")
                    {
                        cbEditClient.SelectedIndex = 1;
                        txtEditClient.Text = "";
                        txtEditClient.Visible = false;
                        rfvOtherClient.Enabled = false;
                    }
                    else
                    {
                        cbEditClient.SelectedIndex = 0;
                        txtEditClient.Text = oDataReader["otherclient"].ToString();
                        txtEditClient.Visible = true;
                        rfvOtherClient.Enabled = true;
                    }
                }
                else
                {
                    txtEditClient.Visible = false;
                    rfvOtherClient.Enabled = false;
                    cbEditClient.SelectedValue = oDataReader["clientid"].ToString();
                }

                if (oDataReader["careerpathid"].ToString() == "0")
                {
                    txtEditCareerpath.Visible = true;
                    rfvOtherCareerpath.Enabled = true;
                    cbEditCareerPath.SelectedIndex = 0;
                    txtEditCareerpath.Text = oDataReader["othercareerpath"].ToString();
                }
                else
                {
                    txtEditCareerpath.Visible = false;
                    rfvOtherCareerpath.Enabled = false;
                    cbEditCareerPath.SelectedValue = oDataReader["careerpathid"].ToString();
                }

                cbEditReceiveUpdates.DataBind();
                List<String> names1 = oDataReader["updatevia"].ToString().Split(',').ToList();

                foreach (RadComboBoxItem itm in cbEditReceiveUpdates.Items)
                {
                    if (names1.Contains(itm.Value))
                    {
                        itm.Checked = true;
                    }
                }

                if (oDataReader["languageid"].ToString() == "0")
                {
                    txtEditLanguage.Visible = true;
                    rfvOtherLanguage.Visible = true;
                    cbEditLanguage.SelectedIndex = 0;
                    txtEditLanguage.Text = oDataReader["otherlanguage"].ToString();
                }
                else
                {
                    txtEditLanguage.Visible = false;
                    rfvOtherLanguage.Visible = false;
                    cbEditLanguage.SelectedValue = oDataReader["languageid"].ToString();
                }

                txtEditMobileNo.Text = oDataReader["mobileno"].ToString();
                lblEditSupervisor.Text = oDataReader["TLName"].ToString();
                txtEditCoursesInterested.Text = oDataReader["courseinterests"].ToString();
            }
        }

        oDataReader.Close();
        _oconn.Close();
    }

    //protected void btnSearch_Click(object sender, EventArgs e)
    //{
    //    //_employeeId = txtSearch.Text;
    //    _employeeId = Request.Form["txtSearch"];
    //    SetDetails(true, false);
    //    //txtSearch.Text = _employeeId;
    //    SearchValue = _employeeId;

    //    foreach (HtmlTableRow thisRow in tblProfile.Rows)
    //    {
    //        thisRow.Cells[1].Visible = true;
    //        thisRow.Cells[2].Visible = false;
    //    }

    //    //btnEditProfile.Visible = User.Identity.Name == txtSearch.Text;
    //    btnEditProfile.Visible = User.Identity.Name == Request.Form["txtSearch"];
    //}

    protected void btnViewMyProfile_Click(object sender, EventArgs e)
    {
        SetPageOnViewing();

        foreach (HtmlTableRow thisRow in tblProfile.Rows)
        {
            thisRow.Cells[1].Visible = true;
            thisRow.Cells[2].Visible = false;
        }
    }

    protected void btnEditProfile_Click(object sender, EventArgs e)
    {
        foreach (HtmlTableRow thisRow in tblProfile.Rows)
        {
            thisRow.Cells[1].Visible = false;
            thisRow.Cells[2].Visible = true;
        }

        _employeeId = lblTWWID.Text;
        SetDetails(false, true);
    }

    private void HideEditColumn()
    {
        foreach (HtmlTableRow thisRow in tblProfile.Rows)
        {
            thisRow.Cells[2].Visible = false;
        }
    }

    private static bool IsImage(HttpPostedFile file)
    {
        return ((file != null) && System.Text.RegularExpressions.Regex.IsMatch(file.ContentType, "image/\\S+") && (file.ContentLength > 0));
    }

    protected void btnUpdateProfile_Click(object sender, EventArgs e)
    {
        _master = Master;
        try
        {
            if (IsValid)
            {

                var file = fileUpload.PostedFile;

                var bytes = new byte[] { };

                if ((file != null) && (file.ContentLength > 0))
                {
                    if (IsImage(file) == false)
                    {
                        _master.AlertMessage(false, "Only .JPG, .JPEG or .PNG format will be accepted.");
                        return;
                    }

                    var iFileSize = file.ContentLength;

                    if (iFileSize > 2097152)
                    {
                        _master.AlertMessage(false, "Maximum of 2MB file size will be accepted. You are trying to upload larger than the limit.");
                        return;
                    }

                    bytes = new byte[file.ContentLength];
                    file.InputStream.Read(bytes, 0, file.ContentLength);
                }

                var updateVia = GetComboBoxSelectedItems(cbEditReceiveUpdates);

                //update profile
                Dbconn("NuskillCheck");
                _ocmd = new SqlCommand("pr_TranscomUniversity_InsertUpdateProfile", _oconn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                _ocmd.Parameters.AddWithValue("@TWWID", lblTWWID.Text);
                _ocmd.Parameters.AddWithValue("@OtherCampaign", cbEditCampaign.SelectedIndex == 1 ? "Shared" : txtEditCampaign.Text);
                _ocmd.Parameters.AddWithValue("@CampaignID", cbEditCampaign.SelectedValue);
                _ocmd.Parameters.AddWithValue("@OtherClient", cbEditClient.SelectedIndex == 1 ? "Shared" : txtEditClient.Text);
                _ocmd.Parameters.AddWithValue("@ClientID", cbEditClient.SelectedValue);
                _ocmd.Parameters.AddWithValue("@RegionID", cbEditRegion.SelectedValue);
                _ocmd.Parameters.AddWithValue("@LanguageID", cbEditLanguage.SelectedValue);
                _ocmd.Parameters.AddWithValue("@MobileNo", txtEditMobileNo.Text);
                _ocmd.Parameters.AddWithValue("@CareerPathID", cbEditCareerPath.SelectedValue);
                _ocmd.Parameters.AddWithValue("@OtherCareerPath", txtEditCareerpath.Text);
                _ocmd.Parameters.AddWithValue("@CourseInterests", txtEditCoursesInterested.Text);
                _ocmd.Parameters.AddWithValue("@UploadedPhoto", bytes);
                _ocmd.Parameters.AddWithValue("@UpdateVia", updateVia);
                _ocmd.Parameters.AddWithValue("@otherlanguage", txtEditLanguage.Text);
                _ocmd.Parameters.AddWithValue("@email", txtEditEmailAddress.Text);
                _ocmd.Parameters.AddWithValue("@departmentid", cbEditDepartment.SelectedValue);
                _ocmd.Parameters.AddWithValue("@OtherDepartment", txtEditDepartment.Text);

                _ocmd.ExecuteReader();
                _oconn.Close();

                _master.AlertMessage(true, "Congratulations! Your profile details are now updated.");
            }
            else
            {
                _master.AlertMessage(false, "Request failed. Please try again.");
            }
        }
        catch (Exception)
        {
        }
    }

    protected void cbDepartment_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (cbEditDepartment.Text == "Other")
        {
            txtEditDepartment.Visible = true;
            rfvOtherDepartment.Enabled = true;
        }
        else
        {
            txtEditDepartment.Visible = false;
            rfvOtherDepartment.Enabled = false;
        }
    }

    protected void cbCampaign_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (cbEditCampaign.Text == "Other")
        {
            txtEditCampaign.Visible = true;
            rfvOtherCampaign.Enabled = true;
        }
        else
        {
            txtEditCampaign.Visible = false;
            rfvOtherCampaign.Enabled = false;
        }
    }

    protected void cbClient_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (cbEditClient.Text == "Other")
        {
            txtEditClient.Visible = true;
            rfvOtherClient.Enabled = true;
        }
        else
        {
            txtEditClient.Visible = false;
            rfvOtherClient.Enabled = false;
        }

        //load campaigns
        cbEditCampaign.Items.Clear();

        var item1 = new RadComboBoxItem { Text = "Other" };
        cbEditCampaign.Items.Add(item1);

        var item2 = new RadComboBoxItem { Text = "Shared" };
        cbEditCampaign.Items.Add(item2);
        cbEditCampaign.SelectedIndex = -1;
        cbEditCampaign.Text = "";

        if (txtEditCampaign.Visible)
        {
            cbEditCampaign.SelectedIndex = 0;
        }
    }

    protected void cbEditLanguage_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (cbEditLanguage.Text == "Other")
        {
            txtEditLanguage.Visible = true;
            rfvOtherLanguage.Enabled = true;
        }
        else
        {
            txtEditLanguage.Visible = false;
            rfvOtherLanguage.Enabled = false;
        }
    }

    protected void cbEditCareerPath_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (cbEditCareerPath.Text == "Other")
        {
            txtEditCareerpath.Visible = true;
            rfvOtherCareerpath.Enabled = true;
        }
        else
        {
            txtEditCareerpath.Visible = false;
            rfvOtherCareerpath.Enabled = false;
        }
    }

    protected void btnSearch_Click(object sender, ImageClickEventArgs e)
    {
        //_employeeId = txtSearch.Text;
        _employeeId = Request.Form["txtSearch"];
        SetDetails(true, false);
        //txtSearch.Text = _employeeId;
        SearchValue = _employeeId;

        foreach (HtmlTableRow thisRow in tblProfile.Rows)
        {
            thisRow.Cells[1].Visible = true;
            thisRow.Cells[2].Visible = false;
        }

        //btnEditProfile.Visible = User.Identity.Name == txtSearch.Text;
        btnEditProfile.Visible = User.Identity.Name == Request.Form["txtSearch"];
    }

    public void ClearEditControls()
    {
        lblTWWID.Text = "";
        txtEditCampaign.Text = "";
        txtEditClient.Text = "";
        txtEditMobileNo.Text = "";
        txtEditCareerpath.Text = "";
        txtEditCoursesInterested.Text = "";
        txtEditLanguage.Text = "";
        txtEditEmailAddress.Text = "";
        txtEditEmailAddress2.Text = "";
        txtEditDepartment.Text = "";

        fileUpload.FileContent.Flush();

        cbEditReceiveUpdates.ClearSelection();

        cbEditDepartment.Text = "";
        cbEditCampaign.Text = "";
        cbEditClient.Text = "";
        cbEditRegion.Text = "";
        cbEditLanguage.Text = "";
        cbEditCareerPath.Text = "";
        cbEditCareerPath.Text = "";

        cbEditDepartment.ClearSelection();
        cbEditCampaign.ClearSelection();
        cbEditClient.ClearSelection();
        cbEditRegion.ClearSelection();
        cbEditLanguage.ClearSelection();
        cbEditCareerPath.ClearSelection();
        cbEditCareerPath.ClearSelection();
    }
}