﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.GridExcelBuilder;
using System.Text;

public partial class CourseHistory : Page
{
    DbHelper _db;
    private DataSet _dsGrd;

    public static string TwwId
    {
        get
        {
            var value = HttpContext.Current.Session["TwwIdSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["TwwIdSession"] = value;
        }
    }

    public static string AccessMode
    {
        get
        {
            var value = HttpContext.Current.Session["AccessModeSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["AccessModeSession"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Disable Page Cache 
        Response.Cache.SetCacheability(HttpCacheability.NoCache);

        if (!Page.IsPostBack)
        {
            hidCIM.Value = User.Identity.Name;
            TwwId = User.Identity.Name;
            SetPage_RolePreviledges();
        }
    }

    private void SetPage_RolePreviledges()
    {
        if (Roles.IsUserInRole("Admin") || Roles.IsUserInRole("Non-APAC Admin") || Roles.IsUserInRole("Trainer") || Roles.IsUserInRole("Manager"))
        {
            tdMode.Visible = true;
            tdMngr.Visible = true;
            tdAdminSearch.Visible = true;
            tdBtnSearch.Visible = true;
        }

        if (Page.User.Identity.Name.Contains("@"))
        {
            AccessMode = "External";
        }
    }

    protected void GridCourseHistoryNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LoadEmployeeCourseHistory(TwwId);
    }

    public void LoadEmployeeCourseHistory(string twwid)
    {
        _dsGrd = GetCourseHistoryByUser(twwid.Replace("'", "").Trim());
        gridCourseHistory.DataSource = _dsGrd;
    }

    private DataSet GetCourseHistoryByUser(string userIDs)
    {
        _db = new DbHelper("Intranet");
        DataSet ds;

        if (userIDs.Contains("@"))
        {
            _db = new DbHelper("NuSkillCheck");
            ds = _db.GetExternalUsersCourseHistory(userIDs);
        }
        else
        {
            _db = new DbHelper("Intranet");
            var qry = DataHelper.GetTranscript(userIDs);//_db.GetCoursesEmployee(userIDs);
            ds = new DataSet();
            ds.Tables.Add(LogicHelper.ConvertToDataTable(qry));
        }
        return ds;
    }

    private static TableElement ParseTable(DataTable dt)
    {
        var ssTable = new TableElement();
        CellElement ssCell;

        var ssRow = new RowElement();
        for (var iCol = 0; iCol < dt.Columns.Count; iCol++)
        {
            string cellWidth;
            switch (iCol)
            {
                case 0:
                    cellWidth = "100";
                    break;
                case 1:
                    cellWidth = "100";
                    break;
                case 2:
                    cellWidth = "200";
                    break;
                default:
                    cellWidth = "105";
                    break;
            }

            var ssColumn = new ColumnElement();
            ssColumn.Attributes.Add("ss:Width", cellWidth); //set column width - col1 = 100pt, col2 = 200pt and so on.
            ssTable.Columns.Add(ssColumn);
            ssCell = new CellElement();
            ssCell.Data.DataItem = dt.Columns[iCol].ColumnName;
            ssRow.Cells.Add(ssCell);
        }
        ssTable.Rows.Add(ssRow);

        for (var iRow = 0; iRow < dt.Rows.Count; iRow++)
        {
            ssRow = new RowElement();
            for (var iCol = 0; iCol < dt.Columns.Count; iCol++)
            {
                ssCell = new CellElement();
                ssCell.Data.DataItem = dt.Rows[iRow][iCol];
                ssRow.Cells.Add(ssCell);
            }
            ssTable.Rows.Add(ssRow);
        }

        return ssTable;
    }

    protected DataTable GetSearchees(string filter)
    {
        var dt = new DataTable();
        DataRow[] gridDr = _dsGrd.Tables[0].Select("CIMNumber = " + filter);

        dt.Columns.Add("CIMNumber", typeof(String));
        dt.Columns.Add("CourseID", typeof(String));
        dt.Columns.Add("CourseName", typeof(String));
        dt.Columns.Add("DateCourseTaken", typeof(String));
        dt.Columns.Add("Score", typeof(String));
        dt.Columns.Add("DateTestTaken", typeof(String));

        foreach (DataRow dr in gridDr)
        {
            dt.ImportRow(dr);
        }

        dt.Columns["CIMNumber"].ColumnName = "CIM";
        dt.Columns["CourseID"].ColumnName = "Course ID";
        dt.Columns["CourseName"].ColumnName = "Course Name";
        dt.Columns["DateCourseTaken"].ColumnName = "Date Course Taken";
        dt.Columns["Score"].ColumnName = "Highest Score";
        dt.Columns["DateTestTaken"].ColumnName = "Date Test Taken";

        return dt;
    }

    protected void GridCourseHistoryGridExporting(object sender, GridExportingArgs e)
    {
        var searchee = TwwId.Split(","[0]);
        if (searchee.Length > 1)
        {
            var sb = new StringBuilder();
            for (Int16 i = 0; i < searchee.Length; i++)
            {
                var workSheet = new WorksheetElement(searchee[i])
                                                 {
                                                     Table =
                                                         ParseTable(
                                                             GetSearchees(searchee[i]))
                                                 };
                workSheet.AutoFilter.Range = "R1C1:R1C6";
                workSheet.Render(sb);
            }
            //generate xmlss code
            e.ExportOutput = e.ExportOutput.Replace("</Styles>", "</Styles>" + sb);
        }
    }

    protected void GridCourseHistoryExcelMlExportRowCreated(object sender, GridExportExcelMLRowCreatedArgs e)
    {
        e.Worksheet.Name = "All Records";
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        TwwId = !string.IsNullOrEmpty(txtSearch.Text) ? txtSearch.Text : User.Identity.Name;
        gridCourseHistory.Rebind();

        gridCourseHistory.MasterTableView.GetColumn("CIMNumber").Display = true;
    }

    protected void btnPreviousScore_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        var testCategoryId = item.GetDataKeyValue("TestCategoryID").ToString();
        var courseName = item.GetDataKeyValue("CourseName").ToString();

        var qryList = DataHelper.GetCourseHistoryByCategoryId(TwwId, Convert.ToInt32(testCategoryId));
        if (qryList != null)
        {
            lblCourseName.Text = courseName;
            gridPreviousScores.DataSource = qryList;
            gridPreviousScores.DataBind();
        }
    }

    protected void cbDirectReports_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        //txtSearch.Text = cbDirectReports.SelectedValue;

        GetCheckedItems(cbDirectReports, txtSearch);
    }

    private static void GetCheckedItems(RadComboBox comboBox, RadTextBox txtSearch)
    {
        var sb = new StringBuilder();
        var collection = comboBox.CheckedItems;

        if (collection.Count != 0)
        {
            foreach (var item in collection)
                sb.Append(item.Value + ",");

            txtSearch.Text = sb.ToString().TrimEnd(',');
            txtSearch.ToolTip = sb.ToString().TrimEnd(',');
        }
        else
        {
            txtSearch.Text = "";
        }
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        ConfigureExport();
    }

    public void ConfigureExport()
    {
        gridCourseHistory.ExportSettings.ExportOnlyData = true;
        gridCourseHistory.ExportSettings.IgnorePaging = true;
        gridCourseHistory.ExportSettings.OpenInNewWindow = true;
        gridCourseHistory.ExportSettings.FileName = "TU_Report_MyTranscsripts_" + DateTime.Now;
        gridCourseHistory.MasterTableView.ExportToExcel();
    }

    protected void gridCourseHistory_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataItem = e.Item as GridDataItem;
            var testCategoryId = dataItem.GetDataKeyValue("TestCategoryID").ToString();
            var dateCourseTaken = dataItem.GetDataKeyValue("DateCourseTaken").ToString();
            //var dateTestTaken = dataItem.GetDataKeyValue("DateTestTaken").ToString();

            int? passed = 0;
            //var passed = dataItem.GetDataKeyValue("Passed") ?? "0";

            //(!string.IsNullOrEmpty(dataItem.GetDataKeyValue("Passed").ToString()))
            // passed = (int?)dataItem.GetDataKeyValue("Passed");
            if (DBNull.Value.Equals(dataItem.GetDataKeyValue("Passed")))
            {
                passed = 0;
            }
            else
            {
                passed = Convert.ToInt32(dataItem.GetDataKeyValue("Passed"));
            }

            var btnPreviousScore = e.Item.FindControl("btnPreviousScore") as LinkButton;
            btnPreviousScore.Visible = Convert.ToUInt32(testCategoryId) > 0;

            var progressval = 0;
            if (testCategoryId != "0") //COURSE WITH ASSESSMENT
            {
                progressval = 20;

                if (Convert.ToInt32(passed) == 1)
                {
                    progressval = progressval + 80;
                }

            }
            else //COURSE WO ASSESSMENT
            {
                if (!string.IsNullOrEmpty(dateCourseTaken))
                {
                    progressval = 100;
                }
            }

            var hidProgressVal = (HiddenField)dataItem["Progress"].FindControl("hidProgressVal");
            hidProgressVal.Value = progressval.ToString();
        }
    }

    protected void gridCourseHistory_ExcelExportCellFormatting(object sender, ExcelExportCellFormattingEventArgs e)
    {
        if ((e.FormattedColumn.UniqueName) == "DateCourseTaken" || (e.FormattedColumn.UniqueName) == "DateTestTaken")
        {
            e.Cell.Style["mso-number-format"] = @"dd.MM.yyyy";
        }
    }
}