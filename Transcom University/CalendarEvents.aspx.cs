﻿using System;
using System.Collections;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Microsoft.Reporting.WebForms;

public partial class CalendarEvents : Page
{
    public static int[] StatusIdsForApproval
    {
        get
        {
            var value = HttpContext.Current.Session["StatusIdsForApprovalSession"];
            return value == null ? new int[] { } : (int[])value;
        }
        set
        {
            HttpContext.Current.Session["StatusIdsForApprovalSession"] = value;
        }
    }

    public static int[] StatusIdsUpcomingEvents
    {
        get
        {
            var value = HttpContext.Current.Session["StatusIdsUpcomingEventsSession"];
            return value == null ? new int[] { } : (int[])value;
        }
        set
        {
            HttpContext.Current.Session["StatusIdsUpcomingEventsSession"] = value;
        }
    }

    public static int ReservationId
    {
        get
        {
            var value = HttpContext.Current.Session["ReservationIdSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["ReservationIdSession"] = value;
        }
    }

    public static string EventId
    {
        get
        {
            var value = HttpContext.Current.Session["EventIdSession"];
            return value == null ? "" : (string)value;
        }
        set
        {
            HttpContext.Current.Session["EventIdSession"] = value;
        }
    }

    public static int ClientId
    {
        get
        {
            var value = HttpContext.Current.Session["ClientIdSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["ClientIdSession"] = value;
        }
    }

    public static int TrainTypeId
    {
        get
        {
            var value = HttpContext.Current.Session["TrainTypeIdSession"];
            return value == null ? 0 : (int)value;
        }
        set
        {
            HttpContext.Current.Session["TrainTypeIdSession"] = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            SetPage_RolePreviledges();

            StatusIdsForApproval = new[] { 1 };
            StatusIdsUpcomingEvents = new[] { 2 };

            dpDateEndTabular.SelectedDate = DateTime.Now;
            dpDateEndTRA.SelectedDate = DateTime.Now;
            dpDateStartTabular.SelectedDate = DateTime.Now;
            dpDateStartTRA.SelectedDate = DateTime.Now;

            if (!string.IsNullOrEmpty(Request.QueryString["ReservId"]))
            {
                hidQueryStr.Value = Security.Decrypt(Request.QueryString["ReservId"]);
                tsEvents.SelectedIndex = 1;
                mpEvents.SelectedIndex = 1;
            }
        }
    }

    private void SetPage_RolePreviledges()
    {
        if (!Roles.IsUserInRole("Coordinator") && !Roles.IsUserInRole("Admin"))
        {
            Response.Redirect("NoAccess.aspx");
        }
    }

    private void InitializeEventsForApproval_Tab()
    {
        gridEventsForApproval.Rebind();
    }

    private void InitializeUpcomingEvents_Tab()
    {
        gridUpcomingEvents.Rebind();
    }

    private void InitializeReports_Tab()
    {
        tsReports.SelectedIndex = 0;
        mpReports.SelectedIndex = 0;

        btnExportTRA.Visible = false;
        btnExportTabular.Visible = false;

        dpDateEndTabular.SelectedDate = DateTime.Now;
        dpDateEndTRA.SelectedDate = DateTime.Now;
        dpDateStartTabular.SelectedDate = DateTime.Now;
        dpDateStartTRA.SelectedDate = DateTime.Now;

        cbTimeSlot.Text = "";
        cbTimeSlot.ClearCheckedItems();
        cbLocation.Text = "";
        cbLocation.ClearCheckedItems();
        cbClientTabular.Text = "";
        cbClientTabular.ClearCheckedItems();

        rvTRA.Visible = false;
        rvTabular.Visible = false;
        //rvTRA.Reset();
        //rvTabular.Reset();
    }

    private void LoadGridEventsForApproval()
    {
        var list = DataHelper.GetCalendarEvents_ByStatus(StatusIdsForApproval);
        gridEventsForApproval.DataSource = list;
    }

    private void LoadGridUpcomingEvents()
    {
        var list = DataHelper.GetCalendarEvents_ByStatus(StatusIdsUpcomingEvents);
        gridUpcomingEvents.DataSource = list;
    }

    protected void tsEvents_TabClick(object sender, RadTabStripEventArgs e)
    {
        if (tsEvents.SelectedIndex == 0)
        {
            InitializeEventsForApproval_Tab();
        }
        if (tsEvents.SelectedIndex == 1)
        {
            InitializeUpcomingEvents_Tab();
        }
        if (tsEvents.SelectedIndex == 2)
        {
            InitializeReports_Tab();
        }
    }

    protected void gridEventsForApproval_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LoadGridEventsForApproval();
    }

    private void ClearEventForm()
    {
        lblTrainingType.Text = "";
        lblClassName.Text = "";
        lblTrainer.Text = "";
        lblSite.Text = "";
        lblTimeSlot.Text = "";
        lblDate.Text = "";
        lblDateTo.Text = "";
        cbAssignedRoom.Text = "";
        cbAssignedRoom.ClearSelection();
        txtCoordinatorAdvisory.Text = "";
        txtInstruction.Content = "";
        txtCourseDesc.Content = "";
    }

    protected void btnCreateEvent_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        ClearEventForm();

        var reservationId = item.GetDataKeyValue("ReservationReqId");

        var result = DataHelper.GetReservations_ById(Convert.ToInt32(reservationId)).FirstOrDefault();
        if (result != null)
        {
            if (result.Client == "OD")
            {
                switch (result.TrainingTypeId)
                {
                    case 1:
                        lblTrainingType.Text = "Agent Training";
                        break;
                    case 2:
                        lblTrainingType.Text = "Supervisor Training";
                        break;
                    case 3:
                        lblTrainingType.Text = "Manager Training";
                        break;
                    case 4:
                        lblTrainingType.Text = "Off Phone Employees (All)";
                        break;
                }
            }
            else
            {
                lblTrainingType.Text = result.TrainingType;
            }
            
            lblClassName.Text = result.TrainingClassName;
            lblTrainer.Text = result.TrainerName;
            lblSite.Text = result.CompanySite;
            lblTimeSlot.Text = result.TimeSlot;
            lblDate.Text = result.Date.ToString();
            lblDateTo.Text = result.DateTo.ToString();
            lblCreatedby.Text = result.CreatedByReserv;
            lblStatus.Text = result.ReservationStatus;
        }

        ReservationId = Convert.ToInt32(reservationId.ToString());
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        txtCancelAdvisory.Text = "";

        var reservationId = item.GetDataKeyValue("ReservationReqId");
        var eventId = item.GetDataKeyValue("EventId");

        ReservationId = Convert.ToInt32(reservationId.ToString());
        EventId = eventId.ToString();
    }

    public void ClearPreviewForm()
    {
        vwlblAdvisory.Text = "";
        vwlblApprove.Text = "";
        vwlblAttrition.Text = "";
        vwlblClassname.Text = "";
        vwlblClient.Text = "";
        vwlblCreate.Text = "";
        vwlblDate.Text = "";
        vwlblDateTo.Text = "";
        vwlblFteCount.Text = "";
        vwlblRtpTandim.Text = "";
        vwlblWaveNo.Text = "";
        vwlblSite.Text = "";
        vwlblStat.Text = "";
        vwlblTime.Text = "";
        vwlblTrainType.Text = "";
        vwlblTrainer.Text = "";
        vwlblWeek.Text = "";
        vwlblUpdatedReserv.Text = "";

        vwlblRoom.Text = "";
        vwlblCoorAdvise.Text = "";
        vwlblEnrolmentIns.Text = "";
        vwlvlCourseDesc.Text = "";
        vwlblCreatedEvent.Text = "";
        vwlblUpdatedEvent.Text = "";
    }

    protected void btnPreview_Click(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        var item = btn.NamingContainer as GridDataItem;

        var reservationId = item.GetDataKeyValue("ReservationReqId");

        ClearPreviewForm();

        var result = DataHelper.GetReservations_ById(Convert.ToInt32(reservationId)).FirstOrDefault();
        if (result != null)
        {
            //RESERVATION DETAILS
            vwlblAdvisory.Text = result.Advisory;
            vwlblApprove.Text = result.ApproveByCoordinator;
            vwlblAttrition.Text = result.AttritionGrowth;
            vwlblClassname.Text = result.TrainingClassName;
            vwlblClient.Text = string.IsNullOrEmpty(result.Client) ? "Shared" : result.Client;

            if (result.Client == "OD")
            {
                switch (result.TrainingTypeId)
                {
                    case 1:
                        vwlblTrainType.Text = "Agent Training";
                        break;
                    case 2:
                        vwlblTrainType.Text = "Supervisor Training";
                        break;
                    case 3:
                        vwlblTrainType.Text = "Manager Training";
                        break;
                    case 4:
                        vwlblTrainType.Text = "Off Phone Employees (All)";
                        break;
                }
            }
            else
            {
                vwlblTrainType.Text = result.TrainingType;
            }

            vwlblCreate.Text = result.CreatedByReserv;
            vwlblDate.Text = result.Date.ToString();
            vwlblDateTo.Text = result.DateTo.ToString();
            vwlblFteCount.Text = result.FteCount.ToString();
            vwlblRtpTandim.Text = result.RtpTandim;
            vwlblWaveNo.Text = result.WaveNo.ToString();
            vwlblSite.Text = result.CompanySite;
            vwlblStat.Text = result.ReservationStatus;
            vwlblTime.Text = result.TimeSlot;
            vwlblTrainer.Text = result.TrainerName;
            vwlblWeek.Text = result.WeekNo.ToString();
            vwlblUpdatedReserv.Text = result.UpdatedByReserv;
            //EVENT DETAILS
            vwlblRoom.Text = result.RoomAssignment;
            vwlblCoorAdvise.Text = result.CoordinatorAdvisory;
            vwlblEnrolmentIns.Text = result.EnrollmentInstruction;
            vwlvlCourseDesc.Text = result.CourseDescription;
            vwlblCreatedEvent.Text = result.CreatedByEvent;
            vwlblUpdatedEvent.Text = result.UpdatedByEvent;
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        var eventCreated = DataHelper.InsertCalendarEvent(ReservationId, Convert.ToInt16(cbAssignedRoom.SelectedValue),
                                                          txtCoordinatorAdvisory.Text, txtInstruction.Content,
                                                          txtCourseDesc.Content, User.Identity.Name);
        if (eventCreated)
        {
            var retVal = DataHelper.UpdateReservation_Status_ById(2, User.Identity.Name, ReservationId);

            if (retVal)
            {
                var msg = "YOUR REQUEST HAS BEEN APPROVED! " + txtCoordinatorAdvisory.Text;
                var recipient = DataHelper.GetUserEmailAddress(lblCreatedby.Text);
                var email = SendEmail.SendEmail_ReservationEvent(recipient, msg, lblTrainingType.Text, lblClassName.Text,
                                           lblTrainer.Text, lblSite.Text, lblTimeSlot.Text,
                                           Convert.ToDateTime(lblDate.Text).ToShortDateString(),
                                           Convert.ToDateTime(lblDateTo.Text).ToShortDateString(), cbAssignedRoom.Text,
                                           txtCoordinatorAdvisory.Text, txtCourseDesc.Content);

                var alert = string.Format("alert('Room reservation request has been approved. The calendar event has been succesfully created. Email: {0}');", email);

                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", alert, true);
                ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseEventForm();", true);

                gridEventsForApproval.Rebind();
                ClearEventForm();
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                        "alert('Error! Approval process failed. Calendar event creation failed.');",
                                                        true);
        }
    }

    protected void btnCancelEvent_Click(object sender, EventArgs e)
    {
        var retVal = DataHelper.UpdateReservation_Status_ById(3, null, ReservationId);

        if (!string.IsNullOrEmpty(EventId))
        {
            DataHelper.DeactivateEvent_ById(Convert.ToInt32(EventId), User.Identity.Name);
        }

        if (retVal)
        {
            var msg = "YOUR RESERVATION HAS BEEN CANCELLED! " + txtCancelAdvisory.Text;

            var result = DataHelper.GetReservations_ById(Convert.ToInt32(ReservationId)).FirstOrDefault();
            if (result != null)
            {
                var trainingType = result.TrainingTypeId == 6 ? result.TrainingTypeOther : result.TrainingType;
                var recipient = DataHelper.GetUserEmailAddress(result.CreatedByReserv);
                if (!string.IsNullOrEmpty(recipient))
                {
                    recipient += ";" + ConfigurationManager.AppSettings["EmailItTo"];
                }

                var email = SendEmail.SendEmail_ReservationEvent(recipient, msg, trainingType, result.TrainingClassName,
                                       result.TrainerName, result.CompanySite, result.TimeSlot,
                                       Convert.ToDateTime(result.Date).ToShortDateString(),
                                       Convert.ToDateTime(result.DateTo).ToShortDateString(), result.RoomAssignment,
                                       result.CoordinatorAdvisory, result.CourseDescription);
                var alert = string.Format("alert('Reservation has been cancelled. Email: {0}');", email);
                ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", alert, true);
                ScriptManager.RegisterStartupScript(this, GetType(), "close", "CloseCancelForm();", true);
            }

            gridUpcomingEvents.Rebind();
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                        "alert('Error! Request failed.');",
                                                        true);
        }
    }

    protected void gridUpcomingEvents_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LoadGridUpcomingEvents();
    }

    protected void gridUpcomingEvents_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            var dataItem = e.Item as GridDataItem;
            var reservationReqId = dataItem.GetDataKeyValue("ReservationReqId").ToString();
            var client = dataItem.GetDataKeyValue("Client").ToString();
            var trainingTypeId = dataItem.GetDataKeyValue("TrainingTypeID").ToString();
            var editColumn = dataItem.OwnerTableView.Columns.FindByUniqueName("EditCommandColumn");

            if (!string.IsNullOrEmpty(dataItem["DateTo"].Text))
            {
                var from = Convert.ToDateTime(dataItem["Date"].Text);
                var to = Convert.ToDateTime(dataItem["DateTo"].Text);

                if (DateTime.Today >= from && DateTime.Today <= to)
                {
                    dataItem.BackColor = System.Drawing.Color.DarkSeaGreen;
                    dataItem.ToolTip = "On-going Event";
                    dataItem.Font.Bold = true;
                }
            }

            if (Roles.IsUserInRole("Coordinator") || Roles.IsUserInRole("Admin"))
            {
                editColumn.Display = true;

                if (!string.IsNullOrEmpty(hidQueryStr.Value))
                {
                    if (reservationReqId == hidQueryStr.Value)
                    {
                        dataItem.Edit = true;
                    }
                }
            }
            else
            {
                editColumn.Display = false;
            }

            if (client == "OD")
            {
                switch (trainingTypeId)
                {
                    case "1":
                        dataItem["TrainingType"].Text = "Agent Training";
                        break;
                    case "2":
                        dataItem["TrainingType"].Text = "Supervisor Training";
                        break;
                    case "3":
                        dataItem["TrainingType"].Text = "Manager Training";
                        break;
                    case "4":
                        dataItem["TrainingType"].Text = "Off Phone Employees (All)";
                        break;
                }
            }
        }
        if (e.Item.IsInEditMode)
        {
            var item = (GridEditableItem)e.Item;

            if (!(e.Item is IGridInsertItem))
            {
                var reservationReqId = item.GetDataKeyValue("ReservationReqId").ToString();
                var eventId = item.GetDataKeyValue("EventId") != null
                                  ? item.GetDataKeyValue("EventId").ToString()
                                  : null;
                var client = item.GetDataKeyValue("Client").ToString();

                //RESERVATION
                var cbTrainingType = (RadComboBox)item.FindControl("cbTrainingType_edit");
                var cbAttritionGrowth_edit = (RadComboBox)item.FindControl("cbAttritionGrowth_edit");
                var txtFteCount_edit = (RadNumericTextBox)item.FindControl("txtFteCount_edit");
                var txtRtpTandim_edit = (RadTextBox)item.FindControl("txtRtpTandim_edit");
                var txtWaveNo_edit = (RadNumericTextBox)item.FindControl("txtWaveNo_edit");
                var cbSite_edit = (RadComboBox)item.FindControl("cbSite_edit");
                var cbClient_edit = (RadComboBox)item.FindControl("cbClient_edit");
                var dpDate_edit = (RadDatePicker)item.FindControl("dpDate_edit");
                var dpDateTo_edit = (RadDatePicker)item.FindControl("dpDateTo_edit");
                var cbTimeSlot_edit = (RadComboBox)item.FindControl("cbTimeSlot_edit");
                var txtTrainerName_edit = (RadTextBox)item.FindControl("txtTrainerName_edit");

                var cbClassName_edit = (RadComboBox)item.FindControl("cbClassName_edit");
              
                var txtWeekNo_edit = (RadNumericTextBox)item.FindControl("txtWeekNo_edit");
                var txtAdvisory_edit = (RadTextBox)item.FindControl("txtAdvisory_edit");
                //EVENT
                var cbAssignedRoom_edit = (RadComboBox)item.FindControl("cbAssignedRoom_edit");
                var txtCoordinatorAdvisory_edit = (RadTextBox)item.FindControl("txtCoordinatorAdvisory_edit");
                var txtInstruction_edit = (RadEditor)item.FindControl("txtInstruction_edit");
                var txtCourseDesc_edit = (RadEditor)item.FindControl("txtCourseDesc_edit");

                var result = DataHelper.GetReservations_ById(Convert.ToInt32(reservationReqId)).FirstOrDefault();
                if (result != null)
                {
                    cbAttritionGrowth_edit.SelectedValue = result.AttritionGrowthId.ToString();
                    txtFteCount_edit.Text = result.FteCount.ToString();
                    txtRtpTandim_edit.Text = result.RtpTandim;
                    txtWaveNo_edit.Text = result.WaveNo.ToString();
                    cbSite_edit.SelectedValue = result.SiteId.ToString();

                    cbClient_edit.SelectedValue = result.ClientId.ToString();
                    //cbTrainingType.SelectedValue = result.TrainingTypeId.ToString();
                    ComboBoxHelper.Populate_TrainingType(cbTrainingType, client);
                    cbTrainingType.SelectedValue = result.TrainingTypeId.ToString();
                    
                    dpDate_edit.SelectedDate = Convert.ToDateTime(result.Date);
                    dpDateTo_edit.SelectedDate = Convert.ToDateTime(result.DateTo);
                    cbTimeSlot_edit.SelectedValue = result.TimeSlotId.ToString();
                    txtTrainerName_edit.Text = result.TrainerName;
                    
                    txtWeekNo_edit.Text = result.WeekNo.ToString();
                    txtAdvisory_edit.Text = result.Advisory;

                    cbAssignedRoom_edit.SelectedValue = result.AssignedRoomId.ToString();
                    txtCoordinatorAdvisory_edit.Text = result.CoordinatorAdvisory;
                    txtInstruction_edit.Content = result.EnrollmentInstruction;
                    txtCourseDesc_edit.Content = result.CourseDescription;

                    GetClassNamesOnEdit(Convert.ToInt32(result.ClientId.ToString()),
                                                       Convert.ToInt32(result.TrainingTypeId.ToString()), cbClassName_edit);

                    cbClassName_edit.Text = result.TrainingClassName;
                }

                var trEventHeader = (HtmlControl)item.FindControl("trEventHeader");
                var trRoomAssign = (HtmlControl)item.FindControl("trRoomAssign");
                var trCoorAdvisory = (HtmlControl)item.FindControl("trCoorAdvisory");
                var trEnrollIns = (HtmlControl)item.FindControl("trEnrollIns");
                var trCourseDesc = (HtmlControl)item.FindControl("trCourseDesc");

                if (string.IsNullOrEmpty(eventId))
                {
                    trEventHeader.Visible = false;
                    trRoomAssign.Visible = false;
                    trCoorAdvisory.Visible = false;
                    trEnrollIns.Visible = false;
                    trCourseDesc.Visible = false;
                }
                else
                {
                    trEventHeader.Visible = true;
                    trRoomAssign.Visible = true;
                    trCoorAdvisory.Visible = true;
                    trEnrollIns.Visible = true;
                    trCourseDesc.Visible = true;
                }
            }
        }
    }

    protected void gridUpcomingEvents_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.UpdateCommandName)
        {
            hidQueryStr.Value = "";
            var editedItem = e.Item as GridEditableItem;

            if (editedItem != null)
            {
                if (Page.IsValid)
                {
                    var reservationId = editedItem.GetDataKeyValue("ReservationReqId").ToString();
                    var statusId = editedItem.GetDataKeyValue("StatusId").ToString();
                    var eventId = editedItem.GetDataKeyValue("EventId") != null
                                      ? editedItem.GetDataKeyValue("EventId").ToString()
                                      : null;

                    var cbTrainingType_edit = (RadComboBox) editedItem.FindControl("cbTrainingType_edit");
                    var cbAttritionGrowth_edit = (RadComboBox) editedItem.FindControl("cbAttritionGrowth_edit");
                    var txtFteCount_edit = (RadNumericTextBox) editedItem.FindControl("txtFteCount_edit");
                    var txtRtpTandim_edit = (RadTextBox) editedItem.FindControl("txtRtpTandim_edit");
                    var txtWaveNo_edit = (RadNumericTextBox) editedItem.FindControl("txtWaveNo_edit");
                    var cbSite_edit = (RadComboBox) editedItem.FindControl("cbSite_edit");
                    var cbClient_edit = (RadComboBox) editedItem.FindControl("cbClient_edit");
                    var dpDate_edit = (RadDatePicker) editedItem.FindControl("dpDate_edit");
                    var cbTimeSlot_edit = (RadComboBox) editedItem.FindControl("cbTimeSlot_edit");
                    var txtTrainerName_edit = (RadTextBox) editedItem.FindControl("txtTrainerName_edit");

                    var cbClassName_edit = (RadComboBox)editedItem.FindControl("cbClassName_edit");

                    var txtWeekNo_edit = (RadNumericTextBox) editedItem.FindControl("txtWeekNo_edit");
                    var txtAdvisory_edit = (RadTextBox) editedItem.FindControl("txtAdvisory_edit");

                    //EVENT
                    var cbAssignedRoom_edit = (RadComboBox) editedItem.FindControl("cbAssignedRoom_edit");
                    var txtCoordinatorAdvisory_edit = (RadTextBox) editedItem.FindControl("txtCoordinatorAdvisory_edit");
                    var txtInstruction_edit = (RadEditor) editedItem.FindControl("txtInstruction_edit");
                    var txtCourseDesc_edit = (RadEditor) editedItem.FindControl("txtCourseDesc_edit");

                    const string trainingTypeOther_edit = "";
                    
                    var retVal = DataHelper.UpdateReservation_ById(Convert.ToInt32(reservationId),
                                                                   Convert.ToInt16(cbTrainingType_edit.SelectedValue),
                                                                   trainingTypeOther_edit,
                                                                   Convert.ToInt16(cbAttritionGrowth_edit.SelectedValue),
                                                                   Convert.ToInt32(txtFteCount_edit.Text),
                                                                   txtRtpTandim_edit.Text,
                                                                   Convert.ToInt32(txtWaveNo_edit.Text),
                                                                   Convert.ToInt32(cbSite_edit.SelectedValue),
                                                                   Convert.ToInt32(cbClient_edit.SelectedValue), null,
                                                                   Convert.ToDateTime(dpDate_edit.SelectedDate),
                                                                   Convert.ToInt16(cbTimeSlot_edit.SelectedValue),
                                                                   txtTrainerName_edit.Text, cbClassName_edit.Text,
                                                                   Convert.ToInt16(txtWeekNo_edit.Text),
                                                                   txtAdvisory_edit.Text, null,
                                                                   Convert.ToInt16(statusId),
                                                                   User.Identity.Name);

                    var retVal2 = false;
                    if (!string.IsNullOrEmpty(eventId))
                    {
                        retVal2 = DataHelper.UpdateCalendarEvent(Convert.ToInt32(eventId),
                                                                 Convert.ToInt16(cbAssignedRoom_edit.SelectedValue),
                                                                 txtCoordinatorAdvisory_edit.Text,
                                                                 txtInstruction_edit.Content,
                                                                 txtCourseDesc_edit.Content, User.Identity.Name);
                    }

                    if (retVal)
                    {
                        var alert = "alert('The Reservation has been updated.');";

                        if (retVal2)
                        {
                            alert = "alert('The Reservation has been updated. The Event has been Updated.');";
                        }

                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox", alert, true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "AlertBox",
                                                                "alert('Error. Reservation update failed.');", true);
                    }
                }
            }
        }
        if (e.CommandName == "Cancel")
        {
            hidQueryStr.Value = "";
        }
    }

    protected void cbTrainingTypeEdit_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        var combo = sender as RadComboBox;

        if (combo != null)
        {
            var editedItem = combo.NamingContainer as GridEditableItem;
            if (editedItem != null)
            {
                var cbClient_edit = editedItem.FindControl("cbClient_edit") as RadComboBox;
                var cbClassName_edit = editedItem.FindControl("cbClassName_edit") as RadComboBox;
              
                cbClassName_edit.Text = "";
                cbClassName_edit.ClearSelection();
                
                if (!string.IsNullOrEmpty(cbClient_edit.SelectedValue))
                {
                    GetClassNamesOnEdit(Convert.ToInt32(cbClient_edit.SelectedValue),
                                                        Convert.ToInt32(combo.SelectedValue), cbClassName_edit);
                }
            }
        }
    }

    public void GetClassNamesOnEdit(int clientId, int trainTypeid, RadComboBox cbClassNameEdit)
    {
        var list = DataHelper.GetClassNames_Active(clientId, trainTypeid);
        cbClassNameEdit.DataSource = list;
        cbClassNameEdit.DataBind();
    }

    protected void cbClientEdit_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        var combo = sender as RadComboBox;

        if (combo != null)
        {
            var editedItem = combo.NamingContainer as GridEditableItem;
            if (editedItem != null)
            {
                var cbTrainingType_edit = editedItem.FindControl("cbTrainingType_edit") as RadComboBox;
                var cbClassName_edit = editedItem.FindControl("cbClassName_edit") as RadComboBox;
                
                cbTrainingType_edit.Text = "";
                cbTrainingType_edit.ClearSelection();

                ComboBoxHelper.Populate_TrainingType(cbTrainingType_edit, combo.Text);
                
                cbClassName_edit.Text = "";
                cbClassName_edit.ClearSelection();
                
                if (!string.IsNullOrEmpty(cbTrainingType_edit.SelectedValue))
                {
                    GetClassNamesOnEdit(Convert.ToInt32(combo.SelectedValue),
                                                        Convert.ToInt32(cbTrainingType_edit.SelectedValue), cbClassName_edit);
                }
            }
        }
    }

    #region .Report

    private static string[] GetCheckedItems(RadComboBox comboBox)
    {
        var retval = new string[comboBox.CheckedItems.Count];
        var collection = comboBox.CheckedItems;

        var al = new ArrayList();

        if (collection.Count != 0)
        {
            foreach (var item in collection)
            {
                al.Add(item.Value);
                retval = (string[])al.ToArray(typeof(string));
            }
        }

        return retval;
    }

    protected void btnViewTRA_Click(object sender, EventArgs e)
    {
        var arrayTimeSlot = GetCheckedItems(cbTimeSlot);

        var arrayLocation = GetCheckedItems(cbLocation);

        rvTRA.ServerReport.ReportServerUrl = new Uri("http://SUSL3PSQLDB06/reportserver");
        while (rvTRA.ServerReport.IsDrillthroughReport)
        {
            rvTRA.PerformBack();
        }

        const string strReport = @"/Reports/TranscomUniversityMyReports/Test Reports/TRA";
        rvTRA.ServerReport.ReportPath = strReport;

        var rptParameters = new ReportParameter[4];

        rptParameters[0] = new ReportParameter("DateFrom", dpDateStartTRA.SelectedDate.ToString());
        rptParameters[1] = new ReportParameter("DateTo", dpDateEndTRA.SelectedDate.ToString());
        rptParameters[2] = new ReportParameter("TimeSlot", arrayTimeSlot);
        rptParameters[3] = new ReportParameter("Room", arrayLocation);
        rvTRA.ServerReport.SetParameters(rptParameters);

        rvTRA.Visible = true;
        btnExportTRA.Visible = true;
    }

    protected void btnExportTRA_Click(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string filenameExtension;
        byte[] bytes = rvTRA.ServerReport.Render("Excel", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "attachment;filename=TRA.xls");
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
    }

    protected void btnViewTabular_Click(object sender, EventArgs e)
    {
        var arrayClient = GetCheckedItems(cbClientTabular);

        rvTabular.ServerReport.ReportServerUrl = new Uri("http://SUSL3PSQLDB06/reportserver");
        while (rvTabular.ServerReport.IsDrillthroughReport)
        {
            rvTabular.PerformBack();
        }

        const string strReport = @"/Reports/TranscomUniversityMyReports/Test Reports/Upcoming Events";
        rvTabular.ServerReport.ReportPath = strReport;

        var rptParameters = new ReportParameter[3];

        rptParameters[0] = new ReportParameter("DateFrom", dpDateStartTabular.SelectedDate.ToString());
        rptParameters[1] = new ReportParameter("DateTo", dpDateEndTabular.SelectedDate.ToString());
        rptParameters[2] = new ReportParameter("Client", arrayClient);
        rvTabular.ServerReport.SetParameters(rptParameters);

        rvTabular.Visible = true;
        btnExportTabular.Visible = true;
    }

    protected void btnExportTabular_Click(object sender, EventArgs e)
    {
        Warning[] warnings;
        string[] streamids;
        string mimeType;
        string encoding;
        string filenameExtension;
        byte[] bytes = rvTabular.ServerReport.Render("Excel", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

        Response.Buffer = true;
        Response.Clear();
        Response.ContentType = mimeType;
        Response.AddHeader("content-disposition", "attachment;filename=TabularReport.xls");
        Response.BinaryWrite(bytes);
        Response.Flush();
        Response.End();
    }

    #endregion
}