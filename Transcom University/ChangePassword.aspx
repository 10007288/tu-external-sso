﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderContent" runat="Server">
    <div class="containerdefault">
        <asp:Panel runat="server" DefaultButton="btnProceed" ID="pnlMain" CssClass="pnlMain">
            <asp:Table runat="server" ID="tblLogin" HorizontalAlign="center" Width="450px" defaultbutton="btnLogin"
                CssClass="tblLogin">
                <asp:TableRow>
                    <asp:TableCell>
                        <table width="100%" align="center">
                            <tr style="height: 60px;">
                                <td colspan="2" style="color: #FFF; text-align: center;">
                                    Change External Password
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    Old Password
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtOldPassword" runat="server" LabelWidth="64px" Width="160px"
                                        AutoCompleteType="None" AutoComplete="Off" TextMode="Password" />&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtOldPassword"
                                        Font-Size="Medium" ErrorMessage="*" runat="server" SetFocusOnError="True" ValidationGroup="changepass"
                                        CssClass="displayerror" />
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    New Password
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtNewPassword" TextMode="Password" runat="server" AutoCompleteType="None"
                                        AutoComplete="Off" />&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtNewPassword"
                                        Font-Size="Medium" ErrorMessage="*" runat="server" SetFocusOnError="True" ValidationGroup="changepass"
                                        CssClass="displayerror" />
                                </td>
                            </tr>
                            <tr>
                                <td style="color: #ffcc00; padding-left: 44px">
                                    Re-Type New Password
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtRetypePassword" TextMode="Password" runat="server" AutoCompleteType="None"
                                        AutoComplete="Off" />&nbsp;
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtRetypePassword"
                                        Font-Size="Medium" ErrorMessage="*" runat="server" SetFocusOnError="True" ValidationGroup="changepass"
                                        CssClass="displayerror" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right" style="padding-right: 55px; padding-top: 10px">
                                    <div id="managecontrol" style="width: 100%">
                                        <asp:LinkButton ID="btnProceed" runat="server" Text="Proceed" OnClick="btnProceed_Click"
                                            ValidationGroup="changepass" />
                                    </div>
                                    <p>
                                        <asp:CompareValidator runat="server" ID="covOldPassword" ControlToValidate="txtNewPassword"
                                            ControlToCompare="txtOldPassword" Operator="NotEqual" Type="String" ErrorMessage="-Old password must not match new password."
                                            CssClass="displayerror" ValidationGroup="changepass" Display="Dynamic" /><br />
                                        <asp:CompareValidator runat="server" ID="covNewPassword" ControlToValidate="txtNewPassword"
                                            ControlToCompare="txtRetypePassword" Operator="Equal" Type="String" ErrorMessage="-New passwords given does not match."
                                            CssClass="displayerror" ValidationGroup="changepass" Display="Dynamic" /><br />
                                        <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txtNewPassword"
                                            ID="revNewPassword" ValidationExpression="^[\s\S]{6,}$" runat="server" ErrorMessage="-Minimum 6 characters required."
                                            ValidationGroup="changepass" CssClass="displayerror"></asp:RegularExpressionValidator>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </asp:TableCell></asp:TableRow></asp:Table></asp:Panel></div></asp:Content>