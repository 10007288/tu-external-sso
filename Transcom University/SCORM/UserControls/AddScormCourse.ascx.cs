﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Web;
using System.Web.UI;
using Telerik.Web.UI;
using System.Web.UI.WebControls;

namespace ScormCourse
{
    public partial class SCORM_UserControls_AddScormCourse : UserControl
    {
        #region .VARIABLES

        public int CurrentIndex { get; set; }
        public string Title { get; set; }

        public DataTable DtPackage1
        {
            get
            {
                return ViewState["DtPackage1"] as DataTable;
            }
            set
            {
                ViewState["DtPackage1"] = value;
            }
        }

        private List<DockState> PackageList1
        {
            get
            {
                var currentDockStates = (List<DockState>)Session["PackageList1"];

                if (Equals(currentDockStates, null))
                {
                    currentDockStates = new List<DockState>();
                    Session["PackageList1"] = currentDockStates;
                }

                return currentDockStates;
            }
            set
            {
                Session["PackageList1"] = value;
            }
        }

        private List<DockState> PackageList2
        {
            get
            {
                var currentDockStates = (List<DockState>)Session["PackageList2"];

                if (Equals(currentDockStates, null))
                {
                    currentDockStates = new List<DockState>();
                    Session["PackageList2"] = currentDockStates;
                }

                return currentDockStates;
            }
            set
            {
                Session["PackageList2"] = value;
            }
        }

        private List<DockState> PackageList3
        {
            get
            {
                var currentDockStates = (List<DockState>)Session["PackageList3"];

                if (Equals(currentDockStates, null))
                {
                    currentDockStates = new List<DockState>();
                    Session["PackageList3"] = currentDockStates;
                }

                return currentDockStates;
            }
            set
            {
                Session["PackageList3"] = value;
            }
        }

        private List<DockState> PackageList4
        {
            get
            {
                var currentDockStates = (List<DockState>)Session["PackageList4"];

                if (Equals(currentDockStates, null))
                {
                    currentDockStates = new List<DockState>();
                    Session["PackageList4"] = currentDockStates;
                }

                return currentDockStates;
            }
            set
            {
                Session["PackageList4"] = value;
            }
        }

        private List<DockState> PackageList5
        {
            get
            {
                var currentDockStates = (List<DockState>)Session["PackageList5"];

                if (Equals(currentDockStates, null))
                {
                    currentDockStates = new List<DockState>();
                    Session["PackageList5"] = currentDockStates;
                }

                return currentDockStates;
            }
            set
            {
                Session["PackageList5"] = value;
            }
        }

        private List<DockState> PackageList6
        {
            get
            {
                var currentDockStates = (List<DockState>)Session["PackageList6"];

                if (Equals(currentDockStates, null))
                {
                    currentDockStates = new List<DockState>();
                    Session["PackageList6"] = currentDockStates;
                }

                return currentDockStates;
            }
            set
            {
                Session["PackageList6"] = value;
            }
        }

        private List<DockState> PackageList7
        {
            get
            {
                var currentDockStates = (List<DockState>)Session["PackageList7"];

                if (Equals(currentDockStates, null))
                {
                    currentDockStates = new List<DockState>();
                    Session["PackageList7"] = currentDockStates;
                }

                return currentDockStates;
            }
            set
            {
                Session["PackageList7"] = value;
            }
        }

        private List<DockState> PackageList8
        {
            get
            {
                var currentDockStates = (List<DockState>)Session["PackageList8"];

                if (Equals(currentDockStates, null))
                {
                    currentDockStates = new List<DockState>();
                    Session["PackageList8"] = currentDockStates;
                }

                return currentDockStates;
            }
            set
            {
                Session["PackageList8"] = value;
            }
        }

        private List<DockState> PackageList9
        {
            get
            {
                var currentDockStates = (List<DockState>)Session["PackageList9"];

                if (Equals(currentDockStates, null))
                {
                    currentDockStates = new List<DockState>();
                    Session["PackageList9"] = currentDockStates;
                }

                return currentDockStates;
            }
            set
            {
                Session["PackageList9"] = value;
            }
        }

        private List<DockState> PackageList10
        {
            get
            {
                var currentDockStates = (List<DockState>)Session["PackageList10"];

                if (Equals(currentDockStates, null))
                {
                    currentDockStates = new List<DockState>();
                    Session["PackageList10"] = currentDockStates;
                }

                return currentDockStates;
            }
            set
            {
                Session["PackageList10"] = value;
            }
        }

        public static int PackageCount
        {
            get
            {
                var value = HttpContext.Current.Session["PackageCount"];
                return value == null ? 0 : (int)value;
            }
            set { HttpContext.Current.Session["PackageCount"] = value; }
        }

        public static int CurrentPackageIndex
        {
            get
            {
                var value = HttpContext.Current.Session["CurrentPackageIndex"];
                return value == null ? 0 : (int)value;
            }
            set { HttpContext.Current.Session["CurrentPackageIndex"] = value; }
        }

        public void CreatePackageDataTableColumns()
        {
            DtPackage1 = new DataTable();
            var column = new DataColumn
            {
                DataType = Type.GetType("System.Int32"),
                ColumnName = "ScoId",
                AutoIncrement = true,
                AutoIncrementSeed = 1,
                AutoIncrementStep = 1
            };
            DtPackage1.Columns.Add(column);
            DtPackage1.Columns.Add("ScoPackageId", typeof(int));
            DtPackage1.Columns.Add("ScoTitle", typeof(String));
            DtPackage1.Columns.Add("ScoTypeId", typeof(int));
            DtPackage1.Columns.Add("ScoType", typeof(String));
            DtPackage1.Columns.Add("CourseTypeID", typeof(int));
            DtPackage1.Columns.Add("curriculumcourseID", typeof(int));
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                PackageCount = 0;
                CurrentPackageIndex = 0;

                //ConfigurePackages(CurrentIndex);
                //gridPackage1.Rebind();
            }
        }

        private void ConfigureScormPackage_Show()
        {
            RadPanelItem item;
            switch (PackageCount)
            {
                case 1:
                    item = RadPanelBar1.FindItemByValue("1");
                    //item.Text = txtPackageTitle.Text;
                    item.Text = "Course 1";
                    PackageTitle1.Text = txtPackageTitle.Text;
                    item.Visible = true;


                    PackageDesc1.Text = string.Empty;
                    cbSCOType1.Text = string.Empty;
                    cbSCOType1.SelectedIndex = -1;


                    break;
                case 2:
                    item = RadPanelBar1.FindItemByValue("2");
                    //item.Text = txtPackageTitle.Text;
                    item.Text = "Course 2";
                    PackageTitle2.Text = txtPackageTitle.Text;
                    item.Visible = true;

                    PackageDesc2.Text = string.Empty;
                    cbSCOType2.Text = string.Empty;
                    cbSCOType2.SelectedIndex = -1;

                    break;
                case 3:
                    item = RadPanelBar1.FindItemByValue("3");
                    //item.Text = txtPackageTitle.Text;
                    item.Text = "Course 3";
                    PackageTitle3.Text = txtPackageTitle.Text;
                    item.Visible = true;

                    PackageDesc3.Text = string.Empty;
                    cbSCOType3.Text = string.Empty;
                    cbSCOType3.SelectedIndex = -1;

                    break;
                case 4:
                    item = RadPanelBar1.FindItemByValue("4");
                    //item.Text = txtPackageTitle.Text;
                    item.Text = "Course 4";
                    PackageTitle4.Text = txtPackageTitle.Text;
                    item.Visible = true;

                    PackageDesc4.Text = string.Empty;
                    cbSCOType4.Text = string.Empty;
                    cbSCOType4.SelectedIndex = -1;


                    break;
                case 5:
                    item = RadPanelBar1.FindItemByValue("5");
                    //item.Text = txtPackageTitle.Text;
                    item.Text = "Course 5";
                    PackageTitle5.Text = txtPackageTitle.Text;
                    item.Visible = true;

                    PackageDesc5.Text = string.Empty;
                    cbSCOType5.Text = string.Empty;
                    cbSCOType5.SelectedIndex = -1;


                    break;
                case 6:
                    item = RadPanelBar1.FindItemByValue("6");
                    //item.Text = txtPackageTitle.Text;
                    item.Text = "Course 6";
                    PackageTitle6.Text = txtPackageTitle.Text;
                    item.Visible = true;

                    PackageDesc6.Text = string.Empty;
                    cbSCOType6.Text = string.Empty;
                    cbSCOType6.SelectedIndex = -1;


                    break;
                case 7:
                    item = RadPanelBar1.FindItemByValue("7");
                    //item.Text = txtPackageTitle.Text;
                    item.Text = "Course 7";
                    PackageTitle7.Text = txtPackageTitle.Text;
                    item.Visible = true;

                    PackageDesc7.Text = string.Empty;
                    cbSCOType7.Text = string.Empty;
                    cbSCOType7.SelectedIndex = -1;


                    break;
                case 8:
                    item = RadPanelBar1.FindItemByValue("8");
                    //item.Text = txtPackageTitle.Text;
                    item.Text = "Course 8";
                    PackageTitle8.Text = txtPackageTitle.Text;
                    item.Visible = true;

                    PackageDesc8.Text = string.Empty;
                    cbSCOType8.Text = string.Empty;
                    cbSCOType8.SelectedIndex = -1;

                    break;
                case 9:
                    item = RadPanelBar1.FindItemByValue("9");
                    //item.Text = txtPackageTitle.Text;
                    item.Text = "Course 9";
                    PackageTitle1.Text = txtPackageTitle.Text;
                    item.Visible = true;

                    PackageDesc9.Text = string.Empty;
                    cbSCOType9.Text = string.Empty;
                    cbSCOType9.SelectedIndex = -1;

                    break;
                case 10:
                    item = RadPanelBar1.FindItemByValue("10");
                    //item.Text = txtPackageTitle.Text;
                    item.Text = "Course 10";
                    PackageTitle10.Text = txtPackageTitle.Text;
                    item.Visible = true;

                    PackageDesc10.Text = string.Empty;
                    cbSCOType10.Text = string.Empty;
                    cbSCOType10.SelectedIndex = -1;


                    break;
            }

            txtPackageTitle.Text = "";
            btnDeletePackage.Visible = PackageCount > 0;
            //btnDeletePackage.Visible = PackageCount != 1;
            divAddPackage.Visible = PackageCount != 10;
            divNote.Visible = PackageCount == 0;
        }

        public void ConfigureScormPackage_Hide()
        {
            RadPanelItem item;


            switch (PackageCount)
            {
                case 1:
                    item = RadPanelBar1.FindItemByValue("1");
                    item.Visible = false;

                    PackageTitle1.Text = string.Empty;
                    PackageDesc1.Text = string.Empty;
                    cbSCOType1.Text = string.Empty;
                    cbSCOType1.SelectedIndex = -1;

                    gridPackage1.DataSource = new string[] { };
                    gridPackage1.DataBind();

                    break;
                case 2:
                    item = RadPanelBar1.FindItemByValue("2");
                    item.Visible = false;

                    PackageTitle2.Text = string.Empty;
                    PackageDesc2.Text = string.Empty;
                    cbSCOType2.Text = string.Empty;
                    cbSCOType2.SelectedIndex = -1;

                    gridPackage2.DataSource = new string[] { };
                    gridPackage2.DataBind();

                    break;
                case 3:
                    item = RadPanelBar1.FindItemByValue("3");
                    item.Visible = false;

                    PackageTitle3.Text = string.Empty;
                    PackageDesc3.Text = string.Empty;
                    cbSCOType3.Text = string.Empty;
                    cbSCOType3.SelectedIndex = -1;

                    gridPackage3.DataSource = new string[] { };
                    gridPackage3.DataBind();
                    break;
                case 4:
                    item = RadPanelBar1.FindItemByValue("4");
                    item.Visible = false;

                    PackageTitle4.Text = string.Empty;
                    PackageDesc4.Text = string.Empty;
                    cbSCOType4.Text = string.Empty;
                    cbSCOType4.SelectedIndex = -1;

                    gridPackage4.DataSource = new string[] { };
                    gridPackage4.DataBind();
                    break;
                case 5:
                    item = RadPanelBar1.FindItemByValue("5");
                    item.Visible = false;

                    PackageTitle5.Text = string.Empty;
                    PackageDesc5.Text = string.Empty;
                    cbSCOType5.Text = string.Empty;
                    cbSCOType5.SelectedIndex = -1;

                    gridPackage5.DataSource = new string[] { };
                    gridPackage5.DataBind();
                    break;
                case 6:
                    item = RadPanelBar1.FindItemByValue("6");
                    item.Visible = false;

                    PackageTitle6.Text = string.Empty;
                    PackageDesc6.Text = string.Empty;
                    cbSCOType6.Text = string.Empty;
                    cbSCOType6.SelectedIndex = -1;

                    gridPackage6.DataSource = new string[] { };
                    gridPackage6.DataBind();
                    break;
                case 7:
                    item = RadPanelBar1.FindItemByValue("7");
                    item.Visible = false;

                    PackageTitle7.Text = string.Empty;
                    PackageDesc7.Text = string.Empty;
                    cbSCOType7.Text = string.Empty;
                    cbSCOType7.SelectedIndex = -1;

                    gridPackage7.DataSource = new string[] { };
                    gridPackage7.DataBind();
                    break;
                case 8:
                    item = RadPanelBar1.FindItemByValue("8");
                    item.Visible = false;

                    PackageTitle8.Text = string.Empty;
                    PackageDesc8.Text = string.Empty;
                    cbSCOType8.Text = string.Empty;
                    cbSCOType8.SelectedIndex = -1;

                    gridPackage8.DataSource = new string[] { };
                    gridPackage8.DataBind();
                    break;
                case 9:
                    item = RadPanelBar1.FindItemByValue("9");
                    item.Visible = false;

                    PackageTitle9.Text = string.Empty;
                    PackageDesc9.Text = string.Empty;
                    cbSCOType9.Text = string.Empty;
                    cbSCOType9.SelectedIndex = -1;

                    gridPackage9.DataSource = new string[] { };
                    gridPackage9.DataBind();
                    break;
                case 10:
                    item = RadPanelBar1.FindItemByValue("10");
                    item.Visible = false;

                    PackageTitle10.Text = string.Empty;
                    PackageDesc10.Text = string.Empty;
                    cbSCOType10.Text = string.Empty;
                    cbSCOType10.SelectedIndex = -1;

                    gridPackage10.DataSource = new string[] { };
                    gridPackage10.DataBind();
                    break;
            }

            PackageCount = PackageCount - 1;

            btnDeletePackage.Visible = PackageCount > 0;

            //btnDeletePackage.Visible = PackageCount != 1;
            divAddPackage.Visible = PackageCount != 10;
            divNote.Visible = PackageCount == 0;
        }

        private void ConfigurePackages(int index)
        {
            RadPanelItem item = RadPanelBar1.FindItemByValue("PanelItem");
            const string a = "Package";

            switch (index)
            {
                case 1:
                    item.Text = a + index;
                    break;
                case 2:
                    item.Text = Title + index;
                    break;
                case 3:
                    item.Text = a + index;
                    break;
                case 4:
                    item.Text = a + index;
                    break;
                case 5:
                    item.Text = a + index;
                    break;
                case 6:
                    item.Text = a + index;
                    break;
                case 7:
                    item.Text = a + index;
                    break;
                case 8:
                    item.Text = a + index;
                    break;
                case 9:
                    item.Text = a + index;
                    break;
                case 10:
                    item.Text = a + index;
                    break;
            }
        }

        private void ResetUploadControls()
        {
            //RadDock1.Visible = false;

            //RadDockZone1.Visible = false;
            //RadDockZone2.Visible = false;
            //RadDockZone3.Visible = false;
            //RadDockZone4.Visible = false;
            //RadDockZone5.Visible = false;
            //RadDockZone6.Visible = false;
            //RadDockZone7.Visible = false;
            //RadDockZone8.Visible = false;
            //RadDockZone9.Visible = false;
            //RadDockZone10.Visible = false;
            //RadDockZone1.Docks.Remove(RadDock1);

            //cbUploadType.Text = "";
            //cbUploadType.ClearSelection();

            //divNew.Visible = true;
            //divCatalog.Visible = false;
        }

        protected void btnAddNewPackage_OnClick(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //clear upload controls
                ResetUploadControls();
                int NumberofPackage;
                NumberofPackage = 0;
                for (int i = 1; i <= 10; i++)
                {
                    if (RadPanelBar1.FindItemByValue(Convert.ToString(i)).Visible == true)
                    {
                        NumberofPackage = NumberofPackage + 1;
                    }
                }

                PackageCount = NumberofPackage + 1;
                ConfigureScormPackage_Show();
            }
        }

        protected void btnDeletePackage_Click(object sender, ImageClickEventArgs e)
        {

            ConfigureScormPackage_Hide();

        }

        protected void btnAddSCO_Click(object sender, EventArgs e)
        {
            ResetUploadControls();
            //RadDock1.Visible = true;

            switch (CurrentPackageIndex)
            {
                case 1:
                    //RadDockZone1.Visible = true;
                    //RadDockZone1.Docks.Add(RadDock1);
                    break;
                case 2:
                    //RadDockZone2.Visible = true;
                    //RadDockZone2.Docks.Add(RadDock1);
                    break;
                case 3:
                    //RadDockZone3.Visible = true;
                    //RadDockZone3.Docks.Add(RadDock1);
                    break;
                case 4:
                    //RadDockZone4.Visible = true;
                    //RadDockZone4.Docks.Add(RadDock1);
                    break;
                case 5:
                    //RadDockZone5.Visible = true;
                    //RadDockZone5.Docks.Add(RadDock1);
                    break;
                case 6:
                    //RadDockZone6.Visible = true;
                    //RadDockZone6.Docks.Add(RadDock1);
                    break;
                case 7:
                    //RadDockZone7.Visible = true;
                    //RadDockZone7.Docks.Add(RadDock1);
                    break;
                case 8:
                    //RadDockZone8.Visible = true;
                    //RadDockZone8.Docks.Add(RadDock1);
                    break;
                case 9:
                    //RadDockZone9.Visible = true;
                    //RadDockZone9.Docks.Add(RadDock1);
                    break;
                case 10:
                    //RadDockZone10.Visible = true;
                    //RadDockZone10.Docks.Add(RadDock1);
                    break;
            }

            LinkButton source = (LinkButton)sender;
            AddButtonSenderID.Text = source.ID.Replace("btnAddSCO", "");
        }

        public static void PersistUploadDock_OnPostback()
        {
            switch (CurrentPackageIndex)
            {
                case 1:
                    //RadDockZone1.Docks.Add(RadDock1);
                    break;
                case 2:
                    //RadDockZone2.Docks.Add(RadDock1);
                    break;
                case 3:
                    //RadDockZone3.Docks.Add(RadDock1);
                    break;
                case 4:
                    //RadDockZone4.Docks.Add(RadDock1);
                    break;
                case 5:
                    //RadDockZone5.Docks.Add(RadDock1);
                    break;
                case 6:
                    //RadDockZone6.Docks.Add(RadDock1);
                    break;
                case 7:
                    //RadDockZone7.Docks.Add(RadDock1);
                    break;
                case 8:
                    //RadDockZone8.Docks.Add(RadDock1);
                    break;
                case 9:
                    //RadDockZone9.Docks.Add(RadDock1);
                    break;
                case 10:
                    //RadDockZone10.Docks.Add(RadDock1);
                    break;
            }
        }

        protected void RadPanelBar1_ItemClicked(object sender, RadPanelBarEventArgs e)
        {
            switch (RadPanelBar1.SelectedItem.Value)
            {
                case "1":
                    CurrentPackageIndex = 1;
                    break;
                case "2":
                    CurrentPackageIndex = 2;
                    break;
                case "3":
                    CurrentPackageIndex = 3;
                    break;
                case "4":
                    CurrentPackageIndex = 4;
                    break;
                case "5":
                    CurrentPackageIndex = 5;
                    break;
                case "6":
                    CurrentPackageIndex = 6;
                    break;
                case "7":
                    CurrentPackageIndex = 7;
                    break;
                case "8":
                    CurrentPackageIndex = 8;
                    break;
                case "9":
                    CurrentPackageIndex = 9;
                    break;
                case "10":
                    CurrentPackageIndex = 10;
                    break;
            }
        }

        public DataTable ToDataTable<TSource>(IList<TSource> data)
        {
            var dataTable = new DataTable(typeof(TSource).Name);
            PropertyInfo[] props = typeof(TSource).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in props)
            {
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (TSource item in data)
            {
                var values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        protected void gridPackage_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == RadGrid.DeleteCommandName)
            {
                RadGrid gridPackage = (RadGrid)sender;
                CreatePackageDataTableColumns();
                foreach (GridDataItem row in gridPackage.Items)
                {
                    var ScoId = (Int32)row.GetDataKeyValue("ScoId");
                    var ScoPackageId = (Int32)row.GetDataKeyValue("ScoPackageId");
                    var ScoTypeId = (Int32)row.GetDataKeyValue("ScoTypeId");
                    var ScoType = (string)row.GetDataKeyValue("ScoType");
                    var ScoTitle = (string)row.GetDataKeyValue("ScoTitle");
                    var CourseTypeID = (Int32)row.GetDataKeyValue("CourseTypeID");
                    var curriculumcourseID = (Int32)row.GetDataKeyValue("curriculumcourseID");

                    DataRow dr = DtPackage1.NewRow();
                    dr["ScoId"] = ScoId;
                    dr["ScoPackageId"] = ScoPackageId;
                    dr["ScoTypeId"] = ScoTypeId;
                    dr["ScoType"] = ScoType;
                    dr["ScoTitle"] = ScoTitle;
                    dr["CourseTypeID"] = CourseTypeID;
                    dr["curriculumcourseID"] = curriculumcourseID;
                    DtPackage1.Rows.Add(dr);
                }

                var deletedItem = e.Item as GridEditableItem;
                if (deletedItem != null)
                {
                    var ScoPackageId = (int)deletedItem.GetDataKeyValue("ScoPackageId");
                    var rows = DtPackage1.Select("ScoPackageId = '" + ScoPackageId + "'");
                    try
                    {
                        foreach (var row in rows)
                            row.Delete();

                        gridPackage.DataSource = DtPackage1;
                        gridPackage.DataBind();
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        protected void gridPackage_ItemDataBound(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridDataItem)
            {
                var gridItem = e.Item as GridDataItem;
                var type = gridItem.GetDataKeyValue("ScoTypeId").ToString();
                var title = gridItem.GetDataKeyValue("ScoTitle").ToString();

                switch (type)
                {
                    case "1":
                        HideShowItems(title);
                        break;
                    case "2":
                        HideShowItems(title);
                        break;
                    case "3":
                        HideShowItems(title);
                        break;
                    case "4":
                        HideShowItems(title);
                        break;
                    case "5":
                        HideShowItems(title);
                        break;
                }
            }
        }

        private void HideShowItems(string title)
        {
            if (title == "-")
            {
                //gridPackage.Columns.FindByUniqueName("DeleteColumn").Visible = false;
                //gridPackage.MasterTableView.Columns.FindByUniqueName("DeleteColumn").Visible = false;
            }
            else
            {
                //gridPackage.MasterTableView.Columns.FindByUniqueName("DeleteColumn").Visible = true;
            }
        }
    }
}