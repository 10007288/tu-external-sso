<%@ Control Language="C#" AutoEventWireup="true" CodeFile="UploadContent.ascx.cs"
    Inherits="DNSControls_UploadContent" %>
<%@ Register TagPrefix="AddScormCourse" TagName="ScormCourse" Src="~/SCORM/UserControls/AddScormCourse.ascx" %>
<script type="text/javascript" language="javascript">
    function validateUpload(sender, args) {
        var upload = $find("RadAsyncUpload1.ClientID");
        args.IsValid = upload.getUploadedFiles().length != 0;
    }

    function Uploading() {
        var b = document.getElementById("UploadPackageButton");
        b.setAttribute('disabled', 'true');
        b.innerHTML = 'Uploading...';
    }
</script>
<asp:Panel ID="panelUpload1" CssClass="UploadPanel" runat="server">
    <div id="managecontrol2" style="width: 100%" align="right">
        <div>
            <telerik:RadUpload runat="server" ID="RadAsyncUpload1" InitialFileInputsCount="1"
                MaxFileInputsCount="1" ControlObjectsVisibility="RemoveButtons" />
        </div>
        <div>
            <asp:CustomValidator runat="server" ID="CustomValidator" ClientValidationFunction="validateUpload"
                ErrorMessage="Please select at valid file" ValidationGroup="upload1">
            </asp:CustomValidator>
        </div>
        <asp:LinkButton runat="server" ID="UploadPackageButton" Text="Upload" CausesValidation="True"
            Font-Size="8" OnClick="UploadPackageButton_OnClick" OnClientClick="Uploading();"
            ClientIDMode="Static" />
    </div>
</asp:Panel>
