<%@ Control Language="C#" AutoEventWireup="true" CodeFile="TrainingList.ascx.cs"
    Inherits="DNSControls_TrainingList" %>
<script type="text/javascript">
    function dnsAddRowToTrainingGrid(rowId, aCells, aClassNames) {
        // add a row to TrainingGrid; <rowId> is the HTML ID to use; <aCells> is an array
        // containing the HTML for each cell in the row; <aClassNames> is corresponding HTML
        // class names
        var TrainingGrid = document.getElementById(dnsTrainingGridControlID);
        var row = document.createElement("tr");
        row.id = rowId;
        row.style.backgroundColor = "#FFFFE0"; // highlight new row
        for (var iCell = 0; iCell < aCells.length; iCell++) {
            var cell = document.createElement("td");
            cell.className = aClassNames[iCell];
            cell.insertAdjacentHTML("beforeEnd", aCells[iCell]);
            row.appendChild(cell);
        }
        TrainingGrid.tBodies[0].appendChild(row);

        // if TrainingGrid previously was hidden (i.e. because there's no
        // training to show), display it now, and hide NoTrainingMessage
        TrainingPanel.style.display = "inline";
        if (typeof (NoTrainingMessage) != "undefined")
            NoTrainingMessage.style.display = "none";

        // update the selection state
        OnSelectionChanged();
    }

    function ShowDialog(strUrl, args, cx, cy, fScroll) {
        // display a dialog box with URL <strUrl>, arguments <args>, width <cx>, height <cy>,
        // scrollbars if <fScroll>; this can be done using either showModalDialog() or
        // window.open(): the former has better modal behavior; the latter allows selection
        // within the window
        var useShowModalDialog = false;
        var strScroll = fScroll ? "yes" : "no";
        if (useShowModalDialog) {
            showModalDialog(strUrl, args,
				    "dialogWidth: " + cx + "px; dialogHeight: " + cy +
					"px; center: yes; resizable: yes; scroll: " + strScroll + ";");
        }
        else {
            dialogArguments = args; // global variable accessed by dialog
            var x = Math.max(0, (screen.width - cx) / 2);
            var y = Math.max(0, (screen.height - cy) / 2);
            window.open(strUrl, "_blank", "left=" + x + ",top=" + y +
					",width=" + cx + ",height=" + cy +
					",location=no,menubar=no,scrollbars=" + strScroll +
					",status=no,toolbar=no,resizable=yes");
        }
    }

</script>
<script type="text/javascript" src="dnscontrols.js"></script>
<h2>
    Administor Training</h2>
<div class="dashedline">
</div>
<!-- action links -->
<asp:Panel ID="Nav" CssClass="Nav" runat="server">
    <span class="Sep">|</span>
    <asp:HyperLink ID="DeletePackagesLink" NavigateUrl="javascript:dsnDeletePackages()"
        runat="server">Delete Selected Packages</asp:HyperLink>
</asp:Panel>
<asp:Panel ID="TrainingPanel" runat="server">
    <!-- table of this user's training -->
    <asp:Table ID="TrainingGrid" CssClass="Grid" runat="server">
        <asp:TableHeaderRow CssClass="Header_">
            <asp:TableCell CssClass="Name_">Name</asp:TableCell>
            <asp:TableCell CssClass="Uploaded_">Uploaded</asp:TableCell>
            <asp:TableCell CssClass="Status_">Status</asp:TableCell>
            <asp:TableCell CssClass="Score_">Score</asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="NoTrainingMessage" runat="server">
    <!-- message that's displayed if the TrainingGrid is empty (and is therefore hidden) -->
    <asp:Table ID="NoTrainingMessageTable" CssClass="MessageTable" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                        <p>Click <span style="font-weight: bold">Upload Training Package </span> above to upload a training package.</p>
                        <p>(Training packages can be SCORM 2004, SCORM 1.2, Class Server LRM,<br/> or Class Server IMS+ format.)</p>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<div class="dashedline">
</div>
