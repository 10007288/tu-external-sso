﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SaveDialog.aspx.cs" Inherits="dialogs_SaveDialog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Enter Course Name</title>
    <link href="../Styles/styles.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <telerik:RadScriptManager ID="ScriptManager1" runat="server" EnableTheming="True">
        <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
        </Scripts>
    </telerik:RadScriptManager>
    <div>
        <table runat="server" id="previewFields" style="padding: 20px 10px 0 10px">
            <tr>
                <td style="width: 82px">
                    Course Name:
                </td>
                <td class="tblfield">
                    <telerik:RadTextBox ID="txtEditorCourseName" runat="Server" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td class="tblfield">
                    <telerik:RadButton ID="btnSave" runat="server" Text="Save" OnClientClicked="SaveCourse"
                        CssClass="linkBtn" />
                    <telerik:RadButton ID="btnCancel" runat="server" Text="Cancel" OnClientClicked="CloseDialog"
                        CssClass="linkBtn" />
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        //<![CDATA[
        //A function that will return a reference to the parent radWindow in case the page is loaded in a RadWindow object
        function getRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
        function SaveCourse(sender, args) {

            var courseName = document.getElementById("<%= txtEditorCourseName.ClientID %>").value
            var wnd = getRadWindow();
            if (courseName !== "") {
                var openerPage = wnd.BrowserWindow;
                document.getElementById("<%= txtEditorCourseName.ClientID %>").value = "";
                openerPage.CreateNewCourseFile(courseName);
                wnd.close();
            }
            else {
                alert("Please Enter Course Name.");
            }
        }
        function CloseDialog(sender, args) {
            document.getElementById("<%= txtEditorCourseName.ClientID %>").value = "";
            var wnd = getRadWindow();
            wnd.close();
        }
        //]]>
    </script>
    </form>
</body>
</html>
